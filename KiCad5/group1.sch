EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 20
Title "linPOL qualification board"
Date "2021-04-04"
Rev "v1"
Comp "LBNL"
Comment1 "Zhicai Zhang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 61A2F57D
P 3100 1900
AR Path="/61A2F57D" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F57D" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F57D" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F57D" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F57D" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F57D" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F57D" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F57D" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F57D" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F57D" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F57D" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F57D" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F57D" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F57D" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F57D" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F57D" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F57D" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F57D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F57D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F57D" Ref="R18"  Part="1" 
F 0 "R18" V 3000 1850 50  0000 L CNN
F 1 "165" V 3100 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 1900 50  0001 C CNN
F 3 "~" H 3100 1900 50  0001 C CNN
	1    3100 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F356
P 1650 2250
AR Path="/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F356" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F356" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1650 2000 50  0001 C CNN
F 1 "GND" H 1655 2077 50  0000 C CNN
F 2 "" H 1650 2250 50  0001 C CNN
F 3 "" H 1650 2250 50  0001 C CNN
	1    1650 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 2050 2850 2150
Wire Wire Line
	3100 2050 3100 2150
Wire Wire Line
	3100 2150 2850 2150
Connection ~ 2850 2150
Wire Wire Line
	1150 1000 850  1000
Wire Wire Line
	600  2150 1150 2150
Wire Wire Line
	2150 850  2700 850 
$Comp
L Device:C C?
U 1 1 61A2F5C5
P 1150 1950
AR Path="/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5C5" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5C5" Ref="C1"  Part="1" 
F 0 "C1" H 1150 2050 50  0000 L CNN
F 1 "2.2uF" H 1150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 1800 50  0001 C CNN
F 3 "~" H 1150 1950 50  0001 C CNN
	1    1150 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F5BE
P 2250 1950
AR Path="/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5BE" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5BE" Ref="C2"  Part="1" 
F 0 "C2" H 2250 2050 50  0000 L CNN
F 1 "10uF" H 2250 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 1800 50  0001 C CNN
F 3 "~" H 2250 1950 50  0001 C CNN
	1    2250 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 2150 2850 2150
Connection ~ 2700 2150
Wire Wire Line
	2150 1150 2250 1150
$Comp
L Device:C C?
U 1 1 61A2F59D
P 2500 1950
AR Path="/61A2F59D" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F59D" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F59D" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F59D" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F59D" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F59D" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F59D" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F59D" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F59D" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F59D" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F59D" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F59D" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F59D" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F59D" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F59D" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F59D" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F59D" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F59D" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F59D" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F59D" Ref="C3"  Part="1" 
F 0 "C3" H 2500 2050 50  0000 L CNN
F 1 "4.7uF" H 2500 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 1800 50  0001 C CNN
F 3 "~" H 2500 1950 50  0001 C CNN
	1    2500 1950
	1    0    0    1   
$EndComp
Connection ~ 2250 1150
Wire Wire Line
	2250 2100 2250 2150
Connection ~ 2250 2150
Wire Wire Line
	2250 2150 2500 2150
Wire Wire Line
	2500 2100 2500 2150
Connection ~ 2500 2150
Wire Wire Line
	2500 2150 2700 2150
Wire Wire Line
	1150 2100 1150 2150
Connection ~ 1150 2150
Wire Wire Line
	1150 2150 1650 2150
$Comp
L Device:R R?
U 1 1 61A2F59F
P 2250 1550
AR Path="/61A2F59F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F59F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F59F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F59F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F59F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F59F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F59F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F59F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F59F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F59F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F59F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F59F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F59F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F59F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F59F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F59F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F59F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F59F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F59F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F59F" Ref="R6"  Part="1" 
F 0 "R6" V 2150 1500 50  0000 L CNN
F 1 "300m" V 2250 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 1550 50  0001 C CNN
F 3 "~" H 2250 1550 50  0001 C CNN
	1    2250 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F5B2
P 2500 1550
AR Path="/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5B2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5B2" Ref="R7"  Part="1" 
F 0 "R7" V 2400 1500 50  0000 L CNN
F 1 "300m" V 2500 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 1550 50  0001 C CNN
F 3 "~" H 2500 1550 50  0001 C CNN
	1    2500 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 1400 2250 1150
Wire Wire Line
	3100 1750 3100 1000
Wire Wire Line
	2700 2150 2700 850 
Wire Wire Line
	2250 1150 2850 1150
Wire Wire Line
	2250 1700 2250 1800
Wire Wire Line
	2500 1700 2500 1800
Wire Wire Line
	1650 1400 1650 2150
Connection ~ 1650 2150
Wire Wire Line
	1650 2150 2250 2150
Wire Wire Line
	2850 1750 2850 1150
$Comp
L Device:R R?
U 1 1 61A2F7AC
P 2850 1900
AR Path="/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7AC" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7AC" Ref="R17"  Part="1" 
F 0 "R17" V 2750 1850 50  0000 L CNN
F 1 "23.2" V 2850 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 1900 50  0001 C CNN
F 3 "~" H 2850 1900 50  0001 C CNN
	1    2850 1900
	1    0    0    1   
$EndComp
Text HLabel 950  1950 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	1150 1800 1150 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F58F
P 1650 950
AR Path="/61A2F58F" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F58F" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F58F" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F58F" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F58F" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F58F" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F58F" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F58F" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F58F" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F58F" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F58F" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F58F" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F58F" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F58F" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F58F" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F58F" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F58F" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F58F" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F58F" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F58F" Ref="U1"  Part="1" 
F 0 "U1" H 1750 1250 60  0000 C CNN
F 1 "LINPOL12V" H 1650 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 1000 260 0001 C CNN
F 3 "" V 1700 1000 260 0001 C CNN
	1    1650 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 2250 1650 2150
Wire Wire Line
	1150 1150 950  1150
Connection ~ 1150 1150
Wire Wire Line
	2500 1400 2500 1000
Wire Wire Line
	2150 1000 2500 1000
Wire Wire Line
	2500 1000 3100 1000
Connection ~ 2500 1000
Text Label 2250 800  1    50   ~ 0
1V4_1
Wire Wire Line
	2250 1150 2250 800 
Text Label 2500 800  1    50   ~ 0
3V3_1
Wire Wire Line
	2500 1000 2500 800 
$Comp
L Device:R R?
U 1 1 61A2F5D4
P 5750 1900
AR Path="/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5D4" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5D4" Ref="R20"  Part="1" 
F 0 "R20" V 5650 1850 50  0000 L CNN
F 1 "165" V 5750 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 1900 50  0001 C CNN
F 3 "~" H 5750 1900 50  0001 C CNN
	1    5750 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F5E6
P 4300 2250
AR Path="/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5E6" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F5E6" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 4300 2000 50  0001 C CNN
F 1 "GND" H 4305 2077 50  0000 C CNN
F 2 "" H 4300 2250 50  0001 C CNN
F 3 "" H 4300 2250 50  0001 C CNN
	1    4300 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 2050 5500 2150
Wire Wire Line
	5750 2050 5750 2150
Wire Wire Line
	5750 2150 5500 2150
Connection ~ 5500 2150
Wire Wire Line
	3250 2150 3800 2150
Wire Wire Line
	4800 850  5350 850 
$Comp
L Device:C C?
U 1 1 61A2F5F2
P 3800 1950
AR Path="/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5F2" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5F2" Ref="C4"  Part="1" 
F 0 "C4" H 3800 2050 50  0000 L CNN
F 1 "2.2uF" H 3800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 1800 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F3EC
P 4900 1950
AR Path="/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3EC" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F3EC" Ref="C5"  Part="1" 
F 0 "C5" H 4900 2050 50  0000 L CNN
F 1 "10uF" H 4900 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 1800 50  0001 C CNN
F 3 "~" H 4900 1950 50  0001 C CNN
	1    4900 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 2150 5500 2150
Connection ~ 5350 2150
Wire Wire Line
	4800 1150 4900 1150
$Comp
L Device:C C?
U 1 1 61A2F5F4
P 5150 1950
AR Path="/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5F4" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5F4" Ref="C6"  Part="1" 
F 0 "C6" H 5150 2050 50  0000 L CNN
F 1 "4.7uF" H 5150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 1800 50  0001 C CNN
F 3 "~" H 5150 1950 50  0001 C CNN
	1    5150 1950
	1    0    0    1   
$EndComp
Connection ~ 4900 1150
Wire Wire Line
	4900 2100 4900 2150
Connection ~ 4900 2150
Wire Wire Line
	4900 2150 5150 2150
Wire Wire Line
	5150 2100 5150 2150
Connection ~ 5150 2150
Wire Wire Line
	5150 2150 5350 2150
Wire Wire Line
	3800 2100 3800 2150
Connection ~ 3800 2150
Wire Wire Line
	3800 2150 4300 2150
$Comp
L Device:R R?
U 1 1 61A2F3FB
P 4900 1550
AR Path="/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3FB" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3FB" Ref="R9"  Part="1" 
F 0 "R9" V 4800 1500 50  0000 L CNN
F 1 "300m" V 4900 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 1550 50  0001 C CNN
F 3 "~" H 4900 1550 50  0001 C CNN
	1    4900 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F40D
P 5150 1550
AR Path="/61A2F40D" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F40D" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F40D" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F40D" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F40D" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F40D" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F40D" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F40D" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F40D" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F40D" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F40D" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F40D" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F40D" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F40D" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F40D" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F40D" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F40D" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F40D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F40D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F40D" Ref="R10"  Part="1" 
F 0 "R10" V 5050 1500 50  0000 L CNN
F 1 "300m" V 5150 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 1550 50  0001 C CNN
F 3 "~" H 5150 1550 50  0001 C CNN
	1    5150 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 1400 4900 1150
Wire Wire Line
	5750 1750 5750 1000
Wire Wire Line
	5350 2150 5350 850 
Wire Wire Line
	4900 1150 5500 1150
Wire Wire Line
	4900 1700 4900 1800
Wire Wire Line
	5150 1700 5150 1800
Wire Wire Line
	4300 1400 4300 2150
Connection ~ 4300 2150
Wire Wire Line
	4300 2150 4900 2150
Wire Wire Line
	5500 1750 5500 1150
$Comp
L Device:R R?
U 1 1 61A2F60A
P 5500 1900
AR Path="/61A2F60A" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F60A" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F60A" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F60A" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F60A" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F60A" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F60A" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F60A" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F60A" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F60A" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F60A" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F60A" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F60A" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F60A" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F60A" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F60A" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F60A" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F60A" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F60A" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F60A" Ref="R19"  Part="1" 
F 0 "R19" V 5400 1850 50  0000 L CNN
F 1 "20" V 5500 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 1900 50  0001 C CNN
F 3 "~" H 5500 1900 50  0001 C CNN
	1    5500 1900
	1    0    0    1   
$EndComp
Text HLabel 3600 1950 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	3800 1800 3800 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F311
P 4300 950
AR Path="/61A2F311" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F311" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F311" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F311" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F311" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F311" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F311" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F311" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F311" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F311" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F311" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F311" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F311" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F311" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F311" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F311" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F311" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F311" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F311" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F311" Ref="U2"  Part="1" 
F 0 "U2" H 4400 1250 60  0000 C CNN
F 1 "LINPOL12V" H 4300 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 1000 260 0001 C CNN
F 3 "" V 4350 1000 260 0001 C CNN
	1    4300 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 2250 4300 2150
Wire Wire Line
	3800 1150 3600 1150
Connection ~ 3800 1150
Wire Wire Line
	5150 1400 5150 1000
Wire Wire Line
	4800 1000 5150 1000
Wire Wire Line
	5150 1000 5750 1000
Connection ~ 5150 1000
Text Label 4900 800  1    50   ~ 0
1V4_2
Wire Wire Line
	4900 1150 4900 800 
Text Label 5150 800  1    50   ~ 0
3V3_2
Wire Wire Line
	5150 1000 5150 800 
$Comp
L Device:R R?
U 1 1 61A2F611
P 8400 1900
AR Path="/61A2F611" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F611" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F611" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F611" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F611" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F611" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F611" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F611" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F611" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F611" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F611" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F611" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F611" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F611" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F611" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F611" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F611" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F611" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F611" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F611" Ref="R22"  Part="1" 
F 0 "R22" V 8300 1850 50  0000 L CNN
F 1 "165" V 8400 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 1900 50  0001 C CNN
F 3 "~" H 8400 1900 50  0001 C CNN
	1    8400 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F622
P 6950 2250
AR Path="/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F622" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F622" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 6950 2000 50  0001 C CNN
F 1 "GND" H 6955 2077 50  0000 C CNN
F 2 "" H 6950 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 2050 8150 2150
Wire Wire Line
	8400 2050 8400 2150
Wire Wire Line
	8400 2150 8150 2150
Connection ~ 8150 2150
Wire Wire Line
	5900 2150 6450 2150
Wire Wire Line
	7450 850  8000 850 
$Comp
L Device:C C?
U 1 1 61A2F31A
P 6450 1950
AR Path="/61A2F31A" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F31A" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F31A" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F31A" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F31A" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F31A" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F31A" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F31A" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F31A" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F31A" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F31A" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F31A" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F31A" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F31A" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F31A" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F31A" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F31A" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F31A" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F31A" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F31A" Ref="C7"  Part="1" 
F 0 "C7" H 6450 2050 50  0000 L CNN
F 1 "2.2uF" H 6450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 1800 50  0001 C CNN
F 3 "~" H 6450 1950 50  0001 C CNN
	1    6450 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F627
P 7550 1950
AR Path="/61A2F627" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F627" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F627" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F627" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F627" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F627" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F627" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F627" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F627" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F627" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F627" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F627" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F627" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F627" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F627" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F627" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F627" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F627" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F627" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F627" Ref="C8"  Part="1" 
F 0 "C8" H 7550 2050 50  0000 L CNN
F 1 "10uF" H 7550 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 1800 50  0001 C CNN
F 3 "~" H 7550 1950 50  0001 C CNN
	1    7550 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 2150 8150 2150
Connection ~ 8000 2150
Wire Wire Line
	7450 1150 7550 1150
$Comp
L Device:C C?
U 1 1 61A2F637
P 7800 1950
AR Path="/61A2F637" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F637" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F637" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F637" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F637" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F637" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F637" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F637" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F637" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F637" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F637" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F637" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F637" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F637" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F637" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F637" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F637" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F637" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F637" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F637" Ref="C9"  Part="1" 
F 0 "C9" H 7800 2050 50  0000 L CNN
F 1 "4.7uF" H 7800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 1800 50  0001 C CNN
F 3 "~" H 7800 1950 50  0001 C CNN
	1    7800 1950
	1    0    0    1   
$EndComp
Connection ~ 7550 1150
Wire Wire Line
	7550 2100 7550 2150
Connection ~ 7550 2150
Wire Wire Line
	7550 2150 7800 2150
Wire Wire Line
	7800 2100 7800 2150
Connection ~ 7800 2150
Wire Wire Line
	7800 2150 8000 2150
Wire Wire Line
	6450 2100 6450 2150
Connection ~ 6450 2150
Wire Wire Line
	6450 2150 6950 2150
$Comp
L Device:R R?
U 1 1 6195FD0F
P 7550 1550
AR Path="/6195FD0F" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FD0F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FD0F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FD0F" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FD0F" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FD0F" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FD0F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FD0F" Ref="R?"  Part="1" 
AR Path="/61849340/6195FD0F" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FD0F" Ref="R?"  Part="1" 
AR Path="/61867626/6195FD0F" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FD0F" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FD0F" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FD0F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FD0F" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FD0F" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FD0F" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FD0F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FD0F" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FD0F" Ref="R12"  Part="1" 
F 0 "R12" V 7450 1500 50  0000 L CNN
F 1 "300m" V 7550 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 1550 50  0001 C CNN
F 3 "~" H 7550 1550 50  0001 C CNN
	1    7550 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F64D
P 7800 1550
AR Path="/61A2F64D" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F64D" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F64D" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F64D" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F64D" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F64D" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F64D" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F64D" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F64D" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F64D" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F64D" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F64D" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F64D" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F64D" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F64D" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F64D" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F64D" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F64D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F64D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F64D" Ref="R13"  Part="1" 
F 0 "R13" V 7700 1500 50  0000 L CNN
F 1 "300m" V 7800 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 1550 50  0001 C CNN
F 3 "~" H 7800 1550 50  0001 C CNN
	1    7800 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 1400 7550 1150
Wire Wire Line
	8400 1750 8400 1000
Wire Wire Line
	8000 2150 8000 850 
Wire Wire Line
	7550 1150 8150 1150
Wire Wire Line
	7550 1700 7550 1800
Wire Wire Line
	7800 1700 7800 1800
Wire Wire Line
	6950 1400 6950 2150
Connection ~ 6950 2150
Wire Wire Line
	6950 2150 7550 2150
Wire Wire Line
	8150 1750 8150 1150
$Comp
L Device:R R?
U 1 1 61A2F413
P 8150 1900
AR Path="/61A2F413" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F413" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F413" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F413" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F413" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F413" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F413" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F413" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F413" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F413" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F413" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F413" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F413" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F413" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F413" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F413" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F413" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F413" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F413" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F413" Ref="R21"  Part="1" 
F 0 "R21" V 8050 1850 50  0000 L CNN
F 1 "17.4" V 8150 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 1900 50  0001 C CNN
F 3 "~" H 8150 1900 50  0001 C CNN
	1    8150 1900
	1    0    0    1   
$EndComp
Text HLabel 6250 1950 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	6450 1800 6450 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F422
P 6950 950
AR Path="/61A2F422" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F422" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F422" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F422" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F422" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F422" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F422" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F422" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F422" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F422" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F422" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F422" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F422" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F422" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F422" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F422" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F422" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F422" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F422" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F422" Ref="U3"  Part="1" 
F 0 "U3" H 7050 1250 60  0000 C CNN
F 1 "LINPOL12V" H 6950 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 1000 260 0001 C CNN
F 3 "" V 7000 1000 260 0001 C CNN
	1    6950 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 2250 6950 2150
Wire Wire Line
	6450 1150 6250 1150
Connection ~ 6450 1150
Wire Wire Line
	7800 1400 7800 1000
Wire Wire Line
	7450 1000 7800 1000
Wire Wire Line
	7800 1000 8400 1000
Connection ~ 7800 1000
Text Label 7550 800  1    50   ~ 0
1V4_3
Wire Wire Line
	7550 1150 7550 800 
Text Label 7800 800  1    50   ~ 0
3V3_3
Wire Wire Line
	7800 1000 7800 800 
$Comp
L Device:R R?
U 1 1 61A2F656
P 11050 1900
AR Path="/61A2F656" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F656" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F656" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F656" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F656" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F656" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F656" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F656" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F656" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F656" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F656" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F656" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F656" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F656" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F656" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F656" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F656" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F656" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F656" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F656" Ref="R24"  Part="1" 
F 0 "R24" V 10950 1850 50  0000 L CNN
F 1 "165" V 11050 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 1900 50  0001 C CNN
F 3 "~" H 11050 1900 50  0001 C CNN
	1    11050 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F42B
P 9600 2250
AR Path="/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F42B" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F42B" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 9600 2000 50  0001 C CNN
F 1 "GND" H 9605 2077 50  0000 C CNN
F 2 "" H 9600 2250 50  0001 C CNN
F 3 "" H 9600 2250 50  0001 C CNN
	1    9600 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 2050 10800 2150
Wire Wire Line
	11050 2050 11050 2150
Wire Wire Line
	11050 2150 10800 2150
Connection ~ 10800 2150
Wire Wire Line
	8550 2150 9100 2150
Wire Wire Line
	10100 850  10650 850 
$Comp
L Device:C C?
U 1 1 61A2F437
P 9100 1950
AR Path="/61A2F437" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F437" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F437" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F437" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F437" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F437" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F437" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F437" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F437" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F437" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F437" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F437" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F437" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F437" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F437" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F437" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F437" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F437" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F437" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F437" Ref="C10"  Part="1" 
F 0 "C10" H 9100 2050 50  0000 L CNN
F 1 "2.2uF" H 9100 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 1800 50  0001 C CNN
F 3 "~" H 9100 1950 50  0001 C CNN
	1    9100 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F44B
P 10200 1950
AR Path="/61A2F44B" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F44B" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F44B" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F44B" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F44B" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F44B" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F44B" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F44B" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F44B" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F44B" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F44B" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F44B" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F44B" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F44B" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F44B" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F44B" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F44B" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F44B" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F44B" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F44B" Ref="C11"  Part="1" 
F 0 "C11" H 10200 2050 50  0000 L CNN
F 1 "10uF" H 10200 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 1800 50  0001 C CNN
F 3 "~" H 10200 1950 50  0001 C CNN
	1    10200 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 2150 10800 2150
Connection ~ 10650 2150
Wire Wire Line
	10100 1150 10200 1150
$Comp
L Device:C C?
U 1 1 61A2F668
P 10450 1950
AR Path="/61A2F668" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F668" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F668" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F668" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F668" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F668" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F668" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F668" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F668" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F668" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F668" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F668" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F668" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F668" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F668" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F668" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F668" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F668" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F668" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F668" Ref="C12"  Part="1" 
F 0 "C12" H 10450 2050 50  0000 L CNN
F 1 "4.7uF" H 10450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1800 50  0001 C CNN
F 3 "~" H 10450 1950 50  0001 C CNN
	1    10450 1950
	1    0    0    1   
$EndComp
Connection ~ 10200 1150
Wire Wire Line
	10200 2100 10200 2150
Connection ~ 10200 2150
Wire Wire Line
	10200 2150 10450 2150
Wire Wire Line
	10450 2100 10450 2150
Connection ~ 10450 2150
Wire Wire Line
	10450 2150 10650 2150
Wire Wire Line
	9100 2100 9100 2150
Connection ~ 9100 2150
Wire Wire Line
	9100 2150 9600 2150
$Comp
L Device:R R?
U 1 1 61A2F456
P 10200 1550
AR Path="/61A2F456" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F456" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F456" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F456" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F456" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F456" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F456" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F456" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F456" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F456" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F456" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F456" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F456" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F456" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F456" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F456" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F456" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F456" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F456" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F456" Ref="R15"  Part="1" 
F 0 "R15" V 10100 1500 50  0000 L CNN
F 1 "300m" V 10200 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 1550 50  0001 C CNN
F 3 "~" H 10200 1550 50  0001 C CNN
	1    10200 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F461
P 10450 1550
AR Path="/61A2F461" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F461" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F461" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F461" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F461" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F461" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F461" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F461" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F461" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F461" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F461" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F461" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F461" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F461" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F461" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F461" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F461" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F461" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F461" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F461" Ref="R16"  Part="1" 
F 0 "R16" V 10350 1500 50  0000 L CNN
F 1 "300m" V 10450 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 1550 50  0001 C CNN
F 3 "~" H 10450 1550 50  0001 C CNN
	1    10450 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 1400 10200 1150
Wire Wire Line
	11050 1750 11050 1000
Wire Wire Line
	10650 2150 10650 850 
Wire Wire Line
	10200 1150 10800 1150
Wire Wire Line
	10200 1700 10200 1800
Wire Wire Line
	10450 1700 10450 1800
Wire Wire Line
	9600 1400 9600 2150
Connection ~ 9600 2150
Wire Wire Line
	9600 2150 10200 2150
Wire Wire Line
	10800 1750 10800 1150
$Comp
L Device:R R?
U 1 1 61A2F675
P 10800 1900
AR Path="/61A2F675" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F675" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F675" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F675" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F675" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F675" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F675" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F675" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F675" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F675" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F675" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F675" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F675" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F675" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F675" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F675" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F675" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F675" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F675" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F675" Ref="R23"  Part="1" 
F 0 "R23" V 10700 1850 50  0000 L CNN
F 1 "17.4" V 10800 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 1900 50  0001 C CNN
F 3 "~" H 10800 1900 50  0001 C CNN
	1    10800 1900
	1    0    0    1   
$EndComp
Text HLabel 8900 1950 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	9100 1800 9100 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F682
P 9600 950
AR Path="/61A2F682" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F682" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F682" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F682" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F682" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F682" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F682" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F682" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F682" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F682" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F682" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F682" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F682" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F682" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F682" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F682" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F682" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F682" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F682" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F682" Ref="U4"  Part="1" 
F 0 "U4" H 9700 1250 60  0000 C CNN
F 1 "LINPOL12V" H 9600 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 1000 260 0001 C CNN
F 3 "" V 9650 1000 260 0001 C CNN
	1    9600 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 2250 9600 2150
Wire Wire Line
	9100 1150 8900 1150
Connection ~ 9100 1150
Wire Wire Line
	10450 1400 10450 1000
Wire Wire Line
	10100 1000 10450 1000
Wire Wire Line
	10450 1000 11050 1000
Connection ~ 10450 1000
Text Label 10200 800  1    50   ~ 0
1V4_4
Wire Wire Line
	10200 1150 10200 800 
Text Label 10450 800  1    50   ~ 0
3V3_4
Wire Wire Line
	10450 1000 10450 800 
$Comp
L Device:R R?
U 1 1 61A2F46C
P 3100 3800
AR Path="/61A2F46C" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F46C" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F46C" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F46C" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F46C" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F46C" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F46C" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F46C" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F46C" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F46C" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F46C" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F46C" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F46C" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F46C" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F46C" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F46C" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F46C" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F46C" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F46C" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F46C" Ref="R42"  Part="1" 
F 0 "R42" V 3000 3750 50  0000 L CNN
F 1 "165" V 3100 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 3800 50  0001 C CNN
F 3 "~" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F345
P 1650 4150
AR Path="/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F345" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F345" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 1650 3900 50  0001 C CNN
F 1 "GND" H 1655 3977 50  0000 C CNN
F 2 "" H 1650 4150 50  0001 C CNN
F 3 "" H 1650 4150 50  0001 C CNN
	1    1650 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 3950 2850 4050
Wire Wire Line
	3100 3950 3100 4050
Wire Wire Line
	3100 4050 2850 4050
Connection ~ 2850 4050
Wire Wire Line
	600  4050 1150 4050
Wire Wire Line
	2150 2750 2700 2750
$Comp
L Device:C C?
U 1 1 61A2F374
P 1150 3850
AR Path="/61A2F374" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F374" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F374" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F374" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F374" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F374" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F374" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F374" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F374" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F374" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F374" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F374" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F374" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F374" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F374" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F374" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F374" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F374" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F374" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F374" Ref="C13"  Part="1" 
F 0 "C13" H 1150 3950 50  0000 L CNN
F 1 "2.2uF" H 1150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 3700 50  0001 C CNN
F 3 "~" H 1150 3850 50  0001 C CNN
	1    1150 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F380
P 2250 3850
AR Path="/61A2F380" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F380" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F380" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F380" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F380" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F380" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F380" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F380" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F380" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F380" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F380" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F380" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F380" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F380" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F380" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F380" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F380" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F380" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F380" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F380" Ref="C14"  Part="1" 
F 0 "C14" H 2250 3950 50  0000 L CNN
F 1 "10uF" H 2250 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 3700 50  0001 C CNN
F 3 "~" H 2250 3850 50  0001 C CNN
	1    2250 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 4050 2850 4050
Connection ~ 2700 4050
Wire Wire Line
	2150 3050 2250 3050
$Comp
L Device:C C?
U 1 1 61A2F689
P 2500 3850
AR Path="/61A2F689" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F689" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F689" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F689" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F689" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F689" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F689" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F689" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F689" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F689" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F689" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F689" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F689" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F689" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F689" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F689" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F689" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F689" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F689" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F689" Ref="C15"  Part="1" 
F 0 "C15" H 2500 3950 50  0000 L CNN
F 1 "4.7uF" H 2500 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 3700 50  0001 C CNN
F 3 "~" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    1   
$EndComp
Connection ~ 2250 3050
Wire Wire Line
	2250 4000 2250 4050
Connection ~ 2250 4050
Wire Wire Line
	2250 4050 2500 4050
Wire Wire Line
	2500 4000 2500 4050
Connection ~ 2500 4050
Wire Wire Line
	2500 4050 2700 4050
Wire Wire Line
	1150 4000 1150 4050
Connection ~ 1150 4050
Wire Wire Line
	1150 4050 1650 4050
$Comp
L Device:R R?
U 1 1 61A2F68F
P 2250 3450
AR Path="/61A2F68F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F68F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F68F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F68F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F68F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F68F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F68F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F68F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F68F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F68F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F68F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F68F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F68F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F68F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F68F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F68F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F68F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F68F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F68F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F68F" Ref="R30"  Part="1" 
F 0 "R30" V 2150 3400 50  0000 L CNN
F 1 "300m" V 2250 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F6A4
P 2500 3450
AR Path="/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6A4" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6A4" Ref="R31"  Part="1" 
F 0 "R31" V 2400 3400 50  0000 L CNN
F 1 "300m" V 2500 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 3300 2250 3050
Wire Wire Line
	3100 3650 3100 2900
Wire Wire Line
	2700 4050 2700 2750
Wire Wire Line
	2250 3050 2850 3050
Wire Wire Line
	2250 3600 2250 3700
Wire Wire Line
	2500 3600 2500 3700
Wire Wire Line
	1650 3300 1650 4050
Connection ~ 1650 4050
Wire Wire Line
	1650 4050 2250 4050
Wire Wire Line
	2850 3650 2850 3050
$Comp
L Device:R R?
U 1 1 6195FD84
P 2850 3800
AR Path="/6195FD84" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FD84" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FD84" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FD84" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FD84" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FD84" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FD84" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FD84" Ref="R?"  Part="1" 
AR Path="/61849340/6195FD84" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FD84" Ref="R?"  Part="1" 
AR Path="/61867626/6195FD84" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FD84" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FD84" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FD84" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FD84" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FD84" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FD84" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FD84" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FD84" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FD84" Ref="R41"  Part="1" 
F 0 "R41" V 2750 3750 50  0000 L CNN
F 1 "17.4" V 2850 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 3800 50  0001 C CNN
F 3 "~" H 2850 3800 50  0001 C CNN
	1    2850 3800
	1    0    0    1   
$EndComp
Text HLabel 950  3850 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	1150 3700 1150 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F470
P 1650 2850
AR Path="/61A2F470" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F470" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F470" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F470" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F470" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F470" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F470" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F470" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F470" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F470" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F470" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F470" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F470" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F470" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F470" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F470" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F470" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F470" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F470" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F470" Ref="U5"  Part="1" 
F 0 "U5" H 1750 3150 60  0000 C CNN
F 1 "LINPOL12V" H 1650 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 2900 260 0001 C CNN
F 3 "" V 1700 2900 260 0001 C CNN
	1    1650 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 4150 1650 4050
Wire Wire Line
	1150 3050 950  3050
Connection ~ 1150 3050
Wire Wire Line
	2500 3300 2500 2900
Wire Wire Line
	2150 2900 2500 2900
Wire Wire Line
	2500 2900 3100 2900
Connection ~ 2500 2900
Text Label 2250 2700 1    50   ~ 0
1V4_5
Wire Wire Line
	2250 3050 2250 2700
Text Label 2500 2700 1    50   ~ 0
3V3_5
Wire Wire Line
	2500 2900 2500 2700
$Comp
L Device:R R?
U 1 1 61A2F6B9
P 5750 3800
AR Path="/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6B9" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6B9" Ref="R44"  Part="1" 
F 0 "R44" V 5650 3750 50  0000 L CNN
F 1 "165" V 5750 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3800 50  0001 C CNN
F 3 "~" H 5750 3800 50  0001 C CNN
	1    5750 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F6C4
P 4300 4150
AR Path="/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6C4" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F6C4" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 4300 3900 50  0001 C CNN
F 1 "GND" H 4305 3977 50  0000 C CNN
F 2 "" H 4300 4150 50  0001 C CNN
F 3 "" H 4300 4150 50  0001 C CNN
	1    4300 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 3950 5500 4050
Wire Wire Line
	5750 3950 5750 4050
Wire Wire Line
	5750 4050 5500 4050
Connection ~ 5500 4050
Wire Wire Line
	3250 4050 3800 4050
Wire Wire Line
	4800 2750 5350 2750
$Comp
L Device:C C?
U 1 1 6195FB41
P 3800 3850
AR Path="/6195FB41" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FB41" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FB41" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FB41" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FB41" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FB41" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FB41" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FB41" Ref="C?"  Part="1" 
AR Path="/61849340/6195FB41" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FB41" Ref="C?"  Part="1" 
AR Path="/61867626/6195FB41" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FB41" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FB41" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FB41" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FB41" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FB41" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FB41" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FB41" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB41" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FB41" Ref="C16"  Part="1" 
F 0 "C16" H 3800 3950 50  0000 L CNN
F 1 "2.2uF" H 3800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 3700 50  0001 C CNN
F 3 "~" H 3800 3850 50  0001 C CNN
	1    3800 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F785
P 4900 3850
AR Path="/61A2F785" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F785" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F785" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F785" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F785" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F785" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F785" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F785" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F785" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F785" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F785" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F785" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F785" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F785" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F785" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F785" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F785" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F785" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F785" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F785" Ref="C17"  Part="1" 
F 0 "C17" H 4900 3950 50  0000 L CNN
F 1 "10uF" H 4900 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 3700 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 4050 5500 4050
Connection ~ 5350 4050
Wire Wire Line
	4800 3050 4900 3050
$Comp
L Device:C C?
U 1 1 61A2F792
P 5150 3850
AR Path="/61A2F792" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F792" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F792" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F792" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F792" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F792" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F792" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F792" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F792" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F792" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F792" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F792" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F792" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F792" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F792" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F792" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F792" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F792" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F792" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F792" Ref="C18"  Part="1" 
F 0 "C18" H 5150 3950 50  0000 L CNN
F 1 "4.7uF" H 5150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 3700 50  0001 C CNN
F 3 "~" H 5150 3850 50  0001 C CNN
	1    5150 3850
	1    0    0    1   
$EndComp
Connection ~ 4900 3050
Wire Wire Line
	4900 4000 4900 4050
Connection ~ 4900 4050
Wire Wire Line
	4900 4050 5150 4050
Wire Wire Line
	5150 4000 5150 4050
Connection ~ 5150 4050
Wire Wire Line
	5150 4050 5350 4050
Wire Wire Line
	3800 4000 3800 4050
Connection ~ 3800 4050
Wire Wire Line
	3800 4050 4300 4050
$Comp
L Device:R R?
U 1 1 61A2F6D3
P 4900 3450
AR Path="/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6D3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6D3" Ref="R33"  Part="1" 
F 0 "R33" V 4800 3400 50  0000 L CNN
F 1 "300m" V 4900 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 3450 50  0001 C CNN
F 3 "~" H 4900 3450 50  0001 C CNN
	1    4900 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F6DA
P 5150 3450
AR Path="/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6DA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6DA" Ref="R34"  Part="1" 
F 0 "R34" V 5050 3400 50  0000 L CNN
F 1 "300m" V 5150 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 3450 50  0001 C CNN
F 3 "~" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 3300 4900 3050
Wire Wire Line
	5750 3650 5750 2900
Wire Wire Line
	5350 4050 5350 2750
Wire Wire Line
	4900 3050 5500 3050
Wire Wire Line
	4900 3600 4900 3700
Wire Wire Line
	5150 3600 5150 3700
Wire Wire Line
	4300 3300 4300 4050
Connection ~ 4300 4050
Wire Wire Line
	4300 4050 4900 4050
Wire Wire Line
	5500 3650 5500 3050
$Comp
L Device:R R?
U 1 1 61A2F6EA
P 5500 3800
AR Path="/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6EA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6EA" Ref="R43"  Part="1" 
F 0 "R43" V 5400 3750 50  0000 L CNN
F 1 "17.4" V 5500 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 3800 50  0001 C CNN
F 3 "~" H 5500 3800 50  0001 C CNN
	1    5500 3800
	1    0    0    1   
$EndComp
Text HLabel 3600 3850 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	3800 3700 3800 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F48B
P 4300 2850
AR Path="/61A2F48B" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F48B" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F48B" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F48B" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F48B" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F48B" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F48B" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F48B" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F48B" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F48B" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F48B" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F48B" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F48B" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F48B" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F48B" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F48B" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F48B" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F48B" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F48B" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F48B" Ref="U6"  Part="1" 
F 0 "U6" H 4400 3150 60  0000 C CNN
F 1 "LINPOL12V" H 4300 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 2900 260 0001 C CNN
F 3 "" V 4350 2900 260 0001 C CNN
	1    4300 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 4150 4300 4050
Wire Wire Line
	3800 3050 3600 3050
Connection ~ 3800 3050
Wire Wire Line
	5150 3300 5150 2900
Wire Wire Line
	4800 2900 5150 2900
Wire Wire Line
	5150 2900 5750 2900
Connection ~ 5150 2900
Text Label 4900 2700 1    50   ~ 0
1V4_6
Wire Wire Line
	4900 3050 4900 2700
Text Label 5150 2700 1    50   ~ 0
3V3_6
Wire Wire Line
	5150 2900 5150 2700
$Comp
L Device:R R?
U 1 1 61A2F6F6
P 8400 3800
AR Path="/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6F6" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6F6" Ref="R46"  Part="1" 
F 0 "R46" V 8300 3750 50  0000 L CNN
F 1 "165" V 8400 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 3800 50  0001 C CNN
F 3 "~" H 8400 3800 50  0001 C CNN
	1    8400 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F49A
P 6950 4150
AR Path="/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F49A" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F49A" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 6950 3900 50  0001 C CNN
F 1 "GND" H 6955 3977 50  0000 C CNN
F 2 "" H 6950 4150 50  0001 C CNN
F 3 "" H 6950 4150 50  0001 C CNN
	1    6950 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 3950 8150 4050
Wire Wire Line
	8400 3950 8400 4050
Wire Wire Line
	8400 4050 8150 4050
Connection ~ 8150 4050
Wire Wire Line
	5900 4050 6450 4050
Wire Wire Line
	7450 2750 8000 2750
$Comp
L Device:C C?
U 1 1 61A2F4A7
P 6450 3850
AR Path="/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4A7" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4A7" Ref="C19"  Part="1" 
F 0 "C19" H 6450 3950 50  0000 L CNN
F 1 "2.2uF" H 6450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 3700 50  0001 C CNN
F 3 "~" H 6450 3850 50  0001 C CNN
	1    6450 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F4AE
P 7550 3850
AR Path="/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4AE" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4AE" Ref="C20"  Part="1" 
F 0 "C20" H 7550 3950 50  0000 L CNN
F 1 "10uF" H 7550 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 3700 50  0001 C CNN
F 3 "~" H 7550 3850 50  0001 C CNN
	1    7550 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 4050 8150 4050
Connection ~ 8000 4050
Wire Wire Line
	7450 3050 7550 3050
$Comp
L Device:C C?
U 1 1 61A2F700
P 7800 3850
AR Path="/61A2F700" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F700" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F700" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F700" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F700" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F700" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F700" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F700" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F700" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F700" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F700" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F700" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F700" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F700" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F700" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F700" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F700" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F700" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F700" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F700" Ref="C21"  Part="1" 
F 0 "C21" H 7800 3950 50  0000 L CNN
F 1 "4.7uF" H 7800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 3700 50  0001 C CNN
F 3 "~" H 7800 3850 50  0001 C CNN
	1    7800 3850
	1    0    0    1   
$EndComp
Connection ~ 7550 3050
Wire Wire Line
	7550 4000 7550 4050
Connection ~ 7550 4050
Wire Wire Line
	7550 4050 7800 4050
Wire Wire Line
	7800 4000 7800 4050
Connection ~ 7800 4050
Wire Wire Line
	7800 4050 8000 4050
Wire Wire Line
	6450 4000 6450 4050
Connection ~ 6450 4050
Wire Wire Line
	6450 4050 6950 4050
$Comp
L Device:R R?
U 1 1 61A2F709
P 7550 3450
AR Path="/61A2F709" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F709" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F709" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F709" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F709" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F709" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F709" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F709" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F709" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F709" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F709" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F709" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F709" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F709" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F709" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F709" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F709" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F709" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F709" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F709" Ref="R36"  Part="1" 
F 0 "R36" V 7450 3400 50  0000 L CNN
F 1 "300m" V 7550 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 3450 50  0001 C CNN
F 3 "~" H 7550 3450 50  0001 C CNN
	1    7550 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F71B
P 7800 3450
AR Path="/61A2F71B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F71B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F71B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F71B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F71B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F71B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F71B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F71B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F71B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F71B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F71B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F71B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F71B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F71B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F71B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F71B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F71B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F71B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F71B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F71B" Ref="R37"  Part="1" 
F 0 "R37" V 7700 3400 50  0000 L CNN
F 1 "300m" V 7800 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 3450 50  0001 C CNN
F 3 "~" H 7800 3450 50  0001 C CNN
	1    7800 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 3300 7550 3050
Wire Wire Line
	8400 3650 8400 2900
Wire Wire Line
	8000 4050 8000 2750
Wire Wire Line
	7550 3050 8150 3050
Wire Wire Line
	7550 3600 7550 3700
Wire Wire Line
	7800 3600 7800 3700
Wire Wire Line
	6950 3300 6950 4050
Connection ~ 6950 4050
Wire Wire Line
	6950 4050 7550 4050
Wire Wire Line
	8150 3650 8150 3050
$Comp
L Device:R R?
U 1 1 61A2F4C3
P 8150 3800
AR Path="/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4C3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F4C3" Ref="R45"  Part="1" 
F 0 "R45" V 8050 3750 50  0000 L CNN
F 1 "17.4" V 8150 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 3800 50  0001 C CNN
F 3 "~" H 8150 3800 50  0001 C CNN
	1    8150 3800
	1    0    0    1   
$EndComp
Text HLabel 6250 3850 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	6450 3700 6450 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F4CA
P 6950 2850
AR Path="/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4CA" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F4CA" Ref="U7"  Part="1" 
F 0 "U7" H 7050 3150 60  0000 C CNN
F 1 "LINPOL12V" H 6950 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 2900 260 0001 C CNN
F 3 "" V 7000 2900 260 0001 C CNN
	1    6950 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 4150 6950 4050
Wire Wire Line
	6450 3050 6250 3050
Connection ~ 6450 3050
Wire Wire Line
	7800 3300 7800 2900
Wire Wire Line
	7450 2900 7800 2900
Wire Wire Line
	7800 2900 8400 2900
Connection ~ 7800 2900
Text Label 7550 2700 1    50   ~ 0
1V4_7
Wire Wire Line
	7550 3050 7550 2700
Text Label 7800 2700 1    50   ~ 0
3V3_7
Wire Wire Line
	7800 2900 7800 2700
$Comp
L Device:R R?
U 1 1 61A2F720
P 11050 3800
AR Path="/61A2F720" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F720" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F720" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F720" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F720" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F720" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F720" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F720" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F720" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F720" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F720" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F720" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F720" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F720" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F720" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F720" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F720" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F720" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F720" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F720" Ref="R48"  Part="1" 
F 0 "R48" V 10950 3750 50  0000 L CNN
F 1 "165" V 11050 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 3800 50  0001 C CNN
F 3 "~" H 11050 3800 50  0001 C CNN
	1    11050 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F4D1
P 9600 4150
AR Path="/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4D1" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F4D1" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 9600 3900 50  0001 C CNN
F 1 "GND" H 9605 3977 50  0000 C CNN
F 2 "" H 9600 4150 50  0001 C CNN
F 3 "" H 9600 4150 50  0001 C CNN
	1    9600 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 3950 10800 4050
Wire Wire Line
	11050 3950 11050 4050
Wire Wire Line
	11050 4050 10800 4050
Connection ~ 10800 4050
Wire Wire Line
	8550 4050 9100 4050
Wire Wire Line
	10100 2750 10650 2750
$Comp
L Device:C C?
U 1 1 6195FB9C
P 9100 3850
AR Path="/6195FB9C" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FB9C" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FB9C" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FB9C" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FB9C" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FB9C" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FB9C" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FB9C" Ref="C?"  Part="1" 
AR Path="/61849340/6195FB9C" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FB9C" Ref="C?"  Part="1" 
AR Path="/61867626/6195FB9C" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FB9C" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FB9C" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FB9C" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FB9C" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FB9C" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FB9C" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FB9C" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB9C" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FB9C" Ref="C22"  Part="1" 
F 0 "C22" H 9100 3950 50  0000 L CNN
F 1 "2.2uF" H 9100 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 3700 50  0001 C CNN
F 3 "~" H 9100 3850 50  0001 C CNN
	1    9100 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F4F3
P 10200 3850
AR Path="/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4F3" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4F3" Ref="C23"  Part="1" 
F 0 "C23" H 10200 3950 50  0000 L CNN
F 1 "10uF" H 10200 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 3700 50  0001 C CNN
F 3 "~" H 10200 3850 50  0001 C CNN
	1    10200 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 4050 10800 4050
Connection ~ 10650 4050
Wire Wire Line
	10100 3050 10200 3050
$Comp
L Device:C C?
U 1 1 61A2F4FE
P 10450 3850
AR Path="/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4FE" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4FE" Ref="C24"  Part="1" 
F 0 "C24" H 10450 3950 50  0000 L CNN
F 1 "4.7uF" H 10450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 3700 50  0001 C CNN
F 3 "~" H 10450 3850 50  0001 C CNN
	1    10450 3850
	1    0    0    1   
$EndComp
Connection ~ 10200 3050
Wire Wire Line
	10200 4000 10200 4050
Connection ~ 10200 4050
Wire Wire Line
	10200 4050 10450 4050
Wire Wire Line
	10450 4000 10450 4050
Connection ~ 10450 4050
Wire Wire Line
	10450 4050 10650 4050
Wire Wire Line
	9100 4000 9100 4050
Connection ~ 9100 4050
Wire Wire Line
	9100 4050 9600 4050
$Comp
L Device:R R?
U 1 1 61A2F736
P 10200 3450
AR Path="/61A2F736" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F736" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F736" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F736" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F736" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F736" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F736" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F736" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F736" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F736" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F736" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F736" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F736" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F736" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F736" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F736" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F736" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F736" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F736" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F736" Ref="R39"  Part="1" 
F 0 "R39" V 10100 3400 50  0000 L CNN
F 1 "300m" V 10200 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 3450 50  0001 C CNN
F 3 "~" H 10200 3450 50  0001 C CNN
	1    10200 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F368
P 10450 3450
AR Path="/61A2F368" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F368" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F368" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F368" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F368" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F368" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F368" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F368" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F368" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F368" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F368" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F368" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F368" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F368" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F368" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F368" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F368" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F368" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F368" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F368" Ref="R40"  Part="1" 
F 0 "R40" V 10350 3400 50  0000 L CNN
F 1 "300m" V 10450 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 3450 50  0001 C CNN
F 3 "~" H 10450 3450 50  0001 C CNN
	1    10450 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 3300 10200 3050
Wire Wire Line
	11050 3650 11050 2900
Wire Wire Line
	10650 4050 10650 2750
Wire Wire Line
	10200 3050 10800 3050
Wire Wire Line
	10200 3600 10200 3700
Wire Wire Line
	10450 3600 10450 3700
Wire Wire Line
	9600 3300 9600 4050
Connection ~ 9600 4050
Wire Wire Line
	9600 4050 10200 4050
Wire Wire Line
	10800 3650 10800 3050
$Comp
L Device:R R?
U 1 1 61A2F502
P 10800 3800
AR Path="/61A2F502" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F502" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F502" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F502" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F502" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F502" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F502" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F502" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F502" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F502" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F502" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F502" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F502" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F502" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F502" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F502" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F502" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F502" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F502" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F502" Ref="R47"  Part="1" 
F 0 "R47" V 10700 3750 50  0000 L CNN
F 1 "17.4" V 10800 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 3800 50  0001 C CNN
F 3 "~" H 10800 3800 50  0001 C CNN
	1    10800 3800
	1    0    0    1   
$EndComp
Text HLabel 8900 3850 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	9100 3700 9100 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F516
P 9600 2850
AR Path="/61A2F516" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F516" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F516" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F516" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F516" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F516" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F516" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F516" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F516" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F516" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F516" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F516" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F516" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F516" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F516" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F516" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F516" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F516" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F516" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F516" Ref="U8"  Part="1" 
F 0 "U8" H 9700 3150 60  0000 C CNN
F 1 "LINPOL12V" H 9600 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 2900 260 0001 C CNN
F 3 "" V 9650 2900 260 0001 C CNN
	1    9600 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 4150 9600 4050
Wire Wire Line
	9100 3050 8900 3050
Connection ~ 9100 3050
Wire Wire Line
	10450 3300 10450 2900
Wire Wire Line
	10100 2900 10450 2900
Wire Wire Line
	10450 2900 11050 2900
Connection ~ 10450 2900
Text Label 10200 2700 1    50   ~ 0
1V4_8
Wire Wire Line
	10200 3050 10200 2700
Text Label 10450 2700 1    50   ~ 0
3V3_8
Wire Wire Line
	10450 2900 10450 2700
$Comp
L Device:R R?
U 1 1 61A2F739
P 3100 5750
AR Path="/61A2F739" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F739" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F739" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F739" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F739" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F739" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F739" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F739" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F739" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F739" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F739" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F739" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F739" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F739" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F739" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F739" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F739" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F739" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F739" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F739" Ref="R58"  Part="1" 
F 0 "R58" V 3000 5700 50  0000 L CNN
F 1 "165" V 3100 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 5750 50  0001 C CNN
F 3 "~" H 3100 5750 50  0001 C CNN
	1    3100 5750
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F519
P 1650 6100
AR Path="/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F519" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F519" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 1650 5850 50  0001 C CNN
F 1 "GND" H 1655 5927 50  0000 C CNN
F 2 "" H 1650 6100 50  0001 C CNN
F 3 "" H 1650 6100 50  0001 C CNN
	1    1650 6100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 5900 2850 6000
Wire Wire Line
	3100 5900 3100 6000
Wire Wire Line
	3100 6000 2850 6000
Connection ~ 2850 6000
Wire Wire Line
	600  6000 1150 6000
Wire Wire Line
	2150 4700 2700 4700
$Comp
L Device:C C?
U 1 1 61A2F748
P 1150 5800
AR Path="/61A2F748" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F748" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F748" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F748" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F748" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F748" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F748" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F748" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F748" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F748" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F748" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F748" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F748" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F748" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F748" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F748" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F748" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F748" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F748" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F748" Ref="C25"  Part="1" 
F 0 "C25" H 1150 5900 50  0000 L CNN
F 1 "2.2uF" H 1150 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 5650 50  0001 C CNN
F 3 "~" H 1150 5800 50  0001 C CNN
	1    1150 5800
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F529
P 2250 5800
AR Path="/61A2F529" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F529" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F529" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F529" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F529" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F529" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F529" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F529" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F529" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F529" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F529" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F529" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F529" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F529" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F529" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F529" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F529" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F529" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F529" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F529" Ref="C26"  Part="1" 
F 0 "C26" H 2250 5900 50  0000 L CNN
F 1 "10uF" H 2250 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 5650 50  0001 C CNN
F 3 "~" H 2250 5800 50  0001 C CNN
	1    2250 5800
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 6000 2850 6000
Connection ~ 2700 6000
Wire Wire Line
	2150 5000 2250 5000
$Comp
L Device:C C?
U 1 1 61A2F537
P 2500 5800
AR Path="/61A2F537" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F537" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F537" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F537" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F537" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F537" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F537" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F537" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F537" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F537" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F537" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F537" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F537" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F537" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F537" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F537" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F537" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F537" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F537" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F537" Ref="C27"  Part="1" 
F 0 "C27" H 2500 5900 50  0000 L CNN
F 1 "4.7uF" H 2500 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 5650 50  0001 C CNN
F 3 "~" H 2500 5800 50  0001 C CNN
	1    2500 5800
	1    0    0    1   
$EndComp
Connection ~ 2250 5000
Wire Wire Line
	2250 5950 2250 6000
Connection ~ 2250 6000
Wire Wire Line
	2250 6000 2500 6000
Wire Wire Line
	2500 5950 2500 6000
Connection ~ 2500 6000
Wire Wire Line
	2500 6000 2700 6000
Wire Wire Line
	1150 5950 1150 6000
Connection ~ 1150 6000
Wire Wire Line
	1150 6000 1650 6000
$Comp
L Device:R R?
U 1 1 6195FBFE
P 2250 5400
AR Path="/6195FBFE" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FBFE" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FBFE" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FBFE" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FBFE" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FBFE" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FBFE" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FBFE" Ref="R?"  Part="1" 
AR Path="/61849340/6195FBFE" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FBFE" Ref="R?"  Part="1" 
AR Path="/61867626/6195FBFE" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FBFE" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FBFE" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FBFE" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FBFE" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FBFE" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FBFE" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FBFE" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FBFE" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FBFE" Ref="R52"  Part="1" 
F 0 "R52" V 2150 5350 50  0000 L CNN
F 1 "300m" V 2250 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 5400 50  0001 C CNN
F 3 "~" H 2250 5400 50  0001 C CNN
	1    2250 5400
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F544
P 2500 5400
AR Path="/61A2F544" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F544" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F544" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F544" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F544" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F544" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F544" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F544" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F544" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F544" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F544" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F544" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F544" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F544" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F544" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F544" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F544" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F544" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F544" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F544" Ref="R53"  Part="1" 
F 0 "R53" V 2400 5350 50  0000 L CNN
F 1 "300m" V 2500 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 5400 50  0001 C CNN
F 3 "~" H 2500 5400 50  0001 C CNN
	1    2500 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 5250 2250 5000
Wire Wire Line
	3100 5600 3100 4850
Wire Wire Line
	2700 6000 2700 4700
Wire Wire Line
	2250 5000 2850 5000
Wire Wire Line
	2250 5550 2250 5650
Wire Wire Line
	2500 5550 2500 5650
Wire Wire Line
	1650 5250 1650 6000
Connection ~ 1650 6000
Wire Wire Line
	1650 6000 2250 6000
Wire Wire Line
	2850 5600 2850 5000
$Comp
L Device:R R?
U 1 1 61A2F32C
P 2850 5750
AR Path="/61A2F32C" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F32C" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F32C" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F32C" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F32C" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F32C" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F32C" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F32C" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F32C" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F32C" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F32C" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F32C" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F32C" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F32C" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F32C" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F32C" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F32C" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F32C" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F32C" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F32C" Ref="R57"  Part="1" 
F 0 "R57" V 2750 5700 50  0000 L CNN
F 1 "17.4" V 2850 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 5750 50  0001 C CNN
F 3 "~" H 2850 5750 50  0001 C CNN
	1    2850 5750
	1    0    0    1   
$EndComp
Text HLabel 950  5800 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	1150 5650 1150 5000
$Comp
L linpol:LREG U?
U 1 1 61A2F556
P 1650 4800
AR Path="/61A2F556" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F556" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F556" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F556" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F556" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F556" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F556" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F556" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F556" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F556" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F556" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F556" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F556" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F556" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F556" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F556" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F556" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F556" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F556" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F556" Ref="U9"  Part="1" 
F 0 "U9" H 1750 5100 60  0000 C CNN
F 1 "LINPOL12V" H 1650 4450 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 4850 260 0001 C CNN
F 3 "" V 1700 4850 260 0001 C CNN
	1    1650 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 6100 1650 6000
Wire Wire Line
	1150 5000 950  5000
Connection ~ 1150 5000
Wire Wire Line
	2500 5250 2500 4850
Wire Wire Line
	2150 4850 2500 4850
Wire Wire Line
	2500 4850 3100 4850
Connection ~ 2500 4850
Text Label 2250 4650 1    50   ~ 0
1V4_9
Wire Wire Line
	2250 5000 2250 4650
Text Label 2500 4650 1    50   ~ 0
3V3_9
Wire Wire Line
	2500 4850 2500 4650
$Comp
L Device:R R?
U 1 1 61A2F563
P 5750 5750
AR Path="/61A2F563" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F563" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F563" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F563" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F563" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F563" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F563" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F563" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F563" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F563" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F563" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F563" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F563" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F563" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F563" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F563" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F563" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F563" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F563" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F563" Ref="R60"  Part="1" 
F 0 "R60" V 5650 5700 50  0000 L CNN
F 1 "165" V 5750 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 5750 50  0001 C CNN
F 3 "~" H 5750 5750 50  0001 C CNN
	1    5750 5750
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F755
P 4300 6100
AR Path="/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F755" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F755" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4300 5850 50  0001 C CNN
F 1 "GND" H 4305 5927 50  0000 C CNN
F 2 "" H 4300 6100 50  0001 C CNN
F 3 "" H 4300 6100 50  0001 C CNN
	1    4300 6100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 5900 5500 6000
Wire Wire Line
	5750 5900 5750 6000
Wire Wire Line
	5750 6000 5500 6000
Connection ~ 5500 6000
Wire Wire Line
	3250 6000 3800 6000
Wire Wire Line
	4800 4700 5350 4700
$Comp
L Device:C C?
U 1 1 61A2F762
P 3800 5800
AR Path="/61A2F762" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F762" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F762" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F762" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F762" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F762" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F762" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F762" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F762" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F762" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F762" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F762" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F762" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F762" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F762" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F762" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F762" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F762" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F762" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F762" Ref="C28"  Part="1" 
F 0 "C28" H 3800 5900 50  0000 L CNN
F 1 "2.2uF" H 3800 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 5650 50  0001 C CNN
F 3 "~" H 3800 5800 50  0001 C CNN
	1    3800 5800
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F769
P 4900 5800
AR Path="/61A2F769" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F769" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F769" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F769" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F769" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F769" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F769" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F769" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F769" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F769" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F769" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F769" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F769" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F769" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F769" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F769" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F769" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F769" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F769" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F769" Ref="C29"  Part="1" 
F 0 "C29" H 4900 5900 50  0000 L CNN
F 1 "10uF" H 4900 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 5650 50  0001 C CNN
F 3 "~" H 4900 5800 50  0001 C CNN
	1    4900 5800
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 6000 5500 6000
Connection ~ 5350 6000
Wire Wire Line
	4800 5000 4900 5000
$Comp
L Device:C C?
U 1 1 61A2F770
P 5150 5800
AR Path="/61A2F770" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F770" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F770" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F770" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F770" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F770" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F770" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F770" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F770" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F770" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F770" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F770" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F770" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F770" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F770" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F770" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F770" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F770" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F770" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F770" Ref="C30"  Part="1" 
F 0 "C30" H 5150 5900 50  0000 L CNN
F 1 "4.7uF" H 5150 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 5650 50  0001 C CNN
F 3 "~" H 5150 5800 50  0001 C CNN
	1    5150 5800
	1    0    0    1   
$EndComp
Connection ~ 4900 5000
Wire Wire Line
	4900 5950 4900 6000
Connection ~ 4900 6000
Wire Wire Line
	4900 6000 5150 6000
Wire Wire Line
	5150 5950 5150 6000
Connection ~ 5150 6000
Wire Wire Line
	5150 6000 5350 6000
Wire Wire Line
	3800 5950 3800 6000
Connection ~ 3800 6000
Wire Wire Line
	3800 6000 4300 6000
$Comp
L Device:R R?
U 1 1 61A2F77C
P 4900 5400
AR Path="/61A2F77C" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F77C" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F77C" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F77C" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F77C" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F77C" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F77C" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F77C" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F77C" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F77C" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F77C" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F77C" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F77C" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F77C" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F77C" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F77C" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F77C" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F77C" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F77C" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F77C" Ref="R55"  Part="1" 
F 0 "R55" V 4800 5350 50  0000 L CNN
F 1 "300m" V 4900 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 5400 50  0001 C CNN
F 3 "~" H 4900 5400 50  0001 C CNN
	1    4900 5400
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F56F
P 5150 5400
AR Path="/61A2F56F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F56F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F56F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F56F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F56F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F56F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F56F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F56F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F56F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F56F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F56F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F56F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F56F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F56F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F56F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F56F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F56F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F56F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F56F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F56F" Ref="R56"  Part="1" 
F 0 "R56" V 5050 5350 50  0000 L CNN
F 1 "300m" V 5150 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 5400 50  0001 C CNN
F 3 "~" H 5150 5400 50  0001 C CNN
	1    5150 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 5250 4900 5000
Wire Wire Line
	5750 5600 5750 4850
Wire Wire Line
	5350 6000 5350 4700
Wire Wire Line
	4900 5000 5500 5000
Wire Wire Line
	4900 5550 4900 5650
Wire Wire Line
	5150 5550 5150 5650
Wire Wire Line
	4300 5250 4300 6000
Connection ~ 4300 6000
Wire Wire Line
	4300 6000 4900 6000
Wire Wire Line
	5500 5600 5500 5000
$Comp
L Device:R R?
U 1 1 61A2F577
P 5500 5750
AR Path="/61A2F577" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F577" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F577" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F577" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F577" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F577" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F577" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F577" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F577" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F577" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F577" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F577" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F577" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F577" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F577" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F577" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F577" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F577" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F577" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F577" Ref="R59"  Part="1" 
F 0 "R59" V 5400 5700 50  0000 L CNN
F 1 "17.4" V 5500 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 5750 50  0001 C CNN
F 3 "~" H 5500 5750 50  0001 C CNN
	1    5500 5750
	1    0    0    1   
$EndComp
Text HLabel 3600 5800 0    50   Input ~ 0
VIN_g1
Wire Wire Line
	3800 5650 3800 5000
$Comp
L linpol:LREG U?
U 1 1 607D7570
P 4300 4800
AR Path="/607D7570" Ref="U?"  Part="1" 
AR Path="/606A40CC/607D7570" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/607D7570" Ref="U?"  Part="1" 
AR Path="/60A71FE2/607D7570" Ref="U?"  Part="1" 
AR Path="/60A749DF/607D7570" Ref="U?"  Part="1" 
AR Path="/60A76681/607D7570" Ref="U?"  Part="1" 
AR Path="/6182C19D/607D7570" Ref="U?"  Part="1" 
AR Path="/6183A7C1/607D7570" Ref="U?"  Part="1" 
AR Path="/61849340/607D7570" Ref="U?"  Part="1" 
AR Path="/61858A9D/607D7570" Ref="U?"  Part="1" 
AR Path="/61867626/607D7570" Ref="U?"  Part="1" 
AR Path="/6187640F/607D7570" Ref="U?"  Part="1" 
AR Path="/618857F1/607D7570" Ref="U?"  Part="1" 
AR Path="/61894FBC/607D7570" Ref="U?"  Part="1" 
AR Path="/618A4CF2/607D7570" Ref="U?"  Part="1" 
AR Path="/618E2B02/607D7570" Ref="U?"  Part="1" 
AR Path="/61987B22/607D7570" Ref="U?"  Part="1" 
AR Path="/607CBABF/607D7570" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/607D7570" Ref="U?"  Part="1" 
AR Path="/607AA45D/607D7570" Ref="U10"  Part="1" 
F 0 "U10" H 4400 5100 60  0000 C CNN
F 1 "LINPOL12V" H 4300 4450 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 4850 260 0001 C CNN
F 3 "" V 4350 4850 260 0001 C CNN
	1    4300 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 6100 4300 6000
Wire Wire Line
	3800 5000 3600 5000
Connection ~ 3800 5000
Wire Wire Line
	5150 5250 5150 4850
Wire Wire Line
	4800 4850 5150 4850
Wire Wire Line
	5150 4850 5750 4850
Connection ~ 5150 4850
Text Label 4900 4650 1    50   ~ 0
1V4_10
Wire Wire Line
	4900 5000 4900 4650
Text Label 5150 4650 1    50   ~ 0
3V3_10
Wire Wire Line
	5150 4850 5150 4650
Text Label 950  1650 2    50   ~ 0
J1_0
Text Label 950  1500 2    50   ~ 0
J1_1
Wire Wire Line
	950  1150 950  1500
Wire Wire Line
	950  1650 950  1950
Text Label 3600 1650 2    50   ~ 0
J2_0
Text Label 3600 1500 2    50   ~ 0
J2_1
Wire Wire Line
	3600 1150 3600 1500
Wire Wire Line
	3600 1650 3600 1950
Text Label 6250 1650 2    50   ~ 0
J3_0
Text Label 6250 1500 2    50   ~ 0
J3_1
Wire Wire Line
	6250 1150 6250 1500
Wire Wire Line
	6250 1650 6250 1950
Text Label 8900 1650 2    50   ~ 0
J4_0
Text Label 8900 1500 2    50   ~ 0
J4_1
Wire Wire Line
	8900 1150 8900 1500
Wire Wire Line
	8900 1650 8900 1950
Text Label 950  3550 2    50   ~ 0
J5_0
Text Label 950  3400 2    50   ~ 0
J5_1
Wire Wire Line
	950  3050 950  3400
Wire Wire Line
	950  3550 950  3850
Text Label 3600 3550 2    50   ~ 0
J6_0
Text Label 3600 3400 2    50   ~ 0
J6_1
Wire Wire Line
	3600 3050 3600 3400
Wire Wire Line
	3600 3550 3600 3850
Text Label 6250 3550 2    50   ~ 0
J7_0
Text Label 6250 3400 2    50   ~ 0
J7_1
Wire Wire Line
	6250 3050 6250 3400
Wire Wire Line
	6250 3550 6250 3850
Text Label 8900 3550 2    50   ~ 0
J8_0
Text Label 8900 3400 2    50   ~ 0
J8_1
Wire Wire Line
	8900 3050 8900 3400
Wire Wire Line
	8900 3550 8900 3850
Text Label 950  5500 2    50   ~ 0
J9_0
Text Label 950  5350 2    50   ~ 0
J9_1
Wire Wire Line
	950  5000 950  5350
Wire Wire Line
	950  5500 950  5800
Text Label 3600 5500 2    50   ~ 0
J10_0
Text Label 3600 5350 2    50   ~ 0
J10_1
Wire Wire Line
	3600 5000 3600 5350
Wire Wire Line
	3600 5500 3600 5800
Text Label 9000 5150 2    50   ~ 0
J1_0
Text Label 9000 5250 2    50   ~ 0
J2_0
Text Label 9000 5350 2    50   ~ 0
J3_0
Text Label 9000 5450 2    50   ~ 0
J4_0
Text Label 9000 5550 2    50   ~ 0
J5_0
Text Label 9000 5650 2    50   ~ 0
J6_0
Text Label 9000 5750 2    50   ~ 0
J7_0
Text Label 9000 5850 2    50   ~ 0
J8_0
Text Label 9000 5950 2    50   ~ 0
J9_0
Text Label 9000 6050 2    50   ~ 0
J10_0
Wire Wire Line
	9000 5150 9100 5150
Wire Wire Line
	9000 5250 9100 5250
Wire Wire Line
	9000 5350 9100 5350
Wire Wire Line
	9000 5450 9100 5450
Wire Wire Line
	9000 5550 9100 5550
Wire Wire Line
	9000 5650 9100 5650
Wire Wire Line
	9000 5750 9100 5750
Wire Wire Line
	9000 5850 9100 5850
Wire Wire Line
	9000 5950 9100 5950
Wire Wire Line
	9000 6050 9100 6050
Text Label 9700 5150 0    50   ~ 0
J1_1
Text Label 9700 5250 0    50   ~ 0
J2_1
Text Label 9700 5350 0    50   ~ 0
J3_1
Text Label 9700 5450 0    50   ~ 0
J4_1
Text Label 9700 5550 0    50   ~ 0
J5_1
Text Label 9700 5650 0    50   ~ 0
J6_1
Text Label 9700 5750 0    50   ~ 0
J7_1
Text Label 9700 5850 0    50   ~ 0
J8_1
Text Label 9700 5950 0    50   ~ 0
J9_1
Text Label 9700 6050 0    50   ~ 0
J10_1
Wire Wire Line
	9700 5150 9600 5150
Wire Wire Line
	9700 5250 9600 5250
Wire Wire Line
	9700 5350 9600 5350
Wire Wire Line
	9700 5450 9600 5450
Wire Wire Line
	9700 5550 9600 5550
Wire Wire Line
	9700 5650 9600 5650
Wire Wire Line
	9700 5750 9600 5750
Wire Wire Line
	9700 5850 9600 5850
Wire Wire Line
	9700 5950 9600 5950
Wire Wire Line
	9700 6050 9600 6050
Entry Wire Line
	7000 4900 7100 5000
Entry Wire Line
	7000 5000 7100 5100
Entry Wire Line
	7000 5100 7100 5200
Entry Wire Line
	7000 5200 7100 5300
Entry Wire Line
	7000 5300 7100 5400
Entry Wire Line
	7000 5400 7100 5500
Entry Wire Line
	7000 5500 7100 5600
Entry Wire Line
	7000 5600 7100 5700
Entry Wire Line
	7000 5700 7100 5800
Entry Wire Line
	7000 5800 7100 5900
Text HLabel 7000 6050 0    50   Output ~ 0
1V4_[1..10]
Wire Bus Line
	7100 6050 7000 6050
Text Label 6850 4900 2    50   ~ 0
1V4_1
Wire Wire Line
	7000 4900 6850 4900
Text Label 6850 5000 2    50   ~ 0
1V4_2
Wire Wire Line
	7000 5000 6850 5000
Text Label 6850 5100 2    50   ~ 0
1V4_3
Wire Wire Line
	7000 5100 6850 5100
Text Label 6850 5200 2    50   ~ 0
1V4_4
Wire Wire Line
	7000 5200 6850 5200
Text Label 6850 5300 2    50   ~ 0
1V4_5
Wire Wire Line
	7000 5300 6850 5300
Text Label 6850 5400 2    50   ~ 0
1V4_6
Wire Wire Line
	7000 5400 6850 5400
Text Label 6850 5500 2    50   ~ 0
1V4_7
Wire Wire Line
	7000 5500 6850 5500
Text Label 6850 5600 2    50   ~ 0
1V4_8
Wire Wire Line
	7000 5600 6850 5600
Text Label 6850 5700 2    50   ~ 0
1V4_9
Wire Wire Line
	7000 5700 6850 5700
Text Label 6850 5800 2    50   ~ 0
1V4_10
Wire Wire Line
	7000 5800 6850 5800
Entry Wire Line
	8000 4900 8100 5000
Entry Wire Line
	8000 5000 8100 5100
Entry Wire Line
	8000 5100 8100 5200
Entry Wire Line
	8000 5200 8100 5300
Entry Wire Line
	8000 5300 8100 5400
Entry Wire Line
	8000 5400 8100 5500
Entry Wire Line
	8000 5500 8100 5600
Entry Wire Line
	8000 5600 8100 5700
Entry Wire Line
	8000 5700 8100 5800
Entry Wire Line
	8000 5800 8100 5900
Text HLabel 8000 6050 0    50   Output ~ 0
3V3_[1..10]
Wire Bus Line
	8100 6050 8000 6050
Text Label 7850 4900 2    50   ~ 0
3V3_1
Wire Wire Line
	8000 4900 7850 4900
Text Label 7850 5000 2    50   ~ 0
3V3_2
Wire Wire Line
	8000 5000 7850 5000
Text Label 7850 5100 2    50   ~ 0
3V3_3
Wire Wire Line
	8000 5100 7850 5100
Text Label 7850 5200 2    50   ~ 0
3V3_4
Wire Wire Line
	8000 5200 7850 5200
Text Label 7850 5300 2    50   ~ 0
3V3_5
Wire Wire Line
	8000 5300 7850 5300
Text Label 7850 5400 2    50   ~ 0
3V3_6
Wire Wire Line
	8000 5400 7850 5400
Text Label 7850 5500 2    50   ~ 0
3V3_7
Wire Wire Line
	8000 5500 7850 5500
Text Label 7850 5600 2    50   ~ 0
3V3_8
Wire Wire Line
	8000 5600 7850 5600
Text Label 7850 5700 2    50   ~ 0
3V3_9
Wire Wire Line
	8000 5700 7850 5700
Text Label 7850 5800 2    50   ~ 0
3V3_10
Wire Wire Line
	8000 5800 7850 5800
$Comp
L Device:R R?
U 1 1 61A2F7A7
P 600 1550
AR Path="/606A40CC/61A2F7A7" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7A7" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7A7" Ref="R5"  Part="1" 
F 0 "R5" V 700 1500 50  0000 L CNN
F 1 "100k" V 600 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 1550 50  0001 C CNN
F 3 "~" H 600 1550 50  0001 C CNN
	1    600  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  1700 600  2150
Wire Wire Line
	600  1400 600  1000
Text Label 750  600  0    50   ~ 0
LREGEN1
$Comp
L Device:R R?
U 1 1 61A2F7C1
P 3250 1550
AR Path="/606A40CC/61A2F7C1" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7C1" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7C1" Ref="R8"  Part="1" 
F 0 "R8" V 3350 1500 50  0000 L CNN
F 1 "100k" V 3250 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 1550 50  0001 C CNN
F 3 "~" H 3250 1550 50  0001 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1700 3250 2150
$Comp
L Device:R R?
U 1 1 61A2F7D2
P 5900 1550
AR Path="/606A40CC/61A2F7D2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7D2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7D2" Ref="R11"  Part="1" 
F 0 "R11" V 6000 1500 50  0000 L CNN
F 1 "100k" V 5900 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 1550 50  0001 C CNN
F 3 "~" H 5900 1550 50  0001 C CNN
	1    5900 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1700 5900 2150
$Comp
L Device:R R?
U 1 1 61A2F7D8
P 8550 1550
AR Path="/606A40CC/61A2F7D8" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7D8" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7D8" Ref="R14"  Part="1" 
F 0 "R14" V 8650 1500 50  0000 L CNN
F 1 "100k" V 8550 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 1550 50  0001 C CNN
F 3 "~" H 8550 1550 50  0001 C CNN
	1    8550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1700 8550 2150
$Comp
L Device:R R?
U 1 1 61A2F7EC
P 8550 3450
AR Path="/606A40CC/61A2F7EC" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7EC" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7EC" Ref="R38"  Part="1" 
F 0 "R38" V 8650 3400 50  0000 L CNN
F 1 "100k" V 8550 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 3450 50  0001 C CNN
F 3 "~" H 8550 3450 50  0001 C CNN
	1    8550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3600 8550 4050
$Comp
L Device:R R?
U 1 1 61A2F7F1
P 5900 3450
AR Path="/606A40CC/61A2F7F1" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7F1" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7F1" Ref="R35"  Part="1" 
F 0 "R35" V 6000 3400 50  0000 L CNN
F 1 "100k" V 5900 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 3450 50  0001 C CNN
F 3 "~" H 5900 3450 50  0001 C CNN
	1    5900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3600 5900 4050
$Comp
L Device:R R?
U 1 1 61A2F806
P 3250 3450
AR Path="/606A40CC/61A2F806" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F806" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F806" Ref="R32"  Part="1" 
F 0 "R32" V 3350 3400 50  0000 L CNN
F 1 "100k" V 3250 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 3450 50  0001 C CNN
F 3 "~" H 3250 3450 50  0001 C CNN
	1    3250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3600 3250 4050
$Comp
L Device:R R?
U 1 1 61A2F813
P 600 3450
AR Path="/606A40CC/61A2F813" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F813" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F813" Ref="R29"  Part="1" 
F 0 "R29" V 700 3400 50  0000 L CNN
F 1 "100k" V 600 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 3450 50  0001 C CNN
F 3 "~" H 600 3450 50  0001 C CNN
	1    600  3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  3600 600  4050
$Comp
L Device:R R?
U 1 1 61A2F815
P 600 5400
AR Path="/606A40CC/61A2F815" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F815" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F815" Ref="R51"  Part="1" 
F 0 "R51" V 700 5350 50  0000 L CNN
F 1 "100k" V 600 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 5400 50  0001 C CNN
F 3 "~" H 600 5400 50  0001 C CNN
	1    600  5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  5550 600  6000
$Comp
L Device:R R?
U 1 1 61A2F81F
P 3250 5400
AR Path="/606A40CC/61A2F81F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F81F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F81F" Ref="R54"  Part="1" 
F 0 "R54" V 3350 5350 50  0000 L CNN
F 1 "100k" V 3250 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 5400 50  0001 C CNN
F 3 "~" H 3250 5400 50  0001 C CNN
	1    3250 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5550 3250 6000
$Comp
L Device:R R?
U 1 1 61A2F335
P 600 800
AR Path="/606A40CC/61A2F335" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F335" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F335" Ref="R1"  Part="1" 
F 0 "R1" V 700 750 50  0000 L CNN
F 1 "0" V 600 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 800 50  0001 C CNN
F 3 "~" H 600 800 50  0001 C CNN
	1    600  800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  950  600  1000
Connection ~ 600  1000
Wire Wire Line
	600  650  600  600 
Wire Wire Line
	600  600  750  600 
Wire Wire Line
	1150 850  850  850 
Wire Wire Line
	850  850  850  1000
Connection ~ 850  1000
Wire Wire Line
	850  1000 600  1000
Wire Wire Line
	3800 1000 3500 1000
Wire Wire Line
	3250 1400 3250 1000
Text Label 3400 600  0    50   ~ 0
LREGEN2
$Comp
L Device:R R?
U 1 1 61A2F38B
P 3250 800
AR Path="/606A40CC/61A2F38B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F38B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F38B" Ref="R2"  Part="1" 
F 0 "R2" V 3350 750 50  0000 L CNN
F 1 "0" V 3250 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 800 50  0001 C CNN
F 3 "~" H 3250 800 50  0001 C CNN
	1    3250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 950  3250 1000
Connection ~ 3250 1000
Wire Wire Line
	3250 650  3250 600 
Wire Wire Line
	3250 600  3400 600 
Wire Wire Line
	3800 850  3500 850 
Wire Wire Line
	3500 850  3500 1000
Connection ~ 3500 1000
Wire Wire Line
	3500 1000 3250 1000
Wire Wire Line
	6450 1000 6150 1000
Wire Wire Line
	5900 1400 5900 1000
Text Label 6050 600  0    50   ~ 0
LREGEN3
$Comp
L Device:R R?
U 1 1 6144BE8E
P 5900 800
AR Path="/606A40CC/6144BE8E" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6144BE8E" Ref="R?"  Part="1" 
AR Path="/607AA45D/6144BE8E" Ref="R3"  Part="1" 
F 0 "R3" V 6000 750 50  0000 L CNN
F 1 "0" V 5900 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 800 50  0001 C CNN
F 3 "~" H 5900 800 50  0001 C CNN
	1    5900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 950  5900 1000
Connection ~ 5900 1000
Wire Wire Line
	5900 650  5900 600 
Wire Wire Line
	5900 600  6050 600 
Wire Wire Line
	6450 850  6150 850 
Wire Wire Line
	6150 850  6150 1000
Connection ~ 6150 1000
Wire Wire Line
	6150 1000 5900 1000
Wire Wire Line
	9100 1000 8800 1000
Wire Wire Line
	8550 1400 8550 1000
Text Label 8700 600  0    50   ~ 0
LREGEN4
$Comp
L Device:R R?
U 1 1 61A2F39F
P 8550 800
AR Path="/606A40CC/61A2F39F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F39F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F39F" Ref="R4"  Part="1" 
F 0 "R4" V 8650 750 50  0000 L CNN
F 1 "0" V 8550 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 800 50  0001 C CNN
F 3 "~" H 8550 800 50  0001 C CNN
	1    8550 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 950  8550 1000
Connection ~ 8550 1000
Wire Wire Line
	8550 650  8550 600 
Wire Wire Line
	8550 600  8700 600 
Wire Wire Line
	9100 850  8800 850 
Wire Wire Line
	8800 850  8800 1000
Connection ~ 8800 1000
Wire Wire Line
	8800 1000 8550 1000
Wire Wire Line
	1150 2900 850  2900
Wire Wire Line
	600  3300 600  2900
Text Label 750  2500 0    50   ~ 0
LREGEN5
$Comp
L Device:R R?
U 1 1 61A2F3AE
P 600 2700
AR Path="/606A40CC/61A2F3AE" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3AE" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3AE" Ref="R25"  Part="1" 
F 0 "R25" V 700 2650 50  0000 L CNN
F 1 "0" V 600 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 2700 50  0001 C CNN
F 3 "~" H 600 2700 50  0001 C CNN
	1    600  2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  2850 600  2900
Connection ~ 600  2900
Wire Wire Line
	600  2550 600  2500
Wire Wire Line
	600  2500 750  2500
Wire Wire Line
	1150 2750 850  2750
Wire Wire Line
	850  2750 850  2900
Connection ~ 850  2900
Wire Wire Line
	850  2900 600  2900
Wire Wire Line
	3800 2900 3500 2900
Wire Wire Line
	3250 3300 3250 2900
Text Label 3400 2500 0    50   ~ 0
LREGEN6
$Comp
L Device:R R?
U 1 1 61599BA7
P 3250 2700
AR Path="/606A40CC/61599BA7" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61599BA7" Ref="R?"  Part="1" 
AR Path="/607AA45D/61599BA7" Ref="R26"  Part="1" 
F 0 "R26" V 3350 2650 50  0000 L CNN
F 1 "0" V 3250 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 2700 50  0001 C CNN
F 3 "~" H 3250 2700 50  0001 C CNN
	1    3250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2850 3250 2900
Connection ~ 3250 2900
Wire Wire Line
	3250 2550 3250 2500
Wire Wire Line
	3250 2500 3400 2500
Wire Wire Line
	3800 2750 3500 2750
Wire Wire Line
	3500 2750 3500 2900
Connection ~ 3500 2900
Wire Wire Line
	3500 2900 3250 2900
Wire Wire Line
	6450 2900 6150 2900
Wire Wire Line
	5900 3300 5900 2900
Text Label 6050 2500 0    50   ~ 0
LREGEN7
$Comp
L Device:R R?
U 1 1 61A2F3C2
P 5900 2700
AR Path="/606A40CC/61A2F3C2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3C2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3C2" Ref="R27"  Part="1" 
F 0 "R27" V 6000 2650 50  0000 L CNN
F 1 "0" V 5900 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 2700 50  0001 C CNN
F 3 "~" H 5900 2700 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2850 5900 2900
Connection ~ 5900 2900
Wire Wire Line
	5900 2550 5900 2500
Wire Wire Line
	5900 2500 6050 2500
Wire Wire Line
	6450 2750 6150 2750
Wire Wire Line
	6150 2750 6150 2900
Connection ~ 6150 2900
Wire Wire Line
	6150 2900 5900 2900
Wire Wire Line
	9100 2900 8800 2900
Wire Wire Line
	8550 3300 8550 2900
Text Label 8700 2500 0    50   ~ 0
LREGEN8
$Comp
L Device:R R?
U 1 1 61A2F3CA
P 8550 2700
AR Path="/606A40CC/61A2F3CA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3CA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3CA" Ref="R28"  Part="1" 
F 0 "R28" V 8650 2650 50  0000 L CNN
F 1 "0" V 8550 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2850 8550 2900
Connection ~ 8550 2900
Wire Wire Line
	8550 2550 8550 2500
Wire Wire Line
	8550 2500 8700 2500
Wire Wire Line
	9100 2750 8800 2750
Wire Wire Line
	8800 2750 8800 2900
Connection ~ 8800 2900
Wire Wire Line
	8800 2900 8550 2900
Wire Wire Line
	1150 4850 850  4850
Wire Wire Line
	600  5250 600  4850
Text Label 750  4450 0    50   ~ 0
LREGEN9
$Comp
L Device:R R?
U 1 1 61A2F3E0
P 600 4650
AR Path="/606A40CC/61A2F3E0" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3E0" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3E0" Ref="R49"  Part="1" 
F 0 "R49" V 700 4600 50  0000 L CNN
F 1 "0" V 600 4600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 4650 50  0001 C CNN
F 3 "~" H 600 4650 50  0001 C CNN
	1    600  4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  4800 600  4850
Connection ~ 600  4850
Wire Wire Line
	600  4500 600  4450
Wire Wire Line
	600  4450 750  4450
Wire Wire Line
	1150 4700 850  4700
Wire Wire Line
	850  4700 850  4850
Connection ~ 850  4850
Wire Wire Line
	850  4850 600  4850
Wire Wire Line
	3800 4850 3500 4850
Wire Wire Line
	3250 5250 3250 4850
Text Label 3400 4450 0    50   ~ 0
LREGEN10
$Comp
L Device:R R?
U 1 1 61A2F3E2
P 3250 4650
AR Path="/606A40CC/61A2F3E2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3E2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3E2" Ref="R50"  Part="1" 
F 0 "R50" V 3350 4600 50  0000 L CNN
F 1 "0" V 3250 4600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4650 50  0001 C CNN
F 3 "~" H 3250 4650 50  0001 C CNN
	1    3250 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4800 3250 4850
Connection ~ 3250 4850
Wire Wire Line
	3250 4500 3250 4450
Wire Wire Line
	3250 4450 3400 4450
Wire Wire Line
	3800 4700 3500 4700
Wire Wire Line
	3500 4700 3500 4850
Connection ~ 3500 4850
Wire Wire Line
	3500 4850 3250 4850
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J1
U 1 1 6132E825
P 9300 5150
F 0 "J1" H 9350 5367 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5150 50  0001 C CNN
F 3 "~" H 9300 5150 50  0001 C CNN
	1    9300 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2
U 1 1 6132F02C
P 9300 5350
F 0 "J2" H 9350 5567 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5350 50  0001 C CNN
F 3 "~" H 9300 5350 50  0001 C CNN
	1    9300 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 6132F872
P 9300 5550
F 0 "J3" H 9350 5767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5550 50  0001 C CNN
F 3 "~" H 9300 5550 50  0001 C CNN
	1    9300 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J4
U 1 1 6132FF5D
P 9300 5750
F 0 "J4" H 9350 5967 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5750 50  0001 C CNN
F 3 "~" H 9300 5750 50  0001 C CNN
	1    9300 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J5
U 1 1 613304F9
P 9300 5950
F 0 "J5" H 9350 6167 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 6076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5950 50  0001 C CNN
F 3 "~" H 9300 5950 50  0001 C CNN
	1    9300 5950
	1    0    0    -1  
$EndComp
Text HLabel 10850 6200 0    50   Input ~ 0
LREGEN[1..10]
Entry Wire Line
	10850 5100 10950 5200
Entry Wire Line
	10850 5200 10950 5300
Entry Wire Line
	10850 5300 10950 5400
Entry Wire Line
	10850 5400 10950 5500
Entry Wire Line
	10850 5500 10950 5600
Entry Wire Line
	10850 5600 10950 5700
Entry Wire Line
	10850 5700 10950 5800
Entry Wire Line
	10850 5800 10950 5900
Entry Wire Line
	10850 5900 10950 6000
Entry Wire Line
	10850 6000 10950 6100
Wire Bus Line
	10850 6200 10950 6200
Text Label 10750 5100 2    50   ~ 0
LREGEN1
Wire Wire Line
	10750 5100 10850 5100
Text Label 10750 5200 2    50   ~ 0
LREGEN2
Wire Wire Line
	10750 5200 10850 5200
Text Label 10750 5300 2    50   ~ 0
LREGEN3
Wire Wire Line
	10750 5300 10850 5300
Text Label 10750 5400 2    50   ~ 0
LREGEN4
Wire Wire Line
	10750 5400 10850 5400
Text Label 10750 5500 2    50   ~ 0
LREGEN5
Wire Wire Line
	10750 5500 10850 5500
Text Label 10750 5600 2    50   ~ 0
LREGEN6
Wire Wire Line
	10750 5600 10850 5600
Text Label 10750 5700 2    50   ~ 0
LREGEN7
Wire Wire Line
	10750 5700 10850 5700
Text Label 10750 5800 2    50   ~ 0
LREGEN8
Wire Wire Line
	10750 5800 10850 5800
Text Label 10750 5900 2    50   ~ 0
LREGEN9
Wire Wire Line
	10750 5900 10850 5900
Text Label 10750 6000 2    50   ~ 0
LREGEN10
Wire Wire Line
	10750 6000 10850 6000
Wire Bus Line
	8100 4900 8100 6050
Wire Bus Line
	7100 4900 7100 6050
Wire Bus Line
	10950 5050 10950 6200
$EndSCHEMATC
