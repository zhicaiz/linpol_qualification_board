EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 20
Title "linPOL qualification board"
Date "2021-04-04"
Rev "v1"
Comp "LBNL"
Comment1 "Zhicai Zhang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 61A2F57E
P 3100 1900
AR Path="/61A2F57E" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F57E" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F57E" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F57E" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F57E" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F57E" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F57E" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F57E" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F57E" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F57E" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F57E" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F57E" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F57E" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F57E" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F57E" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F57E" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F57E" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F57E" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F57E" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F57E" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F57E" Ref="R738"  Part="1" 
F 0 "R738" V 3000 1850 50  0000 L CNN
F 1 "165" V 3100 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 1900 50  0001 C CNN
F 3 "~" H 3100 1900 50  0001 C CNN
	1    3100 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F359
P 1650 2250
AR Path="/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F359" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F359" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 1650 2000 50  0001 C CNN
F 1 "GND" H 1655 2077 50  0000 C CNN
F 2 "" H 1650 2250 50  0001 C CNN
F 3 "" H 1650 2250 50  0001 C CNN
	1    1650 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 2050 2850 2150
Wire Wire Line
	3100 2050 3100 2150
Wire Wire Line
	3100 2150 2850 2150
Connection ~ 2850 2150
Wire Wire Line
	1150 1000 850  1000
Wire Wire Line
	600  2150 1150 2150
Wire Wire Line
	2150 850  2700 850 
$Comp
L Device:C C?
U 1 1 61A2F5CD
P 1150 1950
AR Path="/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5CD" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F5CD" Ref="C361"  Part="1" 
F 0 "C361" H 1150 2050 50  0000 L CNN
F 1 "2.2uF" H 1150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 1800 50  0001 C CNN
F 3 "~" H 1150 1950 50  0001 C CNN
	1    1150 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 6195FC88
P 2250 1950
AR Path="/6195FC88" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FC88" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FC88" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FC88" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FC88" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FC88" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FC88" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FC88" Ref="C?"  Part="1" 
AR Path="/61849340/6195FC88" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FC88" Ref="C?"  Part="1" 
AR Path="/61867626/6195FC88" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FC88" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FC88" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FC88" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FC88" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FC88" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FC88" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FC88" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FC88" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FC88" Ref="C?"  Part="1" 
AR Path="/607F867C/6195FC88" Ref="C362"  Part="1" 
F 0 "C362" H 2250 2050 50  0000 L CNN
F 1 "10uF" H 2250 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 1800 50  0001 C CNN
F 3 "~" H 2250 1950 50  0001 C CNN
	1    2250 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 2150 2850 2150
Connection ~ 2700 2150
Wire Wire Line
	2150 1150 2250 1150
$Comp
L Device:C C?
U 1 1 61A2F598
P 2500 1950
AR Path="/61A2F598" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F598" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F598" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F598" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F598" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F598" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F598" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F598" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F598" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F598" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F598" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F598" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F598" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F598" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F598" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F598" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F598" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F598" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F598" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F598" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F598" Ref="C363"  Part="1" 
F 0 "C363" H 2500 2050 50  0000 L CNN
F 1 "4.7uF" H 2500 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 1800 50  0001 C CNN
F 3 "~" H 2500 1950 50  0001 C CNN
	1    2500 1950
	1    0    0    1   
$EndComp
Connection ~ 2250 1150
Wire Wire Line
	2250 2100 2250 2150
Connection ~ 2250 2150
Wire Wire Line
	2250 2150 2500 2150
Wire Wire Line
	2500 2100 2500 2150
Connection ~ 2500 2150
Wire Wire Line
	2500 2150 2700 2150
Wire Wire Line
	1150 2100 1150 2150
Connection ~ 1150 2150
Wire Wire Line
	1150 2150 1650 2150
$Comp
L Device:R R?
U 1 1 61A2F5A5
P 2250 1550
AR Path="/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5A5" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F5A5" Ref="R726"  Part="1" 
F 0 "R726" V 2150 1500 50  0000 L CNN
F 1 "300m" V 2250 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 1550 50  0001 C CNN
F 3 "~" H 2250 1550 50  0001 C CNN
	1    2250 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F5B6
P 2500 1550
AR Path="/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5B6" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F5B6" Ref="R727"  Part="1" 
F 0 "R727" V 2400 1500 50  0000 L CNN
F 1 "300m" V 2500 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 1550 50  0001 C CNN
F 3 "~" H 2500 1550 50  0001 C CNN
	1    2500 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 1400 2250 1150
Wire Wire Line
	3100 1750 3100 1000
Wire Wire Line
	2700 2150 2700 850 
Wire Wire Line
	2250 1150 2850 1150
Wire Wire Line
	2250 1700 2250 1800
Wire Wire Line
	2500 1700 2500 1800
Wire Wire Line
	1650 1400 1650 2150
Connection ~ 1650 2150
Wire Wire Line
	1650 2150 2250 2150
Wire Wire Line
	2850 1750 2850 1150
$Comp
L Device:R R?
U 1 1 61A2F7AA
P 2850 1900
AR Path="/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7AA" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7AA" Ref="R737"  Part="1" 
F 0 "R737" V 2750 1850 50  0000 L CNN
F 1 "23.2" V 2850 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 1900 50  0001 C CNN
F 3 "~" H 2850 1900 50  0001 C CNN
	1    2850 1900
	1    0    0    1   
$EndComp
Text HLabel 950  1950 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	1150 1800 1150 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F58A
P 1650 950
AR Path="/61A2F58A" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F58A" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F58A" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F58A" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F58A" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F58A" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F58A" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F58A" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F58A" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F58A" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F58A" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F58A" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F58A" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F58A" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F58A" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F58A" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F58A" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F58A" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F58A" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F58A" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F58A" Ref="U121"  Part="1" 
F 0 "U121" H 1750 1250 60  0000 C CNN
F 1 "LINPOL12V" H 1650 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 1000 260 0001 C CNN
F 3 "" V 1700 1000 260 0001 C CNN
	1    1650 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 2250 1650 2150
Wire Wire Line
	1150 1150 950  1150
Connection ~ 1150 1150
Wire Wire Line
	2500 1400 2500 1000
Wire Wire Line
	2150 1000 2500 1000
Wire Wire Line
	2500 1000 3100 1000
Connection ~ 2500 1000
Text Label 2250 800  1    50   ~ 0
1V4_121
Wire Wire Line
	2250 1150 2250 800 
Text Label 2500 800  1    50   ~ 0
3V3_121
Wire Wire Line
	2500 1000 2500 800 
$Comp
L Device:R R?
U 1 1 6195FCA4
P 5750 1900
AR Path="/6195FCA4" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FCA4" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FCA4" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FCA4" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FCA4" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FCA4" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FCA4" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FCA4" Ref="R?"  Part="1" 
AR Path="/61849340/6195FCA4" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FCA4" Ref="R?"  Part="1" 
AR Path="/61867626/6195FCA4" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FCA4" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FCA4" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FCA4" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FCA4" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FCA4" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FCA4" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FCA4" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FCA4" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FCA4" Ref="R?"  Part="1" 
AR Path="/607F867C/6195FCA4" Ref="R740"  Part="1" 
F 0 "R740" V 5650 1850 50  0000 L CNN
F 1 "165" V 5750 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 1900 50  0001 C CNN
F 3 "~" H 5750 1900 50  0001 C CNN
	1    5750 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F5E3
P 4300 2250
AR Path="/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F5E3" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F5E3" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 4300 2000 50  0001 C CNN
F 1 "GND" H 4305 2077 50  0000 C CNN
F 2 "" H 4300 2250 50  0001 C CNN
F 3 "" H 4300 2250 50  0001 C CNN
	1    4300 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 2050 5500 2150
Wire Wire Line
	5750 2050 5750 2150
Wire Wire Line
	5750 2150 5500 2150
Connection ~ 5500 2150
Wire Wire Line
	3250 2150 3800 2150
Wire Wire Line
	4800 850  5350 850 
$Comp
L Device:C C?
U 1 1 61A2F5EB
P 3800 1950
AR Path="/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5EB" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F5EB" Ref="C364"  Part="1" 
F 0 "C364" H 3800 2050 50  0000 L CNN
F 1 "2.2uF" H 3800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 1800 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F3F3
P 4900 1950
AR Path="/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F3F3" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F3F3" Ref="C365"  Part="1" 
F 0 "C365" H 4900 2050 50  0000 L CNN
F 1 "10uF" H 4900 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 1800 50  0001 C CNN
F 3 "~" H 4900 1950 50  0001 C CNN
	1    4900 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 2150 5500 2150
Connection ~ 5350 2150
Wire Wire Line
	4800 1150 4900 1150
$Comp
L Device:C C?
U 1 1 61A2F5FA
P 5150 1950
AR Path="/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5FA" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F5FA" Ref="C366"  Part="1" 
F 0 "C366" H 5150 2050 50  0000 L CNN
F 1 "4.7uF" H 5150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 1800 50  0001 C CNN
F 3 "~" H 5150 1950 50  0001 C CNN
	1    5150 1950
	1    0    0    1   
$EndComp
Connection ~ 4900 1150
Wire Wire Line
	4900 2100 4900 2150
Connection ~ 4900 2150
Wire Wire Line
	4900 2150 5150 2150
Wire Wire Line
	5150 2100 5150 2150
Connection ~ 5150 2150
Wire Wire Line
	5150 2150 5350 2150
Wire Wire Line
	3800 2100 3800 2150
Connection ~ 3800 2150
Wire Wire Line
	3800 2150 4300 2150
$Comp
L Device:R R?
U 1 1 61A2F401
P 4900 1550
AR Path="/61A2F401" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F401" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F401" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F401" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F401" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F401" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F401" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F401" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F401" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F401" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F401" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F401" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F401" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F401" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F401" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F401" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F401" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F401" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F401" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F401" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F401" Ref="R729"  Part="1" 
F 0 "R729" V 4800 1500 50  0000 L CNN
F 1 "300m" V 4900 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 1550 50  0001 C CNN
F 3 "~" H 4900 1550 50  0001 C CNN
	1    4900 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F40B
P 5150 1550
AR Path="/61A2F40B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F40B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F40B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F40B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F40B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F40B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F40B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F40B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F40B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F40B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F40B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F40B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F40B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F40B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F40B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F40B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F40B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F40B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F40B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F40B" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F40B" Ref="R730"  Part="1" 
F 0 "R730" V 5050 1500 50  0000 L CNN
F 1 "300m" V 5150 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 1550 50  0001 C CNN
F 3 "~" H 5150 1550 50  0001 C CNN
	1    5150 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 1400 4900 1150
Wire Wire Line
	5750 1750 5750 1000
Wire Wire Line
	5350 2150 5350 850 
Wire Wire Line
	4900 1150 5500 1150
Wire Wire Line
	4900 1700 4900 1800
Wire Wire Line
	5150 1700 5150 1800
Wire Wire Line
	4300 1400 4300 2150
Connection ~ 4300 2150
Wire Wire Line
	4300 2150 4900 2150
Wire Wire Line
	5500 1750 5500 1150
$Comp
L Device:R R?
U 1 1 61A2F604
P 5500 1900
AR Path="/61A2F604" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F604" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F604" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F604" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F604" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F604" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F604" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F604" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F604" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F604" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F604" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F604" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F604" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F604" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F604" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F604" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F604" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F604" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F604" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F604" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F604" Ref="R739"  Part="1" 
F 0 "R739" V 5400 1850 50  0000 L CNN
F 1 "20" V 5500 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 1900 50  0001 C CNN
F 3 "~" H 5500 1900 50  0001 C CNN
	1    5500 1900
	1    0    0    1   
$EndComp
Text HLabel 3600 1950 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	3800 1800 3800 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F30D
P 4300 950
AR Path="/61A2F30D" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F30D" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F30D" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F30D" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F30D" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F30D" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F30D" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F30D" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F30D" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F30D" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F30D" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F30D" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F30D" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F30D" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F30D" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F30D" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F30D" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F30D" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F30D" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F30D" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F30D" Ref="U122"  Part="1" 
F 0 "U122" H 4400 1250 60  0000 C CNN
F 1 "LINPOL12V" H 4300 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 1000 260 0001 C CNN
F 3 "" V 4350 1000 260 0001 C CNN
	1    4300 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 2250 4300 2150
Wire Wire Line
	3800 1150 3600 1150
Connection ~ 3800 1150
Wire Wire Line
	5150 1400 5150 1000
Wire Wire Line
	4800 1000 5150 1000
Wire Wire Line
	5150 1000 5750 1000
Connection ~ 5150 1000
Text Label 4900 800  1    50   ~ 0
1V4_122
Wire Wire Line
	4900 1150 4900 800 
Text Label 5150 800  1    50   ~ 0
3V3_122
Wire Wire Line
	5150 1000 5150 800 
$Comp
L Device:R R?
U 1 1 61A2F60C
P 8400 1900
AR Path="/61A2F60C" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F60C" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F60C" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F60C" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F60C" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F60C" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F60C" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F60C" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F60C" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F60C" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F60C" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F60C" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F60C" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F60C" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F60C" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F60C" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F60C" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F60C" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F60C" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F60C" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F60C" Ref="R742"  Part="1" 
F 0 "R742" V 8300 1850 50  0000 L CNN
F 1 "165" V 8400 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 1900 50  0001 C CNN
F 3 "~" H 8400 1900 50  0001 C CNN
	1    8400 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F617
P 6950 2250
AR Path="/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F617" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F617" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 6950 2000 50  0001 C CNN
F 1 "GND" H 6955 2077 50  0000 C CNN
F 2 "" H 6950 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 2050 8150 2150
Wire Wire Line
	8400 2050 8400 2150
Wire Wire Line
	8400 2150 8150 2150
Connection ~ 8150 2150
Wire Wire Line
	5900 2150 6450 2150
Wire Wire Line
	7450 850  8000 850 
$Comp
L Device:C C?
U 1 1 61A2F31E
P 6450 1950
AR Path="/61A2F31E" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F31E" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F31E" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F31E" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F31E" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F31E" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F31E" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F31E" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F31E" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F31E" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F31E" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F31E" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F31E" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F31E" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F31E" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F31E" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F31E" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F31E" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F31E" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F31E" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F31E" Ref="C367"  Part="1" 
F 0 "C367" H 6450 2050 50  0000 L CNN
F 1 "2.2uF" H 6450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 1800 50  0001 C CNN
F 3 "~" H 6450 1950 50  0001 C CNN
	1    6450 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F628
P 7550 1950
AR Path="/61A2F628" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F628" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F628" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F628" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F628" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F628" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F628" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F628" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F628" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F628" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F628" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F628" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F628" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F628" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F628" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F628" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F628" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F628" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F628" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F628" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F628" Ref="C368"  Part="1" 
F 0 "C368" H 7550 2050 50  0000 L CNN
F 1 "10uF" H 7550 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 1800 50  0001 C CNN
F 3 "~" H 7550 1950 50  0001 C CNN
	1    7550 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 2150 8150 2150
Connection ~ 8000 2150
Wire Wire Line
	7450 1150 7550 1150
$Comp
L Device:C C?
U 1 1 61A2F639
P 7800 1950
AR Path="/61A2F639" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F639" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F639" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F639" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F639" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F639" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F639" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F639" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F639" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F639" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F639" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F639" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F639" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F639" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F639" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F639" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F639" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F639" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F639" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F639" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F639" Ref="C369"  Part="1" 
F 0 "C369" H 7800 2050 50  0000 L CNN
F 1 "4.7uF" H 7800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 1800 50  0001 C CNN
F 3 "~" H 7800 1950 50  0001 C CNN
	1    7800 1950
	1    0    0    1   
$EndComp
Connection ~ 7550 1150
Wire Wire Line
	7550 2100 7550 2150
Connection ~ 7550 2150
Wire Wire Line
	7550 2150 7800 2150
Wire Wire Line
	7800 2100 7800 2150
Connection ~ 7800 2150
Wire Wire Line
	7800 2150 8000 2150
Wire Wire Line
	6450 2100 6450 2150
Connection ~ 6450 2150
Wire Wire Line
	6450 2150 6950 2150
$Comp
L Device:R R?
U 1 1 61A2F641
P 7550 1550
AR Path="/61A2F641" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F641" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F641" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F641" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F641" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F641" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F641" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F641" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F641" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F641" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F641" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F641" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F641" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F641" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F641" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F641" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F641" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F641" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F641" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F641" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F641" Ref="R732"  Part="1" 
F 0 "R732" V 7450 1500 50  0000 L CNN
F 1 "300m" V 7550 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 1550 50  0001 C CNN
F 3 "~" H 7550 1550 50  0001 C CNN
	1    7550 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6195FD14
P 7800 1550
AR Path="/6195FD14" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FD14" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FD14" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FD14" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FD14" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FD14" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FD14" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FD14" Ref="R?"  Part="1" 
AR Path="/61849340/6195FD14" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FD14" Ref="R?"  Part="1" 
AR Path="/61867626/6195FD14" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FD14" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FD14" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FD14" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FD14" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FD14" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FD14" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FD14" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FD14" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FD14" Ref="R?"  Part="1" 
AR Path="/607F867C/6195FD14" Ref="R733"  Part="1" 
F 0 "R733" V 7700 1500 50  0000 L CNN
F 1 "300m" V 7800 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 1550 50  0001 C CNN
F 3 "~" H 7800 1550 50  0001 C CNN
	1    7800 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 1400 7550 1150
Wire Wire Line
	8400 1750 8400 1000
Wire Wire Line
	8000 2150 8000 850 
Wire Wire Line
	7550 1150 8150 1150
Wire Wire Line
	7550 1700 7550 1800
Wire Wire Line
	7800 1700 7800 1800
Wire Wire Line
	6950 1400 6950 2150
Connection ~ 6950 2150
Wire Wire Line
	6950 2150 7550 2150
Wire Wire Line
	8150 1750 8150 1150
$Comp
L Device:R R?
U 1 1 61A2F419
P 8150 1900
AR Path="/61A2F419" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F419" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F419" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F419" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F419" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F419" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F419" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F419" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F419" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F419" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F419" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F419" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F419" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F419" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F419" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F419" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F419" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F419" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F419" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F419" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F419" Ref="R741"  Part="1" 
F 0 "R741" V 8050 1850 50  0000 L CNN
F 1 "17.4" V 8150 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 1900 50  0001 C CNN
F 3 "~" H 8150 1900 50  0001 C CNN
	1    8150 1900
	1    0    0    1   
$EndComp
Text HLabel 6250 1950 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	6450 1800 6450 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F423
P 6950 950
AR Path="/61A2F423" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F423" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F423" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F423" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F423" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F423" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F423" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F423" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F423" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F423" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F423" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F423" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F423" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F423" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F423" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F423" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F423" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F423" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F423" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F423" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F423" Ref="U123"  Part="1" 
F 0 "U123" H 7050 1250 60  0000 C CNN
F 1 "LINPOL12V" H 6950 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 1000 260 0001 C CNN
F 3 "" V 7000 1000 260 0001 C CNN
	1    6950 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 2250 6950 2150
Wire Wire Line
	6450 1150 6250 1150
Connection ~ 6450 1150
Wire Wire Line
	7800 1400 7800 1000
Wire Wire Line
	7450 1000 7800 1000
Wire Wire Line
	7800 1000 8400 1000
Connection ~ 7800 1000
Text Label 7550 800  1    50   ~ 0
1V4_123
Wire Wire Line
	7550 1150 7550 800 
Text Label 7800 800  1    50   ~ 0
3V3_123
Wire Wire Line
	7800 1000 7800 800 
$Comp
L Device:R R?
U 1 1 61A2F65A
P 11050 1900
AR Path="/61A2F65A" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F65A" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F65A" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F65A" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F65A" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F65A" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F65A" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F65A" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F65A" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F65A" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F65A" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F65A" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F65A" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F65A" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F65A" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F65A" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F65A" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F65A" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F65A" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F65A" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F65A" Ref="R744"  Part="1" 
F 0 "R744" V 10950 1850 50  0000 L CNN
F 1 "165" V 11050 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 1900 50  0001 C CNN
F 3 "~" H 11050 1900 50  0001 C CNN
	1    11050 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F430
P 9600 2250
AR Path="/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F430" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F430" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 9600 2000 50  0001 C CNN
F 1 "GND" H 9605 2077 50  0000 C CNN
F 2 "" H 9600 2250 50  0001 C CNN
F 3 "" H 9600 2250 50  0001 C CNN
	1    9600 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 2050 10800 2150
Wire Wire Line
	11050 2050 11050 2150
Wire Wire Line
	11050 2150 10800 2150
Connection ~ 10800 2150
Wire Wire Line
	8550 2150 9100 2150
Wire Wire Line
	10100 850  10650 850 
$Comp
L Device:C C?
U 1 1 61A2F434
P 9100 1950
AR Path="/61A2F434" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F434" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F434" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F434" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F434" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F434" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F434" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F434" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F434" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F434" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F434" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F434" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F434" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F434" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F434" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F434" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F434" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F434" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F434" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F434" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F434" Ref="C370"  Part="1" 
F 0 "C370" H 9100 2050 50  0000 L CNN
F 1 "2.2uF" H 9100 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 1800 50  0001 C CNN
F 3 "~" H 9100 1950 50  0001 C CNN
	1    9100 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F444
P 10200 1950
AR Path="/61A2F444" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F444" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F444" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F444" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F444" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F444" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F444" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F444" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F444" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F444" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F444" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F444" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F444" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F444" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F444" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F444" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F444" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F444" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F444" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F444" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F444" Ref="C371"  Part="1" 
F 0 "C371" H 10200 2050 50  0000 L CNN
F 1 "10uF" H 10200 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 1800 50  0001 C CNN
F 3 "~" H 10200 1950 50  0001 C CNN
	1    10200 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 2150 10800 2150
Connection ~ 10650 2150
Wire Wire Line
	10100 1150 10200 1150
$Comp
L Device:C C?
U 1 1 61A2F664
P 10450 1950
AR Path="/61A2F664" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F664" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F664" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F664" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F664" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F664" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F664" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F664" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F664" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F664" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F664" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F664" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F664" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F664" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F664" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F664" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F664" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F664" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F664" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F664" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F664" Ref="C372"  Part="1" 
F 0 "C372" H 10450 2050 50  0000 L CNN
F 1 "4.7uF" H 10450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1800 50  0001 C CNN
F 3 "~" H 10450 1950 50  0001 C CNN
	1    10450 1950
	1    0    0    1   
$EndComp
Connection ~ 10200 1150
Wire Wire Line
	10200 2100 10200 2150
Connection ~ 10200 2150
Wire Wire Line
	10200 2150 10450 2150
Wire Wire Line
	10450 2100 10450 2150
Connection ~ 10450 2150
Wire Wire Line
	10450 2150 10650 2150
Wire Wire Line
	9100 2100 9100 2150
Connection ~ 9100 2150
Wire Wire Line
	9100 2150 9600 2150
$Comp
L Device:R R?
U 1 1 61A2F450
P 10200 1550
AR Path="/61A2F450" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F450" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F450" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F450" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F450" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F450" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F450" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F450" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F450" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F450" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F450" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F450" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F450" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F450" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F450" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F450" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F450" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F450" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F450" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F450" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F450" Ref="R735"  Part="1" 
F 0 "R735" V 10100 1500 50  0000 L CNN
F 1 "300m" V 10200 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 1550 50  0001 C CNN
F 3 "~" H 10200 1550 50  0001 C CNN
	1    10200 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F45F
P 10450 1550
AR Path="/61A2F45F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F45F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F45F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F45F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F45F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F45F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F45F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F45F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F45F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F45F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F45F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F45F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F45F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F45F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F45F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F45F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F45F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F45F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F45F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F45F" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F45F" Ref="R736"  Part="1" 
F 0 "R736" V 10350 1500 50  0000 L CNN
F 1 "300m" V 10450 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 1550 50  0001 C CNN
F 3 "~" H 10450 1550 50  0001 C CNN
	1    10450 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 1400 10200 1150
Wire Wire Line
	11050 1750 11050 1000
Wire Wire Line
	10650 2150 10650 850 
Wire Wire Line
	10200 1150 10800 1150
Wire Wire Line
	10200 1700 10200 1800
Wire Wire Line
	10450 1700 10450 1800
Wire Wire Line
	9600 1400 9600 2150
Connection ~ 9600 2150
Wire Wire Line
	9600 2150 10200 2150
Wire Wire Line
	10800 1750 10800 1150
$Comp
L Device:R R?
U 1 1 61A2F66C
P 10800 1900
AR Path="/61A2F66C" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F66C" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F66C" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F66C" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F66C" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F66C" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F66C" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F66C" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F66C" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F66C" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F66C" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F66C" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F66C" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F66C" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F66C" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F66C" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F66C" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F66C" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F66C" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F66C" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F66C" Ref="R743"  Part="1" 
F 0 "R743" V 10700 1850 50  0000 L CNN
F 1 "17.4" V 10800 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 1900 50  0001 C CNN
F 3 "~" H 10800 1900 50  0001 C CNN
	1    10800 1900
	1    0    0    1   
$EndComp
Text HLabel 8900 1950 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	9100 1800 9100 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F680
P 9600 950
AR Path="/61A2F680" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F680" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F680" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F680" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F680" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F680" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F680" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F680" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F680" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F680" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F680" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F680" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F680" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F680" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F680" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F680" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F680" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F680" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F680" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F680" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F680" Ref="U124"  Part="1" 
F 0 "U124" H 9700 1250 60  0000 C CNN
F 1 "LINPOL12V" H 9600 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 1000 260 0001 C CNN
F 3 "" V 9650 1000 260 0001 C CNN
	1    9600 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 2250 9600 2150
Wire Wire Line
	9100 1150 8900 1150
Connection ~ 9100 1150
Wire Wire Line
	10450 1400 10450 1000
Wire Wire Line
	10100 1000 10450 1000
Wire Wire Line
	10450 1000 11050 1000
Connection ~ 10450 1000
Text Label 10200 800  1    50   ~ 0
1V4_124
Wire Wire Line
	10200 1150 10200 800 
Text Label 10450 800  1    50   ~ 0
3V3_124
Wire Wire Line
	10450 1000 10450 800 
$Comp
L Device:R R?
U 1 1 61A2F46B
P 3100 3800
AR Path="/61A2F46B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F46B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F46B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F46B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F46B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F46B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F46B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F46B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F46B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F46B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F46B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F46B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F46B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F46B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F46B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F46B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F46B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F46B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F46B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F46B" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F46B" Ref="R762"  Part="1" 
F 0 "R762" V 3000 3750 50  0000 L CNN
F 1 "165" V 3100 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 3800 50  0001 C CNN
F 3 "~" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F33F
P 1650 4150
AR Path="/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F33F" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F33F" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 1650 3900 50  0001 C CNN
F 1 "GND" H 1655 3977 50  0000 C CNN
F 2 "" H 1650 4150 50  0001 C CNN
F 3 "" H 1650 4150 50  0001 C CNN
	1    1650 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 3950 2850 4050
Wire Wire Line
	3100 3950 3100 4050
Wire Wire Line
	3100 4050 2850 4050
Connection ~ 2850 4050
Wire Wire Line
	600  4050 1150 4050
Wire Wire Line
	2150 2750 2700 2750
$Comp
L Device:C C?
U 1 1 61A2F36E
P 1150 3850
AR Path="/61A2F36E" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F36E" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F36E" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F36E" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F36E" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F36E" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F36E" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F36E" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F36E" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F36E" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F36E" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F36E" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F36E" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F36E" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F36E" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F36E" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F36E" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F36E" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F36E" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F36E" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F36E" Ref="C373"  Part="1" 
F 0 "C373" H 1150 3950 50  0000 L CNN
F 1 "2.2uF" H 1150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 3700 50  0001 C CNN
F 3 "~" H 1150 3850 50  0001 C CNN
	1    1150 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F37C
P 2250 3850
AR Path="/61A2F37C" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F37C" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F37C" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F37C" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F37C" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F37C" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F37C" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F37C" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F37C" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F37C" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F37C" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F37C" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F37C" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F37C" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F37C" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F37C" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F37C" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F37C" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F37C" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F37C" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F37C" Ref="C374"  Part="1" 
F 0 "C374" H 2250 3950 50  0000 L CNN
F 1 "10uF" H 2250 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 3700 50  0001 C CNN
F 3 "~" H 2250 3850 50  0001 C CNN
	1    2250 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 4050 2850 4050
Connection ~ 2700 4050
Wire Wire Line
	2150 3050 2250 3050
$Comp
L Device:C C?
U 1 1 61A2F687
P 2500 3850
AR Path="/61A2F687" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F687" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F687" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F687" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F687" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F687" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F687" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F687" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F687" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F687" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F687" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F687" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F687" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F687" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F687" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F687" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F687" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F687" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F687" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F687" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F687" Ref="C375"  Part="1" 
F 0 "C375" H 2500 3950 50  0000 L CNN
F 1 "4.7uF" H 2500 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 3700 50  0001 C CNN
F 3 "~" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    1   
$EndComp
Connection ~ 2250 3050
Wire Wire Line
	2250 4000 2250 4050
Connection ~ 2250 4050
Wire Wire Line
	2250 4050 2500 4050
Wire Wire Line
	2500 4000 2500 4050
Connection ~ 2500 4050
Wire Wire Line
	2500 4050 2700 4050
Wire Wire Line
	1150 4000 1150 4050
Connection ~ 1150 4050
Wire Wire Line
	1150 4050 1650 4050
$Comp
L Device:R R?
U 1 1 61A2F692
P 2250 3450
AR Path="/61A2F692" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F692" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F692" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F692" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F692" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F692" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F692" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F692" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F692" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F692" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F692" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F692" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F692" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F692" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F692" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F692" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F692" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F692" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F692" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F692" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F692" Ref="R750"  Part="1" 
F 0 "R750" V 2150 3400 50  0000 L CNN
F 1 "300m" V 2250 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F6A5
P 2500 3450
AR Path="/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6A5" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6A5" Ref="R751"  Part="1" 
F 0 "R751" V 2400 3400 50  0000 L CNN
F 1 "300m" V 2500 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 3300 2250 3050
Wire Wire Line
	3100 3650 3100 2900
Wire Wire Line
	2700 4050 2700 2750
Wire Wire Line
	2250 3050 2850 3050
Wire Wire Line
	2250 3600 2250 3700
Wire Wire Line
	2500 3600 2500 3700
Wire Wire Line
	1650 3300 1650 4050
Connection ~ 1650 4050
Wire Wire Line
	1650 4050 2250 4050
Wire Wire Line
	2850 3650 2850 3050
$Comp
L Device:R R?
U 1 1 61A2F6B2
P 2850 3800
AR Path="/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6B2" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6B2" Ref="R761"  Part="1" 
F 0 "R761" V 2750 3750 50  0000 L CNN
F 1 "17.4" V 2850 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 3800 50  0001 C CNN
F 3 "~" H 2850 3800 50  0001 C CNN
	1    2850 3800
	1    0    0    1   
$EndComp
Text HLabel 950  3850 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	1150 3700 1150 3050
$Comp
L linpol:LREG U?
U 1 1 6195FB38
P 1650 2850
AR Path="/6195FB38" Ref="U?"  Part="1" 
AR Path="/606A40CC/6195FB38" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/6195FB38" Ref="U?"  Part="1" 
AR Path="/60A71FE2/6195FB38" Ref="U?"  Part="1" 
AR Path="/60A749DF/6195FB38" Ref="U?"  Part="1" 
AR Path="/60A76681/6195FB38" Ref="U?"  Part="1" 
AR Path="/6182C19D/6195FB38" Ref="U?"  Part="1" 
AR Path="/6183A7C1/6195FB38" Ref="U?"  Part="1" 
AR Path="/61849340/6195FB38" Ref="U?"  Part="1" 
AR Path="/61858A9D/6195FB38" Ref="U?"  Part="1" 
AR Path="/61867626/6195FB38" Ref="U?"  Part="1" 
AR Path="/6187640F/6195FB38" Ref="U?"  Part="1" 
AR Path="/618857F1/6195FB38" Ref="U?"  Part="1" 
AR Path="/61894FBC/6195FB38" Ref="U?"  Part="1" 
AR Path="/618A4CF2/6195FB38" Ref="U?"  Part="1" 
AR Path="/618E2B02/6195FB38" Ref="U?"  Part="1" 
AR Path="/61987B22/6195FB38" Ref="U?"  Part="1" 
AR Path="/607CBABF/6195FB38" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB38" Ref="U?"  Part="1" 
AR Path="/607AA45D/6195FB38" Ref="U?"  Part="1" 
AR Path="/607F867C/6195FB38" Ref="U125"  Part="1" 
F 0 "U125" H 1750 3150 60  0000 C CNN
F 1 "LINPOL12V" H 1650 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 2900 260 0001 C CNN
F 3 "" V 1700 2900 260 0001 C CNN
	1    1650 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 4150 1650 4050
Wire Wire Line
	1150 3050 950  3050
Connection ~ 1150 3050
Wire Wire Line
	2500 3300 2500 2900
Wire Wire Line
	2150 2900 2500 2900
Wire Wire Line
	2500 2900 3100 2900
Connection ~ 2500 2900
Text Label 2250 2700 1    50   ~ 0
1V4_125
Wire Wire Line
	2250 3050 2250 2700
Text Label 2500 2700 1    50   ~ 0
3V3_125
Wire Wire Line
	2500 2900 2500 2700
$Comp
L Device:R R?
U 1 1 61A2F6B3
P 5750 3800
AR Path="/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6B3" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6B3" Ref="R764"  Part="1" 
F 0 "R764" V 5650 3750 50  0000 L CNN
F 1 "165" V 5750 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3800 50  0001 C CNN
F 3 "~" H 5750 3800 50  0001 C CNN
	1    5750 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F6CA
P 4300 4150
AR Path="/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F6CA" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F6CA" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 4300 3900 50  0001 C CNN
F 1 "GND" H 4305 3977 50  0000 C CNN
F 2 "" H 4300 4150 50  0001 C CNN
F 3 "" H 4300 4150 50  0001 C CNN
	1    4300 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 3950 5500 4050
Wire Wire Line
	5750 3950 5750 4050
Wire Wire Line
	5750 4050 5500 4050
Connection ~ 5500 4050
Wire Wire Line
	3250 4050 3800 4050
Wire Wire Line
	4800 2750 5350 2750
$Comp
L Device:C C?
U 1 1 61A2F484
P 3800 3850
AR Path="/61A2F484" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F484" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F484" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F484" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F484" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F484" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F484" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F484" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F484" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F484" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F484" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F484" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F484" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F484" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F484" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F484" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F484" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F484" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F484" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F484" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F484" Ref="C376"  Part="1" 
F 0 "C376" H 3800 3950 50  0000 L CNN
F 1 "2.2uF" H 3800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 3700 50  0001 C CNN
F 3 "~" H 3800 3850 50  0001 C CNN
	1    3800 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F789
P 4900 3850
AR Path="/61A2F789" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F789" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F789" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F789" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F789" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F789" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F789" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F789" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F789" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F789" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F789" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F789" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F789" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F789" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F789" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F789" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F789" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F789" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F789" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F789" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F789" Ref="C377"  Part="1" 
F 0 "C377" H 4900 3950 50  0000 L CNN
F 1 "10uF" H 4900 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 3700 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 4050 5500 4050
Connection ~ 5350 4050
Wire Wire Line
	4800 3050 4900 3050
$Comp
L Device:C C?
U 1 1 61A2F791
P 5150 3850
AR Path="/61A2F791" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F791" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F791" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F791" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F791" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F791" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F791" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F791" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F791" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F791" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F791" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F791" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F791" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F791" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F791" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F791" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F791" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F791" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F791" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F791" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F791" Ref="C378"  Part="1" 
F 0 "C378" H 5150 3950 50  0000 L CNN
F 1 "4.7uF" H 5150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 3700 50  0001 C CNN
F 3 "~" H 5150 3850 50  0001 C CNN
	1    5150 3850
	1    0    0    1   
$EndComp
Connection ~ 4900 3050
Wire Wire Line
	4900 4000 4900 4050
Connection ~ 4900 4050
Wire Wire Line
	4900 4050 5150 4050
Wire Wire Line
	5150 4000 5150 4050
Connection ~ 5150 4050
Wire Wire Line
	5150 4050 5350 4050
Wire Wire Line
	3800 4000 3800 4050
Connection ~ 3800 4050
Wire Wire Line
	3800 4050 4300 4050
$Comp
L Device:R R?
U 1 1 61A2F6CC
P 4900 3450
AR Path="/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6CC" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6CC" Ref="R753"  Part="1" 
F 0 "R753" V 4800 3400 50  0000 L CNN
F 1 "300m" V 4900 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 3450 50  0001 C CNN
F 3 "~" H 4900 3450 50  0001 C CNN
	1    4900 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F6D7
P 5150 3450
AR Path="/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6D7" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6D7" Ref="R754"  Part="1" 
F 0 "R754" V 5050 3400 50  0000 L CNN
F 1 "300m" V 5150 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 3450 50  0001 C CNN
F 3 "~" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 3300 4900 3050
Wire Wire Line
	5750 3650 5750 2900
Wire Wire Line
	5350 4050 5350 2750
Wire Wire Line
	4900 3050 5500 3050
Wire Wire Line
	4900 3600 4900 3700
Wire Wire Line
	5150 3600 5150 3700
Wire Wire Line
	4300 3300 4300 4050
Connection ~ 4300 4050
Wire Wire Line
	4300 4050 4900 4050
Wire Wire Line
	5500 3650 5500 3050
$Comp
L Device:R R?
U 1 1 61A2F6EE
P 5500 3800
AR Path="/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6EE" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F6EE" Ref="R763"  Part="1" 
F 0 "R763" V 5400 3750 50  0000 L CNN
F 1 "17.4" V 5500 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 3800 50  0001 C CNN
F 3 "~" H 5500 3800 50  0001 C CNN
	1    5500 3800
	1    0    0    1   
$EndComp
Text HLabel 3600 3850 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	3800 3700 3800 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F48F
P 4300 2850
AR Path="/61A2F48F" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F48F" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F48F" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F48F" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F48F" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F48F" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F48F" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F48F" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F48F" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F48F" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F48F" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F48F" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F48F" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F48F" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F48F" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F48F" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F48F" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F48F" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F48F" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F48F" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F48F" Ref="U126"  Part="1" 
F 0 "U126" H 4400 3150 60  0000 C CNN
F 1 "LINPOL12V" H 4300 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 2900 260 0001 C CNN
F 3 "" V 4350 2900 260 0001 C CNN
	1    4300 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 4150 4300 4050
Wire Wire Line
	3800 3050 3600 3050
Connection ~ 3800 3050
Wire Wire Line
	5150 3300 5150 2900
Wire Wire Line
	4800 2900 5150 2900
Wire Wire Line
	5150 2900 5750 2900
Connection ~ 5150 2900
Text Label 4900 2700 1    50   ~ 0
1V4_126
Wire Wire Line
	4900 3050 4900 2700
Text Label 5150 2700 1    50   ~ 0
3V3_126
Wire Wire Line
	5150 2900 5150 2700
$Comp
L Device:R R?
U 1 1 6195FDCF
P 8400 3800
AR Path="/6195FDCF" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FDCF" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FDCF" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FDCF" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FDCF" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FDCF" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FDCF" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FDCF" Ref="R?"  Part="1" 
AR Path="/61849340/6195FDCF" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FDCF" Ref="R?"  Part="1" 
AR Path="/61867626/6195FDCF" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FDCF" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FDCF" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FDCF" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FDCF" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FDCF" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FDCF" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FDCF" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FDCF" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FDCF" Ref="R?"  Part="1" 
AR Path="/607F867C/6195FDCF" Ref="R766"  Part="1" 
F 0 "R766" V 8300 3750 50  0000 L CNN
F 1 "165" V 8400 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 3800 50  0001 C CNN
F 3 "~" H 8400 3800 50  0001 C CNN
	1    8400 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F49B
P 6950 4150
AR Path="/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F49B" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F49B" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 6950 3900 50  0001 C CNN
F 1 "GND" H 6955 3977 50  0000 C CNN
F 2 "" H 6950 4150 50  0001 C CNN
F 3 "" H 6950 4150 50  0001 C CNN
	1    6950 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 3950 8150 4050
Wire Wire Line
	8400 3950 8400 4050
Wire Wire Line
	8400 4050 8150 4050
Connection ~ 8150 4050
Wire Wire Line
	5900 4050 6450 4050
Wire Wire Line
	7450 2750 8000 2750
$Comp
L Device:C C?
U 1 1 61A2F4AB
P 6450 3850
AR Path="/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4AB" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F4AB" Ref="C379"  Part="1" 
F 0 "C379" H 6450 3950 50  0000 L CNN
F 1 "2.2uF" H 6450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 3700 50  0001 C CNN
F 3 "~" H 6450 3850 50  0001 C CNN
	1    6450 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F4B4
P 7550 3850
AR Path="/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4B4" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F4B4" Ref="C380"  Part="1" 
F 0 "C380" H 7550 3950 50  0000 L CNN
F 1 "10uF" H 7550 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 3700 50  0001 C CNN
F 3 "~" H 7550 3850 50  0001 C CNN
	1    7550 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 4050 8150 4050
Connection ~ 8000 4050
Wire Wire Line
	7450 3050 7550 3050
$Comp
L Device:C C?
U 1 1 61A2F703
P 7800 3850
AR Path="/61A2F703" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F703" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F703" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F703" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F703" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F703" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F703" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F703" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F703" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F703" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F703" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F703" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F703" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F703" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F703" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F703" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F703" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F703" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F703" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F703" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F703" Ref="C381"  Part="1" 
F 0 "C381" H 7800 3950 50  0000 L CNN
F 1 "4.7uF" H 7800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 3700 50  0001 C CNN
F 3 "~" H 7800 3850 50  0001 C CNN
	1    7800 3850
	1    0    0    1   
$EndComp
Connection ~ 7550 3050
Wire Wire Line
	7550 4000 7550 4050
Connection ~ 7550 4050
Wire Wire Line
	7550 4050 7800 4050
Wire Wire Line
	7800 4000 7800 4050
Connection ~ 7800 4050
Wire Wire Line
	7800 4050 8000 4050
Wire Wire Line
	6450 4000 6450 4050
Connection ~ 6450 4050
Wire Wire Line
	6450 4050 6950 4050
$Comp
L Device:R R?
U 1 1 61A2F710
P 7550 3450
AR Path="/61A2F710" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F710" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F710" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F710" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F710" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F710" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F710" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F710" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F710" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F710" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F710" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F710" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F710" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F710" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F710" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F710" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F710" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F710" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F710" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F710" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F710" Ref="R756"  Part="1" 
F 0 "R756" V 7450 3400 50  0000 L CNN
F 1 "300m" V 7550 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 3450 50  0001 C CNN
F 3 "~" H 7550 3450 50  0001 C CNN
	1    7550 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F716
P 7800 3450
AR Path="/61A2F716" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F716" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F716" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F716" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F716" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F716" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F716" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F716" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F716" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F716" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F716" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F716" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F716" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F716" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F716" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F716" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F716" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F716" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F716" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F716" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F716" Ref="R757"  Part="1" 
F 0 "R757" V 7700 3400 50  0000 L CNN
F 1 "300m" V 7800 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 3450 50  0001 C CNN
F 3 "~" H 7800 3450 50  0001 C CNN
	1    7800 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 3300 7550 3050
Wire Wire Line
	8400 3650 8400 2900
Wire Wire Line
	8000 4050 8000 2750
Wire Wire Line
	7550 3050 8150 3050
Wire Wire Line
	7550 3600 7550 3700
Wire Wire Line
	7800 3600 7800 3700
Wire Wire Line
	6950 3300 6950 4050
Connection ~ 6950 4050
Wire Wire Line
	6950 4050 7550 4050
Wire Wire Line
	8150 3650 8150 3050
$Comp
L Device:R R?
U 1 1 61A2F4BE
P 8150 3800
AR Path="/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F4BE" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F4BE" Ref="R765"  Part="1" 
F 0 "R765" V 8050 3750 50  0000 L CNN
F 1 "17.4" V 8150 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 3800 50  0001 C CNN
F 3 "~" H 8150 3800 50  0001 C CNN
	1    8150 3800
	1    0    0    1   
$EndComp
Text HLabel 6250 3850 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	6450 3700 6450 3050
$Comp
L linpol:LREG U?
U 1 1 6195FB8D
P 6950 2850
AR Path="/6195FB8D" Ref="U?"  Part="1" 
AR Path="/606A40CC/6195FB8D" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/6195FB8D" Ref="U?"  Part="1" 
AR Path="/60A71FE2/6195FB8D" Ref="U?"  Part="1" 
AR Path="/60A749DF/6195FB8D" Ref="U?"  Part="1" 
AR Path="/60A76681/6195FB8D" Ref="U?"  Part="1" 
AR Path="/6182C19D/6195FB8D" Ref="U?"  Part="1" 
AR Path="/6183A7C1/6195FB8D" Ref="U?"  Part="1" 
AR Path="/61849340/6195FB8D" Ref="U?"  Part="1" 
AR Path="/61858A9D/6195FB8D" Ref="U?"  Part="1" 
AR Path="/61867626/6195FB8D" Ref="U?"  Part="1" 
AR Path="/6187640F/6195FB8D" Ref="U?"  Part="1" 
AR Path="/618857F1/6195FB8D" Ref="U?"  Part="1" 
AR Path="/61894FBC/6195FB8D" Ref="U?"  Part="1" 
AR Path="/618A4CF2/6195FB8D" Ref="U?"  Part="1" 
AR Path="/618E2B02/6195FB8D" Ref="U?"  Part="1" 
AR Path="/61987B22/6195FB8D" Ref="U?"  Part="1" 
AR Path="/607CBABF/6195FB8D" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB8D" Ref="U?"  Part="1" 
AR Path="/607AA45D/6195FB8D" Ref="U?"  Part="1" 
AR Path="/607F867C/6195FB8D" Ref="U127"  Part="1" 
F 0 "U127" H 7050 3150 60  0000 C CNN
F 1 "LINPOL12V" H 6950 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 2900 260 0001 C CNN
F 3 "" V 7000 2900 260 0001 C CNN
	1    6950 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 4150 6950 4050
Wire Wire Line
	6450 3050 6250 3050
Connection ~ 6450 3050
Wire Wire Line
	7800 3300 7800 2900
Wire Wire Line
	7450 2900 7800 2900
Wire Wire Line
	7800 2900 8400 2900
Connection ~ 7800 2900
Text Label 7550 2700 1    50   ~ 0
1V4_127
Wire Wire Line
	7550 3050 7550 2700
Text Label 7800 2700 1    50   ~ 0
3V3_127
Wire Wire Line
	7800 2900 7800 2700
$Comp
L Device:R R?
U 1 1 6195FDFB
P 11050 3800
AR Path="/6195FDFB" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FDFB" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FDFB" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FDFB" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FDFB" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FDFB" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FDFB" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FDFB" Ref="R?"  Part="1" 
AR Path="/61849340/6195FDFB" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FDFB" Ref="R?"  Part="1" 
AR Path="/61867626/6195FDFB" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FDFB" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FDFB" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FDFB" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FDFB" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FDFB" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FDFB" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FDFB" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FDFB" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FDFB" Ref="R?"  Part="1" 
AR Path="/607F867C/6195FDFB" Ref="R768"  Part="1" 
F 0 "R768" V 10950 3750 50  0000 L CNN
F 1 "165" V 11050 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 3800 50  0001 C CNN
F 3 "~" H 11050 3800 50  0001 C CNN
	1    11050 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F4D7
P 9600 4150
AR Path="/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F4D7" Ref="#PWR?"  Part="1" 
AR Path="/607F867C/61A2F4D7" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 9600 3900 50  0001 C CNN
F 1 "GND" H 9605 3977 50  0000 C CNN
F 2 "" H 9600 4150 50  0001 C CNN
F 3 "" H 9600 4150 50  0001 C CNN
	1    9600 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 3950 10800 4050
Wire Wire Line
	11050 3950 11050 4050
Wire Wire Line
	11050 4050 10800 4050
Connection ~ 10800 4050
Wire Wire Line
	8550 4050 9100 4050
Wire Wire Line
	10100 2750 10650 2750
$Comp
L Device:C C?
U 1 1 61A2F4DF
P 9100 3850
AR Path="/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4DF" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F4DF" Ref="C382"  Part="1" 
F 0 "C382" H 9100 3950 50  0000 L CNN
F 1 "2.2uF" H 9100 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 3700 50  0001 C CNN
F 3 "~" H 9100 3850 50  0001 C CNN
	1    9100 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F4EB
P 10200 3850
AR Path="/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4EB" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F4EB" Ref="C383"  Part="1" 
F 0 "C383" H 10200 3950 50  0000 L CNN
F 1 "10uF" H 10200 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 3700 50  0001 C CNN
F 3 "~" H 10200 3850 50  0001 C CNN
	1    10200 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 4050 10800 4050
Connection ~ 10650 4050
Wire Wire Line
	10100 3050 10200 3050
$Comp
L Device:C C?
U 1 1 61A2F4F8
P 10450 3850
AR Path="/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4F8" Ref="C?"  Part="1" 
AR Path="/607F867C/61A2F4F8" Ref="C384"  Part="1" 
F 0 "C384" H 10450 3950 50  0000 L CNN
F 1 "4.7uF" H 10450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 3700 50  0001 C CNN
F 3 "~" H 10450 3850 50  0001 C CNN
	1    10450 3850
	1    0    0    1   
$EndComp
Connection ~ 10200 3050
Wire Wire Line
	10200 4000 10200 4050
Connection ~ 10200 4050
Wire Wire Line
	10200 4050 10450 4050
Wire Wire Line
	10450 4000 10450 4050
Connection ~ 10450 4050
Wire Wire Line
	10450 4050 10650 4050
Wire Wire Line
	9100 4000 9100 4050
Connection ~ 9100 4050
Wire Wire Line
	9100 4050 9600 4050
$Comp
L Device:R R?
U 1 1 61A2F731
P 10200 3450
AR Path="/61A2F731" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F731" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F731" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F731" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F731" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F731" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F731" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F731" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F731" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F731" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F731" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F731" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F731" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F731" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F731" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F731" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F731" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F731" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F731" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F731" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F731" Ref="R759"  Part="1" 
F 0 "R759" V 10100 3400 50  0000 L CNN
F 1 "300m" V 10200 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 3450 50  0001 C CNN
F 3 "~" H 10200 3450 50  0001 C CNN
	1    10200 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F367
P 10450 3450
AR Path="/61A2F367" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F367" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F367" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F367" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F367" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F367" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F367" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F367" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F367" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F367" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F367" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F367" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F367" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F367" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F367" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F367" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F367" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F367" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F367" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F367" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F367" Ref="R760"  Part="1" 
F 0 "R760" V 10350 3400 50  0000 L CNN
F 1 "300m" V 10450 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 3450 50  0001 C CNN
F 3 "~" H 10450 3450 50  0001 C CNN
	1    10450 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 3300 10200 3050
Wire Wire Line
	11050 3650 11050 2900
Wire Wire Line
	10650 4050 10650 2750
Wire Wire Line
	10200 3050 10800 3050
Wire Wire Line
	10200 3600 10200 3700
Wire Wire Line
	10450 3600 10450 3700
Wire Wire Line
	9600 3300 9600 4050
Connection ~ 9600 4050
Wire Wire Line
	9600 4050 10200 4050
Wire Wire Line
	10800 3650 10800 3050
$Comp
L Device:R R?
U 1 1 61A2F504
P 10800 3800
AR Path="/61A2F504" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F504" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F504" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F504" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F504" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F504" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F504" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F504" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F504" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F504" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F504" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F504" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F504" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F504" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F504" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F504" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F504" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F504" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F504" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F504" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F504" Ref="R767"  Part="1" 
F 0 "R767" V 10700 3750 50  0000 L CNN
F 1 "17.4" V 10800 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 3800 50  0001 C CNN
F 3 "~" H 10800 3800 50  0001 C CNN
	1    10800 3800
	1    0    0    1   
$EndComp
Text HLabel 8900 3850 0    50   Input ~ 0
VIN_g13
Wire Wire Line
	9100 3700 9100 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F50E
P 9600 2850
AR Path="/61A2F50E" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F50E" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F50E" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F50E" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F50E" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F50E" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F50E" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F50E" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F50E" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F50E" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F50E" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F50E" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F50E" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F50E" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F50E" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F50E" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F50E" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F50E" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F50E" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F50E" Ref="U?"  Part="1" 
AR Path="/607F867C/61A2F50E" Ref="U128"  Part="1" 
F 0 "U128" H 9700 3150 60  0000 C CNN
F 1 "LINPOL12V" H 9600 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 2900 260 0001 C CNN
F 3 "" V 9650 2900 260 0001 C CNN
	1    9600 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 4150 9600 4050
Wire Wire Line
	9100 3050 8900 3050
Connection ~ 9100 3050
Wire Wire Line
	10450 3300 10450 2900
Wire Wire Line
	10100 2900 10450 2900
Wire Wire Line
	10450 2900 11050 2900
Connection ~ 10450 2900
Text Label 10200 2700 1    50   ~ 0
1V4_128
Wire Wire Line
	10200 3050 10200 2700
Text Label 10450 2700 1    50   ~ 0
3V3_128
Wire Wire Line
	10450 2900 10450 2700
Text Label 950  1650 2    50   ~ 0
J1_0
Text Label 950  1500 2    50   ~ 0
J1_1
Wire Wire Line
	950  1150 950  1500
Wire Wire Line
	950  1650 950  1950
Text Label 3600 1650 2    50   ~ 0
J2_0
Text Label 3600 1500 2    50   ~ 0
J2_1
Wire Wire Line
	3600 1150 3600 1500
Wire Wire Line
	3600 1650 3600 1950
Text Label 6250 1650 2    50   ~ 0
J3_0
Text Label 6250 1500 2    50   ~ 0
J3_1
Wire Wire Line
	6250 1150 6250 1500
Wire Wire Line
	6250 1650 6250 1950
Text Label 8900 1650 2    50   ~ 0
J4_0
Text Label 8900 1500 2    50   ~ 0
J4_1
Wire Wire Line
	8900 1150 8900 1500
Wire Wire Line
	8900 1650 8900 1950
Text Label 950  3550 2    50   ~ 0
J5_0
Text Label 950  3400 2    50   ~ 0
J5_1
Wire Wire Line
	950  3050 950  3400
Wire Wire Line
	950  3550 950  3850
Text Label 3600 3550 2    50   ~ 0
J6_0
Text Label 3600 3400 2    50   ~ 0
J6_1
Wire Wire Line
	3600 3050 3600 3400
Wire Wire Line
	3600 3550 3600 3850
Text Label 6250 3550 2    50   ~ 0
J7_0
Text Label 6250 3400 2    50   ~ 0
J7_1
Wire Wire Line
	6250 3050 6250 3400
Wire Wire Line
	6250 3550 6250 3850
Text Label 8900 3550 2    50   ~ 0
J8_0
Text Label 8900 3400 2    50   ~ 0
J8_1
Wire Wire Line
	8900 3050 8900 3400
Wire Wire Line
	8900 3550 8900 3850
Text Label 9000 5150 2    50   ~ 0
J1_0
Text Label 9000 5250 2    50   ~ 0
J2_0
Text Label 9000 5350 2    50   ~ 0
J3_0
Text Label 9000 5450 2    50   ~ 0
J4_0
Text Label 9000 5550 2    50   ~ 0
J5_0
Text Label 9000 5650 2    50   ~ 0
J6_0
Text Label 9000 5750 2    50   ~ 0
J7_0
Text Label 9000 5850 2    50   ~ 0
J8_0
Wire Wire Line
	9000 5150 9100 5150
Wire Wire Line
	9000 5250 9100 5250
Wire Wire Line
	9000 5350 9100 5350
Wire Wire Line
	9000 5450 9100 5450
Wire Wire Line
	9000 5550 9100 5550
Wire Wire Line
	9000 5650 9100 5650
Wire Wire Line
	9000 5750 9100 5750
Wire Wire Line
	9000 5850 9100 5850
Text Label 9700 5150 0    50   ~ 0
J1_1
Text Label 9700 5250 0    50   ~ 0
J2_1
Text Label 9700 5350 0    50   ~ 0
J3_1
Text Label 9700 5450 0    50   ~ 0
J4_1
Text Label 9700 5550 0    50   ~ 0
J5_1
Text Label 9700 5650 0    50   ~ 0
J6_1
Text Label 9700 5750 0    50   ~ 0
J7_1
Text Label 9700 5850 0    50   ~ 0
J8_1
Wire Wire Line
	9700 5150 9600 5150
Wire Wire Line
	9700 5250 9600 5250
Wire Wire Line
	9700 5350 9600 5350
Wire Wire Line
	9700 5450 9600 5450
Wire Wire Line
	9700 5550 9600 5550
Wire Wire Line
	9700 5650 9600 5650
Wire Wire Line
	9700 5750 9600 5750
Wire Wire Line
	9700 5850 9600 5850
Entry Wire Line
	7000 4900 7100 5000
Entry Wire Line
	7000 5000 7100 5100
Entry Wire Line
	7000 5100 7100 5200
Entry Wire Line
	7000 5200 7100 5300
Entry Wire Line
	7000 5300 7100 5400
Entry Wire Line
	7000 5400 7100 5500
Entry Wire Line
	7000 5500 7100 5600
Entry Wire Line
	7000 5600 7100 5700
Text HLabel 7000 6050 0    50   Output ~ 0
1V4_[121..128]
Wire Bus Line
	7100 6050 7000 6050
Text Label 6850 4900 2    50   ~ 0
1V4_121
Wire Wire Line
	7000 4900 6850 4900
Text Label 6850 5000 2    50   ~ 0
1V4_122
Wire Wire Line
	7000 5000 6850 5000
Text Label 6850 5100 2    50   ~ 0
1V4_123
Wire Wire Line
	7000 5100 6850 5100
Text Label 6850 5200 2    50   ~ 0
1V4_124
Wire Wire Line
	7000 5200 6850 5200
Text Label 6850 5300 2    50   ~ 0
1V4_125
Wire Wire Line
	7000 5300 6850 5300
Text Label 6850 5400 2    50   ~ 0
1V4_126
Wire Wire Line
	7000 5400 6850 5400
Text Label 6850 5500 2    50   ~ 0
1V4_127
Wire Wire Line
	7000 5500 6850 5500
Text Label 6850 5600 2    50   ~ 0
1V4_128
Wire Wire Line
	7000 5600 6850 5600
Entry Wire Line
	8000 4900 8100 5000
Entry Wire Line
	8000 5000 8100 5100
Entry Wire Line
	8000 5100 8100 5200
Entry Wire Line
	8000 5200 8100 5300
Entry Wire Line
	8000 5300 8100 5400
Entry Wire Line
	8000 5400 8100 5500
Entry Wire Line
	8000 5500 8100 5600
Text HLabel 8000 6050 0    50   Output ~ 0
3V3_[121..128]
Wire Bus Line
	8100 6050 8000 6050
Text Label 7850 4900 2    50   ~ 0
3V3_121
Wire Wire Line
	8000 4900 7850 4900
Text Label 7850 5000 2    50   ~ 0
3V3_122
Wire Wire Line
	8000 5000 7850 5000
Text Label 7850 5100 2    50   ~ 0
3V3_123
Wire Wire Line
	8000 5100 7850 5100
Text Label 7850 5200 2    50   ~ 0
3V3_124
Wire Wire Line
	8000 5200 7850 5200
Text Label 7850 5300 2    50   ~ 0
3V3_125
Wire Wire Line
	8000 5300 7850 5300
Text Label 7850 5400 2    50   ~ 0
3V3_126
Wire Wire Line
	8000 5400 7850 5400
Text Label 7850 5500 2    50   ~ 0
3V3_127
Wire Wire Line
	8000 5500 7850 5500
Text Label 7850 5600 2    50   ~ 0
3V3_128
Wire Wire Line
	8000 5600 7850 5600
$Comp
L Device:R R?
U 1 1 61A2F7A0
P 600 1550
AR Path="/606A40CC/61A2F7A0" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7A0" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7A0" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7A0" Ref="R725"  Part="1" 
F 0 "R725" V 700 1500 50  0000 L CNN
F 1 "100k" V 600 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 1550 50  0001 C CNN
F 3 "~" H 600 1550 50  0001 C CNN
	1    600  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  1700 600  2150
Wire Wire Line
	600  1400 600  1000
Text Label 750  600  0    50   ~ 0
LREGEN1
$Comp
L Device:R R?
U 1 1 61A2F7C3
P 3250 1550
AR Path="/606A40CC/61A2F7C3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7C3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7C3" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7C3" Ref="R728"  Part="1" 
F 0 "R728" V 3350 1500 50  0000 L CNN
F 1 "100k" V 3250 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 1550 50  0001 C CNN
F 3 "~" H 3250 1550 50  0001 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1700 3250 2150
$Comp
L Device:R R?
U 1 1 61A2F7D5
P 5900 1550
AR Path="/606A40CC/61A2F7D5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7D5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7D5" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7D5" Ref="R731"  Part="1" 
F 0 "R731" V 6000 1500 50  0000 L CNN
F 1 "100k" V 5900 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 1550 50  0001 C CNN
F 3 "~" H 5900 1550 50  0001 C CNN
	1    5900 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1700 5900 2150
$Comp
L Device:R R?
U 1 1 61A2F7E3
P 8550 1550
AR Path="/606A40CC/61A2F7E3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7E3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7E3" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7E3" Ref="R734"  Part="1" 
F 0 "R734" V 8650 1500 50  0000 L CNN
F 1 "100k" V 8550 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 1550 50  0001 C CNN
F 3 "~" H 8550 1550 50  0001 C CNN
	1    8550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1700 8550 2150
$Comp
L Device:R R?
U 1 1 61A2F7E9
P 8550 3450
AR Path="/606A40CC/61A2F7E9" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7E9" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7E9" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7E9" Ref="R758"  Part="1" 
F 0 "R758" V 8650 3400 50  0000 L CNN
F 1 "100k" V 8550 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 3450 50  0001 C CNN
F 3 "~" H 8550 3450 50  0001 C CNN
	1    8550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3600 8550 4050
$Comp
L Device:R R?
U 1 1 61A2F7F2
P 5900 3450
AR Path="/606A40CC/61A2F7F2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7F2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7F2" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F7F2" Ref="R755"  Part="1" 
F 0 "R755" V 6000 3400 50  0000 L CNN
F 1 "100k" V 5900 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 3450 50  0001 C CNN
F 3 "~" H 5900 3450 50  0001 C CNN
	1    5900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3600 5900 4050
$Comp
L Device:R R?
U 1 1 61A2F801
P 3250 3450
AR Path="/606A40CC/61A2F801" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F801" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F801" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F801" Ref="R752"  Part="1" 
F 0 "R752" V 3350 3400 50  0000 L CNN
F 1 "100k" V 3250 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 3450 50  0001 C CNN
F 3 "~" H 3250 3450 50  0001 C CNN
	1    3250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3600 3250 4050
$Comp
L Device:R R?
U 1 1 61A2F810
P 600 3450
AR Path="/606A40CC/61A2F810" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F810" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F810" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F810" Ref="R749"  Part="1" 
F 0 "R749" V 700 3400 50  0000 L CNN
F 1 "100k" V 600 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 3450 50  0001 C CNN
F 3 "~" H 600 3450 50  0001 C CNN
	1    600  3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  3600 600  4050
$Comp
L Device:R R?
U 1 1 61A2F332
P 600 800
AR Path="/606A40CC/61A2F332" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F332" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F332" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F332" Ref="R721"  Part="1" 
F 0 "R721" V 700 750 50  0000 L CNN
F 1 "0" V 600 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 800 50  0001 C CNN
F 3 "~" H 600 800 50  0001 C CNN
	1    600  800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  950  600  1000
Connection ~ 600  1000
Wire Wire Line
	600  650  600  600 
Wire Wire Line
	600  600  750  600 
Wire Wire Line
	1150 850  850  850 
Wire Wire Line
	850  850  850  1000
Connection ~ 850  1000
Wire Wire Line
	850  1000 600  1000
Wire Wire Line
	3800 1000 3500 1000
Wire Wire Line
	3250 1400 3250 1000
Text Label 3400 600  0    50   ~ 0
LREGEN2
$Comp
L Device:R R?
U 1 1 61A2F382
P 3250 800
AR Path="/606A40CC/61A2F382" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F382" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F382" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F382" Ref="R722"  Part="1" 
F 0 "R722" V 3350 750 50  0000 L CNN
F 1 "0" V 3250 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 800 50  0001 C CNN
F 3 "~" H 3250 800 50  0001 C CNN
	1    3250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 950  3250 1000
Connection ~ 3250 1000
Wire Wire Line
	3250 650  3250 600 
Wire Wire Line
	3250 600  3400 600 
Wire Wire Line
	3800 850  3500 850 
Wire Wire Line
	3500 850  3500 1000
Connection ~ 3500 1000
Wire Wire Line
	3500 1000 3250 1000
Wire Wire Line
	6450 1000 6150 1000
Wire Wire Line
	5900 1400 5900 1000
Text Label 6050 600  0    50   ~ 0
LREGEN3
$Comp
L Device:R R?
U 1 1 61A2F38F
P 5900 800
AR Path="/606A40CC/61A2F38F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F38F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F38F" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F38F" Ref="R723"  Part="1" 
F 0 "R723" V 6000 750 50  0000 L CNN
F 1 "0" V 5900 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 800 50  0001 C CNN
F 3 "~" H 5900 800 50  0001 C CNN
	1    5900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 950  5900 1000
Connection ~ 5900 1000
Wire Wire Line
	5900 650  5900 600 
Wire Wire Line
	5900 600  6050 600 
Wire Wire Line
	6450 850  6150 850 
Wire Wire Line
	6150 850  6150 1000
Connection ~ 6150 1000
Wire Wire Line
	6150 1000 5900 1000
Wire Wire Line
	9100 1000 8800 1000
Wire Wire Line
	8550 1400 8550 1000
Text Label 8700 600  0    50   ~ 0
LREGEN4
$Comp
L Device:R R?
U 1 1 61A2F39D
P 8550 800
AR Path="/606A40CC/61A2F39D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F39D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F39D" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F39D" Ref="R724"  Part="1" 
F 0 "R724" V 8650 750 50  0000 L CNN
F 1 "0" V 8550 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 800 50  0001 C CNN
F 3 "~" H 8550 800 50  0001 C CNN
	1    8550 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 950  8550 1000
Connection ~ 8550 1000
Wire Wire Line
	8550 650  8550 600 
Wire Wire Line
	8550 600  8700 600 
Wire Wire Line
	9100 850  8800 850 
Wire Wire Line
	8800 850  8800 1000
Connection ~ 8800 1000
Wire Wire Line
	8800 1000 8550 1000
Wire Wire Line
	1150 2900 850  2900
Wire Wire Line
	600  3300 600  2900
Text Label 750  2500 0    50   ~ 0
LREGEN7
$Comp
L Device:R R?
U 1 1 61A2F3AD
P 600 2700
AR Path="/606A40CC/61A2F3AD" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3AD" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3AD" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F3AD" Ref="R745"  Part="1" 
F 0 "R745" V 700 2650 50  0000 L CNN
F 1 "0" V 600 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 2700 50  0001 C CNN
F 3 "~" H 600 2700 50  0001 C CNN
	1    600  2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  2850 600  2900
Connection ~ 600  2900
Wire Wire Line
	600  2550 600  2500
Wire Wire Line
	600  2500 750  2500
Wire Wire Line
	1150 2750 850  2750
Wire Wire Line
	850  2750 850  2900
Connection ~ 850  2900
Wire Wire Line
	850  2900 600  2900
Wire Wire Line
	3800 2900 3500 2900
Wire Wire Line
	3250 3300 3250 2900
Text Label 3400 2500 0    50   ~ 0
LREGEN8
$Comp
L Device:R R?
U 1 1 61A2F3BC
P 3250 2700
AR Path="/606A40CC/61A2F3BC" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3BC" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3BC" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F3BC" Ref="R746"  Part="1" 
F 0 "R746" V 3350 2650 50  0000 L CNN
F 1 "0" V 3250 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 2700 50  0001 C CNN
F 3 "~" H 3250 2700 50  0001 C CNN
	1    3250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2850 3250 2900
Connection ~ 3250 2900
Wire Wire Line
	3250 2550 3250 2500
Wire Wire Line
	3250 2500 3400 2500
Wire Wire Line
	3800 2750 3500 2750
Wire Wire Line
	3500 2750 3500 2900
Connection ~ 3500 2900
Wire Wire Line
	3500 2900 3250 2900
Wire Wire Line
	6450 2900 6150 2900
Wire Wire Line
	5900 3300 5900 2900
Text Label 6050 2500 0    50   ~ 0
LREGEN9
$Comp
L Device:R R?
U 1 1 61A2F3C3
P 5900 2700
AR Path="/606A40CC/61A2F3C3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3C3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3C3" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F3C3" Ref="R747"  Part="1" 
F 0 "R747" V 6000 2650 50  0000 L CNN
F 1 "0" V 5900 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 2700 50  0001 C CNN
F 3 "~" H 5900 2700 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2850 5900 2900
Connection ~ 5900 2900
Wire Wire Line
	5900 2550 5900 2500
Wire Wire Line
	5900 2500 6050 2500
Wire Wire Line
	6450 2750 6150 2750
Wire Wire Line
	6150 2750 6150 2900
Connection ~ 6150 2900
Wire Wire Line
	6150 2900 5900 2900
Wire Wire Line
	9100 2900 8800 2900
Wire Wire Line
	8550 3300 8550 2900
Text Label 8700 2500 0    50   ~ 0
LREGEN10
$Comp
L Device:R R?
U 1 1 61A2F3CE
P 8550 2700
AR Path="/606A40CC/61A2F3CE" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3CE" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3CE" Ref="R?"  Part="1" 
AR Path="/607F867C/61A2F3CE" Ref="R748"  Part="1" 
F 0 "R748" V 8650 2650 50  0000 L CNN
F 1 "0" V 8550 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2850 8550 2900
Connection ~ 8550 2900
Wire Wire Line
	8550 2550 8550 2500
Wire Wire Line
	8550 2500 8700 2500
Wire Wire Line
	9100 2750 8800 2750
Wire Wire Line
	8800 2750 8800 2900
Connection ~ 8800 2900
Wire Wire Line
	8800 2900 8550 2900
Entry Wire Line
	8000 5600 8100 5700
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61AB6DFD
P 9300 5150
AR Path="/607AA45D/61AB6DFD" Ref="J?"  Part="1" 
AR Path="/607F867C/61AB6DFD" Ref="J61"  Part="1" 
F 0 "J61" H 9350 5367 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5150 50  0001 C CNN
F 3 "~" H 9300 5150 50  0001 C CNN
	1    9300 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61AB6E03
P 9300 5350
AR Path="/607AA45D/61AB6E03" Ref="J?"  Part="1" 
AR Path="/607F867C/61AB6E03" Ref="J62"  Part="1" 
F 0 "J62" H 9350 5567 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5350 50  0001 C CNN
F 3 "~" H 9300 5350 50  0001 C CNN
	1    9300 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61AB6E09
P 9300 5550
AR Path="/607AA45D/61AB6E09" Ref="J?"  Part="1" 
AR Path="/607F867C/61AB6E09" Ref="J63"  Part="1" 
F 0 "J63" H 9350 5767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5550 50  0001 C CNN
F 3 "~" H 9300 5550 50  0001 C CNN
	1    9300 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61AB6E0F
P 9300 5750
AR Path="/607AA45D/61AB6E0F" Ref="J?"  Part="1" 
AR Path="/607F867C/61AB6E0F" Ref="J64"  Part="1" 
F 0 "J64" H 9350 5967 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5750 50  0001 C CNN
F 3 "~" H 9300 5750 50  0001 C CNN
	1    9300 5750
	1    0    0    -1  
$EndComp
Text HLabel 10900 6100 0    50   Input ~ 0
LREGEN[1..10]
Entry Wire Line
	10900 5000 11000 5100
Entry Wire Line
	10900 5100 11000 5200
Entry Wire Line
	10900 5200 11000 5300
Entry Wire Line
	10900 5300 11000 5400
Entry Wire Line
	10900 5400 11000 5500
Entry Wire Line
	10900 5500 11000 5600
Entry Wire Line
	10900 5600 11000 5700
Entry Wire Line
	10900 5700 11000 5800
Entry Wire Line
	10900 5800 11000 5900
Entry Wire Line
	10900 5900 11000 6000
Wire Bus Line
	10900 6100 11000 6100
Text Label 10800 5000 2    50   ~ 0
LREGEN1
Wire Wire Line
	10800 5000 10900 5000
Text Label 10800 5100 2    50   ~ 0
LREGEN2
Wire Wire Line
	10800 5100 10900 5100
Text Label 10800 5200 2    50   ~ 0
LREGEN3
Wire Wire Line
	10800 5200 10900 5200
Text Label 10800 5300 2    50   ~ 0
LREGEN4
Wire Wire Line
	10800 5300 10900 5300
Text Label 10800 5400 2    50   ~ 0
LREGEN5
Wire Wire Line
	10800 5400 10900 5400
Text Label 10800 5500 2    50   ~ 0
LREGEN6
Wire Wire Line
	10800 5500 10900 5500
Text Label 10800 5600 2    50   ~ 0
LREGEN7
Wire Wire Line
	10800 5600 10900 5600
Text Label 10800 5700 2    50   ~ 0
LREGEN8
Wire Wire Line
	10800 5700 10900 5700
Text Label 10800 5800 2    50   ~ 0
LREGEN9
Wire Wire Line
	10800 5800 10900 5800
Text Label 10800 5900 2    50   ~ 0
LREGEN10
Wire Wire Line
	10800 5900 10900 5900
Wire Bus Line
	8100 4900 8100 6050
Wire Bus Line
	7100 4900 7100 6050
Wire Bus Line
	11000 4950 11000 6100
$EndSCHEMATC
