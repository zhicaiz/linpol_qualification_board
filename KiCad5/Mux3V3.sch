EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 20
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1150 7300 0    50   Input ~ 0
3V3_[1..128]
Text Label 1000 750  2    50   ~ 0
3V3_1
Entry Wire Line
	1100 750  1200 850 
Entry Wire Line
	1100 800  1200 900 
Text Label 1000 800  2    50   ~ 0
3V3_2
Entry Wire Line
	1100 850  1200 950 
Entry Wire Line
	1100 900  1200 1000
Entry Wire Line
	1100 950  1200 1050
Entry Wire Line
	1100 1000 1200 1100
Entry Wire Line
	1100 1050 1200 1150
Entry Wire Line
	1100 1100 1200 1200
Entry Wire Line
	1100 1150 1200 1250
Entry Wire Line
	1100 1200 1200 1300
Entry Wire Line
	1100 1250 1200 1350
Entry Wire Line
	1100 1300 1200 1400
Entry Wire Line
	1100 1350 1200 1450
Entry Wire Line
	1100 1400 1200 1500
Entry Wire Line
	1100 1450 1200 1550
Entry Wire Line
	1100 1500 1200 1600
Entry Wire Line
	1100 1550 1200 1650
Entry Wire Line
	1100 1600 1200 1700
Entry Wire Line
	1100 1650 1200 1750
Entry Wire Line
	1100 1700 1200 1800
Entry Wire Line
	1100 1750 1200 1850
Entry Wire Line
	1100 1800 1200 1900
Entry Wire Line
	1100 1850 1200 1950
Entry Wire Line
	1100 1900 1200 2000
Entry Wire Line
	1100 1950 1200 2050
Entry Wire Line
	1100 2000 1200 2100
Entry Wire Line
	1100 2050 1200 2150
Entry Wire Line
	1100 2100 1200 2200
Entry Wire Line
	1100 2150 1200 2250
Entry Wire Line
	1100 2200 1200 2300
Entry Wire Line
	1100 2250 1200 2350
Entry Wire Line
	1100 2300 1200 2400
Entry Wire Line
	1100 2350 1200 2450
Entry Wire Line
	1100 2400 1200 2500
Entry Wire Line
	1100 2450 1200 2550
Entry Wire Line
	1100 2500 1200 2600
Entry Wire Line
	1100 2550 1200 2650
Entry Wire Line
	1100 2600 1200 2700
Entry Wire Line
	1100 2650 1200 2750
Entry Wire Line
	1100 2700 1200 2800
Entry Wire Line
	1100 2750 1200 2850
Entry Wire Line
	1100 2800 1200 2900
Entry Wire Line
	1100 2850 1200 2950
Entry Wire Line
	1100 2900 1200 3000
Entry Wire Line
	1100 2950 1200 3050
Entry Wire Line
	1100 3000 1200 3100
Entry Wire Line
	1100 3050 1200 3150
Entry Wire Line
	1100 3100 1200 3200
Entry Wire Line
	1100 3150 1200 3250
Entry Wire Line
	1100 3200 1200 3300
Entry Wire Line
	1100 3250 1200 3350
Entry Wire Line
	1100 3300 1200 3400
Entry Wire Line
	1100 3350 1200 3450
Entry Wire Line
	1100 3400 1200 3500
Entry Wire Line
	1100 3450 1200 3550
Entry Wire Line
	1100 3500 1200 3600
Entry Wire Line
	1100 3550 1200 3650
Entry Wire Line
	1100 3600 1200 3700
Entry Wire Line
	1100 3650 1200 3750
Entry Wire Line
	1100 3700 1200 3800
Text Label 1000 850  2    50   ~ 0
3V3_3
Text Label 1000 900  2    50   ~ 0
3V3_4
Text Label 1000 950  2    50   ~ 0
3V3_5
Text Label 1000 1000 2    50   ~ 0
3V3_6
Text Label 1000 1050 2    50   ~ 0
3V3_7
Text Label 1000 1100 2    50   ~ 0
3V3_8
Text Label 1000 1150 2    50   ~ 0
3V3_9
Text Label 1000 1200 2    50   ~ 0
3V3_10
Text Label 1000 1250 2    50   ~ 0
3V3_11
Text Label 1000 1300 2    50   ~ 0
3V3_12
Text Label 1000 1350 2    50   ~ 0
3V3_13
Text Label 1000 1400 2    50   ~ 0
3V3_14
Text Label 1000 1450 2    50   ~ 0
3V3_15
Text Label 1000 1500 2    50   ~ 0
3V3_16
Text Label 1000 1550 2    50   ~ 0
3V3_17
Text Label 1000 1600 2    50   ~ 0
3V3_18
Text Label 1000 1650 2    50   ~ 0
3V3_19
Text Label 1000 1700 2    50   ~ 0
3V3_20
Text Label 1000 1750 2    50   ~ 0
3V3_21
Text Label 1000 1800 2    50   ~ 0
3V3_22
Text Label 1000 1850 2    50   ~ 0
3V3_23
Text Label 1000 1900 2    50   ~ 0
3V3_24
Text Label 1000 1950 2    50   ~ 0
3V3_25
Text Label 1000 2000 2    50   ~ 0
3V3_26
Text Label 1000 2050 2    50   ~ 0
3V3_27
Text Label 1000 2100 2    50   ~ 0
3V3_28
Text Label 1000 2150 2    50   ~ 0
3V3_29
Text Label 1000 2200 2    50   ~ 0
3V3_30
Text Label 1000 2250 2    50   ~ 0
3V3_31
Text Label 1000 2300 2    50   ~ 0
3V3_32
Text Label 1000 2350 2    50   ~ 0
3V3_33
Text Label 1000 2400 2    50   ~ 0
3V3_34
Text Label 1000 2450 2    50   ~ 0
3V3_35
Text Label 1000 2500 2    50   ~ 0
3V3_36
Text Label 1000 2550 2    50   ~ 0
3V3_37
Text Label 1000 2600 2    50   ~ 0
3V3_38
Text Label 1000 2650 2    50   ~ 0
3V3_39
Text Label 1000 2700 2    50   ~ 0
3V3_40
Text Label 1000 2750 2    50   ~ 0
3V3_41
Text Label 1000 2800 2    50   ~ 0
3V3_42
Text Label 1000 2850 2    50   ~ 0
3V3_43
Text Label 1000 2900 2    50   ~ 0
3V3_44
Text Label 1000 2950 2    50   ~ 0
3V3_45
Text Label 1000 3000 2    50   ~ 0
3V3_46
Text Label 1000 3050 2    50   ~ 0
3V3_47
Text Label 1000 3100 2    50   ~ 0
3V3_48
Text Label 1000 3150 2    50   ~ 0
3V3_49
Text Label 1000 3200 2    50   ~ 0
3V3_50
Text Label 1000 3250 2    50   ~ 0
3V3_51
Text Label 1000 3300 2    50   ~ 0
3V3_52
Text Label 1000 3350 2    50   ~ 0
3V3_53
Text Label 1000 3400 2    50   ~ 0
3V3_54
Text Label 1000 3450 2    50   ~ 0
3V3_55
Text Label 1000 3500 2    50   ~ 0
3V3_56
Text Label 1000 3550 2    50   ~ 0
3V3_57
Text Label 1000 3600 2    50   ~ 0
3V3_58
Text Label 1000 3650 2    50   ~ 0
3V3_59
Text Label 1000 3700 2    50   ~ 0
3V3_60
Text Label 1000 3750 2    50   ~ 0
3V3_61
Text Label 1000 3800 2    50   ~ 0
3V3_62
Text Label 1000 3850 2    50   ~ 0
3V3_63
Text Label 1000 3900 2    50   ~ 0
3V3_64
Text Label 1000 3950 2    50   ~ 0
3V3_65
Text Label 1000 4000 2    50   ~ 0
3V3_66
Text Label 1000 4050 2    50   ~ 0
3V3_67
Text Label 1000 4100 2    50   ~ 0
3V3_68
Text Label 1000 4150 2    50   ~ 0
3V3_69
Text Label 1000 4200 2    50   ~ 0
3V3_70
Text Label 1000 4250 2    50   ~ 0
3V3_71
Text Label 1000 4300 2    50   ~ 0
3V3_72
Text Label 1000 4350 2    50   ~ 0
3V3_73
Text Label 1000 4400 2    50   ~ 0
3V3_74
Text Label 1000 4450 2    50   ~ 0
3V3_75
Text Label 1000 4500 2    50   ~ 0
3V3_76
Text Label 1000 4550 2    50   ~ 0
3V3_77
Text Label 1000 4600 2    50   ~ 0
3V3_78
Text Label 1000 4650 2    50   ~ 0
3V3_79
Text Label 1000 4700 2    50   ~ 0
3V3_80
Text Label 1000 4750 2    50   ~ 0
3V3_81
Text Label 1000 4800 2    50   ~ 0
3V3_82
Text Label 1000 4850 2    50   ~ 0
3V3_83
Text Label 1000 4900 2    50   ~ 0
3V3_84
Text Label 1000 4950 2    50   ~ 0
3V3_85
Text Label 1000 5000 2    50   ~ 0
3V3_86
Text Label 1000 5050 2    50   ~ 0
3V3_87
Text Label 1000 5100 2    50   ~ 0
3V3_88
Text Label 1000 5150 2    50   ~ 0
3V3_89
Text Label 1000 5200 2    50   ~ 0
3V3_90
Text Label 1000 5250 2    50   ~ 0
3V3_91
Text Label 1000 5300 2    50   ~ 0
3V3_92
Text Label 1000 5350 2    50   ~ 0
3V3_93
Text Label 1000 5400 2    50   ~ 0
3V3_94
Text Label 1000 5450 2    50   ~ 0
3V3_95
Text Label 1000 5500 2    50   ~ 0
3V3_96
Text Label 1000 5550 2    50   ~ 0
3V3_97
Text Label 1000 5600 2    50   ~ 0
3V3_98
Text Label 1000 5650 2    50   ~ 0
3V3_99
Text Label 1000 5700 2    50   ~ 0
3V3_100
Entry Wire Line
	1100 3750 1200 3850
Entry Wire Line
	1100 3800 1200 3900
Entry Wire Line
	1100 3850 1200 3950
Entry Wire Line
	1100 3900 1200 4000
Entry Wire Line
	1100 3950 1200 4050
Entry Wire Line
	1100 4000 1200 4100
Entry Wire Line
	1100 4050 1200 4150
Entry Wire Line
	1100 4100 1200 4200
Entry Wire Line
	1100 4150 1200 4250
Entry Wire Line
	1100 4200 1200 4300
Entry Wire Line
	1100 4250 1200 4350
Entry Wire Line
	1100 4300 1200 4400
Entry Wire Line
	1100 4350 1200 4450
Entry Wire Line
	1100 4400 1200 4500
Entry Wire Line
	1100 4450 1200 4550
Entry Wire Line
	1100 4500 1200 4600
Entry Wire Line
	1100 4550 1200 4650
Entry Wire Line
	1100 4600 1200 4700
Entry Wire Line
	1100 4650 1200 4750
Entry Wire Line
	1100 4700 1200 4800
Entry Wire Line
	1100 4750 1200 4850
Entry Wire Line
	1100 4800 1200 4900
Entry Wire Line
	1100 4850 1200 4950
Entry Wire Line
	1100 4900 1200 5000
Entry Wire Line
	1100 4950 1200 5050
Entry Wire Line
	1100 5000 1200 5100
Entry Wire Line
	1100 5050 1200 5150
Entry Wire Line
	1100 5100 1200 5200
Entry Wire Line
	1100 5150 1200 5250
Entry Wire Line
	1100 5200 1200 5300
Entry Wire Line
	1100 5250 1200 5350
Entry Wire Line
	1100 5300 1200 5400
Entry Wire Line
	1100 5350 1200 5450
Entry Wire Line
	1100 5400 1200 5500
Entry Wire Line
	1100 5450 1200 5550
Entry Wire Line
	1100 5500 1200 5600
Entry Wire Line
	1100 5550 1200 5650
Entry Wire Line
	1100 5600 1200 5700
Entry Wire Line
	1100 5650 1200 5750
Entry Wire Line
	1100 5700 1200 5800
Text Label 1850 2200 2    50   ~ 0
3V3_1
Text Label 1850 2100 2    50   ~ 0
3V3_2
Text Label 1850 2000 2    50   ~ 0
3V3_3
Text Label 1850 1900 2    50   ~ 0
3V3_4
Text Label 1850 1800 2    50   ~ 0
3V3_5
Text Label 1850 1700 2    50   ~ 0
3V3_6
Text Label 1850 1600 2    50   ~ 0
3V3_7
Text Label 1850 1500 2    50   ~ 0
3V3_8
Text Label 1850 1400 2    50   ~ 0
3V3_9
Text Label 1850 1300 2    50   ~ 0
3V3_10
Text Label 1850 1200 2    50   ~ 0
3V3_11
Text Label 1850 1100 2    50   ~ 0
3V3_12
Text Label 3750 1100 0    50   ~ 0
3V3_13
Text Label 3750 1200 0    50   ~ 0
3V3_14
Text Label 3750 1300 0    50   ~ 0
3V3_15
Text Label 3750 1400 0    50   ~ 0
3V3_16
Text Label 3750 1900 0    50   ~ 0
3V3_32
Text Label 3750 2000 0    50   ~ 0
3V3_31
Text Label 3750 2100 0    50   ~ 0
3V3_30
Text Label 3750 2200 0    50   ~ 0
3V3_29
Text Label 3750 2300 0    50   ~ 0
3V3_28
Text Label 3750 2400 0    50   ~ 0
3V3_27
Text Label 3750 2500 0    50   ~ 0
3V3_26
Text Label 3750 2600 0    50   ~ 0
3V3_25
Text Label 3750 2700 0    50   ~ 0
3V3_24
Text Label 3750 2800 0    50   ~ 0
3V3_23
Text Label 3750 2900 0    50   ~ 0
3V3_22
Text Label 3750 3000 0    50   ~ 0
3V3_21
Text Label 3750 3100 0    50   ~ 0
3V3_20
Text Label 3750 3200 0    50   ~ 0
3V3_19
Text Label 3750 3300 0    50   ~ 0
3V3_18
Text Label 3750 3400 0    50   ~ 0
3V3_17
$Comp
L power:GND #PWR?
U 1 1 61EE105F
P 1550 3300
AR Path="/60749898/61EE105F" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/61EE105F" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/61EE105F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/61EE105F" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/61EE105F" Ref="#PWR0132"  Part="1" 
F 0 "#PWR0132" H 1550 3050 50  0001 C CNN
F 1 "GND" H 1555 3127 50  0000 C CNN
F 2 "" H 1550 3300 50  0001 C CNN
F 3 "" H 1550 3300 50  0001 C CNN
	1    1550 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 61F8BE02
P 1950 1100
AR Path="/60749898/61F8BE02" Ref="U?"  Part="1" 
AR Path="/606BFDCA/61F8BE02" Ref="U?"  Part="1" 
AR Path="/61F8BE02" Ref="U?"  Part="1" 
AR Path="/606F4647/61F8BE02" Ref="U?"  Part="1" 
AR Path="/607970C6/607990DD/61F8BE02" Ref="U?"  Part="1" 
AR Path="/60809D73/61F8BE02" Ref="U129"  Part="1" 
F 0 "U129" H 2800 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 2800 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 2800 1340 60  0001 C CNN
F 3 "" H 1950 1100 60  0000 C CNN
	1    1950 1100
	1    0    0    -1  
$EndComp
NoConn ~ 3650 1500
NoConn ~ 3650 1700
NoConn ~ 3650 1800
Text Label 3750 1600 0    50   ~ 0
MUX_out_g1
Text HLabel 1850 2500 0    50   Input ~ 0
A0_3V3_g1
Text HLabel 1850 2600 0    50   Input ~ 0
A1_3V3_g1
Text HLabel 1850 2700 0    50   Input ~ 0
A2_3V3_g1
Text HLabel 1850 2800 0    50   Input ~ 0
A3_3V3_g1
Text HLabel 1850 2900 0    50   Input ~ 0
A4_3V3_g1
Text Label 4750 2200 2    50   ~ 0
3V3_33
Text Label 4750 2100 2    50   ~ 0
3V3_34
Text Label 4750 2000 2    50   ~ 0
3V3_35
Text Label 4750 1900 2    50   ~ 0
3V3_36
Text Label 4750 1800 2    50   ~ 0
3V3_37
Text Label 4750 1700 2    50   ~ 0
3V3_38
Text Label 4750 1600 2    50   ~ 0
3V3_39
Text Label 4750 1500 2    50   ~ 0
3V3_40
Text Label 4750 1400 2    50   ~ 0
3V3_41
Text Label 4750 1300 2    50   ~ 0
3V3_42
Text Label 4750 1200 2    50   ~ 0
3V3_43
Text Label 4750 1100 2    50   ~ 0
3V3_44
Text Label 6650 1100 0    50   ~ 0
3V3_45
Text Label 6650 1200 0    50   ~ 0
3V3_46
Text Label 6650 1300 0    50   ~ 0
3V3_47
Text Label 6650 1400 0    50   ~ 0
3V3_48
Text Label 6650 1900 0    50   ~ 0
3V3_64
Text Label 6650 2000 0    50   ~ 0
3V3_63
Text Label 6650 2100 0    50   ~ 0
3V3_62
Text Label 6650 2200 0    50   ~ 0
3V3_61
Text Label 6650 2300 0    50   ~ 0
3V3_60
Text Label 6650 2400 0    50   ~ 0
3V3_59
Text Label 6650 2500 0    50   ~ 0
3V3_58
Text Label 6650 2600 0    50   ~ 0
3V3_57
Text Label 6650 2700 0    50   ~ 0
3V3_56
Text Label 6650 2800 0    50   ~ 0
3V3_55
Text Label 6650 2900 0    50   ~ 0
3V3_54
Text Label 6650 3000 0    50   ~ 0
3V3_53
Text Label 6650 3100 0    50   ~ 0
3V3_52
Text Label 6650 3200 0    50   ~ 0
3V3_51
Text Label 6650 3300 0    50   ~ 0
3V3_50
Text Label 6650 3400 0    50   ~ 0
3V3_49
$Comp
L power:GND #PWR?
U 1 1 60EB8C29
P 4450 3300
AR Path="/60749898/60EB8C29" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60EB8C29" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C29" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60EB8C29" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60EB8C29" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 4450 3050 50  0001 C CNN
F 1 "GND" H 4455 3127 50  0000 C CNN
F 2 "" H 4450 3300 50  0001 C CNN
F 3 "" H 4450 3300 50  0001 C CNN
	1    4450 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 607CE2C1
P 4850 1100
AR Path="/60749898/607CE2C1" Ref="U?"  Part="1" 
AR Path="/606BFDCA/607CE2C1" Ref="U?"  Part="1" 
AR Path="/607CE2C1" Ref="U?"  Part="1" 
AR Path="/606F4647/607CE2C1" Ref="U?"  Part="1" 
AR Path="/607970C6/607990DD/607CE2C1" Ref="U?"  Part="1" 
AR Path="/60809D73/607CE2C1" Ref="U130"  Part="1" 
F 0 "U130" H 5700 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 5700 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 5700 1340 60  0001 C CNN
F 3 "" H 4850 1100 60  0000 C CNN
	1    4850 1100
	1    0    0    -1  
$EndComp
NoConn ~ 6550 1500
NoConn ~ 6550 1700
NoConn ~ 6550 1800
Text Label 6650 1600 0    50   ~ 0
MUX_out_g2
Text HLabel 4750 2500 0    50   Input ~ 0
A0_3V3_g2
Text HLabel 4750 2600 0    50   Input ~ 0
A1_3V3_g2
Text HLabel 4750 2700 0    50   Input ~ 0
A2_3V3_g2
Text HLabel 4750 2800 0    50   Input ~ 0
A3_3V3_g2
Text HLabel 4750 2900 0    50   Input ~ 0
A4_3V3_g2
Text Label 7600 2200 2    50   ~ 0
3V3_65
Text Label 7600 2100 2    50   ~ 0
3V3_66
Text Label 7600 2000 2    50   ~ 0
3V3_67
Text Label 7600 1900 2    50   ~ 0
3V3_68
Text Label 7600 1800 2    50   ~ 0
3V3_69
Text Label 7600 1700 2    50   ~ 0
3V3_70
Text Label 7600 1600 2    50   ~ 0
3V3_71
Text Label 7600 1500 2    50   ~ 0
3V3_72
Text Label 7600 1400 2    50   ~ 0
3V3_73
Text Label 7600 1300 2    50   ~ 0
3V3_74
Text Label 7600 1200 2    50   ~ 0
3V3_75
Text Label 7600 1100 2    50   ~ 0
3V3_76
Text Label 9500 1100 0    50   ~ 0
3V3_77
Text Label 9500 1200 0    50   ~ 0
3V3_78
Text Label 9500 1300 0    50   ~ 0
3V3_79
Text Label 9500 1400 0    50   ~ 0
3V3_80
Text Label 9500 1900 0    50   ~ 0
3V3_96
Text Label 9500 2000 0    50   ~ 0
3V3_95
Text Label 9500 2100 0    50   ~ 0
3V3_94
Text Label 9500 2200 0    50   ~ 0
3V3_93
Text Label 9500 2300 0    50   ~ 0
3V3_92
Text Label 9500 2400 0    50   ~ 0
3V3_91
Text Label 9500 2500 0    50   ~ 0
3V3_90
Text Label 9500 2600 0    50   ~ 0
3V3_89
Text Label 9500 2700 0    50   ~ 0
3V3_88
Text Label 9500 2800 0    50   ~ 0
3V3_87
Text Label 9500 2900 0    50   ~ 0
3V3_86
Text Label 9500 3000 0    50   ~ 0
3V3_85
Text Label 9500 3100 0    50   ~ 0
3V3_84
Text Label 9500 3200 0    50   ~ 0
3V3_83
Text Label 9500 3300 0    50   ~ 0
3V3_82
Text Label 9500 3400 0    50   ~ 0
3V3_81
$Comp
L power:GND #PWR?
U 1 1 607CE2C2
P 7300 3300
AR Path="/60749898/607CE2C2" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2C2" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2C2" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/607CE2C2" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/607CE2C2" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 7300 3050 50  0001 C CNN
F 1 "GND" H 7305 3127 50  0000 C CNN
F 2 "" H 7300 3300 50  0001 C CNN
F 3 "" H 7300 3300 50  0001 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 607CE2C3
P 7700 1100
AR Path="/60749898/607CE2C3" Ref="U?"  Part="1" 
AR Path="/606BFDCA/607CE2C3" Ref="U?"  Part="1" 
AR Path="/607CE2C3" Ref="U?"  Part="1" 
AR Path="/606F4647/607CE2C3" Ref="U?"  Part="1" 
AR Path="/607970C6/607990DD/607CE2C3" Ref="U?"  Part="1" 
AR Path="/60809D73/607CE2C3" Ref="U131"  Part="1" 
F 0 "U131" H 8550 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 8550 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 8550 1340 60  0001 C CNN
F 3 "" H 7700 1100 60  0000 C CNN
	1    7700 1100
	1    0    0    -1  
$EndComp
NoConn ~ 9400 1500
NoConn ~ 9400 1700
NoConn ~ 9400 1800
Text Label 9500 1600 0    50   ~ 0
MUX_out_g3
Text HLabel 7600 2500 0    50   Input ~ 0
A0_3V3_g3
Text HLabel 7600 2600 0    50   Input ~ 0
A1_3V3_g3
Text HLabel 7600 2700 0    50   Input ~ 0
A2_3V3_g3
Text HLabel 7600 2800 0    50   Input ~ 0
A3_3V3_g3
Text HLabel 7600 2900 0    50   Input ~ 0
A4_3V3_g3
Text Label 1850 5500 2    50   ~ 0
3V3_97
Text Label 1850 5400 2    50   ~ 0
3V3_98
Text Label 1850 5300 2    50   ~ 0
3V3_99
Text Label 1850 5200 2    50   ~ 0
3V3_100
$Comp
L power:GND #PWR?
U 1 1 607CE2C4
P 1550 6600
AR Path="/60749898/607CE2C4" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2C4" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2C4" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/607CE2C4" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/607CE2C4" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 1550 6350 50  0001 C CNN
F 1 "GND" H 1555 6427 50  0000 C CNN
F 2 "" H 1550 6600 50  0001 C CNN
F 3 "" H 1550 6600 50  0001 C CNN
	1    1550 6600
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 60EB8C2E
P 1950 4400
AR Path="/60749898/60EB8C2E" Ref="U?"  Part="1" 
AR Path="/606BFDCA/60EB8C2E" Ref="U?"  Part="1" 
AR Path="/60EB8C2E" Ref="U?"  Part="1" 
AR Path="/606F4647/60EB8C2E" Ref="U?"  Part="1" 
AR Path="/607970C6/607990DD/60EB8C2E" Ref="U?"  Part="1" 
AR Path="/60809D73/60EB8C2E" Ref="U132"  Part="1" 
F 0 "U132" H 2800 4787 60  0000 C CNN
F 1 "ADG732BSUZ" H 2800 4681 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 2800 4640 60  0001 C CNN
F 3 "" H 1950 4400 60  0000 C CNN
	1    1950 4400
	1    0    0    -1  
$EndComp
NoConn ~ 3650 4800
NoConn ~ 3650 5000
NoConn ~ 3650 5100
Text Label 3750 4900 0    50   ~ 0
MUX_out_g4
Text HLabel 1850 5800 0    50   Input ~ 0
A0_3V3_g4
Text HLabel 1850 5900 0    50   Input ~ 0
A1_3V3_g4
Text HLabel 1850 6000 0    50   Input ~ 0
A2_3V3_g4
Text HLabel 1850 6100 0    50   Input ~ 0
A3_3V3_g4
Text HLabel 1850 6200 0    50   Input ~ 0
A4_3V3_g4
Text Label 1000 5750 2    50   ~ 0
3V3_101
Entry Wire Line
	1100 5750 1200 5850
Entry Wire Line
	1100 5800 1200 5900
Text Label 1000 5800 2    50   ~ 0
3V3_102
Entry Wire Line
	1100 5850 1200 5950
Entry Wire Line
	1100 5900 1200 6000
Entry Wire Line
	1100 5950 1200 6050
Entry Wire Line
	1100 6000 1200 6100
Entry Wire Line
	1100 6050 1200 6150
Entry Wire Line
	1100 6100 1200 6200
Entry Wire Line
	1100 6150 1200 6250
Entry Wire Line
	1100 6200 1200 6300
Entry Wire Line
	1100 6250 1200 6350
Entry Wire Line
	1100 6300 1200 6400
Entry Wire Line
	1100 6350 1200 6450
Entry Wire Line
	1100 6400 1200 6500
Entry Wire Line
	1100 6450 1200 6550
Entry Wire Line
	1100 6500 1200 6600
Entry Wire Line
	1100 6550 1200 6650
Entry Wire Line
	1100 6600 1200 6700
Entry Wire Line
	1100 6650 1200 6750
Entry Wire Line
	1100 6700 1200 6800
Entry Wire Line
	1100 6750 1200 6850
Entry Wire Line
	1100 6800 1200 6900
Entry Wire Line
	1100 6850 1200 6950
Entry Wire Line
	1100 6900 1200 7000
Entry Wire Line
	1100 6950 1200 7050
Entry Wire Line
	1100 7000 1200 7100
Entry Wire Line
	1100 7050 1200 7150
Text Label 1000 5850 2    50   ~ 0
3V3_103
Text Label 1000 5900 2    50   ~ 0
3V3_104
Text Label 1000 5950 2    50   ~ 0
3V3_105
Text Label 1000 6000 2    50   ~ 0
3V3_106
Text Label 1000 6050 2    50   ~ 0
3V3_107
Text Label 1000 6100 2    50   ~ 0
3V3_108
Text Label 1000 6150 2    50   ~ 0
3V3_109
Text Label 1000 6200 2    50   ~ 0
3V3_110
Text Label 1000 6250 2    50   ~ 0
3V3_111
Text Label 1000 6300 2    50   ~ 0
3V3_112
Text Label 1000 6350 2    50   ~ 0
3V3_113
Text Label 1000 6400 2    50   ~ 0
3V3_114
Text Label 1000 6450 2    50   ~ 0
3V3_115
Text Label 1000 6500 2    50   ~ 0
3V3_116
Text Label 1000 6550 2    50   ~ 0
3V3_117
Text Label 1000 6600 2    50   ~ 0
3V3_118
Text Label 1000 6650 2    50   ~ 0
3V3_119
Text Label 1000 6700 2    50   ~ 0
3V3_120
Text Label 1000 6750 2    50   ~ 0
3V3_121
Text Label 1000 6800 2    50   ~ 0
3V3_122
Text Label 1000 6850 2    50   ~ 0
3V3_123
Text Label 1000 6900 2    50   ~ 0
3V3_124
Text Label 1000 6950 2    50   ~ 0
3V3_125
Text Label 1000 7000 2    50   ~ 0
3V3_126
Text Label 1000 7050 2    50   ~ 0
3V3_127
Text Label 1000 7100 2    50   ~ 0
3V3_128
Wire Wire Line
	1100 750  1000 750 
Wire Wire Line
	1100 800  1000 800 
Wire Wire Line
	1100 850  1000 850 
Wire Wire Line
	1100 900  1000 900 
Wire Wire Line
	1100 950  1000 950 
Wire Wire Line
	1100 1000 1000 1000
Wire Wire Line
	1100 1050 1000 1050
Wire Wire Line
	1100 1100 1000 1100
Wire Wire Line
	1100 1150 1000 1150
Wire Wire Line
	1100 1200 1000 1200
Wire Wire Line
	1100 1250 1000 1250
Wire Wire Line
	1100 1300 1000 1300
Wire Wire Line
	1100 1350 1000 1350
Wire Wire Line
	1100 1400 1000 1400
Wire Wire Line
	1100 1450 1000 1450
Wire Wire Line
	1100 1500 1000 1500
Wire Wire Line
	1100 1550 1000 1550
Wire Wire Line
	1100 1600 1000 1600
Wire Wire Line
	1100 1650 1000 1650
Wire Wire Line
	1100 1700 1000 1700
Wire Wire Line
	1100 1750 1000 1750
Wire Wire Line
	1100 1800 1000 1800
Wire Wire Line
	1100 1850 1000 1850
Wire Wire Line
	1100 1900 1000 1900
Wire Wire Line
	1100 1950 1000 1950
Wire Wire Line
	1100 2000 1000 2000
Wire Wire Line
	1100 2050 1000 2050
Wire Wire Line
	1100 2100 1000 2100
Wire Wire Line
	1100 2150 1000 2150
Wire Wire Line
	1100 2200 1000 2200
Wire Wire Line
	1100 2250 1000 2250
Wire Wire Line
	1100 2300 1000 2300
Wire Wire Line
	1100 2350 1000 2350
Wire Wire Line
	1100 2400 1000 2400
Wire Wire Line
	1100 2450 1000 2450
Wire Wire Line
	1100 2500 1000 2500
Wire Wire Line
	1100 2550 1000 2550
Wire Wire Line
	1100 2600 1000 2600
Wire Wire Line
	1100 2650 1000 2650
Wire Wire Line
	1100 2700 1000 2700
Wire Wire Line
	1100 2750 1000 2750
Wire Wire Line
	1100 2800 1000 2800
Wire Wire Line
	1100 2850 1000 2850
Wire Wire Line
	1100 2900 1000 2900
Wire Wire Line
	1100 2950 1000 2950
Wire Wire Line
	1100 3000 1000 3000
Wire Wire Line
	1100 3050 1000 3050
Wire Wire Line
	1100 3100 1000 3100
Wire Wire Line
	1100 3150 1000 3150
Wire Wire Line
	1100 3200 1000 3200
Wire Wire Line
	1100 3250 1000 3250
Wire Wire Line
	1100 3300 1000 3300
Wire Wire Line
	1100 3350 1000 3350
Wire Wire Line
	1100 3400 1000 3400
Wire Wire Line
	1100 3450 1000 3450
Wire Wire Line
	1100 3500 1000 3500
Wire Wire Line
	1100 3550 1000 3550
Wire Wire Line
	1100 3600 1000 3600
Wire Wire Line
	1100 3650 1000 3650
Wire Wire Line
	1100 3700 1000 3700
Wire Wire Line
	1100 3750 1000 3750
Wire Wire Line
	1100 3800 1000 3800
Wire Wire Line
	1100 3850 1000 3850
Wire Wire Line
	1100 3900 1000 3900
Wire Wire Line
	1100 3950 1000 3950
Wire Wire Line
	1100 4000 1000 4000
Wire Wire Line
	1100 4050 1000 4050
Wire Wire Line
	1100 4100 1000 4100
Wire Wire Line
	1100 4150 1000 4150
Wire Wire Line
	1100 4200 1000 4200
Wire Wire Line
	1100 4250 1000 4250
Wire Wire Line
	1100 4300 1000 4300
Wire Wire Line
	1100 4350 1000 4350
Wire Wire Line
	1100 4400 1000 4400
Wire Wire Line
	1100 4450 1000 4450
Wire Wire Line
	1100 4500 1000 4500
Wire Wire Line
	1100 4550 1000 4550
Wire Wire Line
	1100 4600 1000 4600
Wire Wire Line
	1100 4650 1000 4650
Wire Wire Line
	1100 4700 1000 4700
Wire Wire Line
	1100 4750 1000 4750
Wire Wire Line
	1100 4800 1000 4800
Wire Wire Line
	1100 4850 1000 4850
Wire Wire Line
	1100 4900 1000 4900
Wire Wire Line
	1100 4950 1000 4950
Wire Wire Line
	1100 5000 1000 5000
Wire Wire Line
	1100 5050 1000 5050
Wire Wire Line
	1100 5100 1000 5100
Wire Wire Line
	1100 5150 1000 5150
Wire Wire Line
	1100 5200 1000 5200
Wire Wire Line
	1100 5250 1000 5250
Wire Wire Line
	1100 5300 1000 5300
Wire Wire Line
	1100 5350 1000 5350
Wire Wire Line
	1100 5400 1000 5400
Wire Wire Line
	1100 5450 1000 5450
Wire Wire Line
	1100 5500 1000 5500
Wire Wire Line
	1100 5550 1000 5550
Wire Wire Line
	1100 5600 1000 5600
Wire Wire Line
	1100 5650 1000 5650
Wire Wire Line
	1100 5700 1000 5700
Wire Wire Line
	1950 2200 1850 2200
Wire Wire Line
	1950 2100 1850 2100
Wire Wire Line
	1950 2000 1850 2000
Wire Wire Line
	1950 1900 1850 1900
Wire Wire Line
	1950 1800 1850 1800
Wire Wire Line
	1950 1700 1850 1700
Wire Wire Line
	1950 1600 1850 1600
Wire Wire Line
	1950 1500 1850 1500
Wire Wire Line
	1950 1400 1850 1400
Wire Wire Line
	1950 1300 1850 1300
Wire Wire Line
	1950 1200 1850 1200
Wire Wire Line
	1950 1100 1850 1100
Wire Wire Line
	3650 1100 3750 1100
Wire Wire Line
	3650 1200 3750 1200
Wire Wire Line
	3650 1300 3750 1300
Wire Wire Line
	3650 1400 3750 1400
Wire Wire Line
	3650 1900 3750 1900
Wire Wire Line
	3650 2000 3750 2000
Wire Wire Line
	3650 2100 3750 2100
Wire Wire Line
	3650 2200 3750 2200
Wire Wire Line
	3650 2300 3750 2300
Wire Wire Line
	3650 2400 3750 2400
Wire Wire Line
	3650 2500 3750 2500
Wire Wire Line
	3650 2600 3750 2600
Wire Wire Line
	3650 2700 3750 2700
Wire Wire Line
	3650 2800 3750 2800
Wire Wire Line
	3650 2900 3750 2900
Wire Wire Line
	3650 3000 3750 3000
Wire Wire Line
	3650 3100 3750 3100
Wire Wire Line
	3650 3200 3750 3200
Wire Wire Line
	3650 3300 3750 3300
Wire Wire Line
	3650 3400 3750 3400
Wire Wire Line
	1950 3300 1550 3300
Wire Wire Line
	1950 3000 1950 3100
Wire Wire Line
	1950 3200 1950 3100
Connection ~ 1950 3100
Wire Wire Line
	1950 3200 1950 3300
Connection ~ 1950 3200
Connection ~ 1950 3300
Wire Wire Line
	1950 3300 1950 3400
Wire Wire Line
	3750 1600 3650 1600
Wire Wire Line
	1950 2500 1850 2500
Wire Wire Line
	1950 2600 1850 2600
Wire Wire Line
	1950 2700 1850 2700
Wire Wire Line
	1950 2800 1850 2800
Wire Wire Line
	1950 2900 1850 2900
Wire Wire Line
	4850 2200 4750 2200
Wire Wire Line
	4850 2100 4750 2100
Wire Wire Line
	4850 2000 4750 2000
Wire Wire Line
	4850 1900 4750 1900
Wire Wire Line
	4850 1800 4750 1800
Wire Wire Line
	4850 1700 4750 1700
Wire Wire Line
	4850 1600 4750 1600
Wire Wire Line
	4850 1500 4750 1500
Wire Wire Line
	4850 1400 4750 1400
Wire Wire Line
	4850 1300 4750 1300
Wire Wire Line
	4850 1200 4750 1200
Wire Wire Line
	4850 1100 4750 1100
Wire Wire Line
	6550 1100 6650 1100
Wire Wire Line
	6550 1200 6650 1200
Wire Wire Line
	6550 1300 6650 1300
Wire Wire Line
	6550 1400 6650 1400
Wire Wire Line
	6550 1900 6650 1900
Wire Wire Line
	6550 2000 6650 2000
Wire Wire Line
	6550 2100 6650 2100
Wire Wire Line
	6550 2200 6650 2200
Wire Wire Line
	6550 2300 6650 2300
Wire Wire Line
	6550 2400 6650 2400
Wire Wire Line
	6550 2500 6650 2500
Wire Wire Line
	6550 2600 6650 2600
Wire Wire Line
	6550 2700 6650 2700
Wire Wire Line
	6550 2800 6650 2800
Wire Wire Line
	6550 2900 6650 2900
Wire Wire Line
	6550 3000 6650 3000
Wire Wire Line
	6550 3100 6650 3100
Wire Wire Line
	6550 3200 6650 3200
Wire Wire Line
	6550 3300 6650 3300
Wire Wire Line
	6550 3400 6650 3400
Wire Wire Line
	4850 3300 4450 3300
Wire Wire Line
	4850 3000 4850 3100
Wire Wire Line
	4850 3200 4850 3100
Connection ~ 4850 3100
Wire Wire Line
	4850 3200 4850 3300
Connection ~ 4850 3200
Connection ~ 4850 3300
Wire Wire Line
	4850 3300 4850 3400
Wire Wire Line
	6650 1600 6550 1600
Wire Wire Line
	4850 2500 4750 2500
Wire Wire Line
	4850 2600 4750 2600
Wire Wire Line
	4850 2700 4750 2700
Wire Wire Line
	4850 2800 4750 2800
Wire Wire Line
	4850 2900 4750 2900
Wire Wire Line
	7700 2200 7600 2200
Wire Wire Line
	7700 2100 7600 2100
Wire Wire Line
	7700 2000 7600 2000
Wire Wire Line
	7700 1900 7600 1900
Wire Wire Line
	7700 1800 7600 1800
Wire Wire Line
	7700 1700 7600 1700
Wire Wire Line
	7700 1600 7600 1600
Wire Wire Line
	7700 1500 7600 1500
Wire Wire Line
	7700 1400 7600 1400
Wire Wire Line
	7700 1300 7600 1300
Wire Wire Line
	7700 1200 7600 1200
Wire Wire Line
	7700 1100 7600 1100
Wire Wire Line
	9400 1100 9500 1100
Wire Wire Line
	9400 1200 9500 1200
Wire Wire Line
	9400 1300 9500 1300
Wire Wire Line
	9400 1400 9500 1400
Wire Wire Line
	9400 1900 9500 1900
Wire Wire Line
	9400 2000 9500 2000
Wire Wire Line
	9400 2100 9500 2100
Wire Wire Line
	9400 2200 9500 2200
Wire Wire Line
	9400 2300 9500 2300
Wire Wire Line
	9400 2400 9500 2400
Wire Wire Line
	9400 2500 9500 2500
Wire Wire Line
	9400 2600 9500 2600
Wire Wire Line
	9400 2700 9500 2700
Wire Wire Line
	9400 2800 9500 2800
Wire Wire Line
	9400 2900 9500 2900
Wire Wire Line
	9400 3000 9500 3000
Wire Wire Line
	9400 3100 9500 3100
Wire Wire Line
	9400 3200 9500 3200
Wire Wire Line
	9400 3300 9500 3300
Wire Wire Line
	9400 3400 9500 3400
Wire Wire Line
	7700 3300 7300 3300
Wire Wire Line
	7700 3000 7700 3100
Wire Wire Line
	7700 3200 7700 3100
Connection ~ 7700 3100
Wire Wire Line
	7700 3200 7700 3300
Connection ~ 7700 3200
Connection ~ 7700 3300
Wire Wire Line
	7700 3300 7700 3400
Wire Wire Line
	9500 1600 9400 1600
Wire Wire Line
	7700 2500 7600 2500
Wire Wire Line
	7700 2600 7600 2600
Wire Wire Line
	7700 2700 7600 2700
Wire Wire Line
	7700 2800 7600 2800
Wire Wire Line
	7700 2900 7600 2900
Wire Wire Line
	1950 5500 1850 5500
Wire Wire Line
	1950 5400 1850 5400
Wire Wire Line
	1950 5300 1850 5300
Wire Wire Line
	1950 5200 1850 5200
Wire Wire Line
	1950 6600 1550 6600
Wire Wire Line
	1950 6300 1950 6400
Wire Wire Line
	1950 6500 1950 6400
Connection ~ 1950 6400
Wire Wire Line
	1950 6500 1950 6600
Connection ~ 1950 6500
Connection ~ 1950 6600
Wire Wire Line
	1950 6600 1950 6700
Wire Wire Line
	3750 4900 3650 4900
Wire Wire Line
	1950 5800 1850 5800
Wire Wire Line
	1950 5900 1850 5900
Wire Wire Line
	1950 6000 1850 6000
Wire Wire Line
	1950 6100 1850 6100
Wire Wire Line
	1950 6200 1850 6200
Wire Wire Line
	1100 5750 1000 5750
Wire Wire Line
	1100 5800 1000 5800
Wire Wire Line
	1100 5850 1000 5850
Wire Wire Line
	1100 5900 1000 5900
Wire Wire Line
	1100 5950 1000 5950
Wire Wire Line
	1100 6000 1000 6000
Wire Wire Line
	1100 6050 1000 6050
Wire Wire Line
	1100 6100 1000 6100
Wire Wire Line
	1100 6150 1000 6150
Wire Wire Line
	1100 6200 1000 6200
Wire Wire Line
	1100 6250 1000 6250
Wire Wire Line
	1100 6300 1000 6300
Wire Wire Line
	1100 6350 1000 6350
Wire Wire Line
	1100 6400 1000 6400
Wire Wire Line
	1100 6450 1000 6450
Wire Wire Line
	1100 6500 1000 6500
Wire Wire Line
	1100 6550 1000 6550
Wire Wire Line
	1100 6600 1000 6600
Wire Wire Line
	1100 6650 1000 6650
Wire Wire Line
	1100 6700 1000 6700
Wire Wire Line
	1100 6750 1000 6750
Wire Wire Line
	1100 6800 1000 6800
Wire Wire Line
	1100 6850 1000 6850
Wire Wire Line
	1100 6900 1000 6900
Wire Wire Line
	1100 6950 1000 6950
Wire Wire Line
	1100 7000 1000 7000
Wire Wire Line
	1100 7050 1000 7050
Wire Bus Line
	1200 7300 1150 7300
Entry Wire Line
	1100 7100 1200 7200
Wire Wire Line
	1000 7100 1100 7100
Text Label 3750 5200 0    50   ~ 0
3V3_128
Text Label 3750 5300 0    50   ~ 0
3V3_127
Text Label 3750 5400 0    50   ~ 0
3V3_126
Text Label 3750 5500 0    50   ~ 0
3V3_125
Text Label 3750 5600 0    50   ~ 0
3V3_124
Text Label 3750 5700 0    50   ~ 0
3V3_123
Text Label 3750 5800 0    50   ~ 0
3V3_122
Text Label 3750 5900 0    50   ~ 0
3V3_121
Text Label 3750 6000 0    50   ~ 0
3V3_120
Text Label 3750 6100 0    50   ~ 0
3V3_119
Text Label 3750 6200 0    50   ~ 0
3V3_118
Text Label 3750 6300 0    50   ~ 0
3V3_117
Text Label 3750 6400 0    50   ~ 0
3V3_116
Text Label 3750 6500 0    50   ~ 0
3V3_115
Text Label 3750 6600 0    50   ~ 0
3V3_114
Text Label 3750 6700 0    50   ~ 0
3V3_113
Wire Wire Line
	3650 5200 3750 5200
Wire Wire Line
	3650 5300 3750 5300
Wire Wire Line
	3650 5400 3750 5400
Wire Wire Line
	3650 5500 3750 5500
Wire Wire Line
	3650 5600 3750 5600
Wire Wire Line
	3650 5700 3750 5700
Wire Wire Line
	3650 5800 3750 5800
Wire Wire Line
	3650 5900 3750 5900
Wire Wire Line
	3650 6000 3750 6000
Wire Wire Line
	3650 6100 3750 6100
Wire Wire Line
	3650 6200 3750 6200
Wire Wire Line
	3650 6300 3750 6300
Wire Wire Line
	3650 6400 3750 6400
Wire Wire Line
	3650 6500 3750 6500
Wire Wire Line
	3650 6600 3750 6600
Wire Wire Line
	3650 6700 3750 6700
Text Label 3750 4400 0    50   ~ 0
3V3_109
Text Label 3750 4500 0    50   ~ 0
3V3_110
Text Label 3750 4600 0    50   ~ 0
3V3_111
Text Label 3750 4700 0    50   ~ 0
3V3_112
Wire Wire Line
	3650 4400 3750 4400
Wire Wire Line
	3650 4500 3750 4500
Wire Wire Line
	3650 4600 3750 4600
Wire Wire Line
	3650 4700 3750 4700
Text Label 1850 5100 2    50   ~ 0
3V3_101
Text Label 1850 5000 2    50   ~ 0
3V3_102
Text Label 1850 4900 2    50   ~ 0
3V3_103
Text Label 1850 4800 2    50   ~ 0
3V3_104
Text Label 1850 4700 2    50   ~ 0
3V3_105
Text Label 1850 4600 2    50   ~ 0
3V3_106
Text Label 1850 4500 2    50   ~ 0
3V3_107
Text Label 1850 4400 2    50   ~ 0
3V3_108
Wire Wire Line
	1950 5100 1850 5100
Wire Wire Line
	1950 5000 1850 5000
Wire Wire Line
	1950 4900 1850 4900
Wire Wire Line
	1950 4800 1850 4800
Wire Wire Line
	1950 4700 1850 4700
Wire Wire Line
	1950 4600 1850 4600
Wire Wire Line
	1950 4500 1850 4500
Wire Wire Line
	1950 4400 1850 4400
Wire Wire Line
	5750 4550 5550 4550
Wire Wire Line
	5550 4550 5550 4400
Wire Wire Line
	9500 5550 9500 5600
Wire Wire Line
	9500 5250 9500 5150
Text Label 9250 4750 1    50   ~ 0
MUX_out_g1
$Comp
L power:GND #PWR?
U 1 1 62022619
P 9500 5700
AR Path="/60749898/62022619" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/62022619" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/62022619" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/62022619" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/62022619" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 9500 5450 50  0001 C CNN
F 1 "GND" H 9505 5527 50  0000 C CNN
F 2 "" H 9500 5700 50  0001 C CNN
F 3 "" H 9500 5700 50  0001 C CNN
	1    9500 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60EBDFB6
P 9500 5400
AR Path="/60749898/60EBDFB6" Ref="C?"  Part="1" 
AR Path="/606F4647/60EBDFB6" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60EBDFB6" Ref="C?"  Part="1" 
AR Path="/60809D73/60EBDFB6" Ref="C387"  Part="1" 
F 0 "C387" H 9500 5500 50  0000 L CNN
F 1 "1uF" H 9500 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9538 5250 50  0001 C CNN
F 3 "~" H 9500 5400 50  0001 C CNN
	1    9500 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60EB8C30
P 6600 6000
AR Path="/60749898/60EB8C30" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C30" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60EB8C30" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60EB8C30" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 6600 5750 50  0001 C CNN
F 1 "GND" H 6605 5827 50  0000 C CNN
F 2 "" H 6600 6000 50  0001 C CNN
F 3 "" H 6600 6000 50  0001 C CNN
	1    6600 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 610ABE0A
P 7000 4150
AR Path="/60749898/610ABE0A" Ref="C?"  Part="1" 
AR Path="/606F4647/610ABE0A" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/610ABE0A" Ref="C?"  Part="1" 
AR Path="/60809D73/610ABE0A" Ref="C385"  Part="1" 
F 0 "C385" H 7000 4250 50  0000 L CNN
F 1 "10uF" H 7000 4050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 4000 50  0001 C CNN
F 3 "~" H 7000 4150 50  0001 C CNN
	1    7000 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 610AD755
P 7450 4150
AR Path="/60749898/610AD755" Ref="C?"  Part="1" 
AR Path="/606F4647/610AD755" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/610AD755" Ref="C?"  Part="1" 
AR Path="/60809D73/610AD755" Ref="C386"  Part="1" 
F 0 "C386" H 7450 4250 50  0000 L CNN
F 1 "0.1uF" H 7450 4050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7488 4000 50  0001 C CNN
F 3 "~" H 7450 4150 50  0001 C CNN
	1    7450 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3900 6600 4000
Connection ~ 6600 4000
Wire Wire Line
	6600 4000 6600 4150
Wire Wire Line
	6600 4000 7000 4000
Connection ~ 7000 4000
Wire Wire Line
	7000 4000 7450 4000
Wire Wire Line
	7000 4300 7250 4300
$Comp
L power:GND #PWR?
U 1 1 61179AB3
P 7250 4350
AR Path="/60749898/61179AB3" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/61179AB3" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/61179AB3" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/61179AB3" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 7250 4100 50  0001 C CNN
F 1 "GND" H 7255 4177 50  0000 C CNN
F 2 "" H 7250 4350 50  0001 C CNN
F 3 "" H 7250 4350 50  0001 C CNN
	1    7250 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4350 7250 4300
Connection ~ 7250 4300
Wire Wire Line
	7250 4300 7450 4300
Text HLabel 5650 5050 0    50   Input ~ 0
SCL
Text HLabel 5650 5150 0    50   BiDi ~ 0
SDA
Wire Wire Line
	5750 5050 5650 5050
Wire Wire Line
	5750 5150 5650 5150
Wire Wire Line
	6600 6000 6600 5850
Connection ~ 6600 5850
Wire Wire Line
	5750 4850 5150 4850
Wire Wire Line
	5150 4850 5150 5850
NoConn ~ 5750 4750
Wire Wire Line
	7600 4950 7450 4950
Text Label 7600 4950 0    50   ~ 0
MUX_sense_g4
Wire Wire Line
	7600 4850 7450 4850
Text Label 7600 4850 0    50   ~ 0
MUX_sense_g3
Wire Wire Line
	7600 4750 7450 4750
Text Label 7600 4750 0    50   ~ 0
MUX_sense_g2
Wire Wire Line
	7600 4650 7450 4650
Text Label 7600 4650 0    50   ~ 0
MUX_sense_g1
Wire Wire Line
	6650 4150 6600 4150
Connection ~ 6600 4150
Wire Wire Line
	6600 4150 6550 4150
NoConn ~ 7450 5350
NoConn ~ 7450 5250
Text HLabel 7600 5050 2    50   Input ~ 0
VIN_curr
Text HLabel 7600 5150 2    50   Input ~ 0
VIN_volt
Wire Wire Line
	7600 5050 7450 5050
Wire Wire Line
	7600 5150 7450 5150
Wire Wire Line
	1450 2300 1950 2300
Wire Wire Line
	4350 2300 4850 2300
Wire Wire Line
	7200 2300 7700 2300
Wire Wire Line
	1400 5600 1950 5600
$Comp
L power:+3V3 #PWR?
U 1 1 60A07172
P 6600 3900
AR Path="/606F4647/60A07172" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A07172" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A07172" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 6600 3750 50  0001 C CNN
F 1 "+3V3" H 6615 4073 50  0000 C CNN
F 2 "" H 6600 3900 50  0001 C CNN
F 3 "" H 6600 3900 50  0001 C CNN
	1    6600 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR?
U 1 1 60A33452
P 5550 4400
AR Path="/606F4647/60A33452" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A33452" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A33452" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 5550 4250 50  0001 C CNN
F 1 "+3V0" H 5565 4573 50  0000 C CNN
F 2 "" H 5550 4400 50  0001 C CNN
F 3 "" H 5550 4400 50  0001 C CNN
	1    5550 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A62F89
P 9250 5300
AR Path="/606F4647/60A62F89" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60A62F89" Ref="R?"  Part="1" 
AR Path="/60809D73/60A62F89" Ref="R773"  Part="1" 
F 0 "R773" V 9350 5250 50  0000 L CNN
F 1 "133k" V 9250 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 5300 50  0001 C CNN
F 3 "~" H 9250 5300 50  0001 C CNN
	1    9250 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A6436D
P 9250 4950
AR Path="/606F4647/60A6436D" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60A6436D" Ref="R?"  Part="1" 
AR Path="/60809D73/60A6436D" Ref="R769"  Part="1" 
F 0 "R769" V 9350 4900 50  0000 L CNN
F 1 "66.5k" V 9250 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 4950 50  0001 C CNN
F 3 "~" H 9250 4950 50  0001 C CNN
	1    9250 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 5100 9250 5150
Wire Wire Line
	9500 5150 9250 5150
Connection ~ 9250 5150
Wire Wire Line
	9250 5450 9250 5600
Wire Wire Line
	9250 5600 9500 5600
Connection ~ 9500 5600
Wire Wire Line
	9500 5600 9500 5700
Text Label 9500 5100 1    50   ~ 0
MUX_sense_g1
Wire Wire Line
	9500 5100 9500 5150
Connection ~ 9500 5150
Wire Wire Line
	10000 5550 10000 5600
Wire Wire Line
	10000 5250 10000 5150
Text Label 9750 4750 1    50   ~ 0
MUX_out_g2
$Comp
L power:GND #PWR?
U 1 1 60BF6C73
P 10000 5700
AR Path="/60749898/60BF6C73" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60BF6C73" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60BF6C73" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60BF6C73" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60BF6C73" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 10000 5450 50  0001 C CNN
F 1 "GND" H 10005 5527 50  0000 C CNN
F 2 "" H 10000 5700 50  0001 C CNN
F 3 "" H 10000 5700 50  0001 C CNN
	1    10000 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60BF6C79
P 10000 5400
AR Path="/60749898/60BF6C79" Ref="C?"  Part="1" 
AR Path="/606F4647/60BF6C79" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60BF6C79" Ref="C?"  Part="1" 
AR Path="/60809D73/60BF6C79" Ref="C388"  Part="1" 
F 0 "C388" H 10000 5500 50  0000 L CNN
F 1 "1uF" H 10000 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10038 5250 50  0001 C CNN
F 3 "~" H 10000 5400 50  0001 C CNN
	1    10000 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60BF6C7F
P 9750 5300
AR Path="/606F4647/60BF6C7F" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60BF6C7F" Ref="R?"  Part="1" 
AR Path="/60809D73/60BF6C7F" Ref="R774"  Part="1" 
F 0 "R774" V 9850 5250 50  0000 L CNN
F 1 "133k" V 9750 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9680 5300 50  0001 C CNN
F 3 "~" H 9750 5300 50  0001 C CNN
	1    9750 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60BF6C85
P 9750 4950
AR Path="/606F4647/60BF6C85" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60BF6C85" Ref="R?"  Part="1" 
AR Path="/60809D73/60BF6C85" Ref="R770"  Part="1" 
F 0 "R770" V 9850 4900 50  0000 L CNN
F 1 "66.5k" V 9750 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9680 4950 50  0001 C CNN
F 3 "~" H 9750 4950 50  0001 C CNN
	1    9750 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5100 9750 5150
Wire Wire Line
	10000 5150 9750 5150
Connection ~ 9750 5150
Wire Wire Line
	9750 5450 9750 5600
Wire Wire Line
	9750 5600 10000 5600
Connection ~ 10000 5600
Wire Wire Line
	10000 5600 10000 5700
Text Label 10000 5100 1    50   ~ 0
MUX_sense_g2
Wire Wire Line
	10000 5100 10000 5150
Connection ~ 10000 5150
Wire Wire Line
	10450 5550 10450 5600
Wire Wire Line
	10450 5250 10450 5150
Text Label 10200 4750 1    50   ~ 0
MUX_out_g3
$Comp
L power:GND #PWR?
U 1 1 60C2425F
P 10450 5700
AR Path="/60749898/60C2425F" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60C2425F" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60C2425F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60C2425F" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60C2425F" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 10450 5450 50  0001 C CNN
F 1 "GND" H 10455 5527 50  0000 C CNN
F 2 "" H 10450 5700 50  0001 C CNN
F 3 "" H 10450 5700 50  0001 C CNN
	1    10450 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60C24265
P 10450 5400
AR Path="/60749898/60C24265" Ref="C?"  Part="1" 
AR Path="/606F4647/60C24265" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60C24265" Ref="C?"  Part="1" 
AR Path="/60809D73/60C24265" Ref="C389"  Part="1" 
F 0 "C389" H 10450 5500 50  0000 L CNN
F 1 "1uF" H 10450 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10488 5250 50  0001 C CNN
F 3 "~" H 10450 5400 50  0001 C CNN
	1    10450 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60C2426B
P 10200 5300
AR Path="/606F4647/60C2426B" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60C2426B" Ref="R?"  Part="1" 
AR Path="/60809D73/60C2426B" Ref="R775"  Part="1" 
F 0 "R775" V 10300 5250 50  0000 L CNN
F 1 "133k" V 10200 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 5300 50  0001 C CNN
F 3 "~" H 10200 5300 50  0001 C CNN
	1    10200 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60C24271
P 10200 4950
AR Path="/606F4647/60C24271" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60C24271" Ref="R?"  Part="1" 
AR Path="/60809D73/60C24271" Ref="R771"  Part="1" 
F 0 "R771" V 10300 4900 50  0000 L CNN
F 1 "66.5k" V 10200 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 4950 50  0001 C CNN
F 3 "~" H 10200 4950 50  0001 C CNN
	1    10200 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 5100 10200 5150
Wire Wire Line
	10450 5150 10200 5150
Connection ~ 10200 5150
Wire Wire Line
	10200 5450 10200 5600
Wire Wire Line
	10200 5600 10450 5600
Connection ~ 10450 5600
Wire Wire Line
	10450 5600 10450 5700
Text Label 10450 5100 1    50   ~ 0
MUX_sense_g3
Wire Wire Line
	10450 5100 10450 5150
Connection ~ 10450 5150
Wire Wire Line
	10950 5550 10950 5600
Wire Wire Line
	10950 5250 10950 5150
Text Label 10700 4750 1    50   ~ 0
MUX_out_g4
$Comp
L power:GND #PWR?
U 1 1 60C52721
P 10950 5700
AR Path="/60749898/60C52721" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60C52721" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60C52721" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60C52721" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60C52721" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 10950 5450 50  0001 C CNN
F 1 "GND" H 10955 5527 50  0000 C CNN
F 2 "" H 10950 5700 50  0001 C CNN
F 3 "" H 10950 5700 50  0001 C CNN
	1    10950 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60C52727
P 10950 5400
AR Path="/60749898/60C52727" Ref="C?"  Part="1" 
AR Path="/606F4647/60C52727" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60C52727" Ref="C?"  Part="1" 
AR Path="/60809D73/60C52727" Ref="C390"  Part="1" 
F 0 "C390" H 10950 5500 50  0000 L CNN
F 1 "1uF" H 10950 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10988 5250 50  0001 C CNN
F 3 "~" H 10950 5400 50  0001 C CNN
	1    10950 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60C5272D
P 10700 5300
AR Path="/606F4647/60C5272D" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60C5272D" Ref="R?"  Part="1" 
AR Path="/60809D73/60C5272D" Ref="R776"  Part="1" 
F 0 "R776" V 10800 5250 50  0000 L CNN
F 1 "133k" V 10700 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10630 5300 50  0001 C CNN
F 3 "~" H 10700 5300 50  0001 C CNN
	1    10700 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60C52733
P 10700 4950
AR Path="/606F4647/60C52733" Ref="R?"  Part="1" 
AR Path="/607970C6/607990DD/60C52733" Ref="R?"  Part="1" 
AR Path="/60809D73/60C52733" Ref="R772"  Part="1" 
F 0 "R772" V 10800 4900 50  0000 L CNN
F 1 "66.5k" V 10700 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10630 4950 50  0001 C CNN
F 3 "~" H 10700 4950 50  0001 C CNN
	1    10700 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 5100 10700 5150
Wire Wire Line
	10950 5150 10700 5150
Connection ~ 10700 5150
Wire Wire Line
	10700 5450 10700 5600
Wire Wire Line
	10700 5600 10950 5600
Connection ~ 10950 5600
Wire Wire Line
	10950 5600 10950 5700
Text Label 10950 5100 1    50   ~ 0
MUX_sense_g4
Wire Wire Line
	10950 5100 10950 5150
Connection ~ 10950 5150
Text Notes 9300 6100 0    50   ~ 0
MUX_sense = 3.3V * 133/(133+66.5) = 2.20 V
Wire Wire Line
	9250 4750 9250 4800
Wire Wire Line
	9750 4750 9750 4800
Wire Wire Line
	10200 4750 10200 4800
Wire Wire Line
	10700 4750 10700 4800
$Comp
L power:+3V3 #PWR?
U 1 1 60743B83
P 1450 2300
AR Path="/606F4647/60743B83" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60743B83" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60743B83" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 1450 2150 50  0001 C CNN
F 1 "+3V3" H 1465 2473 50  0000 C CNN
F 2 "" H 1450 2300 50  0001 C CNN
F 3 "" H 1450 2300 50  0001 C CNN
	1    1450 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6074602B
P 1400 5600
AR Path="/606F4647/6074602B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6074602B" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6074602B" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 1400 5450 50  0001 C CNN
F 1 "+3V3" H 1415 5773 50  0000 C CNN
F 2 "" H 1400 5600 50  0001 C CNN
F 3 "" H 1400 5600 50  0001 C CNN
	1    1400 5600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6077533B
P 4350 2300
AR Path="/606F4647/6077533B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6077533B" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6077533B" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 4350 2150 50  0001 C CNN
F 1 "+3V3" H 4365 2473 50  0000 C CNN
F 2 "" H 4350 2300 50  0001 C CNN
F 3 "" H 4350 2300 50  0001 C CNN
	1    4350 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 607A412B
P 7200 2300
AR Path="/606F4647/607A412B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/607A412B" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/607A412B" Ref="#PWR0131"  Part="1" 
F 0 "#PWR0131" H 7200 2150 50  0001 C CNN
F 1 "+3V3" H 7215 2473 50  0000 C CNN
F 2 "" H 7200 2300 50  0001 C CNN
F 3 "" H 7200 2300 50  0001 C CNN
	1    7200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2400 1950 2300
Connection ~ 1950 2300
Wire Wire Line
	4850 2400 4850 2300
Connection ~ 4850 2300
Wire Wire Line
	7700 2400 7700 2300
Connection ~ 7700 2300
Wire Wire Line
	1950 5700 1950 5600
Connection ~ 1950 5600
Wire Wire Line
	6650 5850 6750 5850
Wire Wire Line
	6600 5850 6650 5850
Connection ~ 6650 5850
Wire Wire Line
	6550 5850 6600 5850
Connection ~ 6550 5850
Wire Wire Line
	6450 5850 6550 5850
Connection ~ 6450 5850
$Comp
L linpol:AD7997 U?
U 1 1 60A5AE5B
P 5750 4550
AR Path="/606F4647/60A5AE5B" Ref="U?"  Part="1" 
AR Path="/607970C6/607990DD/60A5AE5B" Ref="U?"  Part="1" 
AR Path="/60809D73/60A5AE5B" Ref="U133"  Part="1" 
F 0 "U133" H 6250 3500 60  0000 C CNN
F 1 "AD7997" H 6250 4700 60  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 7050 3500 60  0001 C CNN
F 3 "" H 5750 4550 60  0000 C CNN
	1    5750 4550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J73
U 1 1 60804D74
P 8600 6100
F 0 "J73" H 8650 6417 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 8650 6326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical_SMD" H 8600 6100 50  0001 C CNN
F 3 "~" H 8600 6100 50  0001 C CNN
	1    8600 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 6000 8900 6100
Connection ~ 8900 6100
Wire Wire Line
	8900 6100 8900 6200
Connection ~ 8900 6200
Wire Wire Line
	8900 6200 8900 6300
Text Label 8250 6000 2    50   ~ 0
MUX_sense_g1
Text Label 8250 6100 2    50   ~ 0
MUX_sense_g2
Text Label 8250 6200 2    50   ~ 0
MUX_sense_g3
Text Label 8250 6300 2    50   ~ 0
MUX_sense_g4
Wire Wire Line
	8400 6000 8250 6000
Wire Wire Line
	8400 6100 8250 6100
Wire Wire Line
	8250 6200 8400 6200
Wire Wire Line
	8400 6300 8250 6300
$Comp
L power:GND #PWR0235
U 1 1 6098A82C
P 9150 6300
F 0 "#PWR0235" H 9150 6050 50  0001 C CNN
F 1 "GND" H 9155 6127 50  0000 C CNN
F 2 "" H 9150 6300 50  0001 C CNN
F 3 "" H 9150 6300 50  0001 C CNN
	1    9150 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 6100 9150 6100
Wire Wire Line
	9150 6100 9150 6300
$Comp
L power:+3V3 #PWR?
U 1 1 6085B7DF
P 1350 6900
AR Path="/606F4647/6085B7DF" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6085B7DF" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6085B7DF" Ref="#PWR0266"  Part="1" 
F 0 "#PWR0266" H 1350 6750 50  0001 C CNN
F 1 "+3V3" H 1365 7073 50  0000 C CNN
F 2 "" H 1350 6900 50  0001 C CNN
F 3 "" H 1350 6900 50  0001 C CNN
	1    1350 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6088C14F
P 1350 7050
AR Path="/60749898/6088C14F" Ref="C?"  Part="1" 
AR Path="/606F4647/6088C14F" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/6088C14F" Ref="C?"  Part="1" 
AR Path="/60809D73/6088C14F" Ref="C416"  Part="1" 
F 0 "C416" H 1350 7150 50  0000 L CNN
F 1 "0.1uF" H 1350 6950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1388 6900 50  0001 C CNN
F 3 "~" H 1350 7050 50  0001 C CNN
	1    1350 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 608BC53E
P 1350 7200
AR Path="/60749898/608BC53E" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/608BC53E" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/608BC53E" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/608BC53E" Ref="#PWR0267"  Part="1" 
F 0 "#PWR0267" H 1350 6950 50  0001 C CNN
F 1 "GND" H 1355 7027 50  0000 C CNN
F 2 "" H 1350 7200 50  0001 C CNN
F 3 "" H 1350 7200 50  0001 C CNN
	1    1350 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 608EF71F
P 1350 3550
AR Path="/606F4647/608EF71F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/608EF71F" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/608EF71F" Ref="#PWR0262"  Part="1" 
F 0 "#PWR0262" H 1350 3400 50  0001 C CNN
F 1 "+3V3" H 1365 3723 50  0000 C CNN
F 2 "" H 1350 3550 50  0001 C CNN
F 3 "" H 1350 3550 50  0001 C CNN
	1    1350 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 608EF725
P 1350 3700
AR Path="/60749898/608EF725" Ref="C?"  Part="1" 
AR Path="/606F4647/608EF725" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/608EF725" Ref="C?"  Part="1" 
AR Path="/60809D73/608EF725" Ref="C415"  Part="1" 
F 0 "C415" H 1350 3800 50  0000 L CNN
F 1 "0.1uF" H 1350 3600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1388 3550 50  0001 C CNN
F 3 "~" H 1350 3700 50  0001 C CNN
	1    1350 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 608EF72B
P 1350 3850
AR Path="/60749898/608EF72B" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/608EF72B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/608EF72B" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/608EF72B" Ref="#PWR0265"  Part="1" 
F 0 "#PWR0265" H 1350 3600 50  0001 C CNN
F 1 "GND" H 1355 3677 50  0000 C CNN
F 2 "" H 1350 3850 50  0001 C CNN
F 3 "" H 1350 3850 50  0001 C CNN
	1    1350 3850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6091FCCD
P 4200 3500
AR Path="/606F4647/6091FCCD" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6091FCCD" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6091FCCD" Ref="#PWR0261"  Part="1" 
F 0 "#PWR0261" H 4200 3350 50  0001 C CNN
F 1 "+3V3" H 4215 3673 50  0000 C CNN
F 2 "" H 4200 3500 50  0001 C CNN
F 3 "" H 4200 3500 50  0001 C CNN
	1    4200 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6091FCD3
P 4200 3650
AR Path="/60749898/6091FCD3" Ref="C?"  Part="1" 
AR Path="/606F4647/6091FCD3" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/6091FCD3" Ref="C?"  Part="1" 
AR Path="/60809D73/6091FCD3" Ref="C414"  Part="1" 
F 0 "C414" H 4200 3750 50  0000 L CNN
F 1 "0.1uF" H 4200 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4238 3500 50  0001 C CNN
F 3 "~" H 4200 3650 50  0001 C CNN
	1    4200 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6091FCD9
P 4200 3800
AR Path="/60749898/6091FCD9" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/6091FCD9" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6091FCD9" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6091FCD9" Ref="#PWR0264"  Part="1" 
F 0 "#PWR0264" H 4200 3550 50  0001 C CNN
F 1 "GND" H 4205 3627 50  0000 C CNN
F 2 "" H 4200 3800 50  0001 C CNN
F 3 "" H 4200 3800 50  0001 C CNN
	1    4200 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6094FF23
P 7100 3300
AR Path="/606F4647/6094FF23" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6094FF23" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6094FF23" Ref="#PWR0260"  Part="1" 
F 0 "#PWR0260" H 7100 3150 50  0001 C CNN
F 1 "+3V3" H 7115 3473 50  0000 C CNN
F 2 "" H 7100 3300 50  0001 C CNN
F 3 "" H 7100 3300 50  0001 C CNN
	1    7100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6094FF29
P 7100 3450
AR Path="/60749898/6094FF29" Ref="C?"  Part="1" 
AR Path="/606F4647/6094FF29" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/6094FF29" Ref="C?"  Part="1" 
AR Path="/60809D73/6094FF29" Ref="C413"  Part="1" 
F 0 "C413" H 7100 3550 50  0000 L CNN
F 1 "0.1uF" H 7100 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7138 3300 50  0001 C CNN
F 3 "~" H 7100 3450 50  0001 C CNN
	1    7100 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6094FF2F
P 7100 3600
AR Path="/60749898/6094FF2F" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/6094FF2F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/6094FF2F" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/6094FF2F" Ref="#PWR0263"  Part="1" 
F 0 "#PWR0263" H 7100 3350 50  0001 C CNN
F 1 "GND" H 7105 3427 50  0000 C CNN
F 2 "" H 7100 3600 50  0001 C CNN
F 3 "" H 7100 3600 50  0001 C CNN
	1    7100 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5850 6450 5850
$Comp
L power:+3V3 #PWR?
U 1 1 60A2EC5F
P 5300 5350
AR Path="/60749898/60A2EC5F" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60A2EC5F" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A2EC5F" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A2EC5F" Ref="#PWR0276"  Part="1" 
F 0 "#PWR0276" H 5300 5200 50  0001 C CNN
F 1 "+3V3" H 5315 5523 50  0000 C CNN
F 2 "" H 5300 5350 50  0001 C CNN
F 3 "" H 5300 5350 50  0001 C CNN
	1    5300 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A2EC65
P 5550 5350
AR Path="/60809E10/60A2EC65" Ref="R?"  Part="1" 
AR Path="/60809D73/60A2EC65" Ref="R834"  Part="1" 
F 0 "R834" V 5650 5350 50  0000 C CNN
F 1 "10k" V 5550 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5480 5350 50  0001 C CNN
F 3 "~" H 5550 5350 50  0001 C CNN
	1    5550 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 5350 5300 5350
Wire Wire Line
	5750 5350 5700 5350
Wire Bus Line
	1200 750  1200 7300
$EndSCHEMATC
