EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 20
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1150 7300 0    50   Input ~ 0
1V4_[1..128]
Text Label 1000 750  2    50   ~ 0
1V4_1
Entry Wire Line
	1100 750  1200 850 
Entry Wire Line
	1100 800  1200 900 
Text Label 1000 800  2    50   ~ 0
1V4_2
Entry Wire Line
	1100 850  1200 950 
Entry Wire Line
	1100 900  1200 1000
Entry Wire Line
	1100 950  1200 1050
Entry Wire Line
	1100 1000 1200 1100
Entry Wire Line
	1100 1050 1200 1150
Entry Wire Line
	1100 1100 1200 1200
Entry Wire Line
	1100 1150 1200 1250
Entry Wire Line
	1100 1200 1200 1300
Entry Wire Line
	1100 1250 1200 1350
Entry Wire Line
	1100 1300 1200 1400
Entry Wire Line
	1100 1350 1200 1450
Entry Wire Line
	1100 1400 1200 1500
Entry Wire Line
	1100 1450 1200 1550
Entry Wire Line
	1100 1500 1200 1600
Entry Wire Line
	1100 1550 1200 1650
Entry Wire Line
	1100 1600 1200 1700
Entry Wire Line
	1100 1650 1200 1750
Entry Wire Line
	1100 1700 1200 1800
Entry Wire Line
	1100 1750 1200 1850
Entry Wire Line
	1100 1800 1200 1900
Entry Wire Line
	1100 1850 1200 1950
Entry Wire Line
	1100 1900 1200 2000
Entry Wire Line
	1100 1950 1200 2050
Entry Wire Line
	1100 2000 1200 2100
Entry Wire Line
	1100 2050 1200 2150
Entry Wire Line
	1100 2100 1200 2200
Entry Wire Line
	1100 2150 1200 2250
Entry Wire Line
	1100 2200 1200 2300
Entry Wire Line
	1100 2250 1200 2350
Entry Wire Line
	1100 2300 1200 2400
Entry Wire Line
	1100 2350 1200 2450
Entry Wire Line
	1100 2400 1200 2500
Entry Wire Line
	1100 2450 1200 2550
Entry Wire Line
	1100 2500 1200 2600
Entry Wire Line
	1100 2550 1200 2650
Entry Wire Line
	1100 2600 1200 2700
Entry Wire Line
	1100 2650 1200 2750
Entry Wire Line
	1100 2700 1200 2800
Entry Wire Line
	1100 2750 1200 2850
Entry Wire Line
	1100 2800 1200 2900
Entry Wire Line
	1100 2850 1200 2950
Entry Wire Line
	1100 2900 1200 3000
Entry Wire Line
	1100 2950 1200 3050
Entry Wire Line
	1100 3000 1200 3100
Entry Wire Line
	1100 3050 1200 3150
Entry Wire Line
	1100 3100 1200 3200
Entry Wire Line
	1100 3150 1200 3250
Entry Wire Line
	1100 3200 1200 3300
Entry Wire Line
	1100 3250 1200 3350
Entry Wire Line
	1100 3300 1200 3400
Entry Wire Line
	1100 3350 1200 3450
Entry Wire Line
	1100 3400 1200 3500
Entry Wire Line
	1100 3450 1200 3550
Entry Wire Line
	1100 3500 1200 3600
Entry Wire Line
	1100 3550 1200 3650
Entry Wire Line
	1100 3600 1200 3700
Entry Wire Line
	1100 3650 1200 3750
Entry Wire Line
	1100 3700 1200 3800
Text Label 1000 850  2    50   ~ 0
1V4_3
Text Label 1000 900  2    50   ~ 0
1V4_4
Text Label 1000 950  2    50   ~ 0
1V4_5
Text Label 1000 1000 2    50   ~ 0
1V4_6
Text Label 1000 1050 2    50   ~ 0
1V4_7
Text Label 1000 1100 2    50   ~ 0
1V4_8
Text Label 1000 1150 2    50   ~ 0
1V4_9
Text Label 1000 1200 2    50   ~ 0
1V4_10
Text Label 1000 1250 2    50   ~ 0
1V4_11
Text Label 1000 1300 2    50   ~ 0
1V4_12
Text Label 1000 1350 2    50   ~ 0
1V4_13
Text Label 1000 1400 2    50   ~ 0
1V4_14
Text Label 1000 1450 2    50   ~ 0
1V4_15
Text Label 1000 1500 2    50   ~ 0
1V4_16
Text Label 1000 1550 2    50   ~ 0
1V4_17
Text Label 1000 1600 2    50   ~ 0
1V4_18
Text Label 1000 1650 2    50   ~ 0
1V4_19
Text Label 1000 1700 2    50   ~ 0
1V4_20
Text Label 1000 1750 2    50   ~ 0
1V4_21
Text Label 1000 1800 2    50   ~ 0
1V4_22
Text Label 1000 1850 2    50   ~ 0
1V4_23
Text Label 1000 1900 2    50   ~ 0
1V4_24
Text Label 1000 1950 2    50   ~ 0
1V4_25
Text Label 1000 2000 2    50   ~ 0
1V4_26
Text Label 1000 2050 2    50   ~ 0
1V4_27
Text Label 1000 2100 2    50   ~ 0
1V4_28
Text Label 1000 2150 2    50   ~ 0
1V4_29
Text Label 1000 2200 2    50   ~ 0
1V4_30
Text Label 1000 2250 2    50   ~ 0
1V4_31
Text Label 1000 2300 2    50   ~ 0
1V4_32
Text Label 1000 2350 2    50   ~ 0
1V4_33
Text Label 1000 2400 2    50   ~ 0
1V4_34
Text Label 1000 2450 2    50   ~ 0
1V4_35
Text Label 1000 2500 2    50   ~ 0
1V4_36
Text Label 1000 2550 2    50   ~ 0
1V4_37
Text Label 1000 2600 2    50   ~ 0
1V4_38
Text Label 1000 2650 2    50   ~ 0
1V4_39
Text Label 1000 2700 2    50   ~ 0
1V4_40
Text Label 1000 2750 2    50   ~ 0
1V4_41
Text Label 1000 2800 2    50   ~ 0
1V4_42
Text Label 1000 2850 2    50   ~ 0
1V4_43
Text Label 1000 2900 2    50   ~ 0
1V4_44
Text Label 1000 2950 2    50   ~ 0
1V4_45
Text Label 1000 3000 2    50   ~ 0
1V4_46
Text Label 1000 3050 2    50   ~ 0
1V4_47
Text Label 1000 3100 2    50   ~ 0
1V4_48
Text Label 1000 3150 2    50   ~ 0
1V4_49
Text Label 1000 3200 2    50   ~ 0
1V4_50
Text Label 1000 3250 2    50   ~ 0
1V4_51
Text Label 1000 3300 2    50   ~ 0
1V4_52
Text Label 1000 3350 2    50   ~ 0
1V4_53
Text Label 1000 3400 2    50   ~ 0
1V4_54
Text Label 1000 3450 2    50   ~ 0
1V4_55
Text Label 1000 3500 2    50   ~ 0
1V4_56
Text Label 1000 3550 2    50   ~ 0
1V4_57
Text Label 1000 3600 2    50   ~ 0
1V4_58
Text Label 1000 3650 2    50   ~ 0
1V4_59
Text Label 1000 3700 2    50   ~ 0
1V4_60
Text Label 1000 3750 2    50   ~ 0
1V4_61
Text Label 1000 3800 2    50   ~ 0
1V4_62
Text Label 1000 3850 2    50   ~ 0
1V4_63
Text Label 1000 3900 2    50   ~ 0
1V4_64
Text Label 1000 3950 2    50   ~ 0
1V4_65
Text Label 1000 4000 2    50   ~ 0
1V4_66
Text Label 1000 4050 2    50   ~ 0
1V4_67
Text Label 1000 4100 2    50   ~ 0
1V4_68
Text Label 1000 4150 2    50   ~ 0
1V4_69
Text Label 1000 4200 2    50   ~ 0
1V4_70
Text Label 1000 4250 2    50   ~ 0
1V4_71
Text Label 1000 4300 2    50   ~ 0
1V4_72
Text Label 1000 4350 2    50   ~ 0
1V4_73
Text Label 1000 4400 2    50   ~ 0
1V4_74
Text Label 1000 4450 2    50   ~ 0
1V4_75
Text Label 1000 4500 2    50   ~ 0
1V4_76
Text Label 1000 4550 2    50   ~ 0
1V4_77
Text Label 1000 4600 2    50   ~ 0
1V4_78
Text Label 1000 4650 2    50   ~ 0
1V4_79
Text Label 1000 4700 2    50   ~ 0
1V4_80
Text Label 1000 4750 2    50   ~ 0
1V4_81
Text Label 1000 4800 2    50   ~ 0
1V4_82
Text Label 1000 4850 2    50   ~ 0
1V4_83
Text Label 1000 4900 2    50   ~ 0
1V4_84
Text Label 1000 4950 2    50   ~ 0
1V4_85
Text Label 1000 5000 2    50   ~ 0
1V4_86
Text Label 1000 5050 2    50   ~ 0
1V4_87
Text Label 1000 5100 2    50   ~ 0
1V4_88
Text Label 1000 5150 2    50   ~ 0
1V4_89
Text Label 1000 5200 2    50   ~ 0
1V4_90
Text Label 1000 5250 2    50   ~ 0
1V4_91
Text Label 1000 5300 2    50   ~ 0
1V4_92
Text Label 1000 5350 2    50   ~ 0
1V4_93
Text Label 1000 5400 2    50   ~ 0
1V4_94
Text Label 1000 5450 2    50   ~ 0
1V4_95
Text Label 1000 5500 2    50   ~ 0
1V4_96
Text Label 1000 5550 2    50   ~ 0
1V4_97
Text Label 1000 5600 2    50   ~ 0
1V4_98
Text Label 1000 5650 2    50   ~ 0
1V4_99
Text Label 1000 5700 2    50   ~ 0
1V4_100
Entry Wire Line
	1100 3750 1200 3850
Entry Wire Line
	1100 3800 1200 3900
Entry Wire Line
	1100 3850 1200 3950
Entry Wire Line
	1100 3900 1200 4000
Entry Wire Line
	1100 3950 1200 4050
Entry Wire Line
	1100 4000 1200 4100
Entry Wire Line
	1100 4050 1200 4150
Entry Wire Line
	1100 4100 1200 4200
Entry Wire Line
	1100 4150 1200 4250
Entry Wire Line
	1100 4200 1200 4300
Entry Wire Line
	1100 4250 1200 4350
Entry Wire Line
	1100 4300 1200 4400
Entry Wire Line
	1100 4350 1200 4450
Entry Wire Line
	1100 4400 1200 4500
Entry Wire Line
	1100 4450 1200 4550
Entry Wire Line
	1100 4500 1200 4600
Entry Wire Line
	1100 4550 1200 4650
Entry Wire Line
	1100 4600 1200 4700
Entry Wire Line
	1100 4650 1200 4750
Entry Wire Line
	1100 4700 1200 4800
Entry Wire Line
	1100 4750 1200 4850
Entry Wire Line
	1100 4800 1200 4900
Entry Wire Line
	1100 4850 1200 4950
Entry Wire Line
	1100 4900 1200 5000
Entry Wire Line
	1100 4950 1200 5050
Entry Wire Line
	1100 5000 1200 5100
Entry Wire Line
	1100 5050 1200 5150
Entry Wire Line
	1100 5100 1200 5200
Entry Wire Line
	1100 5150 1200 5250
Entry Wire Line
	1100 5200 1200 5300
Entry Wire Line
	1100 5250 1200 5350
Entry Wire Line
	1100 5300 1200 5400
Entry Wire Line
	1100 5350 1200 5450
Entry Wire Line
	1100 5400 1200 5500
Entry Wire Line
	1100 5450 1200 5550
Entry Wire Line
	1100 5500 1200 5600
Entry Wire Line
	1100 5550 1200 5650
Entry Wire Line
	1100 5600 1200 5700
Entry Wire Line
	1100 5650 1200 5750
Entry Wire Line
	1100 5700 1200 5800
Text Label 1850 2200 2    50   ~ 0
1V4_1
Text Label 1850 2100 2    50   ~ 0
1V4_2
Text Label 1850 2000 2    50   ~ 0
1V4_3
Text Label 1850 1900 2    50   ~ 0
1V4_4
Text Label 1850 1800 2    50   ~ 0
1V4_5
Text Label 1850 1700 2    50   ~ 0
1V4_6
Text Label 1850 1600 2    50   ~ 0
1V4_7
Text Label 1850 1500 2    50   ~ 0
1V4_8
Text Label 1850 1400 2    50   ~ 0
1V4_9
Text Label 1850 1300 2    50   ~ 0
1V4_10
Text Label 1850 1200 2    50   ~ 0
1V4_11
Text Label 1850 1100 2    50   ~ 0
1V4_12
Text Label 3750 1100 0    50   ~ 0
1V4_13
Text Label 3750 1200 0    50   ~ 0
1V4_14
Text Label 3750 1300 0    50   ~ 0
1V4_15
Text Label 3750 1400 0    50   ~ 0
1V4_16
Text Label 3750 1900 0    50   ~ 0
1V4_32
Text Label 3750 2000 0    50   ~ 0
1V4_31
Text Label 3750 2100 0    50   ~ 0
1V4_30
Text Label 3750 2200 0    50   ~ 0
1V4_29
Text Label 3750 2300 0    50   ~ 0
1V4_28
Text Label 3750 2400 0    50   ~ 0
1V4_27
Text Label 3750 2500 0    50   ~ 0
1V4_26
Text Label 3750 2600 0    50   ~ 0
1V4_25
Text Label 3750 2700 0    50   ~ 0
1V4_24
Text Label 3750 2800 0    50   ~ 0
1V4_23
Text Label 3750 2900 0    50   ~ 0
1V4_22
Text Label 3750 3000 0    50   ~ 0
1V4_21
Text Label 3750 3100 0    50   ~ 0
1V4_20
Text Label 3750 3200 0    50   ~ 0
1V4_19
Text Label 3750 3300 0    50   ~ 0
1V4_18
Text Label 3750 3400 0    50   ~ 0
1V4_17
$Comp
L power:GND #PWR?
U 1 1 60EB8C34
P 1550 3300
AR Path="/60749898/60EB8C34" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60EB8C34" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C34" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C34" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60EB8C34" Ref="#PWR0216"  Part="1" 
F 0 "#PWR0216" H 1550 3050 50  0001 C CNN
F 1 "GND" H 1555 3127 50  0000 C CNN
F 2 "" H 1550 3300 50  0001 C CNN
F 3 "" H 1550 3300 50  0001 C CNN
	1    1550 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 60EB8C35
P 1950 1100
AR Path="/60749898/60EB8C35" Ref="U?"  Part="1" 
AR Path="/606BFDCA/60EB8C35" Ref="U?"  Part="1" 
AR Path="/60EB8C35" Ref="U?"  Part="1" 
AR Path="/606F4647/60EB8C35" Ref="U?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C35" Ref="U?"  Part="1" 
AR Path="/60809E10/60EB8C35" Ref="U145"  Part="1" 
F 0 "U145" H 2800 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 2800 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 2800 1340 60  0001 C CNN
F 3 "" H 1950 1100 60  0000 C CNN
	1    1950 1100
	1    0    0    -1  
$EndComp
NoConn ~ 3650 1500
NoConn ~ 3650 1700
NoConn ~ 3650 1800
Text Label 3750 1600 0    50   ~ 0
MUX_sense_g1
Text HLabel 1850 2500 0    50   Input ~ 0
A0_1V4_g1
Text HLabel 1850 2600 0    50   Input ~ 0
A1_1V4_g1
Text HLabel 1850 2700 0    50   Input ~ 0
A2_1V4_g1
Text HLabel 1850 2800 0    50   Input ~ 0
A3_1V4_g1
Text HLabel 1850 2900 0    50   Input ~ 0
A4_1V4_g1
Text Label 4750 2200 2    50   ~ 0
1V4_33
Text Label 4750 2100 2    50   ~ 0
1V4_34
Text Label 4750 2000 2    50   ~ 0
1V4_35
Text Label 4750 1900 2    50   ~ 0
1V4_36
Text Label 4750 1800 2    50   ~ 0
1V4_37
Text Label 4750 1700 2    50   ~ 0
1V4_38
Text Label 4750 1600 2    50   ~ 0
1V4_39
Text Label 4750 1500 2    50   ~ 0
1V4_40
Text Label 4750 1400 2    50   ~ 0
1V4_41
Text Label 4750 1300 2    50   ~ 0
1V4_42
Text Label 4750 1200 2    50   ~ 0
1V4_43
Text Label 4750 1100 2    50   ~ 0
1V4_44
Text Label 6650 1100 0    50   ~ 0
1V4_45
Text Label 6650 1200 0    50   ~ 0
1V4_46
Text Label 6650 1300 0    50   ~ 0
1V4_47
Text Label 6650 1400 0    50   ~ 0
1V4_48
Text Label 6650 1900 0    50   ~ 0
1V4_64
Text Label 6650 2000 0    50   ~ 0
1V4_63
Text Label 6650 2100 0    50   ~ 0
1V4_62
Text Label 6650 2200 0    50   ~ 0
1V4_61
Text Label 6650 2300 0    50   ~ 0
1V4_60
Text Label 6650 2400 0    50   ~ 0
1V4_59
Text Label 6650 2500 0    50   ~ 0
1V4_58
Text Label 6650 2600 0    50   ~ 0
1V4_57
Text Label 6650 2700 0    50   ~ 0
1V4_56
Text Label 6650 2800 0    50   ~ 0
1V4_55
Text Label 6650 2900 0    50   ~ 0
1V4_54
Text Label 6650 3000 0    50   ~ 0
1V4_53
Text Label 6650 3100 0    50   ~ 0
1V4_52
Text Label 6650 3200 0    50   ~ 0
1V4_51
Text Label 6650 3300 0    50   ~ 0
1V4_50
Text Label 6650 3400 0    50   ~ 0
1V4_49
$Comp
L power:GND #PWR?
U 1 1 607CE2C0
P 4450 3300
AR Path="/60749898/607CE2C0" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2C0" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2C0" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/607CE2C0" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/607CE2C0" Ref="#PWR0217"  Part="1" 
F 0 "#PWR0217" H 4450 3050 50  0001 C CNN
F 1 "GND" H 4455 3127 50  0000 C CNN
F 2 "" H 4450 3300 50  0001 C CNN
F 3 "" H 4450 3300 50  0001 C CNN
	1    4450 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 60EB8C2A
P 4850 1100
AR Path="/60749898/60EB8C2A" Ref="U?"  Part="1" 
AR Path="/606BFDCA/60EB8C2A" Ref="U?"  Part="1" 
AR Path="/60EB8C2A" Ref="U?"  Part="1" 
AR Path="/606F4647/60EB8C2A" Ref="U?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C2A" Ref="U?"  Part="1" 
AR Path="/60809E10/60EB8C2A" Ref="U146"  Part="1" 
F 0 "U146" H 5700 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 5700 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 5700 1340 60  0001 C CNN
F 3 "" H 4850 1100 60  0000 C CNN
	1    4850 1100
	1    0    0    -1  
$EndComp
NoConn ~ 6550 1500
NoConn ~ 6550 1700
NoConn ~ 6550 1800
Text Label 6650 1600 0    50   ~ 0
MUX_sense_g2
Text HLabel 4750 2500 0    50   Input ~ 0
A0_1V4_g2
Text HLabel 4750 2600 0    50   Input ~ 0
A1_1V4_g2
Text HLabel 4750 2700 0    50   Input ~ 0
A2_1V4_g2
Text HLabel 4750 2800 0    50   Input ~ 0
A3_1V4_g2
Text HLabel 4750 2900 0    50   Input ~ 0
A4_1V4_g2
Text Label 7600 2200 2    50   ~ 0
1V4_65
Text Label 7600 2100 2    50   ~ 0
1V4_66
Text Label 7600 2000 2    50   ~ 0
1V4_67
Text Label 7600 1900 2    50   ~ 0
1V4_68
Text Label 7600 1800 2    50   ~ 0
1V4_69
Text Label 7600 1700 2    50   ~ 0
1V4_70
Text Label 7600 1600 2    50   ~ 0
1V4_71
Text Label 7600 1500 2    50   ~ 0
1V4_72
Text Label 7600 1400 2    50   ~ 0
1V4_73
Text Label 7600 1300 2    50   ~ 0
1V4_74
Text Label 7600 1200 2    50   ~ 0
1V4_75
Text Label 7600 1100 2    50   ~ 0
1V4_76
Text Label 9500 1100 0    50   ~ 0
1V4_77
Text Label 9500 1200 0    50   ~ 0
1V4_78
Text Label 9500 1300 0    50   ~ 0
1V4_79
Text Label 9500 1400 0    50   ~ 0
1V4_80
Text Label 9500 1900 0    50   ~ 0
1V4_96
Text Label 9500 2000 0    50   ~ 0
1V4_95
Text Label 9500 2100 0    50   ~ 0
1V4_94
Text Label 9500 2200 0    50   ~ 0
1V4_93
Text Label 9500 2300 0    50   ~ 0
1V4_92
Text Label 9500 2400 0    50   ~ 0
1V4_91
Text Label 9500 2500 0    50   ~ 0
1V4_90
Text Label 9500 2600 0    50   ~ 0
1V4_89
Text Label 9500 2700 0    50   ~ 0
1V4_88
Text Label 9500 2800 0    50   ~ 0
1V4_87
Text Label 9500 2900 0    50   ~ 0
1V4_86
Text Label 9500 3000 0    50   ~ 0
1V4_85
Text Label 9500 3100 0    50   ~ 0
1V4_84
Text Label 9500 3200 0    50   ~ 0
1V4_83
Text Label 9500 3300 0    50   ~ 0
1V4_82
Text Label 9500 3400 0    50   ~ 0
1V4_81
$Comp
L power:GND #PWR?
U 1 1 60EB8C2B
P 7300 3300
AR Path="/60749898/60EB8C2B" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60EB8C2B" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C2B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C2B" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60EB8C2B" Ref="#PWR0218"  Part="1" 
F 0 "#PWR0218" H 7300 3050 50  0001 C CNN
F 1 "GND" H 7305 3127 50  0000 C CNN
F 2 "" H 7300 3300 50  0001 C CNN
F 3 "" H 7300 3300 50  0001 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 60EB8C2C
P 7700 1100
AR Path="/60749898/60EB8C2C" Ref="U?"  Part="1" 
AR Path="/606BFDCA/60EB8C2C" Ref="U?"  Part="1" 
AR Path="/60EB8C2C" Ref="U?"  Part="1" 
AR Path="/606F4647/60EB8C2C" Ref="U?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C2C" Ref="U?"  Part="1" 
AR Path="/60809E10/60EB8C2C" Ref="U147"  Part="1" 
F 0 "U147" H 8550 1487 60  0000 C CNN
F 1 "ADG732BSUZ" H 8550 1381 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 8550 1340 60  0001 C CNN
F 3 "" H 7700 1100 60  0000 C CNN
	1    7700 1100
	1    0    0    -1  
$EndComp
NoConn ~ 9400 1500
NoConn ~ 9400 1700
NoConn ~ 9400 1800
Text Label 9500 1600 0    50   ~ 0
MUX_sense_g3
Text HLabel 7600 2500 0    50   Input ~ 0
A0_1V4_g3
Text HLabel 7600 2600 0    50   Input ~ 0
A1_1V4_g3
Text HLabel 7600 2700 0    50   Input ~ 0
A2_1V4_g3
Text HLabel 7600 2800 0    50   Input ~ 0
A3_1V4_g3
Text HLabel 7600 2900 0    50   Input ~ 0
A4_1V4_g3
Text Label 1850 5500 2    50   ~ 0
1V4_97
Text Label 1850 5400 2    50   ~ 0
1V4_98
Text Label 1850 5300 2    50   ~ 0
1V4_99
Text Label 1850 5200 2    50   ~ 0
1V4_100
$Comp
L power:GND #PWR?
U 1 1 60EB8C2D
P 1550 6600
AR Path="/60749898/60EB8C2D" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60EB8C2D" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C2D" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C2D" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60EB8C2D" Ref="#PWR0228"  Part="1" 
F 0 "#PWR0228" H 1550 6350 50  0001 C CNN
F 1 "GND" H 1555 6427 50  0000 C CNN
F 2 "" H 1550 6600 50  0001 C CNN
F 3 "" H 1550 6600 50  0001 C CNN
	1    1550 6600
	1    0    0    -1  
$EndComp
$Comp
L linpol:ADG732BSUZ U?
U 1 1 607CE2C5
P 1950 4400
AR Path="/60749898/607CE2C5" Ref="U?"  Part="1" 
AR Path="/606BFDCA/607CE2C5" Ref="U?"  Part="1" 
AR Path="/607CE2C5" Ref="U?"  Part="1" 
AR Path="/606F4647/607CE2C5" Ref="U?"  Part="1" 
AR Path="/607970C6/60799185/607CE2C5" Ref="U?"  Part="1" 
AR Path="/60809E10/607CE2C5" Ref="U148"  Part="1" 
F 0 "U148" H 2800 4787 60  0000 C CNN
F 1 "ADG732BSUZ" H 2800 4681 60  0000 C CNN
F 2 "linpol:ADG732BSUZ" H 2800 4640 60  0001 C CNN
F 3 "" H 1950 4400 60  0000 C CNN
	1    1950 4400
	1    0    0    -1  
$EndComp
NoConn ~ 3650 4800
NoConn ~ 3650 5000
NoConn ~ 3650 5100
Text Label 3750 4900 0    50   ~ 0
MUX_sense_g4
Text HLabel 1850 5800 0    50   Input ~ 0
A0_1V4_g4
Text HLabel 1850 5900 0    50   Input ~ 0
A1_1V4_g4
Text HLabel 1850 6000 0    50   Input ~ 0
A2_1V4_g4
Text HLabel 1850 6100 0    50   Input ~ 0
A3_1V4_g4
Text HLabel 1850 6200 0    50   Input ~ 0
A4_1V4_g4
Text Label 1000 5750 2    50   ~ 0
1V4_101
Entry Wire Line
	1100 5750 1200 5850
Entry Wire Line
	1100 5800 1200 5900
Text Label 1000 5800 2    50   ~ 0
1V4_102
Entry Wire Line
	1100 5850 1200 5950
Entry Wire Line
	1100 5900 1200 6000
Entry Wire Line
	1100 5950 1200 6050
Entry Wire Line
	1100 6000 1200 6100
Entry Wire Line
	1100 6050 1200 6150
Entry Wire Line
	1100 6100 1200 6200
Entry Wire Line
	1100 6150 1200 6250
Entry Wire Line
	1100 6200 1200 6300
Entry Wire Line
	1100 6250 1200 6350
Entry Wire Line
	1100 6300 1200 6400
Entry Wire Line
	1100 6350 1200 6450
Entry Wire Line
	1100 6400 1200 6500
Entry Wire Line
	1100 6450 1200 6550
Entry Wire Line
	1100 6500 1200 6600
Entry Wire Line
	1100 6550 1200 6650
Entry Wire Line
	1100 6600 1200 6700
Entry Wire Line
	1100 6650 1200 6750
Entry Wire Line
	1100 6700 1200 6800
Entry Wire Line
	1100 6750 1200 6850
Entry Wire Line
	1100 6800 1200 6900
Entry Wire Line
	1100 6850 1200 6950
Entry Wire Line
	1100 6900 1200 7000
Entry Wire Line
	1100 6950 1200 7050
Entry Wire Line
	1100 7000 1200 7100
Entry Wire Line
	1100 7050 1200 7150
Text Label 1000 5850 2    50   ~ 0
1V4_103
Text Label 1000 5900 2    50   ~ 0
1V4_104
Text Label 1000 5950 2    50   ~ 0
1V4_105
Text Label 1000 6000 2    50   ~ 0
1V4_106
Text Label 1000 6050 2    50   ~ 0
1V4_107
Text Label 1000 6100 2    50   ~ 0
1V4_108
Text Label 1000 6150 2    50   ~ 0
1V4_109
Text Label 1000 6200 2    50   ~ 0
1V4_110
Text Label 1000 6250 2    50   ~ 0
1V4_111
Text Label 1000 6300 2    50   ~ 0
1V4_112
Text Label 1000 6350 2    50   ~ 0
1V4_113
Text Label 1000 6400 2    50   ~ 0
1V4_114
Text Label 1000 6450 2    50   ~ 0
1V4_115
Text Label 1000 6500 2    50   ~ 0
1V4_116
Text Label 1000 6550 2    50   ~ 0
1V4_117
Text Label 1000 6600 2    50   ~ 0
1V4_118
Text Label 1000 6650 2    50   ~ 0
1V4_119
Text Label 1000 6700 2    50   ~ 0
1V4_120
Text Label 1000 6750 2    50   ~ 0
1V4_121
Text Label 1000 6800 2    50   ~ 0
1V4_122
Text Label 1000 6850 2    50   ~ 0
1V4_123
Text Label 1000 6900 2    50   ~ 0
1V4_124
Text Label 1000 6950 2    50   ~ 0
1V4_125
Text Label 1000 7000 2    50   ~ 0
1V4_126
Text Label 1000 7050 2    50   ~ 0
1V4_127
Text Label 1000 7100 2    50   ~ 0
1V4_128
Wire Wire Line
	1100 750  1000 750 
Wire Wire Line
	1100 800  1000 800 
Wire Wire Line
	1100 850  1000 850 
Wire Wire Line
	1100 900  1000 900 
Wire Wire Line
	1100 950  1000 950 
Wire Wire Line
	1100 1000 1000 1000
Wire Wire Line
	1100 1050 1000 1050
Wire Wire Line
	1100 1100 1000 1100
Wire Wire Line
	1100 1150 1000 1150
Wire Wire Line
	1100 1200 1000 1200
Wire Wire Line
	1100 1250 1000 1250
Wire Wire Line
	1100 1300 1000 1300
Wire Wire Line
	1100 1350 1000 1350
Wire Wire Line
	1100 1400 1000 1400
Wire Wire Line
	1100 1450 1000 1450
Wire Wire Line
	1100 1500 1000 1500
Wire Wire Line
	1100 1550 1000 1550
Wire Wire Line
	1100 1600 1000 1600
Wire Wire Line
	1100 1650 1000 1650
Wire Wire Line
	1100 1700 1000 1700
Wire Wire Line
	1100 1750 1000 1750
Wire Wire Line
	1100 1800 1000 1800
Wire Wire Line
	1100 1850 1000 1850
Wire Wire Line
	1100 1900 1000 1900
Wire Wire Line
	1100 1950 1000 1950
Wire Wire Line
	1100 2000 1000 2000
Wire Wire Line
	1100 2050 1000 2050
Wire Wire Line
	1100 2100 1000 2100
Wire Wire Line
	1100 2150 1000 2150
Wire Wire Line
	1100 2200 1000 2200
Wire Wire Line
	1100 2250 1000 2250
Wire Wire Line
	1100 2300 1000 2300
Wire Wire Line
	1100 2350 1000 2350
Wire Wire Line
	1100 2400 1000 2400
Wire Wire Line
	1100 2450 1000 2450
Wire Wire Line
	1100 2500 1000 2500
Wire Wire Line
	1100 2550 1000 2550
Wire Wire Line
	1100 2600 1000 2600
Wire Wire Line
	1100 2650 1000 2650
Wire Wire Line
	1100 2700 1000 2700
Wire Wire Line
	1100 2750 1000 2750
Wire Wire Line
	1100 2800 1000 2800
Wire Wire Line
	1100 2850 1000 2850
Wire Wire Line
	1100 2900 1000 2900
Wire Wire Line
	1100 2950 1000 2950
Wire Wire Line
	1100 3000 1000 3000
Wire Wire Line
	1100 3050 1000 3050
Wire Wire Line
	1100 3100 1000 3100
Wire Wire Line
	1100 3150 1000 3150
Wire Wire Line
	1100 3200 1000 3200
Wire Wire Line
	1100 3250 1000 3250
Wire Wire Line
	1100 3300 1000 3300
Wire Wire Line
	1100 3350 1000 3350
Wire Wire Line
	1100 3400 1000 3400
Wire Wire Line
	1100 3450 1000 3450
Wire Wire Line
	1100 3500 1000 3500
Wire Wire Line
	1100 3550 1000 3550
Wire Wire Line
	1100 3600 1000 3600
Wire Wire Line
	1100 3650 1000 3650
Wire Wire Line
	1100 3700 1000 3700
Wire Wire Line
	1100 3750 1000 3750
Wire Wire Line
	1100 3800 1000 3800
Wire Wire Line
	1100 3850 1000 3850
Wire Wire Line
	1100 3900 1000 3900
Wire Wire Line
	1100 3950 1000 3950
Wire Wire Line
	1100 4000 1000 4000
Wire Wire Line
	1100 4050 1000 4050
Wire Wire Line
	1100 4100 1000 4100
Wire Wire Line
	1100 4150 1000 4150
Wire Wire Line
	1100 4200 1000 4200
Wire Wire Line
	1100 4250 1000 4250
Wire Wire Line
	1100 4300 1000 4300
Wire Wire Line
	1100 4350 1000 4350
Wire Wire Line
	1100 4400 1000 4400
Wire Wire Line
	1100 4450 1000 4450
Wire Wire Line
	1100 4500 1000 4500
Wire Wire Line
	1100 4550 1000 4550
Wire Wire Line
	1100 4600 1000 4600
Wire Wire Line
	1100 4650 1000 4650
Wire Wire Line
	1100 4700 1000 4700
Wire Wire Line
	1100 4750 1000 4750
Wire Wire Line
	1100 4800 1000 4800
Wire Wire Line
	1100 4850 1000 4850
Wire Wire Line
	1100 4900 1000 4900
Wire Wire Line
	1100 4950 1000 4950
Wire Wire Line
	1100 5000 1000 5000
Wire Wire Line
	1100 5050 1000 5050
Wire Wire Line
	1100 5100 1000 5100
Wire Wire Line
	1100 5150 1000 5150
Wire Wire Line
	1100 5200 1000 5200
Wire Wire Line
	1100 5250 1000 5250
Wire Wire Line
	1100 5300 1000 5300
Wire Wire Line
	1100 5350 1000 5350
Wire Wire Line
	1100 5400 1000 5400
Wire Wire Line
	1100 5450 1000 5450
Wire Wire Line
	1100 5500 1000 5500
Wire Wire Line
	1100 5550 1000 5550
Wire Wire Line
	1100 5600 1000 5600
Wire Wire Line
	1100 5650 1000 5650
Wire Wire Line
	1100 5700 1000 5700
Wire Wire Line
	1950 2200 1850 2200
Wire Wire Line
	1950 2100 1850 2100
Wire Wire Line
	1950 2000 1850 2000
Wire Wire Line
	1950 1900 1850 1900
Wire Wire Line
	1950 1800 1850 1800
Wire Wire Line
	1950 1700 1850 1700
Wire Wire Line
	1950 1600 1850 1600
Wire Wire Line
	1950 1500 1850 1500
Wire Wire Line
	1950 1400 1850 1400
Wire Wire Line
	1950 1300 1850 1300
Wire Wire Line
	1950 1200 1850 1200
Wire Wire Line
	1950 1100 1850 1100
Wire Wire Line
	3650 1100 3750 1100
Wire Wire Line
	3650 1200 3750 1200
Wire Wire Line
	3650 1300 3750 1300
Wire Wire Line
	3650 1400 3750 1400
Wire Wire Line
	3650 1900 3750 1900
Wire Wire Line
	3650 2000 3750 2000
Wire Wire Line
	3650 2100 3750 2100
Wire Wire Line
	3650 2200 3750 2200
Wire Wire Line
	3650 2300 3750 2300
Wire Wire Line
	3650 2400 3750 2400
Wire Wire Line
	3650 2500 3750 2500
Wire Wire Line
	3650 2600 3750 2600
Wire Wire Line
	3650 2700 3750 2700
Wire Wire Line
	3650 2800 3750 2800
Wire Wire Line
	3650 2900 3750 2900
Wire Wire Line
	3650 3000 3750 3000
Wire Wire Line
	3650 3100 3750 3100
Wire Wire Line
	3650 3200 3750 3200
Wire Wire Line
	3650 3300 3750 3300
Wire Wire Line
	3650 3400 3750 3400
Wire Wire Line
	1950 3300 1550 3300
Wire Wire Line
	1950 3000 1950 3100
Wire Wire Line
	1950 3200 1950 3100
Connection ~ 1950 3100
Wire Wire Line
	1950 3200 1950 3300
Connection ~ 1950 3200
Connection ~ 1950 3300
Wire Wire Line
	1950 3300 1950 3400
Wire Wire Line
	3750 1600 3650 1600
Wire Wire Line
	1950 2500 1850 2500
Wire Wire Line
	1950 2600 1850 2600
Wire Wire Line
	1950 2700 1850 2700
Wire Wire Line
	1950 2800 1850 2800
Wire Wire Line
	1950 2900 1850 2900
Wire Wire Line
	4850 2200 4750 2200
Wire Wire Line
	4850 2100 4750 2100
Wire Wire Line
	4850 2000 4750 2000
Wire Wire Line
	4850 1900 4750 1900
Wire Wire Line
	4850 1800 4750 1800
Wire Wire Line
	4850 1700 4750 1700
Wire Wire Line
	4850 1600 4750 1600
Wire Wire Line
	4850 1500 4750 1500
Wire Wire Line
	4850 1400 4750 1400
Wire Wire Line
	4850 1300 4750 1300
Wire Wire Line
	4850 1200 4750 1200
Wire Wire Line
	4850 1100 4750 1100
Wire Wire Line
	6550 1100 6650 1100
Wire Wire Line
	6550 1200 6650 1200
Wire Wire Line
	6550 1300 6650 1300
Wire Wire Line
	6550 1400 6650 1400
Wire Wire Line
	6550 1900 6650 1900
Wire Wire Line
	6550 2000 6650 2000
Wire Wire Line
	6550 2100 6650 2100
Wire Wire Line
	6550 2200 6650 2200
Wire Wire Line
	6550 2300 6650 2300
Wire Wire Line
	6550 2400 6650 2400
Wire Wire Line
	6550 2500 6650 2500
Wire Wire Line
	6550 2600 6650 2600
Wire Wire Line
	6550 2700 6650 2700
Wire Wire Line
	6550 2800 6650 2800
Wire Wire Line
	6550 2900 6650 2900
Wire Wire Line
	6550 3000 6650 3000
Wire Wire Line
	6550 3100 6650 3100
Wire Wire Line
	6550 3200 6650 3200
Wire Wire Line
	6550 3300 6650 3300
Wire Wire Line
	6550 3400 6650 3400
Wire Wire Line
	4850 3300 4450 3300
Wire Wire Line
	4850 3000 4850 3100
Wire Wire Line
	4850 3200 4850 3100
Connection ~ 4850 3100
Wire Wire Line
	4850 3200 4850 3300
Connection ~ 4850 3200
Connection ~ 4850 3300
Wire Wire Line
	4850 3300 4850 3400
Wire Wire Line
	6650 1600 6550 1600
Wire Wire Line
	4850 2500 4750 2500
Wire Wire Line
	4850 2600 4750 2600
Wire Wire Line
	4850 2700 4750 2700
Wire Wire Line
	4850 2800 4750 2800
Wire Wire Line
	4850 2900 4750 2900
Wire Wire Line
	7700 2200 7600 2200
Wire Wire Line
	7700 2100 7600 2100
Wire Wire Line
	7700 2000 7600 2000
Wire Wire Line
	7700 1900 7600 1900
Wire Wire Line
	7700 1800 7600 1800
Wire Wire Line
	7700 1700 7600 1700
Wire Wire Line
	7700 1600 7600 1600
Wire Wire Line
	7700 1500 7600 1500
Wire Wire Line
	7700 1400 7600 1400
Wire Wire Line
	7700 1300 7600 1300
Wire Wire Line
	7700 1200 7600 1200
Wire Wire Line
	7700 1100 7600 1100
Wire Wire Line
	9400 1100 9500 1100
Wire Wire Line
	9400 1200 9500 1200
Wire Wire Line
	9400 1300 9500 1300
Wire Wire Line
	9400 1400 9500 1400
Wire Wire Line
	9400 1900 9500 1900
Wire Wire Line
	9400 2000 9500 2000
Wire Wire Line
	9400 2100 9500 2100
Wire Wire Line
	9400 2200 9500 2200
Wire Wire Line
	9400 2300 9500 2300
Wire Wire Line
	9400 2400 9500 2400
Wire Wire Line
	9400 2500 9500 2500
Wire Wire Line
	9400 2600 9500 2600
Wire Wire Line
	9400 2700 9500 2700
Wire Wire Line
	9400 2800 9500 2800
Wire Wire Line
	9400 2900 9500 2900
Wire Wire Line
	9400 3000 9500 3000
Wire Wire Line
	9400 3100 9500 3100
Wire Wire Line
	9400 3200 9500 3200
Wire Wire Line
	9400 3300 9500 3300
Wire Wire Line
	9400 3400 9500 3400
Wire Wire Line
	7700 3300 7300 3300
Wire Wire Line
	7700 3000 7700 3100
Wire Wire Line
	7700 3200 7700 3100
Connection ~ 7700 3100
Wire Wire Line
	7700 3200 7700 3300
Connection ~ 7700 3200
Connection ~ 7700 3300
Wire Wire Line
	7700 3300 7700 3400
Wire Wire Line
	9500 1600 9400 1600
Wire Wire Line
	7700 2500 7600 2500
Wire Wire Line
	7700 2600 7600 2600
Wire Wire Line
	7700 2700 7600 2700
Wire Wire Line
	7700 2800 7600 2800
Wire Wire Line
	7700 2900 7600 2900
Wire Wire Line
	1950 5500 1850 5500
Wire Wire Line
	1950 5400 1850 5400
Wire Wire Line
	1950 5300 1850 5300
Wire Wire Line
	1950 5200 1850 5200
Wire Wire Line
	1950 6600 1550 6600
Wire Wire Line
	1950 6300 1950 6400
Wire Wire Line
	1950 6500 1950 6400
Connection ~ 1950 6400
Wire Wire Line
	1950 6500 1950 6600
Connection ~ 1950 6500
Connection ~ 1950 6600
Wire Wire Line
	1950 6600 1950 6700
Wire Wire Line
	3750 4900 3650 4900
Wire Wire Line
	1950 5800 1850 5800
Wire Wire Line
	1950 5900 1850 5900
Wire Wire Line
	1950 6000 1850 6000
Wire Wire Line
	1950 6100 1850 6100
Wire Wire Line
	1950 6200 1850 6200
Wire Wire Line
	1100 5750 1000 5750
Wire Wire Line
	1100 5800 1000 5800
Wire Wire Line
	1100 5850 1000 5850
Wire Wire Line
	1100 5900 1000 5900
Wire Wire Line
	1100 5950 1000 5950
Wire Wire Line
	1100 6000 1000 6000
Wire Wire Line
	1100 6050 1000 6050
Wire Wire Line
	1100 6100 1000 6100
Wire Wire Line
	1100 6150 1000 6150
Wire Wire Line
	1100 6200 1000 6200
Wire Wire Line
	1100 6250 1000 6250
Wire Wire Line
	1100 6300 1000 6300
Wire Wire Line
	1100 6350 1000 6350
Wire Wire Line
	1100 6400 1000 6400
Wire Wire Line
	1100 6450 1000 6450
Wire Wire Line
	1100 6500 1000 6500
Wire Wire Line
	1100 6550 1000 6550
Wire Wire Line
	1100 6600 1000 6600
Wire Wire Line
	1100 6650 1000 6650
Wire Wire Line
	1100 6700 1000 6700
Wire Wire Line
	1100 6750 1000 6750
Wire Wire Line
	1100 6800 1000 6800
Wire Wire Line
	1100 6850 1000 6850
Wire Wire Line
	1100 6900 1000 6900
Wire Wire Line
	1100 6950 1000 6950
Wire Wire Line
	1100 7000 1000 7000
Wire Wire Line
	1100 7050 1000 7050
Wire Bus Line
	1200 7300 1150 7300
Entry Wire Line
	1100 7100 1200 7200
Wire Wire Line
	1000 7100 1100 7100
Text Label 3750 5200 0    50   ~ 0
1V4_128
Text Label 3750 5300 0    50   ~ 0
1V4_127
Text Label 3750 5400 0    50   ~ 0
1V4_126
Text Label 3750 5500 0    50   ~ 0
1V4_125
Text Label 3750 5600 0    50   ~ 0
1V4_124
Text Label 3750 5700 0    50   ~ 0
1V4_123
Text Label 3750 5800 0    50   ~ 0
1V4_122
Text Label 3750 5900 0    50   ~ 0
1V4_121
Text Label 3750 6000 0    50   ~ 0
1V4_120
Text Label 3750 6100 0    50   ~ 0
1V4_119
Text Label 3750 6200 0    50   ~ 0
1V4_118
Text Label 3750 6300 0    50   ~ 0
1V4_117
Text Label 3750 6400 0    50   ~ 0
1V4_116
Text Label 3750 6500 0    50   ~ 0
1V4_115
Text Label 3750 6600 0    50   ~ 0
1V4_114
Text Label 3750 6700 0    50   ~ 0
1V4_113
Wire Wire Line
	3650 5200 3750 5200
Wire Wire Line
	3650 5300 3750 5300
Wire Wire Line
	3650 5400 3750 5400
Wire Wire Line
	3650 5500 3750 5500
Wire Wire Line
	3650 5600 3750 5600
Wire Wire Line
	3650 5700 3750 5700
Wire Wire Line
	3650 5800 3750 5800
Wire Wire Line
	3650 5900 3750 5900
Wire Wire Line
	3650 6000 3750 6000
Wire Wire Line
	3650 6100 3750 6100
Wire Wire Line
	3650 6200 3750 6200
Wire Wire Line
	3650 6300 3750 6300
Wire Wire Line
	3650 6400 3750 6400
Wire Wire Line
	3650 6500 3750 6500
Wire Wire Line
	3650 6600 3750 6600
Wire Wire Line
	3650 6700 3750 6700
Text Label 3750 4400 0    50   ~ 0
1V4_109
Text Label 3750 4500 0    50   ~ 0
1V4_110
Text Label 3750 4600 0    50   ~ 0
1V4_111
Text Label 3750 4700 0    50   ~ 0
1V4_112
Wire Wire Line
	3650 4400 3750 4400
Wire Wire Line
	3650 4500 3750 4500
Wire Wire Line
	3650 4600 3750 4600
Wire Wire Line
	3650 4700 3750 4700
Text Label 1850 5100 2    50   ~ 0
1V4_101
Text Label 1850 5000 2    50   ~ 0
1V4_102
Text Label 1850 4900 2    50   ~ 0
1V4_103
Text Label 1850 4800 2    50   ~ 0
1V4_104
Text Label 1850 4700 2    50   ~ 0
1V4_105
Text Label 1850 4600 2    50   ~ 0
1V4_106
Text Label 1850 4500 2    50   ~ 0
1V4_107
Text Label 1850 4400 2    50   ~ 0
1V4_108
Wire Wire Line
	1950 5100 1850 5100
Wire Wire Line
	1950 5000 1850 5000
Wire Wire Line
	1950 4900 1850 4900
Wire Wire Line
	1950 4800 1850 4800
Wire Wire Line
	1950 4700 1850 4700
Wire Wire Line
	1950 4600 1850 4600
Wire Wire Line
	1950 4500 1850 4500
Wire Wire Line
	1950 4400 1850 4400
$Comp
L linpol:AD7994 U?
U 1 1 60C9027C
P 5200 5100
AR Path="/60749898/60C9027C" Ref="U?"  Part="1" 
AR Path="/606F4647/60C9027C" Ref="U?"  Part="1" 
AR Path="/607970C6/60799185/60C9027C" Ref="U?"  Part="1" 
AR Path="/60809E10/60C9027C" Ref="U149"  Part="1" 
F 0 "U149" H 5600 4000 60  0000 C CNN
F 1 "AD7993" H 5700 5300 60  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 6500 4050 60  0001 C CNN
F 3 "" H 5200 5100 60  0000 C CNN
	1    5200 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5100 5000 5100
Wire Wire Line
	5000 5100 5000 4950
Text Label 6950 5200 0    50   ~ 0
MUX_sense_g1
Wire Wire Line
	6950 5200 6800 5200
Text Label 6950 5300 0    50   ~ 0
MUX_sense_g2
Wire Wire Line
	6950 5300 6800 5300
Text Label 6950 5400 0    50   ~ 0
MUX_sense_g3
Wire Wire Line
	6950 5400 6800 5400
Text Label 6950 5500 0    50   ~ 0
MUX_sense_g4
Wire Wire Line
	6950 5500 6800 5500
Wire Wire Line
	10900 5900 10900 6050
Wire Wire Line
	10900 5600 10900 5500
Wire Wire Line
	10500 5900 10500 6050
Wire Wire Line
	10500 5600 10500 5500
Wire Wire Line
	10100 5900 10100 6050
Wire Wire Line
	10100 5600 10100 5500
Wire Wire Line
	9650 5900 9650 6050
Wire Wire Line
	9650 5600 9650 5500
Text Label 10900 5500 1    50   ~ 0
MUX_sense_g4
$Comp
L power:GND #PWR?
U 1 1 607CE2CB
P 10900 6050
AR Path="/60749898/607CE2CB" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2CB" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2CB" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/607CE2CB" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/607CE2CB" Ref="#PWR0226"  Part="1" 
F 0 "#PWR0226" H 10900 5800 50  0001 C CNN
F 1 "GND" H 10905 5877 50  0000 C CNN
F 2 "" H 10900 6050 50  0001 C CNN
F 3 "" H 10900 6050 50  0001 C CNN
	1    10900 6050
	1    0    0    -1  
$EndComp
Text Label 10500 5500 1    50   ~ 0
MUX_sense_g3
$Comp
L power:GND #PWR?
U 1 1 607CE2C9
P 10500 6050
AR Path="/60749898/607CE2C9" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2C9" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2C9" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/607CE2C9" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/607CE2C9" Ref="#PWR0225"  Part="1" 
F 0 "#PWR0225" H 10500 5800 50  0001 C CNN
F 1 "GND" H 10505 5877 50  0000 C CNN
F 2 "" H 10500 6050 50  0001 C CNN
F 3 "" H 10500 6050 50  0001 C CNN
	1    10500 6050
	1    0    0    -1  
$EndComp
Text Label 10100 5500 1    50   ~ 0
MUX_sense_g2
$Comp
L power:GND #PWR?
U 1 1 607CE2C7
P 10100 6050
AR Path="/60749898/607CE2C7" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/607CE2C7" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/607CE2C7" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/607CE2C7" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/607CE2C7" Ref="#PWR0224"  Part="1" 
F 0 "#PWR0224" H 10100 5800 50  0001 C CNN
F 1 "GND" H 10105 5877 50  0000 C CNN
F 2 "" H 10100 6050 50  0001 C CNN
F 3 "" H 10100 6050 50  0001 C CNN
	1    10100 6050
	1    0    0    -1  
$EndComp
Text Label 9650 5500 1    50   ~ 0
MUX_sense_g1
$Comp
L power:GND #PWR?
U 1 1 60EB8C36
P 9650 6050
AR Path="/60749898/60EB8C36" Ref="#PWR?"  Part="1" 
AR Path="/606BFDCA/60EB8C36" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C36" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C36" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60EB8C36" Ref="#PWR0223"  Part="1" 
F 0 "#PWR0223" H 9650 5800 50  0001 C CNN
F 1 "GND" H 9655 5877 50  0000 C CNN
F 2 "" H 9650 6050 50  0001 C CNN
F 3 "" H 9650 6050 50  0001 C CNN
	1    9650 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60EB8C2F
P 9650 5750
AR Path="/60749898/60EB8C2F" Ref="C?"  Part="1" 
AR Path="/606F4647/60EB8C2F" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C2F" Ref="C?"  Part="1" 
AR Path="/60809E10/60EB8C2F" Ref="C409"  Part="1" 
F 0 "C409" H 9650 5850 50  0000 L CNN
F 1 "1uF" H 9650 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9688 5600 50  0001 C CNN
F 3 "~" H 9650 5750 50  0001 C CNN
	1    9650 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60EBEC37
P 10100 5750
AR Path="/60749898/60EBEC37" Ref="C?"  Part="1" 
AR Path="/606F4647/60EBEC37" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60EBEC37" Ref="C?"  Part="1" 
AR Path="/60809E10/60EBEC37" Ref="C410"  Part="1" 
F 0 "C410" H 10100 5850 50  0000 L CNN
F 1 "1uF" H 10100 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10138 5600 50  0001 C CNN
F 3 "~" H 10100 5750 50  0001 C CNN
	1    10100 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60EE5501
P 10500 5750
AR Path="/60749898/60EE5501" Ref="C?"  Part="1" 
AR Path="/606F4647/60EE5501" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60EE5501" Ref="C?"  Part="1" 
AR Path="/60809E10/60EE5501" Ref="C411"  Part="1" 
F 0 "C411" H 10500 5850 50  0000 L CNN
F 1 "1uF" H 10500 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10538 5600 50  0001 C CNN
F 3 "~" H 10500 5750 50  0001 C CNN
	1    10500 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60F0BC64
P 10900 5750
AR Path="/60749898/60F0BC64" Ref="C?"  Part="1" 
AR Path="/606F4647/60F0BC64" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60F0BC64" Ref="C?"  Part="1" 
AR Path="/60809E10/60F0BC64" Ref="C412"  Part="1" 
F 0 "C412" H 10900 5850 50  0000 L CNN
F 1 "1uF" H 10900 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10938 5600 50  0001 C CNN
F 3 "~" H 10900 5750 50  0001 C CNN
	1    10900 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60F3480E
P 6000 6500
AR Path="/60749898/60F3480E" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60F3480E" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60F3480E" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60F3480E" Ref="#PWR0227"  Part="1" 
F 0 "#PWR0227" H 6000 6250 50  0001 C CNN
F 1 "GND" H 6005 6327 50  0000 C CNN
F 2 "" H 6000 6500 50  0001 C CNN
F 3 "" H 6000 6500 50  0001 C CNN
	1    6000 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 6400 5900 6400
Wire Wire Line
	5900 6400 6000 6400
Connection ~ 5900 6400
Wire Wire Line
	6000 6400 6100 6400
Connection ~ 6000 6400
Wire Wire Line
	6100 6400 6200 6400
Connection ~ 6100 6400
Wire Wire Line
	6000 6400 6000 6500
Connection ~ 5800 6400
Wire Wire Line
	5200 5400 4650 5400
Wire Wire Line
	4650 5400 4650 6400
$Comp
L Device:C C?
U 1 1 60EB8C31
P 6400 4700
AR Path="/60749898/60EB8C31" Ref="C?"  Part="1" 
AR Path="/606F4647/60EB8C31" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C31" Ref="C?"  Part="1" 
AR Path="/60809E10/60EB8C31" Ref="C407"  Part="1" 
F 0 "C407" H 6400 4800 50  0000 L CNN
F 1 "10uF" H 6400 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6438 4550 50  0001 C CNN
F 3 "~" H 6400 4700 50  0001 C CNN
	1    6400 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60EB8C32
P 6850 4700
AR Path="/60749898/60EB8C32" Ref="C?"  Part="1" 
AR Path="/606F4647/60EB8C32" Ref="C?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C32" Ref="C?"  Part="1" 
AR Path="/60809E10/60EB8C32" Ref="C408"  Part="1" 
F 0 "C408" H 6850 4800 50  0000 L CNN
F 1 "0.1uF" H 6850 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6888 4550 50  0001 C CNN
F 3 "~" H 6850 4700 50  0001 C CNN
	1    6850 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4450 6000 4550
Connection ~ 6000 4550
Wire Wire Line
	6000 4550 6000 4700
Wire Wire Line
	6000 4550 6400 4550
Connection ~ 6400 4550
Wire Wire Line
	6400 4550 6850 4550
Wire Wire Line
	6400 4850 6650 4850
$Comp
L power:GND #PWR?
U 1 1 60EB8C33
P 6650 4900
AR Path="/60749898/60EB8C33" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60EB8C33" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60EB8C33" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60EB8C33" Ref="#PWR0220"  Part="1" 
F 0 "#PWR0220" H 6650 4650 50  0001 C CNN
F 1 "GND" H 6655 4727 50  0000 C CNN
F 2 "" H 6650 4900 50  0001 C CNN
F 3 "" H 6650 4900 50  0001 C CNN
	1    6650 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4900 6650 4850
Connection ~ 6650 4850
Wire Wire Line
	6650 4850 6850 4850
NoConn ~ 5200 5300
Text HLabel 5100 5600 0    50   Input ~ 0
SCL
Text HLabel 5100 5700 0    50   BiDi ~ 0
SDA
Wire Wire Line
	5200 5600 5100 5600
Wire Wire Line
	5200 5700 5100 5700
Wire Wire Line
	1450 2300 1950 2300
Wire Wire Line
	4350 2300 4850 2300
Wire Wire Line
	7200 2300 7700 2300
Wire Wire Line
	1400 5600 1950 5600
$Comp
L power:+2V5 #PWR?
U 1 1 60AFFEF8
P 5000 4950
AR Path="/60749898/60AFFEF8" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60AFFEF8" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60AFFEF8" Ref="#PWR0221"  Part="1" 
F 0 "#PWR0221" H 5000 4800 50  0001 C CNN
F 1 "+2V5" H 5015 5123 50  0000 C CNN
F 2 "" H 5000 4950 50  0001 C CNN
F 3 "" H 5000 4950 50  0001 C CNN
	1    5000 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60B2AC3B
P 6000 4450
AR Path="/60749898/60B2AC3B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60B2AC3B" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60B2AC3B" Ref="#PWR0219"  Part="1" 
F 0 "#PWR0219" H 6000 4300 50  0001 C CNN
F 1 "+3V3" H 6015 4623 50  0000 C CNN
F 2 "" H 6000 4450 50  0001 C CNN
F 3 "" H 6000 4450 50  0001 C CNN
	1    6000 4450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6091BFB6
P 1450 2300
AR Path="/606F4647/6091BFB6" Ref="#PWR?"  Part="1" 
AR Path="/60749898/6091BFB6" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/6091BFB6" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/6091BFB6" Ref="#PWR0213"  Part="1" 
F 0 "#PWR0213" H 1450 2150 50  0001 C CNN
F 1 "+3V3" H 1465 2473 50  0000 C CNN
F 2 "" H 1450 2300 50  0001 C CNN
F 3 "" H 1450 2300 50  0001 C CNN
	1    1450 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2400 1950 2300
Connection ~ 1950 2300
$Comp
L power:+3V3 #PWR?
U 1 1 6097141C
P 4350 2300
AR Path="/606F4647/6097141C" Ref="#PWR?"  Part="1" 
AR Path="/60749898/6097141C" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/6097141C" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/6097141C" Ref="#PWR0214"  Part="1" 
F 0 "#PWR0214" H 4350 2150 50  0001 C CNN
F 1 "+3V3" H 4365 2473 50  0000 C CNN
F 2 "" H 4350 2300 50  0001 C CNN
F 3 "" H 4350 2300 50  0001 C CNN
	1    4350 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2400 4850 2300
Connection ~ 4850 2300
Wire Wire Line
	7700 2400 7700 2300
Connection ~ 7700 2300
$Comp
L power:+3V3 #PWR?
U 1 1 609F127A
P 7200 2300
AR Path="/606F4647/609F127A" Ref="#PWR?"  Part="1" 
AR Path="/60749898/609F127A" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/609F127A" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/609F127A" Ref="#PWR0215"  Part="1" 
F 0 "#PWR0215" H 7200 2150 50  0001 C CNN
F 1 "+3V3" H 7215 2473 50  0000 C CNN
F 2 "" H 7200 2300 50  0001 C CNN
F 3 "" H 7200 2300 50  0001 C CNN
	1    7200 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60A1EBFD
P 1400 5600
AR Path="/606F4647/60A1EBFD" Ref="#PWR?"  Part="1" 
AR Path="/60749898/60A1EBFD" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/60799185/60A1EBFD" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A1EBFD" Ref="#PWR0222"  Part="1" 
F 0 "#PWR0222" H 1400 5450 50  0001 C CNN
F 1 "+3V3" H 1415 5773 50  0000 C CNN
F 2 "" H 1400 5600 50  0001 C CNN
F 3 "" H 1400 5600 50  0001 C CNN
	1    1400 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 5700 1950 5600
Connection ~ 1950 5600
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J74
U 1 1 60986367
P 8550 5950
F 0 "J74" H 8600 6267 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 8600 6176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical_SMD" H 8550 5950 50  0001 C CNN
F 3 "~" H 8550 5950 50  0001 C CNN
	1    8550 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5850 8850 5950
Connection ~ 8850 5950
Wire Wire Line
	8850 5950 8850 6050
Connection ~ 8850 6050
Wire Wire Line
	8850 6050 8850 6150
$Comp
L power:GND #PWR0236
U 1 1 609E81AE
P 9150 6250
F 0 "#PWR0236" H 9150 6000 50  0001 C CNN
F 1 "GND" H 9155 6077 50  0000 C CNN
F 2 "" H 9150 6250 50  0001 C CNN
F 3 "" H 9150 6250 50  0001 C CNN
	1    9150 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5950 9150 5950
Wire Wire Line
	9150 5950 9150 6250
Text Label 8250 5850 2    50   ~ 0
MUX_sense_g1
Text Label 8250 5950 2    50   ~ 0
MUX_sense_g2
Text Label 8250 6050 2    50   ~ 0
MUX_sense_g3
Text Label 8250 6150 2    50   ~ 0
MUX_sense_g4
Wire Wire Line
	8350 5850 8250 5850
Wire Wire Line
	8350 5950 8250 5950
Wire Wire Line
	8350 6050 8250 6050
Wire Wire Line
	8350 6150 8250 6150
$Comp
L power:+3V3 #PWR?
U 1 1 609DF89A
P 1350 3300
AR Path="/606F4647/609DF89A" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/609DF89A" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/609DF89A" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/609DF89A" Ref="#PWR0268"  Part="1" 
F 0 "#PWR0268" H 1350 3150 50  0001 C CNN
F 1 "+3V3" H 1365 3473 50  0000 C CNN
F 2 "" H 1350 3300 50  0001 C CNN
F 3 "" H 1350 3300 50  0001 C CNN
	1    1350 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 609DF8A0
P 1350 3450
AR Path="/60749898/609DF8A0" Ref="C?"  Part="1" 
AR Path="/606F4647/609DF8A0" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/609DF8A0" Ref="C?"  Part="1" 
AR Path="/60809D73/609DF8A0" Ref="C?"  Part="1" 
AR Path="/60809E10/609DF8A0" Ref="C417"  Part="1" 
F 0 "C417" H 1350 3550 50  0000 L CNN
F 1 "0.1uF" H 1350 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1388 3300 50  0001 C CNN
F 3 "~" H 1350 3450 50  0001 C CNN
	1    1350 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 609DF8A6
P 1350 3600
AR Path="/60749898/609DF8A6" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/609DF8A6" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/609DF8A6" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/609DF8A6" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/609DF8A6" Ref="#PWR0271"  Part="1" 
F 0 "#PWR0271" H 1350 3350 50  0001 C CNN
F 1 "GND" H 1355 3427 50  0000 C CNN
F 2 "" H 1350 3600 50  0001 C CNN
F 3 "" H 1350 3600 50  0001 C CNN
	1    1350 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60A0BFFC
P 4200 3400
AR Path="/606F4647/60A0BFFC" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A0BFFC" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A0BFFC" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A0BFFC" Ref="#PWR0269"  Part="1" 
F 0 "#PWR0269" H 4200 3250 50  0001 C CNN
F 1 "+3V3" H 4215 3573 50  0000 C CNN
F 2 "" H 4200 3400 50  0001 C CNN
F 3 "" H 4200 3400 50  0001 C CNN
	1    4200 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A0C002
P 4200 3550
AR Path="/60749898/60A0C002" Ref="C?"  Part="1" 
AR Path="/606F4647/60A0C002" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60A0C002" Ref="C?"  Part="1" 
AR Path="/60809D73/60A0C002" Ref="C?"  Part="1" 
AR Path="/60809E10/60A0C002" Ref="C418"  Part="1" 
F 0 "C418" H 4200 3650 50  0000 L CNN
F 1 "0.1uF" H 4200 3450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4238 3400 50  0001 C CNN
F 3 "~" H 4200 3550 50  0001 C CNN
	1    4200 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60A0C008
P 4200 3700
AR Path="/60749898/60A0C008" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60A0C008" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A0C008" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A0C008" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A0C008" Ref="#PWR0272"  Part="1" 
F 0 "#PWR0272" H 4200 3450 50  0001 C CNN
F 1 "GND" H 4205 3527 50  0000 C CNN
F 2 "" H 4200 3700 50  0001 C CNN
F 3 "" H 4200 3700 50  0001 C CNN
	1    4200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60A384BA
P 7050 3500
AR Path="/606F4647/60A384BA" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A384BA" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A384BA" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A384BA" Ref="#PWR0270"  Part="1" 
F 0 "#PWR0270" H 7050 3350 50  0001 C CNN
F 1 "+3V3" H 7065 3673 50  0000 C CNN
F 2 "" H 7050 3500 50  0001 C CNN
F 3 "" H 7050 3500 50  0001 C CNN
	1    7050 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A384C0
P 7050 3650
AR Path="/60749898/60A384C0" Ref="C?"  Part="1" 
AR Path="/606F4647/60A384C0" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60A384C0" Ref="C?"  Part="1" 
AR Path="/60809D73/60A384C0" Ref="C?"  Part="1" 
AR Path="/60809E10/60A384C0" Ref="C419"  Part="1" 
F 0 "C419" H 7050 3750 50  0000 L CNN
F 1 "0.1uF" H 7050 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 3500 50  0001 C CNN
F 3 "~" H 7050 3650 50  0001 C CNN
	1    7050 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60A384C6
P 7050 3800
AR Path="/60749898/60A384C6" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60A384C6" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A384C6" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A384C6" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A384C6" Ref="#PWR0273"  Part="1" 
F 0 "#PWR0273" H 7050 3550 50  0001 C CNN
F 1 "GND" H 7055 3627 50  0000 C CNN
F 2 "" H 7050 3800 50  0001 C CNN
F 3 "" H 7050 3800 50  0001 C CNN
	1    7050 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60A6467D
P 1350 6900
AR Path="/606F4647/60A6467D" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A6467D" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A6467D" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A6467D" Ref="#PWR0274"  Part="1" 
F 0 "#PWR0274" H 1350 6750 50  0001 C CNN
F 1 "+3V3" H 1365 7073 50  0000 C CNN
F 2 "" H 1350 6900 50  0001 C CNN
F 3 "" H 1350 6900 50  0001 C CNN
	1    1350 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A64683
P 1350 7050
AR Path="/60749898/60A64683" Ref="C?"  Part="1" 
AR Path="/606F4647/60A64683" Ref="C?"  Part="1" 
AR Path="/607970C6/607990DD/60A64683" Ref="C?"  Part="1" 
AR Path="/60809D73/60A64683" Ref="C?"  Part="1" 
AR Path="/60809E10/60A64683" Ref="C420"  Part="1" 
F 0 "C420" H 1350 7150 50  0000 L CNN
F 1 "0.1uF" H 1350 6950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1388 6900 50  0001 C CNN
F 3 "~" H 1350 7050 50  0001 C CNN
	1    1350 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60A64689
P 1350 7200
AR Path="/60749898/60A64689" Ref="#PWR?"  Part="1" 
AR Path="/606F4647/60A64689" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607990DD/60A64689" Ref="#PWR?"  Part="1" 
AR Path="/60809D73/60A64689" Ref="#PWR?"  Part="1" 
AR Path="/60809E10/60A64689" Ref="#PWR0275"  Part="1" 
F 0 "#PWR0275" H 1350 6950 50  0001 C CNN
F 1 "GND" H 1355 7027 50  0000 C CNN
F 2 "" H 1350 7200 50  0001 C CNN
F 3 "" H 1350 7200 50  0001 C CNN
	1    1350 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6400 5150 6400
Wire Wire Line
	5200 5900 5150 5900
Wire Wire Line
	5150 5900 5150 6400
Wire Bus Line
	1200 750  1200 7300
Connection ~ 5150 6400
Wire Wire Line
	5150 6400 5800 6400
$EndSCHEMATC
