EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 20
Title "linPOL qualification board"
Date "2021-04-04"
Rev "v1"
Comp "LBNL"
Comment1 "Zhicai Zhang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 61A2F585
P 3100 1900
AR Path="/61A2F585" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F585" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F585" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F585" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F585" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F585" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F585" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F585" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F585" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F585" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F585" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F585" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F585" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F585" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F585" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F585" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F585" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F585" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F585" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F585" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F585" Ref="R258"  Part="1" 
F 0 "R258" V 3000 1850 50  0000 L CNN
F 1 "165" V 3100 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 1900 50  0001 C CNN
F 3 "~" H 3100 1900 50  0001 C CNN
	1    3100 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F353
P 1650 2250
AR Path="/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F353" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F353" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 1650 2000 50  0001 C CNN
F 1 "GND" H 1655 2077 50  0000 C CNN
F 2 "" H 1650 2250 50  0001 C CNN
F 3 "" H 1650 2250 50  0001 C CNN
	1    1650 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 2050 2850 2150
Wire Wire Line
	3100 2050 3100 2150
Wire Wire Line
	3100 2150 2850 2150
Connection ~ 2850 2150
Wire Wire Line
	1150 1000 850  1000
Wire Wire Line
	600  2150 1150 2150
Wire Wire Line
	2150 850  2700 850 
$Comp
L Device:C C?
U 1 1 61A2F5CA
P 1150 1950
AR Path="/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5CA" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F5CA" Ref="C121"  Part="1" 
F 0 "C121" H 1150 2050 50  0000 L CNN
F 1 "2.2uF" H 1150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 1800 50  0001 C CNN
F 3 "~" H 1150 1950 50  0001 C CNN
	1    1150 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F5C0
P 2250 1950
AR Path="/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5C0" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F5C0" Ref="C122"  Part="1" 
F 0 "C122" H 2250 2050 50  0000 L CNN
F 1 "10uF" H 2250 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 1800 50  0001 C CNN
F 3 "~" H 2250 1950 50  0001 C CNN
	1    2250 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 2150 2850 2150
Connection ~ 2700 2150
Wire Wire Line
	2150 1150 2250 1150
$Comp
L Device:C C?
U 1 1 61A2F593
P 2500 1950
AR Path="/61A2F593" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F593" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F593" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F593" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F593" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F593" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F593" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F593" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F593" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F593" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F593" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F593" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F593" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F593" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F593" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F593" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F593" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F593" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F593" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F593" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F593" Ref="C123"  Part="1" 
F 0 "C123" H 2500 2050 50  0000 L CNN
F 1 "4.7uF" H 2500 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 1800 50  0001 C CNN
F 3 "~" H 2500 1950 50  0001 C CNN
	1    2500 1950
	1    0    0    1   
$EndComp
Connection ~ 2250 1150
Wire Wire Line
	2250 2100 2250 2150
Connection ~ 2250 2150
Wire Wire Line
	2250 2150 2500 2150
Wire Wire Line
	2500 2100 2500 2150
Connection ~ 2500 2150
Wire Wire Line
	2500 2150 2700 2150
Wire Wire Line
	1150 2100 1150 2150
Connection ~ 1150 2150
Wire Wire Line
	1150 2150 1650 2150
$Comp
L Device:R R?
U 1 1 61A2F5A8
P 2250 1550
AR Path="/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5A8" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F5A8" Ref="R246"  Part="1" 
F 0 "R246" V 2150 1500 50  0000 L CNN
F 1 "300m" V 2250 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 1550 50  0001 C CNN
F 3 "~" H 2250 1550 50  0001 C CNN
	1    2250 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F5B5
P 2500 1550
AR Path="/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5B5" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F5B5" Ref="R247"  Part="1" 
F 0 "R247" V 2400 1500 50  0000 L CNN
F 1 "300m" V 2500 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 1550 50  0001 C CNN
F 3 "~" H 2500 1550 50  0001 C CNN
	1    2500 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 1400 2250 1150
Wire Wire Line
	3100 1750 3100 1000
Wire Wire Line
	2700 2150 2700 850 
Wire Wire Line
	2250 1150 2850 1150
Wire Wire Line
	2250 1700 2250 1800
Wire Wire Line
	2500 1700 2500 1800
Wire Wire Line
	1650 1400 1650 2150
Connection ~ 1650 2150
Wire Wire Line
	1650 2150 2250 2150
Wire Wire Line
	2850 1750 2850 1150
$Comp
L Device:R R?
U 1 1 61A2F7B0
P 2850 1900
AR Path="/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7B0" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F7B0" Ref="R257"  Part="1" 
F 0 "R257" V 2750 1850 50  0000 L CNN
F 1 "23.2" V 2850 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 1900 50  0001 C CNN
F 3 "~" H 2850 1900 50  0001 C CNN
	1    2850 1900
	1    0    0    1   
$EndComp
Text HLabel 950  1950 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	1150 1800 1150 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F590
P 1650 950
AR Path="/61A2F590" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F590" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F590" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F590" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F590" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F590" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F590" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F590" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F590" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F590" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F590" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F590" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F590" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F590" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F590" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F590" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F590" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F590" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F590" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F590" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F590" Ref="U41"  Part="1" 
F 0 "U41" H 1750 1250 60  0000 C CNN
F 1 "LINPOL12V" H 1650 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 1000 260 0001 C CNN
F 3 "" V 1700 1000 260 0001 C CNN
	1    1650 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 2250 1650 2150
Wire Wire Line
	1150 1150 950  1150
Connection ~ 1150 1150
Wire Wire Line
	2500 1400 2500 1000
Wire Wire Line
	2150 1000 2500 1000
Wire Wire Line
	2500 1000 3100 1000
Connection ~ 2500 1000
Text Label 2250 800  1    50   ~ 0
1V4_41
Wire Wire Line
	2250 1150 2250 800 
Text Label 2500 800  1    50   ~ 0
3V3_41
Wire Wire Line
	2500 1000 2500 800 
$Comp
L Device:R R?
U 1 1 61A2F5DA
P 5750 1900
AR Path="/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F5DA" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F5DA" Ref="R260"  Part="1" 
F 0 "R260" V 5650 1850 50  0000 L CNN
F 1 "165" V 5750 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 1900 50  0001 C CNN
F 3 "~" H 5750 1900 50  0001 C CNN
	1    5750 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F5DD
P 4300 2250
AR Path="/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F5DD" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F5DD" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 4300 2000 50  0001 C CNN
F 1 "GND" H 4305 2077 50  0000 C CNN
F 2 "" H 4300 2250 50  0001 C CNN
F 3 "" H 4300 2250 50  0001 C CNN
	1    4300 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 2050 5500 2150
Wire Wire Line
	5750 2050 5750 2150
Wire Wire Line
	5750 2150 5500 2150
Connection ~ 5500 2150
Wire Wire Line
	3250 2150 3800 2150
Wire Wire Line
	4800 850  5350 850 
$Comp
L Device:C C?
U 1 1 61A2F5EA
P 3800 1950
AR Path="/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F5EA" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F5EA" Ref="C124"  Part="1" 
F 0 "C124" H 3800 2050 50  0000 L CNN
F 1 "2.2uF" H 3800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 1800 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F3F1
P 4900 1950
AR Path="/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F3F1" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F3F1" Ref="C125"  Part="1" 
F 0 "C125" H 4900 2050 50  0000 L CNN
F 1 "10uF" H 4900 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 1800 50  0001 C CNN
F 3 "~" H 4900 1950 50  0001 C CNN
	1    4900 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 2150 5500 2150
Connection ~ 5350 2150
Wire Wire Line
	4800 1150 4900 1150
$Comp
L Device:C C?
U 1 1 6195FCC4
P 5150 1950
AR Path="/6195FCC4" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FCC4" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FCC4" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FCC4" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FCC4" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FCC4" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FCC4" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FCC4" Ref="C?"  Part="1" 
AR Path="/61849340/6195FCC4" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FCC4" Ref="C?"  Part="1" 
AR Path="/61867626/6195FCC4" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FCC4" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FCC4" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FCC4" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FCC4" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FCC4" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FCC4" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FCC4" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FCC4" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FCC4" Ref="C?"  Part="1" 
AR Path="/607ECD6D/6195FCC4" Ref="C126"  Part="1" 
F 0 "C126" H 5150 2050 50  0000 L CNN
F 1 "4.7uF" H 5150 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 1800 50  0001 C CNN
F 3 "~" H 5150 1950 50  0001 C CNN
	1    5150 1950
	1    0    0    1   
$EndComp
Connection ~ 4900 1150
Wire Wire Line
	4900 2100 4900 2150
Connection ~ 4900 2150
Wire Wire Line
	4900 2150 5150 2150
Wire Wire Line
	5150 2100 5150 2150
Connection ~ 5150 2150
Wire Wire Line
	5150 2150 5350 2150
Wire Wire Line
	3800 2100 3800 2150
Connection ~ 3800 2150
Wire Wire Line
	3800 2150 4300 2150
$Comp
L Device:R R?
U 1 1 61A2F3FF
P 4900 1550
AR Path="/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3FF" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3FF" Ref="R249"  Part="1" 
F 0 "R249" V 4800 1500 50  0000 L CNN
F 1 "300m" V 4900 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 1550 50  0001 C CNN
F 3 "~" H 4900 1550 50  0001 C CNN
	1    4900 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F40E
P 5150 1550
AR Path="/61A2F40E" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F40E" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F40E" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F40E" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F40E" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F40E" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F40E" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F40E" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F40E" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F40E" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F40E" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F40E" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F40E" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F40E" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F40E" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F40E" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F40E" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F40E" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F40E" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F40E" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F40E" Ref="R250"  Part="1" 
F 0 "R250" V 5050 1500 50  0000 L CNN
F 1 "300m" V 5150 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 1550 50  0001 C CNN
F 3 "~" H 5150 1550 50  0001 C CNN
	1    5150 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 1400 4900 1150
Wire Wire Line
	5750 1750 5750 1000
Wire Wire Line
	5350 2150 5350 850 
Wire Wire Line
	4900 1150 5500 1150
Wire Wire Line
	4900 1700 4900 1800
Wire Wire Line
	5150 1700 5150 1800
Wire Wire Line
	4300 1400 4300 2150
Connection ~ 4300 2150
Wire Wire Line
	4300 2150 4900 2150
Wire Wire Line
	5500 1750 5500 1150
$Comp
L Device:R R?
U 1 1 61A2F601
P 5500 1900
AR Path="/61A2F601" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F601" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F601" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F601" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F601" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F601" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F601" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F601" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F601" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F601" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F601" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F601" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F601" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F601" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F601" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F601" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F601" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F601" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F601" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F601" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F601" Ref="R259"  Part="1" 
F 0 "R259" V 5400 1850 50  0000 L CNN
F 1 "20" V 5500 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 1900 50  0001 C CNN
F 3 "~" H 5500 1900 50  0001 C CNN
	1    5500 1900
	1    0    0    1   
$EndComp
Text HLabel 3600 1950 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	3800 1800 3800 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F316
P 4300 950
AR Path="/61A2F316" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F316" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F316" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F316" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F316" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F316" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F316" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F316" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F316" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F316" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F316" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F316" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F316" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F316" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F316" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F316" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F316" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F316" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F316" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F316" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F316" Ref="U42"  Part="1" 
F 0 "U42" H 4400 1250 60  0000 C CNN
F 1 "LINPOL12V" H 4300 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 1000 260 0001 C CNN
F 3 "" V 4350 1000 260 0001 C CNN
	1    4300 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 2250 4300 2150
Wire Wire Line
	3800 1150 3600 1150
Connection ~ 3800 1150
Wire Wire Line
	5150 1400 5150 1000
Wire Wire Line
	4800 1000 5150 1000
Wire Wire Line
	5150 1000 5750 1000
Connection ~ 5150 1000
Text Label 4900 800  1    50   ~ 0
1V4_42
Wire Wire Line
	4900 1150 4900 800 
Text Label 5150 800  1    50   ~ 0
3V3_42
Wire Wire Line
	5150 1000 5150 800 
$Comp
L Device:R R?
U 1 1 61A2F616
P 8400 1900
AR Path="/61A2F616" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F616" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F616" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F616" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F616" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F616" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F616" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F616" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F616" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F616" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F616" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F616" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F616" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F616" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F616" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F616" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F616" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F616" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F616" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F616" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F616" Ref="R262"  Part="1" 
F 0 "R262" V 8300 1850 50  0000 L CNN
F 1 "165" V 8400 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 1900 50  0001 C CNN
F 3 "~" H 8400 1900 50  0001 C CNN
	1    8400 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F61B
P 6950 2250
AR Path="/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F61B" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F61B" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 6950 2000 50  0001 C CNN
F 1 "GND" H 6955 2077 50  0000 C CNN
F 2 "" H 6950 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 2050 8150 2150
Wire Wire Line
	8400 2050 8400 2150
Wire Wire Line
	8400 2150 8150 2150
Connection ~ 8150 2150
Wire Wire Line
	5900 2150 6450 2150
Wire Wire Line
	7450 850  8000 850 
$Comp
L Device:C C?
U 1 1 61A2F31B
P 6450 1950
AR Path="/61A2F31B" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F31B" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F31B" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F31B" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F31B" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F31B" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F31B" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F31B" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F31B" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F31B" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F31B" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F31B" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F31B" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F31B" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F31B" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F31B" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F31B" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F31B" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F31B" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F31B" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F31B" Ref="C127"  Part="1" 
F 0 "C127" H 6450 2050 50  0000 L CNN
F 1 "2.2uF" H 6450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 1800 50  0001 C CNN
F 3 "~" H 6450 1950 50  0001 C CNN
	1    6450 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F62D
P 7550 1950
AR Path="/61A2F62D" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F62D" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F62D" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F62D" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F62D" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F62D" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F62D" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F62D" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F62D" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F62D" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F62D" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F62D" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F62D" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F62D" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F62D" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F62D" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F62D" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F62D" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F62D" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F62D" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F62D" Ref="C128"  Part="1" 
F 0 "C128" H 7550 2050 50  0000 L CNN
F 1 "10uF" H 7550 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 1800 50  0001 C CNN
F 3 "~" H 7550 1950 50  0001 C CNN
	1    7550 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 2150 8150 2150
Connection ~ 8000 2150
Wire Wire Line
	7450 1150 7550 1150
$Comp
L Device:C C?
U 1 1 61A2F632
P 7800 1950
AR Path="/61A2F632" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F632" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F632" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F632" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F632" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F632" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F632" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F632" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F632" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F632" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F632" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F632" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F632" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F632" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F632" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F632" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F632" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F632" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F632" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F632" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F632" Ref="C129"  Part="1" 
F 0 "C129" H 7800 2050 50  0000 L CNN
F 1 "4.7uF" H 7800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 1800 50  0001 C CNN
F 3 "~" H 7800 1950 50  0001 C CNN
	1    7800 1950
	1    0    0    1   
$EndComp
Connection ~ 7550 1150
Wire Wire Line
	7550 2100 7550 2150
Connection ~ 7550 2150
Wire Wire Line
	7550 2150 7800 2150
Wire Wire Line
	7800 2100 7800 2150
Connection ~ 7800 2150
Wire Wire Line
	7800 2150 8000 2150
Wire Wire Line
	6450 2100 6450 2150
Connection ~ 6450 2150
Wire Wire Line
	6450 2150 6950 2150
$Comp
L Device:R R?
U 1 1 61A2F646
P 7550 1550
AR Path="/61A2F646" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F646" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F646" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F646" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F646" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F646" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F646" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F646" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F646" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F646" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F646" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F646" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F646" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F646" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F646" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F646" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F646" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F646" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F646" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F646" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F646" Ref="R252"  Part="1" 
F 0 "R252" V 7450 1500 50  0000 L CNN
F 1 "300m" V 7550 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 1550 50  0001 C CNN
F 3 "~" H 7550 1550 50  0001 C CNN
	1    7550 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F64A
P 7800 1550
AR Path="/61A2F64A" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F64A" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F64A" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F64A" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F64A" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F64A" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F64A" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F64A" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F64A" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F64A" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F64A" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F64A" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F64A" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F64A" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F64A" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F64A" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F64A" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F64A" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F64A" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F64A" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F64A" Ref="R253"  Part="1" 
F 0 "R253" V 7700 1500 50  0000 L CNN
F 1 "300m" V 7800 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 1550 50  0001 C CNN
F 3 "~" H 7800 1550 50  0001 C CNN
	1    7800 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 1400 7550 1150
Wire Wire Line
	8400 1750 8400 1000
Wire Wire Line
	8000 2150 8000 850 
Wire Wire Line
	7550 1150 8150 1150
Wire Wire Line
	7550 1700 7550 1800
Wire Wire Line
	7800 1700 7800 1800
Wire Wire Line
	6950 1400 6950 2150
Connection ~ 6950 2150
Wire Wire Line
	6950 2150 7550 2150
Wire Wire Line
	8150 1750 8150 1150
$Comp
L Device:R R?
U 1 1 61A2F41B
P 8150 1900
AR Path="/61A2F41B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F41B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F41B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F41B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F41B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F41B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F41B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F41B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F41B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F41B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F41B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F41B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F41B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F41B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F41B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F41B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F41B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F41B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F41B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F41B" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F41B" Ref="R261"  Part="1" 
F 0 "R261" V 8050 1850 50  0000 L CNN
F 1 "17.4" V 8150 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 1900 50  0001 C CNN
F 3 "~" H 8150 1900 50  0001 C CNN
	1    8150 1900
	1    0    0    1   
$EndComp
Text HLabel 6250 1950 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	6450 1800 6450 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F41D
P 6950 950
AR Path="/61A2F41D" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F41D" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F41D" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F41D" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F41D" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F41D" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F41D" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F41D" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F41D" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F41D" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F41D" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F41D" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F41D" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F41D" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F41D" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F41D" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F41D" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F41D" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F41D" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F41D" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F41D" Ref="U43"  Part="1" 
F 0 "U43" H 7050 1250 60  0000 C CNN
F 1 "LINPOL12V" H 6950 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 1000 260 0001 C CNN
F 3 "" V 7000 1000 260 0001 C CNN
	1    6950 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 2250 6950 2150
Wire Wire Line
	6450 1150 6250 1150
Connection ~ 6450 1150
Wire Wire Line
	7800 1400 7800 1000
Wire Wire Line
	7450 1000 7800 1000
Wire Wire Line
	7800 1000 8400 1000
Connection ~ 7800 1000
Text Label 7550 800  1    50   ~ 0
1V4_43
Wire Wire Line
	7550 1150 7550 800 
Text Label 7800 800  1    50   ~ 0
3V3_43
Wire Wire Line
	7800 1000 7800 800 
$Comp
L Device:R R?
U 1 1 61A2F654
P 11050 1900
AR Path="/61A2F654" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F654" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F654" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F654" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F654" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F654" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F654" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F654" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F654" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F654" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F654" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F654" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F654" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F654" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F654" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F654" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F654" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F654" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F654" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F654" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F654" Ref="R264"  Part="1" 
F 0 "R264" V 10950 1850 50  0000 L CNN
F 1 "165" V 11050 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 1900 50  0001 C CNN
F 3 "~" H 11050 1900 50  0001 C CNN
	1    11050 1900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F42D
P 9600 2250
AR Path="/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F42D" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F42D" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 9600 2000 50  0001 C CNN
F 1 "GND" H 9605 2077 50  0000 C CNN
F 2 "" H 9600 2250 50  0001 C CNN
F 3 "" H 9600 2250 50  0001 C CNN
	1    9600 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 2050 10800 2150
Wire Wire Line
	11050 2050 11050 2150
Wire Wire Line
	11050 2150 10800 2150
Connection ~ 10800 2150
Wire Wire Line
	8550 2150 9100 2150
Wire Wire Line
	10100 850  10650 850 
$Comp
L Device:C C?
U 1 1 61A2F43E
P 9100 1950
AR Path="/61A2F43E" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F43E" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F43E" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F43E" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F43E" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F43E" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F43E" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F43E" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F43E" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F43E" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F43E" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F43E" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F43E" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F43E" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F43E" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F43E" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F43E" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F43E" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F43E" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F43E" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F43E" Ref="C130"  Part="1" 
F 0 "C130" H 9100 2050 50  0000 L CNN
F 1 "2.2uF" H 9100 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 1800 50  0001 C CNN
F 3 "~" H 9100 1950 50  0001 C CNN
	1    9100 1950
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F44A
P 10200 1950
AR Path="/61A2F44A" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F44A" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F44A" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F44A" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F44A" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F44A" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F44A" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F44A" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F44A" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F44A" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F44A" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F44A" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F44A" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F44A" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F44A" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F44A" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F44A" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F44A" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F44A" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F44A" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F44A" Ref="C131"  Part="1" 
F 0 "C131" H 10200 2050 50  0000 L CNN
F 1 "10uF" H 10200 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 1800 50  0001 C CNN
F 3 "~" H 10200 1950 50  0001 C CNN
	1    10200 1950
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 2150 10800 2150
Connection ~ 10650 2150
Wire Wire Line
	10100 1150 10200 1150
$Comp
L Device:C C?
U 1 1 61A2F660
P 10450 1950
AR Path="/61A2F660" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F660" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F660" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F660" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F660" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F660" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F660" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F660" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F660" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F660" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F660" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F660" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F660" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F660" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F660" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F660" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F660" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F660" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F660" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F660" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F660" Ref="C132"  Part="1" 
F 0 "C132" H 10450 2050 50  0000 L CNN
F 1 "4.7uF" H 10450 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1800 50  0001 C CNN
F 3 "~" H 10450 1950 50  0001 C CNN
	1    10450 1950
	1    0    0    1   
$EndComp
Connection ~ 10200 1150
Wire Wire Line
	10200 2100 10200 2150
Connection ~ 10200 2150
Wire Wire Line
	10200 2150 10450 2150
Wire Wire Line
	10450 2100 10450 2150
Connection ~ 10450 2150
Wire Wire Line
	10450 2150 10650 2150
Wire Wire Line
	9100 2100 9100 2150
Connection ~ 9100 2150
Wire Wire Line
	9100 2150 9600 2150
$Comp
L Device:R R?
U 1 1 61A2F455
P 10200 1550
AR Path="/61A2F455" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F455" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F455" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F455" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F455" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F455" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F455" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F455" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F455" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F455" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F455" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F455" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F455" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F455" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F455" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F455" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F455" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F455" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F455" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F455" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F455" Ref="R255"  Part="1" 
F 0 "R255" V 10100 1500 50  0000 L CNN
F 1 "300m" V 10200 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 1550 50  0001 C CNN
F 3 "~" H 10200 1550 50  0001 C CNN
	1    10200 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F45A
P 10450 1550
AR Path="/61A2F45A" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F45A" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F45A" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F45A" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F45A" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F45A" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F45A" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F45A" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F45A" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F45A" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F45A" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F45A" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F45A" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F45A" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F45A" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F45A" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F45A" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F45A" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F45A" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F45A" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F45A" Ref="R256"  Part="1" 
F 0 "R256" V 10350 1500 50  0000 L CNN
F 1 "300m" V 10450 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 1550 50  0001 C CNN
F 3 "~" H 10450 1550 50  0001 C CNN
	1    10450 1550
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 1400 10200 1150
Wire Wire Line
	11050 1750 11050 1000
Wire Wire Line
	10650 2150 10650 850 
Wire Wire Line
	10200 1150 10800 1150
Wire Wire Line
	10200 1700 10200 1800
Wire Wire Line
	10450 1700 10450 1800
Wire Wire Line
	9600 1400 9600 2150
Connection ~ 9600 2150
Wire Wire Line
	9600 2150 10200 2150
Wire Wire Line
	10800 1750 10800 1150
$Comp
L Device:R R?
U 1 1 61A2F673
P 10800 1900
AR Path="/61A2F673" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F673" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F673" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F673" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F673" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F673" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F673" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F673" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F673" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F673" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F673" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F673" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F673" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F673" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F673" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F673" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F673" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F673" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F673" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F673" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F673" Ref="R263"  Part="1" 
F 0 "R263" V 10700 1850 50  0000 L CNN
F 1 "17.4" V 10800 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 1900 50  0001 C CNN
F 3 "~" H 10800 1900 50  0001 C CNN
	1    10800 1900
	1    0    0    1   
$EndComp
Text HLabel 8900 1950 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	9100 1800 9100 1150
$Comp
L linpol:LREG U?
U 1 1 61A2F67B
P 9600 950
AR Path="/61A2F67B" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F67B" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F67B" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F67B" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F67B" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F67B" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F67B" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F67B" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F67B" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F67B" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F67B" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F67B" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F67B" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F67B" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F67B" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F67B" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F67B" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F67B" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F67B" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F67B" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F67B" Ref="U44"  Part="1" 
F 0 "U44" H 9700 1250 60  0000 C CNN
F 1 "LINPOL12V" H 9600 600 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 1000 260 0001 C CNN
F 3 "" V 9650 1000 260 0001 C CNN
	1    9600 950 
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 2250 9600 2150
Wire Wire Line
	9100 1150 8900 1150
Connection ~ 9100 1150
Wire Wire Line
	10450 1400 10450 1000
Wire Wire Line
	10100 1000 10450 1000
Wire Wire Line
	10450 1000 11050 1000
Connection ~ 10450 1000
Text Label 10200 800  1    50   ~ 0
1V4_44
Wire Wire Line
	10200 1150 10200 800 
Text Label 10450 800  1    50   ~ 0
3V3_44
Wire Wire Line
	10450 1000 10450 800 
$Comp
L Device:R R?
U 1 1 61A2F464
P 3100 3800
AR Path="/61A2F464" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F464" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F464" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F464" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F464" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F464" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F464" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F464" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F464" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F464" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F464" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F464" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F464" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F464" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F464" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F464" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F464" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F464" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F464" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F464" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F464" Ref="R282"  Part="1" 
F 0 "R282" V 3000 3750 50  0000 L CNN
F 1 "165" V 3100 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 3800 50  0001 C CNN
F 3 "~" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F342
P 1650 4150
AR Path="/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F342" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F342" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 1650 3900 50  0001 C CNN
F 1 "GND" H 1655 3977 50  0000 C CNN
F 2 "" H 1650 4150 50  0001 C CNN
F 3 "" H 1650 4150 50  0001 C CNN
	1    1650 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 3950 2850 4050
Wire Wire Line
	3100 3950 3100 4050
Wire Wire Line
	3100 4050 2850 4050
Connection ~ 2850 4050
Wire Wire Line
	600  4050 1150 4050
Wire Wire Line
	2150 2750 2700 2750
$Comp
L Device:C C?
U 1 1 61A2F373
P 1150 3850
AR Path="/61A2F373" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F373" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F373" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F373" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F373" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F373" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F373" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F373" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F373" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F373" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F373" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F373" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F373" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F373" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F373" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F373" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F373" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F373" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F373" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F373" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F373" Ref="C133"  Part="1" 
F 0 "C133" H 1150 3950 50  0000 L CNN
F 1 "2.2uF" H 1150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 3700 50  0001 C CNN
F 3 "~" H 1150 3850 50  0001 C CNN
	1    1150 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60A177E9
P 2250 3850
AR Path="/60A177E9" Ref="C?"  Part="1" 
AR Path="/606A40CC/60A177E9" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/60A177E9" Ref="C?"  Part="1" 
AR Path="/60A71FE2/60A177E9" Ref="C?"  Part="1" 
AR Path="/60A749DF/60A177E9" Ref="C?"  Part="1" 
AR Path="/60A76681/60A177E9" Ref="C?"  Part="1" 
AR Path="/6182C19D/60A177E9" Ref="C?"  Part="1" 
AR Path="/6183A7C1/60A177E9" Ref="C?"  Part="1" 
AR Path="/61849340/60A177E9" Ref="C?"  Part="1" 
AR Path="/61858A9D/60A177E9" Ref="C?"  Part="1" 
AR Path="/61867626/60A177E9" Ref="C?"  Part="1" 
AR Path="/6187640F/60A177E9" Ref="C?"  Part="1" 
AR Path="/618857F1/60A177E9" Ref="C?"  Part="1" 
AR Path="/61894FBC/60A177E9" Ref="C?"  Part="1" 
AR Path="/618A4CF2/60A177E9" Ref="C?"  Part="1" 
AR Path="/618E2B02/60A177E9" Ref="C?"  Part="1" 
AR Path="/61987B22/60A177E9" Ref="C?"  Part="1" 
AR Path="/607CBABF/60A177E9" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/60A177E9" Ref="C?"  Part="1" 
AR Path="/607AA45D/60A177E9" Ref="C?"  Part="1" 
AR Path="/607ECD6D/60A177E9" Ref="C134"  Part="1" 
F 0 "C134" H 2250 3950 50  0000 L CNN
F 1 "10uF" H 2250 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 3700 50  0001 C CNN
F 3 "~" H 2250 3850 50  0001 C CNN
	1    2250 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 4050 2850 4050
Connection ~ 2700 4050
Wire Wire Line
	2150 3050 2250 3050
$Comp
L Device:C C?
U 1 1 61A2F68D
P 2500 3850
AR Path="/61A2F68D" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F68D" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F68D" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F68D" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F68D" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F68D" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F68D" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F68D" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F68D" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F68D" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F68D" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F68D" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F68D" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F68D" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F68D" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F68D" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F68D" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F68D" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F68D" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F68D" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F68D" Ref="C135"  Part="1" 
F 0 "C135" H 2500 3950 50  0000 L CNN
F 1 "4.7uF" H 2500 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 3700 50  0001 C CNN
F 3 "~" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    1   
$EndComp
Connection ~ 2250 3050
Wire Wire Line
	2250 4000 2250 4050
Connection ~ 2250 4050
Wire Wire Line
	2250 4050 2500 4050
Wire Wire Line
	2500 4000 2500 4050
Connection ~ 2500 4050
Wire Wire Line
	2500 4050 2700 4050
Wire Wire Line
	1150 4000 1150 4050
Connection ~ 1150 4050
Wire Wire Line
	1150 4050 1650 4050
$Comp
L Device:R R?
U 1 1 61A2F690
P 2250 3450
AR Path="/61A2F690" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F690" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F690" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F690" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F690" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F690" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F690" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F690" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F690" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F690" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F690" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F690" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F690" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F690" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F690" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F690" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F690" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F690" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F690" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F690" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F690" Ref="R270"  Part="1" 
F 0 "R270" V 2150 3400 50  0000 L CNN
F 1 "300m" V 2250 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F69F
P 2500 3450
AR Path="/61A2F69F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F69F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F69F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F69F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F69F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F69F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F69F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F69F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F69F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F69F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F69F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F69F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F69F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F69F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F69F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F69F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F69F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F69F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F69F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F69F" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F69F" Ref="R271"  Part="1" 
F 0 "R271" V 2400 3400 50  0000 L CNN
F 1 "300m" V 2500 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 3300 2250 3050
Wire Wire Line
	3100 3650 3100 2900
Wire Wire Line
	2700 4050 2700 2750
Wire Wire Line
	2250 3050 2850 3050
Wire Wire Line
	2250 3600 2250 3700
Wire Wire Line
	2500 3600 2500 3700
Wire Wire Line
	1650 3300 1650 4050
Connection ~ 1650 4050
Wire Wire Line
	1650 4050 2250 4050
Wire Wire Line
	2850 3650 2850 3050
$Comp
L Device:R R?
U 1 1 61A2F6B1
P 2850 3800
AR Path="/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6B1" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F6B1" Ref="R281"  Part="1" 
F 0 "R281" V 2750 3750 50  0000 L CNN
F 1 "17.4" V 2850 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 3800 50  0001 C CNN
F 3 "~" H 2850 3800 50  0001 C CNN
	1    2850 3800
	1    0    0    1   
$EndComp
Text HLabel 950  3850 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	1150 3700 1150 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F47B
P 1650 2850
AR Path="/61A2F47B" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F47B" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F47B" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F47B" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F47B" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F47B" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F47B" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F47B" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F47B" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F47B" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F47B" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F47B" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F47B" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F47B" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F47B" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F47B" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F47B" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F47B" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F47B" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F47B" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F47B" Ref="U45"  Part="1" 
F 0 "U45" H 1750 3150 60  0000 C CNN
F 1 "LINPOL12V" H 1650 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 2900 260 0001 C CNN
F 3 "" V 1700 2900 260 0001 C CNN
	1    1650 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 4150 1650 4050
Wire Wire Line
	1150 3050 950  3050
Connection ~ 1150 3050
Wire Wire Line
	2500 3300 2500 2900
Wire Wire Line
	2150 2900 2500 2900
Wire Wire Line
	2500 2900 3100 2900
Connection ~ 2500 2900
Text Label 2250 2700 1    50   ~ 0
1V4_45
Wire Wire Line
	2250 3050 2250 2700
Text Label 2500 2700 1    50   ~ 0
3V3_45
Wire Wire Line
	2500 2900 2500 2700
$Comp
L Device:R R?
U 1 1 61A2F6B6
P 5750 3800
AR Path="/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6B6" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F6B6" Ref="R284"  Part="1" 
F 0 "R284" V 5650 3750 50  0000 L CNN
F 1 "165" V 5750 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3800 50  0001 C CNN
F 3 "~" H 5750 3800 50  0001 C CNN
	1    5750 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F6C2
P 4300 4150
AR Path="/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F6C2" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F6C2" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 4300 3900 50  0001 C CNN
F 1 "GND" H 4305 3977 50  0000 C CNN
F 2 "" H 4300 4150 50  0001 C CNN
F 3 "" H 4300 4150 50  0001 C CNN
	1    4300 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 3950 5500 4050
Wire Wire Line
	5750 3950 5750 4050
Wire Wire Line
	5750 4050 5500 4050
Connection ~ 5500 4050
Wire Wire Line
	3250 4050 3800 4050
Wire Wire Line
	4800 2750 5350 2750
$Comp
L Device:C C?
U 1 1 61A2F47C
P 3800 3850
AR Path="/61A2F47C" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F47C" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F47C" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F47C" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F47C" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F47C" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F47C" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F47C" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F47C" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F47C" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F47C" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F47C" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F47C" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F47C" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F47C" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F47C" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F47C" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F47C" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F47C" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F47C" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F47C" Ref="C136"  Part="1" 
F 0 "C136" H 3800 3950 50  0000 L CNN
F 1 "2.2uF" H 3800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 3700 50  0001 C CNN
F 3 "~" H 3800 3850 50  0001 C CNN
	1    3800 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F78A
P 4900 3850
AR Path="/61A2F78A" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F78A" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F78A" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F78A" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F78A" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F78A" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F78A" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F78A" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F78A" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F78A" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F78A" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F78A" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F78A" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F78A" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F78A" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F78A" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F78A" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F78A" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F78A" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F78A" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F78A" Ref="C137"  Part="1" 
F 0 "C137" H 4900 3950 50  0000 L CNN
F 1 "10uF" H 4900 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 3700 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 4050 5500 4050
Connection ~ 5350 4050
Wire Wire Line
	4800 3050 4900 3050
$Comp
L Device:C C?
U 1 1 6195FE86
P 5150 3850
AR Path="/6195FE86" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FE86" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FE86" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FE86" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FE86" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FE86" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FE86" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FE86" Ref="C?"  Part="1" 
AR Path="/61849340/6195FE86" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FE86" Ref="C?"  Part="1" 
AR Path="/61867626/6195FE86" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FE86" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FE86" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FE86" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FE86" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FE86" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FE86" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FE86" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FE86" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FE86" Ref="C?"  Part="1" 
AR Path="/607ECD6D/6195FE86" Ref="C138"  Part="1" 
F 0 "C138" H 5150 3950 50  0000 L CNN
F 1 "4.7uF" H 5150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 3700 50  0001 C CNN
F 3 "~" H 5150 3850 50  0001 C CNN
	1    5150 3850
	1    0    0    1   
$EndComp
Connection ~ 4900 3050
Wire Wire Line
	4900 4000 4900 4050
Connection ~ 4900 4050
Wire Wire Line
	4900 4050 5150 4050
Wire Wire Line
	5150 4000 5150 4050
Connection ~ 5150 4050
Wire Wire Line
	5150 4050 5350 4050
Wire Wire Line
	3800 4000 3800 4050
Connection ~ 3800 4050
Wire Wire Line
	3800 4050 4300 4050
$Comp
L Device:R R?
U 1 1 61A2F6D5
P 4900 3450
AR Path="/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6D5" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F6D5" Ref="R273"  Part="1" 
F 0 "R273" V 4800 3400 50  0000 L CNN
F 1 "300m" V 4900 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 3450 50  0001 C CNN
F 3 "~" H 4900 3450 50  0001 C CNN
	1    4900 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F6D8
P 5150 3450
AR Path="/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6D8" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F6D8" Ref="R274"  Part="1" 
F 0 "R274" V 5050 3400 50  0000 L CNN
F 1 "300m" V 5150 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 3450 50  0001 C CNN
F 3 "~" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 3300 4900 3050
Wire Wire Line
	5750 3650 5750 2900
Wire Wire Line
	5350 4050 5350 2750
Wire Wire Line
	4900 3050 5500 3050
Wire Wire Line
	4900 3600 4900 3700
Wire Wire Line
	5150 3600 5150 3700
Wire Wire Line
	4300 3300 4300 4050
Connection ~ 4300 4050
Wire Wire Line
	4300 4050 4900 4050
Wire Wire Line
	5500 3650 5500 3050
$Comp
L Device:R R?
U 1 1 6195FDBD
P 5500 3800
AR Path="/6195FDBD" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FDBD" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FDBD" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FDBD" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FDBD" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FDBD" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FDBD" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FDBD" Ref="R?"  Part="1" 
AR Path="/61849340/6195FDBD" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FDBD" Ref="R?"  Part="1" 
AR Path="/61867626/6195FDBD" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FDBD" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FDBD" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FDBD" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FDBD" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FDBD" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FDBD" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FDBD" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FDBD" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FDBD" Ref="R?"  Part="1" 
AR Path="/607ECD6D/6195FDBD" Ref="R283"  Part="1" 
F 0 "R283" V 5400 3750 50  0000 L CNN
F 1 "17.4" V 5500 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 3800 50  0001 C CNN
F 3 "~" H 5500 3800 50  0001 C CNN
	1    5500 3800
	1    0    0    1   
$EndComp
Text HLabel 3600 3850 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	3800 3700 3800 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F490
P 4300 2850
AR Path="/61A2F490" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F490" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F490" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F490" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F490" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F490" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F490" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F490" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F490" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F490" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F490" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F490" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F490" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F490" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F490" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F490" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F490" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F490" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F490" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F490" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F490" Ref="U46"  Part="1" 
F 0 "U46" H 4400 3150 60  0000 C CNN
F 1 "LINPOL12V" H 4300 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 2900 260 0001 C CNN
F 3 "" V 4350 2900 260 0001 C CNN
	1    4300 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 4150 4300 4050
Wire Wire Line
	3800 3050 3600 3050
Connection ~ 3800 3050
Wire Wire Line
	5150 3300 5150 2900
Wire Wire Line
	4800 2900 5150 2900
Wire Wire Line
	5150 2900 5750 2900
Connection ~ 5150 2900
Text Label 4900 2700 1    50   ~ 0
1V4_46
Wire Wire Line
	4900 3050 4900 2700
Text Label 5150 2700 1    50   ~ 0
3V3_46
Wire Wire Line
	5150 2900 5150 2700
$Comp
L Device:R R?
U 1 1 61A2F6F3
P 8400 3800
AR Path="/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F6F3" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F6F3" Ref="R286"  Part="1" 
F 0 "R286" V 8300 3750 50  0000 L CNN
F 1 "165" V 8400 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8330 3800 50  0001 C CNN
F 3 "~" H 8400 3800 50  0001 C CNN
	1    8400 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6195FB5B
P 6950 4150
AR Path="/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/61849340/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/61867626/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/6195FB5B" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/6195FB5B" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 6950 3900 50  0001 C CNN
F 1 "GND" H 6955 3977 50  0000 C CNN
F 2 "" H 6950 4150 50  0001 C CNN
F 3 "" H 6950 4150 50  0001 C CNN
	1    6950 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 3950 8150 4050
Wire Wire Line
	8400 3950 8400 4050
Wire Wire Line
	8400 4050 8150 4050
Connection ~ 8150 4050
Wire Wire Line
	5900 4050 6450 4050
Wire Wire Line
	7450 2750 8000 2750
$Comp
L Device:C C?
U 1 1 61A2F4A6
P 6450 3850
AR Path="/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4A6" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F4A6" Ref="C139"  Part="1" 
F 0 "C139" H 6450 3950 50  0000 L CNN
F 1 "2.2uF" H 6450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 3700 50  0001 C CNN
F 3 "~" H 6450 3850 50  0001 C CNN
	1    6450 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 6195FB76
P 7550 3850
AR Path="/6195FB76" Ref="C?"  Part="1" 
AR Path="/606A40CC/6195FB76" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/6195FB76" Ref="C?"  Part="1" 
AR Path="/60A71FE2/6195FB76" Ref="C?"  Part="1" 
AR Path="/60A749DF/6195FB76" Ref="C?"  Part="1" 
AR Path="/60A76681/6195FB76" Ref="C?"  Part="1" 
AR Path="/6182C19D/6195FB76" Ref="C?"  Part="1" 
AR Path="/6183A7C1/6195FB76" Ref="C?"  Part="1" 
AR Path="/61849340/6195FB76" Ref="C?"  Part="1" 
AR Path="/61858A9D/6195FB76" Ref="C?"  Part="1" 
AR Path="/61867626/6195FB76" Ref="C?"  Part="1" 
AR Path="/6187640F/6195FB76" Ref="C?"  Part="1" 
AR Path="/618857F1/6195FB76" Ref="C?"  Part="1" 
AR Path="/61894FBC/6195FB76" Ref="C?"  Part="1" 
AR Path="/618A4CF2/6195FB76" Ref="C?"  Part="1" 
AR Path="/618E2B02/6195FB76" Ref="C?"  Part="1" 
AR Path="/61987B22/6195FB76" Ref="C?"  Part="1" 
AR Path="/607CBABF/6195FB76" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB76" Ref="C?"  Part="1" 
AR Path="/607AA45D/6195FB76" Ref="C?"  Part="1" 
AR Path="/607ECD6D/6195FB76" Ref="C140"  Part="1" 
F 0 "C140" H 7550 3950 50  0000 L CNN
F 1 "10uF" H 7550 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 3700 50  0001 C CNN
F 3 "~" H 7550 3850 50  0001 C CNN
	1    7550 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 4050 8150 4050
Connection ~ 8000 4050
Wire Wire Line
	7450 3050 7550 3050
$Comp
L Device:C C?
U 1 1 61A2F6FD
P 7800 3850
AR Path="/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F6FD" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F6FD" Ref="C141"  Part="1" 
F 0 "C141" H 7800 3950 50  0000 L CNN
F 1 "4.7uF" H 7800 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 3700 50  0001 C CNN
F 3 "~" H 7800 3850 50  0001 C CNN
	1    7800 3850
	1    0    0    1   
$EndComp
Connection ~ 7550 3050
Wire Wire Line
	7550 4000 7550 4050
Connection ~ 7550 4050
Wire Wire Line
	7550 4050 7800 4050
Wire Wire Line
	7800 4000 7800 4050
Connection ~ 7800 4050
Wire Wire Line
	7800 4050 8000 4050
Wire Wire Line
	6450 4000 6450 4050
Connection ~ 6450 4050
Wire Wire Line
	6450 4050 6950 4050
$Comp
L Device:R R?
U 1 1 61A2F711
P 7550 3450
AR Path="/61A2F711" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F711" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F711" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F711" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F711" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F711" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F711" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F711" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F711" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F711" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F711" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F711" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F711" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F711" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F711" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F711" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F711" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F711" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F711" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F711" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F711" Ref="R276"  Part="1" 
F 0 "R276" V 7450 3400 50  0000 L CNN
F 1 "300m" V 7550 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7480 3450 50  0001 C CNN
F 3 "~" H 7550 3450 50  0001 C CNN
	1    7550 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F71D
P 7800 3450
AR Path="/61A2F71D" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F71D" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F71D" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F71D" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F71D" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F71D" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F71D" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F71D" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F71D" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F71D" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F71D" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F71D" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F71D" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F71D" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F71D" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F71D" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F71D" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F71D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F71D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F71D" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F71D" Ref="R277"  Part="1" 
F 0 "R277" V 7700 3400 50  0000 L CNN
F 1 "300m" V 7800 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7730 3450 50  0001 C CNN
F 3 "~" H 7800 3450 50  0001 C CNN
	1    7800 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	7550 3300 7550 3050
Wire Wire Line
	8400 3650 8400 2900
Wire Wire Line
	8000 4050 8000 2750
Wire Wire Line
	7550 3050 8150 3050
Wire Wire Line
	7550 3600 7550 3700
Wire Wire Line
	7800 3600 7800 3700
Wire Wire Line
	6950 3300 6950 4050
Connection ~ 6950 4050
Wire Wire Line
	6950 4050 7550 4050
Wire Wire Line
	8150 3650 8150 3050
$Comp
L Device:R R?
U 1 1 61A2F4BA
P 8150 3800
AR Path="/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F4BA" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F4BA" Ref="R285"  Part="1" 
F 0 "R285" V 8050 3750 50  0000 L CNN
F 1 "17.4" V 8150 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 3800 50  0001 C CNN
F 3 "~" H 8150 3800 50  0001 C CNN
	1    8150 3800
	1    0    0    1   
$EndComp
Text HLabel 6250 3850 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	6450 3700 6450 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F4C9
P 6950 2850
AR Path="/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F4C9" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F4C9" Ref="U47"  Part="1" 
F 0 "U47" H 7050 3150 60  0000 C CNN
F 1 "LINPOL12V" H 6950 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 7000 2900 260 0001 C CNN
F 3 "" V 7000 2900 260 0001 C CNN
	1    6950 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 4150 6950 4050
Wire Wire Line
	6450 3050 6250 3050
Connection ~ 6450 3050
Wire Wire Line
	7800 3300 7800 2900
Wire Wire Line
	7450 2900 7800 2900
Wire Wire Line
	7800 2900 8400 2900
Connection ~ 7800 2900
Text Label 7550 2700 1    50   ~ 0
1V4_47
Wire Wire Line
	7550 3050 7550 2700
Text Label 7800 2700 1    50   ~ 0
3V3_47
Wire Wire Line
	7800 2900 7800 2700
$Comp
L Device:R R?
U 1 1 61A2F729
P 11050 3800
AR Path="/61A2F729" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F729" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F729" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F729" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F729" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F729" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F729" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F729" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F729" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F729" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F729" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F729" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F729" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F729" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F729" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F729" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F729" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F729" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F729" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F729" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F729" Ref="R288"  Part="1" 
F 0 "R288" V 10950 3750 50  0000 L CNN
F 1 "165" V 11050 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10980 3800 50  0001 C CNN
F 3 "~" H 11050 3800 50  0001 C CNN
	1    11050 3800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6195FB9A
P 9600 4150
AR Path="/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/61849340/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/61867626/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/6195FB9A" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/6195FB9A" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 9600 3900 50  0001 C CNN
F 1 "GND" H 9605 3977 50  0000 C CNN
F 2 "" H 9600 4150 50  0001 C CNN
F 3 "" H 9600 4150 50  0001 C CNN
	1    9600 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10800 3950 10800 4050
Wire Wire Line
	11050 3950 11050 4050
Wire Wire Line
	11050 4050 10800 4050
Connection ~ 10800 4050
Wire Wire Line
	8550 4050 9100 4050
Wire Wire Line
	10100 2750 10650 2750
$Comp
L Device:C C?
U 1 1 61A2F4E2
P 9100 3850
AR Path="/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4E2" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F4E2" Ref="C142"  Part="1" 
F 0 "C142" H 9100 3950 50  0000 L CNN
F 1 "2.2uF" H 9100 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9138 3700 50  0001 C CNN
F 3 "~" H 9100 3850 50  0001 C CNN
	1    9100 3850
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F4E9
P 10200 3850
AR Path="/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4E9" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F4E9" Ref="C143"  Part="1" 
F 0 "C143" H 10200 3950 50  0000 L CNN
F 1 "10uF" H 10200 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10238 3700 50  0001 C CNN
F 3 "~" H 10200 3850 50  0001 C CNN
	1    10200 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	10650 4050 10800 4050
Connection ~ 10650 4050
Wire Wire Line
	10100 3050 10200 3050
$Comp
L Device:C C?
U 1 1 61A2F4F7
P 10450 3850
AR Path="/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F4F7" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F4F7" Ref="C144"  Part="1" 
F 0 "C144" H 10450 3950 50  0000 L CNN
F 1 "4.7uF" H 10450 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 3700 50  0001 C CNN
F 3 "~" H 10450 3850 50  0001 C CNN
	1    10450 3850
	1    0    0    1   
$EndComp
Connection ~ 10200 3050
Wire Wire Line
	10200 4000 10200 4050
Connection ~ 10200 4050
Wire Wire Line
	10200 4050 10450 4050
Wire Wire Line
	10450 4000 10450 4050
Connection ~ 10450 4050
Wire Wire Line
	10450 4050 10650 4050
Wire Wire Line
	9100 4000 9100 4050
Connection ~ 9100 4050
Wire Wire Line
	9100 4050 9600 4050
$Comp
L Device:R R?
U 1 1 61A2F732
P 10200 3450
AR Path="/61A2F732" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F732" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F732" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F732" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F732" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F732" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F732" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F732" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F732" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F732" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F732" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F732" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F732" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F732" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F732" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F732" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F732" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F732" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F732" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F732" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F732" Ref="R279"  Part="1" 
F 0 "R279" V 10100 3400 50  0000 L CNN
F 1 "300m" V 10200 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10130 3450 50  0001 C CNN
F 3 "~" H 10200 3450 50  0001 C CNN
	1    10200 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F362
P 10450 3450
AR Path="/61A2F362" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F362" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F362" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F362" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F362" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F362" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F362" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F362" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F362" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F362" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F362" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F362" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F362" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F362" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F362" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F362" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F362" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F362" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F362" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F362" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F362" Ref="R280"  Part="1" 
F 0 "R280" V 10350 3400 50  0000 L CNN
F 1 "300m" V 10450 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10380 3450 50  0001 C CNN
F 3 "~" H 10450 3450 50  0001 C CNN
	1    10450 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 3300 10200 3050
Wire Wire Line
	11050 3650 11050 2900
Wire Wire Line
	10650 4050 10650 2750
Wire Wire Line
	10200 3050 10800 3050
Wire Wire Line
	10200 3600 10200 3700
Wire Wire Line
	10450 3600 10450 3700
Wire Wire Line
	9600 3300 9600 4050
Connection ~ 9600 4050
Wire Wire Line
	9600 4050 10200 4050
Wire Wire Line
	10800 3650 10800 3050
$Comp
L Device:R R?
U 1 1 61A2F50B
P 10800 3800
AR Path="/61A2F50B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F50B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F50B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F50B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F50B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F50B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F50B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F50B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F50B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F50B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F50B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F50B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F50B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F50B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F50B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F50B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F50B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F50B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F50B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F50B" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F50B" Ref="R287"  Part="1" 
F 0 "R287" V 10700 3750 50  0000 L CNN
F 1 "17.4" V 10800 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10730 3800 50  0001 C CNN
F 3 "~" H 10800 3800 50  0001 C CNN
	1    10800 3800
	1    0    0    1   
$EndComp
Text HLabel 8900 3850 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	9100 3700 9100 3050
$Comp
L linpol:LREG U?
U 1 1 61A2F50C
P 9600 2850
AR Path="/61A2F50C" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F50C" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F50C" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F50C" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F50C" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F50C" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F50C" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F50C" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F50C" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F50C" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F50C" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F50C" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F50C" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F50C" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F50C" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F50C" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F50C" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F50C" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F50C" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F50C" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F50C" Ref="U48"  Part="1" 
F 0 "U48" H 9700 3150 60  0000 C CNN
F 1 "LINPOL12V" H 9600 2500 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 9650 2900 260 0001 C CNN
F 3 "" V 9650 2900 260 0001 C CNN
	1    9600 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 4150 9600 4050
Wire Wire Line
	9100 3050 8900 3050
Connection ~ 9100 3050
Wire Wire Line
	10450 3300 10450 2900
Wire Wire Line
	10100 2900 10450 2900
Wire Wire Line
	10450 2900 11050 2900
Connection ~ 10450 2900
Text Label 10200 2700 1    50   ~ 0
1V4_48
Wire Wire Line
	10200 3050 10200 2700
Text Label 10450 2700 1    50   ~ 0
3V3_48
Wire Wire Line
	10450 2900 10450 2700
$Comp
L Device:R R?
U 1 1 61A2F73F
P 3100 5750
AR Path="/61A2F73F" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F73F" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F73F" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F73F" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F73F" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F73F" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F73F" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F73F" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F73F" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F73F" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F73F" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F73F" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F73F" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F73F" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F73F" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F73F" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F73F" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F73F" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F73F" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F73F" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F73F" Ref="R298"  Part="1" 
F 0 "R298" V 3000 5700 50  0000 L CNN
F 1 "165" V 3100 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 5750 50  0001 C CNN
F 3 "~" H 3100 5750 50  0001 C CNN
	1    3100 5750
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61A2F51D
P 1650 6100
AR Path="/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/61849340/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/61867626/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/61A2F51D" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/61A2F51D" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 1650 5850 50  0001 C CNN
F 1 "GND" H 1655 5927 50  0000 C CNN
F 2 "" H 1650 6100 50  0001 C CNN
F 3 "" H 1650 6100 50  0001 C CNN
	1    1650 6100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2850 5900 2850 6000
Wire Wire Line
	3100 5900 3100 6000
Wire Wire Line
	3100 6000 2850 6000
Connection ~ 2850 6000
Wire Wire Line
	600  6000 1150 6000
Wire Wire Line
	2150 4700 2700 4700
$Comp
L Device:C C?
U 1 1 61A2F74C
P 1150 5800
AR Path="/61A2F74C" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F74C" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F74C" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F74C" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F74C" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F74C" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F74C" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F74C" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F74C" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F74C" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F74C" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F74C" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F74C" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F74C" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F74C" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F74C" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F74C" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F74C" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F74C" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F74C" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F74C" Ref="C145"  Part="1" 
F 0 "C145" H 1150 5900 50  0000 L CNN
F 1 "2.2uF" H 1150 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1188 5650 50  0001 C CNN
F 3 "~" H 1150 5800 50  0001 C CNN
	1    1150 5800
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F528
P 2250 5800
AR Path="/61A2F528" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F528" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F528" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F528" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F528" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F528" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F528" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F528" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F528" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F528" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F528" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F528" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F528" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F528" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F528" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F528" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F528" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F528" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F528" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F528" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F528" Ref="C146"  Part="1" 
F 0 "C146" H 2250 5900 50  0000 L CNN
F 1 "10uF" H 2250 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 5650 50  0001 C CNN
F 3 "~" H 2250 5800 50  0001 C CNN
	1    2250 5800
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 6000 2850 6000
Connection ~ 2700 6000
Wire Wire Line
	2150 5000 2250 5000
$Comp
L Device:C C?
U 1 1 61A2F531
P 2500 5800
AR Path="/61A2F531" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F531" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F531" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F531" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F531" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F531" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F531" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F531" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F531" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F531" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F531" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F531" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F531" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F531" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F531" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F531" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F531" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F531" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F531" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F531" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F531" Ref="C147"  Part="1" 
F 0 "C147" H 2500 5900 50  0000 L CNN
F 1 "4.7uF" H 2500 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 5650 50  0001 C CNN
F 3 "~" H 2500 5800 50  0001 C CNN
	1    2500 5800
	1    0    0    1   
$EndComp
Connection ~ 2250 5000
Wire Wire Line
	2250 5950 2250 6000
Connection ~ 2250 6000
Wire Wire Line
	2250 6000 2500 6000
Wire Wire Line
	2500 5950 2500 6000
Connection ~ 2500 6000
Wire Wire Line
	2500 6000 2700 6000
Wire Wire Line
	1150 5950 1150 6000
Connection ~ 1150 6000
Wire Wire Line
	1150 6000 1650 6000
$Comp
L Device:R R?
U 1 1 61A2F543
P 2250 5400
AR Path="/61A2F543" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F543" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F543" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F543" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F543" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F543" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F543" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F543" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F543" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F543" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F543" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F543" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F543" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F543" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F543" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F543" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F543" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F543" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F543" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F543" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F543" Ref="R292"  Part="1" 
F 0 "R292" V 2150 5350 50  0000 L CNN
F 1 "300m" V 2250 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2180 5400 50  0001 C CNN
F 3 "~" H 2250 5400 50  0001 C CNN
	1    2250 5400
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61A2F547
P 2500 5400
AR Path="/61A2F547" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F547" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F547" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F547" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F547" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F547" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F547" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F547" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F547" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F547" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F547" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F547" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F547" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F547" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F547" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F547" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F547" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F547" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F547" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F547" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F547" Ref="R293"  Part="1" 
F 0 "R293" V 2400 5350 50  0000 L CNN
F 1 "300m" V 2500 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 5400 50  0001 C CNN
F 3 "~" H 2500 5400 50  0001 C CNN
	1    2500 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 5250 2250 5000
Wire Wire Line
	3100 5600 3100 4850
Wire Wire Line
	2700 6000 2700 4700
Wire Wire Line
	2250 5000 2850 5000
Wire Wire Line
	2250 5550 2250 5650
Wire Wire Line
	2500 5550 2500 5650
Wire Wire Line
	1650 5250 1650 6000
Connection ~ 1650 6000
Wire Wire Line
	1650 6000 2250 6000
Wire Wire Line
	2850 5600 2850 5000
$Comp
L Device:R R?
U 1 1 61A2F32B
P 2850 5750
AR Path="/61A2F32B" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F32B" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F32B" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F32B" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F32B" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F32B" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F32B" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F32B" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F32B" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F32B" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F32B" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F32B" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F32B" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F32B" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F32B" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F32B" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F32B" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F32B" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F32B" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F32B" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F32B" Ref="R297"  Part="1" 
F 0 "R297" V 2750 5700 50  0000 L CNN
F 1 "17.4" V 2850 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2780 5750 50  0001 C CNN
F 3 "~" H 2850 5750 50  0001 C CNN
	1    2850 5750
	1    0    0    1   
$EndComp
Text HLabel 950  5800 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	1150 5650 1150 5000
$Comp
L linpol:LREG U?
U 1 1 6195FC19
P 1650 4800
AR Path="/6195FC19" Ref="U?"  Part="1" 
AR Path="/606A40CC/6195FC19" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/6195FC19" Ref="U?"  Part="1" 
AR Path="/60A71FE2/6195FC19" Ref="U?"  Part="1" 
AR Path="/60A749DF/6195FC19" Ref="U?"  Part="1" 
AR Path="/60A76681/6195FC19" Ref="U?"  Part="1" 
AR Path="/6182C19D/6195FC19" Ref="U?"  Part="1" 
AR Path="/6183A7C1/6195FC19" Ref="U?"  Part="1" 
AR Path="/61849340/6195FC19" Ref="U?"  Part="1" 
AR Path="/61858A9D/6195FC19" Ref="U?"  Part="1" 
AR Path="/61867626/6195FC19" Ref="U?"  Part="1" 
AR Path="/6187640F/6195FC19" Ref="U?"  Part="1" 
AR Path="/618857F1/6195FC19" Ref="U?"  Part="1" 
AR Path="/61894FBC/6195FC19" Ref="U?"  Part="1" 
AR Path="/618A4CF2/6195FC19" Ref="U?"  Part="1" 
AR Path="/618E2B02/6195FC19" Ref="U?"  Part="1" 
AR Path="/61987B22/6195FC19" Ref="U?"  Part="1" 
AR Path="/607CBABF/6195FC19" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/6195FC19" Ref="U?"  Part="1" 
AR Path="/607AA45D/6195FC19" Ref="U?"  Part="1" 
AR Path="/607ECD6D/6195FC19" Ref="U49"  Part="1" 
F 0 "U49" H 1750 5100 60  0000 C CNN
F 1 "LINPOL12V" H 1650 4450 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 1700 4850 260 0001 C CNN
F 3 "" V 1700 4850 260 0001 C CNN
	1    1650 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 6100 1650 6000
Wire Wire Line
	1150 5000 950  5000
Connection ~ 1150 5000
Wire Wire Line
	2500 5250 2500 4850
Wire Wire Line
	2150 4850 2500 4850
Wire Wire Line
	2500 4850 3100 4850
Connection ~ 2500 4850
Text Label 2250 4650 1    50   ~ 0
1V4_49
Wire Wire Line
	2250 5000 2250 4650
Text Label 2500 4650 1    50   ~ 0
3V3_49
Wire Wire Line
	2500 4850 2500 4650
$Comp
L Device:R R?
U 1 1 61A2F55D
P 5750 5750
AR Path="/61A2F55D" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F55D" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F55D" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F55D" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F55D" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F55D" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F55D" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F55D" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F55D" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F55D" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F55D" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F55D" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F55D" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F55D" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F55D" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F55D" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F55D" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F55D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F55D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F55D" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F55D" Ref="R300"  Part="1" 
F 0 "R300" V 5650 5700 50  0000 L CNN
F 1 "165" V 5750 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 5750 50  0001 C CNN
F 3 "~" H 5750 5750 50  0001 C CNN
	1    5750 5750
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6195FE2C
P 4300 6100
AR Path="/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/606A40CC/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/60A4CEA0/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/60A71FE2/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/60A749DF/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/60A76681/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/6182C19D/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/6183A7C1/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/61849340/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/61858A9D/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/61867626/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/6187640F/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/618857F1/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/61894FBC/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/618A4CF2/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/618E2B02/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/61987B22/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/607CBABF/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991F3/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/607AA45D/6195FE2C" Ref="#PWR?"  Part="1" 
AR Path="/607ECD6D/6195FE2C" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 4300 5850 50  0001 C CNN
F 1 "GND" H 4305 5927 50  0000 C CNN
F 2 "" H 4300 6100 50  0001 C CNN
F 3 "" H 4300 6100 50  0001 C CNN
	1    4300 6100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 5900 5500 6000
Wire Wire Line
	5750 5900 5750 6000
Wire Wire Line
	5750 6000 5500 6000
Connection ~ 5500 6000
Wire Wire Line
	3250 6000 3800 6000
Wire Wire Line
	4800 4700 5350 4700
$Comp
L Device:C C?
U 1 1 61A2F75F
P 3800 5800
AR Path="/61A2F75F" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F75F" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F75F" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F75F" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F75F" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F75F" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F75F" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F75F" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F75F" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F75F" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F75F" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F75F" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F75F" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F75F" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F75F" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F75F" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F75F" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F75F" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F75F" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F75F" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F75F" Ref="C148"  Part="1" 
F 0 "C148" H 3800 5900 50  0000 L CNN
F 1 "2.2uF" H 3800 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 5650 50  0001 C CNN
F 3 "~" H 3800 5800 50  0001 C CNN
	1    3800 5800
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 61A2F768
P 4900 5800
AR Path="/61A2F768" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F768" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F768" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F768" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F768" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F768" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F768" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F768" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F768" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F768" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F768" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F768" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F768" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F768" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F768" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F768" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F768" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F768" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F768" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F768" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F768" Ref="C149"  Part="1" 
F 0 "C149" H 4900 5900 50  0000 L CNN
F 1 "10uF" H 4900 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 5650 50  0001 C CNN
F 3 "~" H 4900 5800 50  0001 C CNN
	1    4900 5800
	1    0    0    1   
$EndComp
Wire Wire Line
	5350 6000 5500 6000
Connection ~ 5350 6000
Wire Wire Line
	4800 5000 4900 5000
$Comp
L Device:C C?
U 1 1 61A2F771
P 5150 5800
AR Path="/61A2F771" Ref="C?"  Part="1" 
AR Path="/606A40CC/61A2F771" Ref="C?"  Part="1" 
AR Path="/60A4CEA0/61A2F771" Ref="C?"  Part="1" 
AR Path="/60A71FE2/61A2F771" Ref="C?"  Part="1" 
AR Path="/60A749DF/61A2F771" Ref="C?"  Part="1" 
AR Path="/60A76681/61A2F771" Ref="C?"  Part="1" 
AR Path="/6182C19D/61A2F771" Ref="C?"  Part="1" 
AR Path="/6183A7C1/61A2F771" Ref="C?"  Part="1" 
AR Path="/61849340/61A2F771" Ref="C?"  Part="1" 
AR Path="/61858A9D/61A2F771" Ref="C?"  Part="1" 
AR Path="/61867626/61A2F771" Ref="C?"  Part="1" 
AR Path="/6187640F/61A2F771" Ref="C?"  Part="1" 
AR Path="/618857F1/61A2F771" Ref="C?"  Part="1" 
AR Path="/61894FBC/61A2F771" Ref="C?"  Part="1" 
AR Path="/618A4CF2/61A2F771" Ref="C?"  Part="1" 
AR Path="/618E2B02/61A2F771" Ref="C?"  Part="1" 
AR Path="/61987B22/61A2F771" Ref="C?"  Part="1" 
AR Path="/607CBABF/61A2F771" Ref="C?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F771" Ref="C?"  Part="1" 
AR Path="/607AA45D/61A2F771" Ref="C?"  Part="1" 
AR Path="/607ECD6D/61A2F771" Ref="C150"  Part="1" 
F 0 "C150" H 5150 5900 50  0000 L CNN
F 1 "4.7uF" H 5150 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 5650 50  0001 C CNN
F 3 "~" H 5150 5800 50  0001 C CNN
	1    5150 5800
	1    0    0    1   
$EndComp
Connection ~ 4900 5000
Wire Wire Line
	4900 5950 4900 6000
Connection ~ 4900 6000
Wire Wire Line
	4900 6000 5150 6000
Wire Wire Line
	5150 5950 5150 6000
Connection ~ 5150 6000
Wire Wire Line
	5150 6000 5350 6000
Wire Wire Line
	3800 5950 3800 6000
Connection ~ 3800 6000
Wire Wire Line
	3800 6000 4300 6000
$Comp
L Device:R R?
U 1 1 61A2F77E
P 4900 5400
AR Path="/61A2F77E" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F77E" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F77E" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F77E" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F77E" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F77E" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F77E" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F77E" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F77E" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F77E" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F77E" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F77E" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F77E" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F77E" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F77E" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F77E" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F77E" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F77E" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F77E" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F77E" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F77E" Ref="R295"  Part="1" 
F 0 "R295" V 4800 5350 50  0000 L CNN
F 1 "300m" V 4900 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4830 5400 50  0001 C CNN
F 3 "~" H 4900 5400 50  0001 C CNN
	1    4900 5400
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6195FC36
P 5150 5400
AR Path="/6195FC36" Ref="R?"  Part="1" 
AR Path="/606A40CC/6195FC36" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/6195FC36" Ref="R?"  Part="1" 
AR Path="/60A71FE2/6195FC36" Ref="R?"  Part="1" 
AR Path="/60A749DF/6195FC36" Ref="R?"  Part="1" 
AR Path="/60A76681/6195FC36" Ref="R?"  Part="1" 
AR Path="/6182C19D/6195FC36" Ref="R?"  Part="1" 
AR Path="/6183A7C1/6195FC36" Ref="R?"  Part="1" 
AR Path="/61849340/6195FC36" Ref="R?"  Part="1" 
AR Path="/61858A9D/6195FC36" Ref="R?"  Part="1" 
AR Path="/61867626/6195FC36" Ref="R?"  Part="1" 
AR Path="/6187640F/6195FC36" Ref="R?"  Part="1" 
AR Path="/618857F1/6195FC36" Ref="R?"  Part="1" 
AR Path="/61894FBC/6195FC36" Ref="R?"  Part="1" 
AR Path="/618A4CF2/6195FC36" Ref="R?"  Part="1" 
AR Path="/618E2B02/6195FC36" Ref="R?"  Part="1" 
AR Path="/61987B22/6195FC36" Ref="R?"  Part="1" 
AR Path="/607CBABF/6195FC36" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FC36" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FC36" Ref="R?"  Part="1" 
AR Path="/607ECD6D/6195FC36" Ref="R296"  Part="1" 
F 0 "R296" V 5050 5350 50  0000 L CNN
F 1 "300m" V 5150 5300 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5080 5400 50  0001 C CNN
F 3 "~" H 5150 5400 50  0001 C CNN
	1    5150 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 5250 4900 5000
Wire Wire Line
	5750 5600 5750 4850
Wire Wire Line
	5350 6000 5350 4700
Wire Wire Line
	4900 5000 5500 5000
Wire Wire Line
	4900 5550 4900 5650
Wire Wire Line
	5150 5550 5150 5650
Wire Wire Line
	4300 5250 4300 6000
Connection ~ 4300 6000
Wire Wire Line
	4300 6000 4900 6000
Wire Wire Line
	5500 5600 5500 5000
$Comp
L Device:R R?
U 1 1 61A2F579
P 5500 5750
AR Path="/61A2F579" Ref="R?"  Part="1" 
AR Path="/606A40CC/61A2F579" Ref="R?"  Part="1" 
AR Path="/60A4CEA0/61A2F579" Ref="R?"  Part="1" 
AR Path="/60A71FE2/61A2F579" Ref="R?"  Part="1" 
AR Path="/60A749DF/61A2F579" Ref="R?"  Part="1" 
AR Path="/60A76681/61A2F579" Ref="R?"  Part="1" 
AR Path="/6182C19D/61A2F579" Ref="R?"  Part="1" 
AR Path="/6183A7C1/61A2F579" Ref="R?"  Part="1" 
AR Path="/61849340/61A2F579" Ref="R?"  Part="1" 
AR Path="/61858A9D/61A2F579" Ref="R?"  Part="1" 
AR Path="/61867626/61A2F579" Ref="R?"  Part="1" 
AR Path="/6187640F/61A2F579" Ref="R?"  Part="1" 
AR Path="/618857F1/61A2F579" Ref="R?"  Part="1" 
AR Path="/61894FBC/61A2F579" Ref="R?"  Part="1" 
AR Path="/618A4CF2/61A2F579" Ref="R?"  Part="1" 
AR Path="/618E2B02/61A2F579" Ref="R?"  Part="1" 
AR Path="/61987B22/61A2F579" Ref="R?"  Part="1" 
AR Path="/607CBABF/61A2F579" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F579" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F579" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F579" Ref="R299"  Part="1" 
F 0 "R299" V 5400 5700 50  0000 L CNN
F 1 "17.4" V 5500 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 5750 50  0001 C CNN
F 3 "~" H 5500 5750 50  0001 C CNN
	1    5500 5750
	1    0    0    1   
$EndComp
Text HLabel 3600 5800 0    50   Input ~ 0
VIN_g5
Wire Wire Line
	3800 5650 3800 5000
$Comp
L linpol:LREG U?
U 1 1 61A2F34D
P 4300 4800
AR Path="/61A2F34D" Ref="U?"  Part="1" 
AR Path="/606A40CC/61A2F34D" Ref="U?"  Part="1" 
AR Path="/60A4CEA0/61A2F34D" Ref="U?"  Part="1" 
AR Path="/60A71FE2/61A2F34D" Ref="U?"  Part="1" 
AR Path="/60A749DF/61A2F34D" Ref="U?"  Part="1" 
AR Path="/60A76681/61A2F34D" Ref="U?"  Part="1" 
AR Path="/6182C19D/61A2F34D" Ref="U?"  Part="1" 
AR Path="/6183A7C1/61A2F34D" Ref="U?"  Part="1" 
AR Path="/61849340/61A2F34D" Ref="U?"  Part="1" 
AR Path="/61858A9D/61A2F34D" Ref="U?"  Part="1" 
AR Path="/61867626/61A2F34D" Ref="U?"  Part="1" 
AR Path="/6187640F/61A2F34D" Ref="U?"  Part="1" 
AR Path="/618857F1/61A2F34D" Ref="U?"  Part="1" 
AR Path="/61894FBC/61A2F34D" Ref="U?"  Part="1" 
AR Path="/618A4CF2/61A2F34D" Ref="U?"  Part="1" 
AR Path="/618E2B02/61A2F34D" Ref="U?"  Part="1" 
AR Path="/61987B22/61A2F34D" Ref="U?"  Part="1" 
AR Path="/607CBABF/61A2F34D" Ref="U?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F34D" Ref="U?"  Part="1" 
AR Path="/607AA45D/61A2F34D" Ref="U?"  Part="1" 
AR Path="/607ECD6D/61A2F34D" Ref="U50"  Part="1" 
F 0 "U50" H 4400 5100 60  0000 C CNN
F 1 "LINPOL12V" H 4300 4450 60  0000 C CNN
F 2 "linpol:DFN-8_2x2mm_Pitch0.5mm" V 4350 4850 260 0001 C CNN
F 3 "" V 4350 4850 260 0001 C CNN
	1    4300 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	4300 6100 4300 6000
Wire Wire Line
	3800 5000 3600 5000
Connection ~ 3800 5000
Wire Wire Line
	5150 5250 5150 4850
Wire Wire Line
	4800 4850 5150 4850
Wire Wire Line
	5150 4850 5750 4850
Connection ~ 5150 4850
Text Label 4900 4650 1    50   ~ 0
1V4_50
Wire Wire Line
	4900 5000 4900 4650
Text Label 5150 4650 1    50   ~ 0
3V3_50
Wire Wire Line
	5150 4850 5150 4650
Text Label 950  1650 2    50   ~ 0
J1_0
Text Label 950  1500 2    50   ~ 0
J1_1
Wire Wire Line
	950  1150 950  1500
Wire Wire Line
	950  1650 950  1950
Text Label 3600 1650 2    50   ~ 0
J2_0
Text Label 3600 1500 2    50   ~ 0
J2_1
Wire Wire Line
	3600 1150 3600 1500
Wire Wire Line
	3600 1650 3600 1950
Text Label 6250 1650 2    50   ~ 0
J3_0
Text Label 6250 1500 2    50   ~ 0
J3_1
Wire Wire Line
	6250 1150 6250 1500
Wire Wire Line
	6250 1650 6250 1950
Text Label 8900 1650 2    50   ~ 0
J4_0
Text Label 8900 1500 2    50   ~ 0
J4_1
Wire Wire Line
	8900 1150 8900 1500
Wire Wire Line
	8900 1650 8900 1950
Text Label 950  3550 2    50   ~ 0
J5_0
Text Label 950  3400 2    50   ~ 0
J5_1
Wire Wire Line
	950  3050 950  3400
Wire Wire Line
	950  3550 950  3850
Text Label 3600 3550 2    50   ~ 0
J6_0
Text Label 3600 3400 2    50   ~ 0
J6_1
Wire Wire Line
	3600 3050 3600 3400
Wire Wire Line
	3600 3550 3600 3850
Text Label 6250 3550 2    50   ~ 0
J7_0
Text Label 6250 3400 2    50   ~ 0
J7_1
Wire Wire Line
	6250 3050 6250 3400
Wire Wire Line
	6250 3550 6250 3850
Text Label 8900 3550 2    50   ~ 0
J8_0
Text Label 8900 3400 2    50   ~ 0
J8_1
Wire Wire Line
	8900 3050 8900 3400
Wire Wire Line
	8900 3550 8900 3850
Text Label 950  5500 2    50   ~ 0
J9_0
Text Label 950  5350 2    50   ~ 0
J9_1
Wire Wire Line
	950  5000 950  5350
Wire Wire Line
	950  5500 950  5800
Text Label 3600 5500 2    50   ~ 0
J10_0
Text Label 3600 5350 2    50   ~ 0
J10_1
Wire Wire Line
	3600 5000 3600 5350
Wire Wire Line
	3600 5500 3600 5800
Text Label 9000 5150 2    50   ~ 0
J1_0
Text Label 9000 5250 2    50   ~ 0
J2_0
Text Label 9000 5350 2    50   ~ 0
J3_0
Text Label 9000 5450 2    50   ~ 0
J4_0
Text Label 9000 5550 2    50   ~ 0
J5_0
Text Label 9000 5650 2    50   ~ 0
J6_0
Text Label 9000 5750 2    50   ~ 0
J7_0
Text Label 9000 5850 2    50   ~ 0
J8_0
Text Label 9000 5950 2    50   ~ 0
J9_0
Text Label 9000 6050 2    50   ~ 0
J10_0
Wire Wire Line
	9000 5150 9100 5150
Wire Wire Line
	9000 5250 9100 5250
Wire Wire Line
	9000 5350 9100 5350
Wire Wire Line
	9000 5450 9100 5450
Wire Wire Line
	9000 5550 9100 5550
Wire Wire Line
	9000 5650 9100 5650
Wire Wire Line
	9000 5750 9100 5750
Wire Wire Line
	9000 5850 9100 5850
Wire Wire Line
	9000 5950 9100 5950
Wire Wire Line
	9000 6050 9100 6050
Text Label 9700 5150 0    50   ~ 0
J1_1
Text Label 9700 5250 0    50   ~ 0
J2_1
Text Label 9700 5350 0    50   ~ 0
J3_1
Text Label 9700 5450 0    50   ~ 0
J4_1
Text Label 9700 5550 0    50   ~ 0
J5_1
Text Label 9700 5650 0    50   ~ 0
J6_1
Text Label 9700 5750 0    50   ~ 0
J7_1
Text Label 9700 5850 0    50   ~ 0
J8_1
Text Label 9700 5950 0    50   ~ 0
J9_1
Text Label 9700 6050 0    50   ~ 0
J10_1
Wire Wire Line
	9700 5150 9600 5150
Wire Wire Line
	9700 5250 9600 5250
Wire Wire Line
	9700 5350 9600 5350
Wire Wire Line
	9700 5450 9600 5450
Wire Wire Line
	9700 5550 9600 5550
Wire Wire Line
	9700 5650 9600 5650
Wire Wire Line
	9700 5750 9600 5750
Wire Wire Line
	9700 5850 9600 5850
Wire Wire Line
	9700 5950 9600 5950
Wire Wire Line
	9700 6050 9600 6050
Entry Wire Line
	7000 4900 7100 5000
Entry Wire Line
	7000 5000 7100 5100
Entry Wire Line
	7000 5100 7100 5200
Entry Wire Line
	7000 5200 7100 5300
Entry Wire Line
	7000 5300 7100 5400
Entry Wire Line
	7000 5400 7100 5500
Entry Wire Line
	7000 5500 7100 5600
Entry Wire Line
	7000 5600 7100 5700
Entry Wire Line
	7000 5700 7100 5800
Entry Wire Line
	7000 5800 7100 5900
Text HLabel 7000 6050 0    50   Output ~ 0
1V4_[41..50]
Wire Bus Line
	7100 6050 7000 6050
Text Label 6850 4900 2    50   ~ 0
1V4_41
Wire Wire Line
	7000 4900 6850 4900
Text Label 6850 5000 2    50   ~ 0
1V4_42
Wire Wire Line
	7000 5000 6850 5000
Text Label 6850 5100 2    50   ~ 0
1V4_43
Wire Wire Line
	7000 5100 6850 5100
Text Label 6850 5200 2    50   ~ 0
1V4_44
Wire Wire Line
	7000 5200 6850 5200
Text Label 6850 5300 2    50   ~ 0
1V4_45
Wire Wire Line
	7000 5300 6850 5300
Text Label 6850 5400 2    50   ~ 0
1V4_46
Wire Wire Line
	7000 5400 6850 5400
Text Label 6850 5500 2    50   ~ 0
1V4_47
Wire Wire Line
	7000 5500 6850 5500
Text Label 6850 5600 2    50   ~ 0
1V4_48
Wire Wire Line
	7000 5600 6850 5600
Text Label 6850 5700 2    50   ~ 0
1V4_49
Wire Wire Line
	7000 5700 6850 5700
Text Label 6850 5800 2    50   ~ 0
1V4_50
Wire Wire Line
	7000 5800 6850 5800
Entry Wire Line
	8000 4900 8100 5000
Entry Wire Line
	8000 5000 8100 5100
Entry Wire Line
	8000 5100 8100 5200
Entry Wire Line
	8000 5200 8100 5300
Entry Wire Line
	8000 5300 8100 5400
Entry Wire Line
	8000 5400 8100 5500
Entry Wire Line
	8000 5500 8100 5600
Entry Wire Line
	8000 5600 8100 5700
Entry Wire Line
	8000 5700 8100 5800
Entry Wire Line
	8000 5800 8100 5900
Text HLabel 8000 6050 0    50   Output ~ 0
3V3_[41..50]
Wire Bus Line
	8100 6050 8000 6050
Text Label 7850 4900 2    50   ~ 0
3V3_41
Wire Wire Line
	8000 4900 7850 4900
Text Label 7850 5000 2    50   ~ 0
3V3_42
Wire Wire Line
	8000 5000 7850 5000
Text Label 7850 5100 2    50   ~ 0
3V3_43
Wire Wire Line
	8000 5100 7850 5100
Text Label 7850 5200 2    50   ~ 0
3V3_44
Wire Wire Line
	8000 5200 7850 5200
Text Label 7850 5300 2    50   ~ 0
3V3_45
Wire Wire Line
	8000 5300 7850 5300
Text Label 7850 5400 2    50   ~ 0
3V3_46
Wire Wire Line
	8000 5400 7850 5400
Text Label 7850 5500 2    50   ~ 0
3V3_47
Wire Wire Line
	8000 5500 7850 5500
Text Label 7850 5600 2    50   ~ 0
3V3_48
Wire Wire Line
	8000 5600 7850 5600
Text Label 7850 5700 2    50   ~ 0
3V3_49
Wire Wire Line
	8000 5700 7850 5700
Text Label 7850 5800 2    50   ~ 0
3V3_50
Wire Wire Line
	8000 5800 7850 5800
$Comp
L Device:R R?
U 1 1 6195FE98
P 600 1550
AR Path="/606A40CC/6195FE98" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FE98" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FE98" Ref="R?"  Part="1" 
AR Path="/607ECD6D/6195FE98" Ref="R245"  Part="1" 
F 0 "R245" V 700 1500 50  0000 L CNN
F 1 "100k" V 600 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 1550 50  0001 C CNN
F 3 "~" H 600 1550 50  0001 C CNN
	1    600  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  1700 600  2150
Wire Wire Line
	600  1400 600  1000
Text Label 750  600  0    50   ~ 0
LREGEN1
$Comp
L Device:R R?
U 1 1 61A2F7C9
P 3250 1550
AR Path="/606A40CC/61A2F7C9" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7C9" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7C9" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F7C9" Ref="R248"  Part="1" 
F 0 "R248" V 3350 1500 50  0000 L CNN
F 1 "100k" V 3250 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 1550 50  0001 C CNN
F 3 "~" H 3250 1550 50  0001 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1700 3250 2150
$Comp
L Device:R R?
U 1 1 61A2F7CF
P 5900 1550
AR Path="/606A40CC/61A2F7CF" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7CF" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7CF" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F7CF" Ref="R251"  Part="1" 
F 0 "R251" V 6000 1500 50  0000 L CNN
F 1 "100k" V 5900 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 1550 50  0001 C CNN
F 3 "~" H 5900 1550 50  0001 C CNN
	1    5900 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1700 5900 2150
$Comp
L Device:R R?
U 1 1 61A2F7DA
P 8550 1550
AR Path="/606A40CC/61A2F7DA" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7DA" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7DA" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F7DA" Ref="R254"  Part="1" 
F 0 "R254" V 8650 1500 50  0000 L CNN
F 1 "100k" V 8550 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 1550 50  0001 C CNN
F 3 "~" H 8550 1550 50  0001 C CNN
	1    8550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1700 8550 2150
$Comp
L Device:R R?
U 1 1 61A2F7EB
P 8550 3450
AR Path="/606A40CC/61A2F7EB" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F7EB" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F7EB" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F7EB" Ref="R278"  Part="1" 
F 0 "R278" V 8650 3400 50  0000 L CNN
F 1 "100k" V 8550 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 3450 50  0001 C CNN
F 3 "~" H 8550 3450 50  0001 C CNN
	1    8550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3600 8550 4050
$Comp
L Device:R R?
U 1 1 6195FF0A
P 5900 3450
AR Path="/606A40CC/6195FF0A" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/6195FF0A" Ref="R?"  Part="1" 
AR Path="/607AA45D/6195FF0A" Ref="R?"  Part="1" 
AR Path="/607ECD6D/6195FF0A" Ref="R275"  Part="1" 
F 0 "R275" V 6000 3400 50  0000 L CNN
F 1 "100k" V 5900 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 3450 50  0001 C CNN
F 3 "~" H 5900 3450 50  0001 C CNN
	1    5900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3600 5900 4050
$Comp
L Device:R R?
U 1 1 61A2F803
P 3250 3450
AR Path="/606A40CC/61A2F803" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F803" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F803" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F803" Ref="R272"  Part="1" 
F 0 "R272" V 3350 3400 50  0000 L CNN
F 1 "100k" V 3250 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 3450 50  0001 C CNN
F 3 "~" H 3250 3450 50  0001 C CNN
	1    3250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3600 3250 4050
$Comp
L Device:R R?
U 1 1 61A2F811
P 600 3450
AR Path="/606A40CC/61A2F811" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F811" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F811" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F811" Ref="R269"  Part="1" 
F 0 "R269" V 700 3400 50  0000 L CNN
F 1 "100k" V 600 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 3450 50  0001 C CNN
F 3 "~" H 600 3450 50  0001 C CNN
	1    600  3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  3600 600  4050
$Comp
L Device:R R?
U 1 1 61A2F81D
P 600 5400
AR Path="/606A40CC/61A2F81D" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F81D" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F81D" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F81D" Ref="R291"  Part="1" 
F 0 "R291" V 700 5350 50  0000 L CNN
F 1 "100k" V 600 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 5400 50  0001 C CNN
F 3 "~" H 600 5400 50  0001 C CNN
	1    600  5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  5550 600  6000
$Comp
L Device:R R?
U 1 1 61A2F826
P 3250 5400
AR Path="/606A40CC/61A2F826" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F826" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F826" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F826" Ref="R294"  Part="1" 
F 0 "R294" V 3350 5350 50  0000 L CNN
F 1 "100k" V 3250 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 5400 50  0001 C CNN
F 3 "~" H 3250 5400 50  0001 C CNN
	1    3250 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5550 3250 6000
$Comp
L Device:R R?
U 1 1 61A2F338
P 600 800
AR Path="/606A40CC/61A2F338" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F338" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F338" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F338" Ref="R241"  Part="1" 
F 0 "R241" V 700 750 50  0000 L CNN
F 1 "0" V 600 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 800 50  0001 C CNN
F 3 "~" H 600 800 50  0001 C CNN
	1    600  800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  950  600  1000
Connection ~ 600  1000
Wire Wire Line
	600  650  600  600 
Wire Wire Line
	600  600  750  600 
Wire Wire Line
	1150 850  850  850 
Wire Wire Line
	850  850  850  1000
Connection ~ 850  1000
Wire Wire Line
	850  1000 600  1000
Wire Wire Line
	3800 1000 3500 1000
Wire Wire Line
	3250 1400 3250 1000
Text Label 3400 600  0    50   ~ 0
LREGEN2
$Comp
L Device:R R?
U 1 1 61A2F386
P 3250 800
AR Path="/606A40CC/61A2F386" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F386" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F386" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F386" Ref="R242"  Part="1" 
F 0 "R242" V 3350 750 50  0000 L CNN
F 1 "0" V 3250 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 800 50  0001 C CNN
F 3 "~" H 3250 800 50  0001 C CNN
	1    3250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 950  3250 1000
Connection ~ 3250 1000
Wire Wire Line
	3250 650  3250 600 
Wire Wire Line
	3250 600  3400 600 
Wire Wire Line
	3800 850  3500 850 
Wire Wire Line
	3500 850  3500 1000
Connection ~ 3500 1000
Wire Wire Line
	3500 1000 3250 1000
Wire Wire Line
	6450 1000 6150 1000
Wire Wire Line
	5900 1400 5900 1000
Text Label 6050 600  0    50   ~ 0
LREGEN3
$Comp
L Device:R R?
U 1 1 61A2F38E
P 5900 800
AR Path="/606A40CC/61A2F38E" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F38E" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F38E" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F38E" Ref="R243"  Part="1" 
F 0 "R243" V 6000 750 50  0000 L CNN
F 1 "0" V 5900 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 800 50  0001 C CNN
F 3 "~" H 5900 800 50  0001 C CNN
	1    5900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 950  5900 1000
Connection ~ 5900 1000
Wire Wire Line
	5900 650  5900 600 
Wire Wire Line
	5900 600  6050 600 
Wire Wire Line
	6450 850  6150 850 
Wire Wire Line
	6150 850  6150 1000
Connection ~ 6150 1000
Wire Wire Line
	6150 1000 5900 1000
Wire Wire Line
	9100 1000 8800 1000
Wire Wire Line
	8550 1400 8550 1000
Text Label 8700 600  0    50   ~ 0
LREGEN4
$Comp
L Device:R R?
U 1 1 61A2F3A2
P 8550 800
AR Path="/606A40CC/61A2F3A2" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3A2" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3A2" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3A2" Ref="R244"  Part="1" 
F 0 "R244" V 8650 750 50  0000 L CNN
F 1 "0" V 8550 750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 800 50  0001 C CNN
F 3 "~" H 8550 800 50  0001 C CNN
	1    8550 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 950  8550 1000
Connection ~ 8550 1000
Wire Wire Line
	8550 650  8550 600 
Wire Wire Line
	8550 600  8700 600 
Wire Wire Line
	9100 850  8800 850 
Wire Wire Line
	8800 850  8800 1000
Connection ~ 8800 1000
Wire Wire Line
	8800 1000 8550 1000
Wire Wire Line
	1150 2900 850  2900
Wire Wire Line
	600  3300 600  2900
Text Label 750  2500 0    50   ~ 0
LREGEN5
$Comp
L Device:R R?
U 1 1 61A2F3A6
P 600 2700
AR Path="/606A40CC/61A2F3A6" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3A6" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3A6" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3A6" Ref="R265"  Part="1" 
F 0 "R265" V 700 2650 50  0000 L CNN
F 1 "0" V 600 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 2700 50  0001 C CNN
F 3 "~" H 600 2700 50  0001 C CNN
	1    600  2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  2850 600  2900
Connection ~ 600  2900
Wire Wire Line
	600  2550 600  2500
Wire Wire Line
	600  2500 750  2500
Wire Wire Line
	1150 2750 850  2750
Wire Wire Line
	850  2750 850  2900
Connection ~ 850  2900
Wire Wire Line
	850  2900 600  2900
Wire Wire Line
	3800 2900 3500 2900
Wire Wire Line
	3250 3300 3250 2900
Text Label 3400 2500 0    50   ~ 0
LREGEN6
$Comp
L Device:R R?
U 1 1 61A2F3B8
P 3250 2700
AR Path="/606A40CC/61A2F3B8" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3B8" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3B8" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3B8" Ref="R266"  Part="1" 
F 0 "R266" V 3350 2650 50  0000 L CNN
F 1 "0" V 3250 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 2700 50  0001 C CNN
F 3 "~" H 3250 2700 50  0001 C CNN
	1    3250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2850 3250 2900
Connection ~ 3250 2900
Wire Wire Line
	3250 2550 3250 2500
Wire Wire Line
	3250 2500 3400 2500
Wire Wire Line
	3800 2750 3500 2750
Wire Wire Line
	3500 2750 3500 2900
Connection ~ 3500 2900
Wire Wire Line
	3500 2900 3250 2900
Wire Wire Line
	6450 2900 6150 2900
Wire Wire Line
	5900 3300 5900 2900
Text Label 6050 2500 0    50   ~ 0
LREGEN7
$Comp
L Device:R R?
U 1 1 61A2F3C5
P 5900 2700
AR Path="/606A40CC/61A2F3C5" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3C5" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3C5" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3C5" Ref="R267"  Part="1" 
F 0 "R267" V 6000 2650 50  0000 L CNN
F 1 "0" V 5900 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5830 2700 50  0001 C CNN
F 3 "~" H 5900 2700 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2850 5900 2900
Connection ~ 5900 2900
Wire Wire Line
	5900 2550 5900 2500
Wire Wire Line
	5900 2500 6050 2500
Wire Wire Line
	6450 2750 6150 2750
Wire Wire Line
	6150 2750 6150 2900
Connection ~ 6150 2900
Wire Wire Line
	6150 2900 5900 2900
Wire Wire Line
	9100 2900 8800 2900
Wire Wire Line
	8550 3300 8550 2900
Text Label 8700 2500 0    50   ~ 0
LREGEN8
$Comp
L Device:R R?
U 1 1 61A2F3CD
P 8550 2700
AR Path="/606A40CC/61A2F3CD" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3CD" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3CD" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3CD" Ref="R268"  Part="1" 
F 0 "R268" V 8650 2650 50  0000 L CNN
F 1 "0" V 8550 2650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2850 8550 2900
Connection ~ 8550 2900
Wire Wire Line
	8550 2550 8550 2500
Wire Wire Line
	8550 2500 8700 2500
Wire Wire Line
	9100 2750 8800 2750
Wire Wire Line
	8800 2750 8800 2900
Connection ~ 8800 2900
Wire Wire Line
	8800 2900 8550 2900
Wire Wire Line
	1150 4850 850  4850
Wire Wire Line
	600  5250 600  4850
Text Label 750  4450 0    50   ~ 0
LREGEN9
$Comp
L Device:R R?
U 1 1 61A2F3DC
P 600 4650
AR Path="/606A40CC/61A2F3DC" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3DC" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3DC" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3DC" Ref="R289"  Part="1" 
F 0 "R289" V 700 4600 50  0000 L CNN
F 1 "0" V 600 4600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 530 4650 50  0001 C CNN
F 3 "~" H 600 4650 50  0001 C CNN
	1    600  4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  4800 600  4850
Connection ~ 600  4850
Wire Wire Line
	600  4500 600  4450
Wire Wire Line
	600  4450 750  4450
Wire Wire Line
	1150 4700 850  4700
Wire Wire Line
	850  4700 850  4850
Connection ~ 850  4850
Wire Wire Line
	850  4850 600  4850
Wire Wire Line
	3800 4850 3500 4850
Wire Wire Line
	3250 5250 3250 4850
Text Label 3400 4450 0    50   ~ 0
LREGEN10
$Comp
L Device:R R?
U 1 1 61A2F3E8
P 3250 4650
AR Path="/606A40CC/61A2F3E8" Ref="R?"  Part="1" 
AR Path="/607970C6/607991F3/61A2F3E8" Ref="R?"  Part="1" 
AR Path="/607AA45D/61A2F3E8" Ref="R?"  Part="1" 
AR Path="/607ECD6D/61A2F3E8" Ref="R290"  Part="1" 
F 0 "R290" V 3350 4600 50  0000 L CNN
F 1 "0" V 3250 4600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4650 50  0001 C CNN
F 3 "~" H 3250 4650 50  0001 C CNN
	1    3250 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4800 3250 4850
Connection ~ 3250 4850
Wire Wire Line
	3250 4500 3250 4450
Wire Wire Line
	3250 4450 3400 4450
Wire Wire Line
	3800 4700 3500 4700
Wire Wire Line
	3500 4700 3500 4850
Connection ~ 3500 4850
Wire Wire Line
	3500 4850 3250 4850
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 6157658F
P 9300 5150
AR Path="/607AA45D/6157658F" Ref="J?"  Part="1" 
AR Path="/607ECD6D/6157658F" Ref="J21"  Part="1" 
F 0 "J21" H 9350 5367 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5150 50  0001 C CNN
F 3 "~" H 9300 5150 50  0001 C CNN
	1    9300 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61576595
P 9300 5350
AR Path="/607AA45D/61576595" Ref="J?"  Part="1" 
AR Path="/607ECD6D/61576595" Ref="J22"  Part="1" 
F 0 "J22" H 9350 5567 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5350 50  0001 C CNN
F 3 "~" H 9300 5350 50  0001 C CNN
	1    9300 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 6157659B
P 9300 5550
AR Path="/607AA45D/6157659B" Ref="J?"  Part="1" 
AR Path="/607ECD6D/6157659B" Ref="J23"  Part="1" 
F 0 "J23" H 9350 5767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5550 50  0001 C CNN
F 3 "~" H 9300 5550 50  0001 C CNN
	1    9300 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 615765A1
P 9300 5750
AR Path="/607AA45D/615765A1" Ref="J?"  Part="1" 
AR Path="/607ECD6D/615765A1" Ref="J24"  Part="1" 
F 0 "J24" H 9350 5967 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 5876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5750 50  0001 C CNN
F 3 "~" H 9300 5750 50  0001 C CNN
	1    9300 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 615765A7
P 9300 5950
AR Path="/607AA45D/615765A7" Ref="J?"  Part="1" 
AR Path="/607ECD6D/615765A7" Ref="J25"  Part="1" 
F 0 "J25" H 9350 6167 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9350 6076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 9300 5950 50  0001 C CNN
F 3 "~" H 9300 5950 50  0001 C CNN
	1    9300 5950
	1    0    0    -1  
$EndComp
Text HLabel 10850 6100 0    50   Input ~ 0
LREGEN[1..10]
Entry Wire Line
	10850 5000 10950 5100
Entry Wire Line
	10850 5100 10950 5200
Entry Wire Line
	10850 5200 10950 5300
Entry Wire Line
	10850 5300 10950 5400
Entry Wire Line
	10850 5400 10950 5500
Entry Wire Line
	10850 5500 10950 5600
Entry Wire Line
	10850 5600 10950 5700
Entry Wire Line
	10850 5700 10950 5800
Entry Wire Line
	10850 5800 10950 5900
Entry Wire Line
	10850 5900 10950 6000
Wire Bus Line
	10850 6100 10950 6100
Text Label 10750 5000 2    50   ~ 0
LREGEN1
Wire Wire Line
	10750 5000 10850 5000
Text Label 10750 5100 2    50   ~ 0
LREGEN2
Wire Wire Line
	10750 5100 10850 5100
Text Label 10750 5200 2    50   ~ 0
LREGEN3
Wire Wire Line
	10750 5200 10850 5200
Text Label 10750 5300 2    50   ~ 0
LREGEN4
Wire Wire Line
	10750 5300 10850 5300
Text Label 10750 5400 2    50   ~ 0
LREGEN5
Wire Wire Line
	10750 5400 10850 5400
Text Label 10750 5500 2    50   ~ 0
LREGEN6
Wire Wire Line
	10750 5500 10850 5500
Text Label 10750 5600 2    50   ~ 0
LREGEN7
Wire Wire Line
	10750 5600 10850 5600
Text Label 10750 5700 2    50   ~ 0
LREGEN8
Wire Wire Line
	10750 5700 10850 5700
Text Label 10750 5800 2    50   ~ 0
LREGEN9
Wire Wire Line
	10750 5800 10850 5800
Text Label 10750 5900 2    50   ~ 0
LREGEN10
Wire Wire Line
	10750 5900 10850 5900
Wire Bus Line
	8100 4900 8100 6050
Wire Bus Line
	7100 4900 7100 6050
Wire Bus Line
	10950 4950 10950 6100
$EndSCHEMATC
