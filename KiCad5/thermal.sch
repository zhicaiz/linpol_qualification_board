EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 19 20
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Humidity:SHT35-DIS U?
U 1 1 60EE83F2
P 4050 4100
AR Path="/60EE83F2" Ref="U?"  Part="1" 
AR Path="/60EE689D/60EE83F2" Ref="U?"  Part="1" 
AR Path="/607970C6/607991E1/60EE83F2" Ref="U?"  Part="1" 
AR Path="/60809E66/60EE83F2" Ref="U150"  Part="1" 
F 0 "U150" H 3800 4350 50  0000 C CNN
F 1 "SHT35-DIS" H 4300 4350 50  0000 C CNN
F 2 "Sensor_Humidity:Sensirion_DFN-8-1EP_2.5x2.5mm_P0.5mm_EP1.1x1.7mm" H 4050 4150 50  0001 C CNN
F 3 "https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Datasheets/Sensirion_Humidity_Sensors_SHT3x_Datasheet_digital.pdf" H 4050 4150 50  0001 C CNN
	1    4050 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60EE83F8
P 4050 3700
AR Path="/60EE83F8" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE83F8" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE83F8" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE83F8" Ref="#PWR0229"  Part="1" 
F 0 "#PWR0229" H 4050 3550 50  0001 C CNN
F 1 "+3V3" H 4065 3873 50  0000 C CNN
F 2 "" H 4050 3700 50  0001 C CNN
F 3 "" H 4050 3700 50  0001 C CNN
	1    4050 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3800 4050 3700
Wire Wire Line
	4550 4000 4450 4000
Wire Wire Line
	4550 4100 4450 4100
NoConn ~ 4450 4200
$Comp
L Device:R R?
U 1 1 60EE8404
P 3400 4100
AR Path="/60EE8404" Ref="R?"  Part="1" 
AR Path="/60EE689D/60EE8404" Ref="R?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8404" Ref="R?"  Part="1" 
AR Path="/60809E66/60EE8404" Ref="R799"  Part="1" 
F 0 "R799" V 3300 4100 50  0000 C CNN
F 1 "10k" V 3400 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3330 4100 50  0001 C CNN
F 3 "~" H 3400 4100 50  0001 C CNN
	1    3400 4100
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60EE840A
P 3250 4050
AR Path="/60EE840A" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE840A" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE840A" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE840A" Ref="#PWR0231"  Part="1" 
F 0 "#PWR0231" H 3250 3900 50  0001 C CNN
F 1 "+3V3" H 3265 4223 50  0000 C CNN
F 2 "" H 3250 4050 50  0001 C CNN
F 3 "" H 3250 4050 50  0001 C CNN
	1    3250 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4100 3250 4050
Wire Wire Line
	3550 4100 3650 4100
$Comp
L power:GND #PWR?
U 1 1 60EE8412
P 3750 4450
AR Path="/60EE8412" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE8412" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8412" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE8412" Ref="#PWR0233"  Part="1" 
F 0 "#PWR0233" H 3750 4200 50  0001 C CNN
F 1 "GND" H 3755 4277 50  0000 C CNN
F 2 "" H 3750 4450 50  0001 C CNN
F 3 "" H 3750 4450 50  0001 C CNN
	1    3750 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4000 3600 4000
Wire Wire Line
	3600 4000 3600 4200
Wire Wire Line
	3600 4200 3650 4200
Wire Wire Line
	3600 4200 3600 4450
Wire Wire Line
	3600 4450 3750 4450
Connection ~ 3600 4200
Wire Wire Line
	4050 4400 4050 4450
Wire Wire Line
	4050 4450 3750 4450
Connection ~ 3750 4450
$Comp
L Sensor_Humidity:SHT35-DIS U?
U 1 1 60EE8421
P 6150 4100
AR Path="/60EE8421" Ref="U?"  Part="1" 
AR Path="/60EE689D/60EE8421" Ref="U?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8421" Ref="U?"  Part="1" 
AR Path="/60809E66/60EE8421" Ref="U151"  Part="1" 
F 0 "U151" H 5900 4350 50  0000 C CNN
F 1 "SHT35-DIS" H 6400 4350 50  0000 C CNN
F 2 "Sensor_Humidity:Sensirion_DFN-8-1EP_2.5x2.5mm_P0.5mm_EP1.1x1.7mm" H 6150 4150 50  0001 C CNN
F 3 "https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Datasheets/Sensirion_Humidity_Sensors_SHT3x_Datasheet_digital.pdf" H 6150 4150 50  0001 C CNN
	1    6150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60EE8427
P 6150 3700
AR Path="/60EE8427" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE8427" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8427" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE8427" Ref="#PWR0230"  Part="1" 
F 0 "#PWR0230" H 6150 3550 50  0001 C CNN
F 1 "+3V3" H 6165 3873 50  0000 C CNN
F 2 "" H 6150 3700 50  0001 C CNN
F 3 "" H 6150 3700 50  0001 C CNN
	1    6150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3800 6150 3700
Wire Wire Line
	6650 4000 6550 4000
Wire Wire Line
	6650 4100 6550 4100
NoConn ~ 6550 4200
$Comp
L Device:R R?
U 1 1 60EE8433
P 5500 4100
AR Path="/60EE8433" Ref="R?"  Part="1" 
AR Path="/60EE689D/60EE8433" Ref="R?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8433" Ref="R?"  Part="1" 
AR Path="/60809E66/60EE8433" Ref="R800"  Part="1" 
F 0 "R800" V 5400 4100 50  0000 C CNN
F 1 "10k" V 5500 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 4100 50  0001 C CNN
F 3 "~" H 5500 4100 50  0001 C CNN
	1    5500 4100
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60EE8439
P 5350 4050
AR Path="/60EE8439" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE8439" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8439" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE8439" Ref="#PWR0232"  Part="1" 
F 0 "#PWR0232" H 5350 3900 50  0001 C CNN
F 1 "+3V3" H 5365 4223 50  0000 C CNN
F 2 "" H 5350 4050 50  0001 C CNN
F 3 "" H 5350 4050 50  0001 C CNN
	1    5350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4100 5350 4050
Wire Wire Line
	5650 4100 5700 4100
$Comp
L power:GND #PWR?
U 1 1 60EE8441
P 5850 4450
AR Path="/60EE8441" Ref="#PWR?"  Part="1" 
AR Path="/60EE689D/60EE8441" Ref="#PWR?"  Part="1" 
AR Path="/607970C6/607991E1/60EE8441" Ref="#PWR?"  Part="1" 
AR Path="/60809E66/60EE8441" Ref="#PWR0234"  Part="1" 
F 0 "#PWR0234" H 5850 4200 50  0001 C CNN
F 1 "GND" H 5855 4277 50  0000 C CNN
F 2 "" H 5850 4450 50  0001 C CNN
F 3 "" H 5850 4450 50  0001 C CNN
	1    5850 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4000 5700 4000
Wire Wire Line
	5700 4200 5750 4200
Wire Wire Line
	5700 4200 5700 4450
Wire Wire Line
	5700 4450 5850 4450
Wire Wire Line
	6150 4400 6150 4450
Wire Wire Line
	6150 4450 5850 4450
Connection ~ 5850 4450
Wire Wire Line
	5700 4000 5700 4100
Connection ~ 5700 4100
Wire Wire Line
	5700 4100 5750 4100
Text HLabel 4550 4000 2    50   BiDi ~ 0
SDA
Text HLabel 4550 4100 2    50   Input ~ 0
SCL
Text HLabel 6650 4000 2    50   BiDi ~ 0
SDA
Text HLabel 6650 4100 2    50   Input ~ 0
SCL
$EndSCHEMATC
