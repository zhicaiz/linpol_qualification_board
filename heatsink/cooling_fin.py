
import ROOT as rt
import numpy as np

x1 = [0.260]
y1 = [2.945]
x2 = [0.06]
y2 = [0.13]
x3 = [1.04, 2.08]
y3 = [0.13, 0.13]

x4 = [2.76, 2.76]
y4 = [1.305, 4.595]

for idx in range(13):
    if idx == 6:
        continue
    x2.append(0.06)
    y2.append(y2[0]+0.47*idx)

for idx in range(13):
    x3.append(1.04)
    y3.append(y3[0]+0.47*idx)

for idx in range(13):
    x3.append(2.08)
    y3.append(y3[1]+0.47*idx)

myC = rt.TCanvas("myC","myC", 580, 1200)
myC.SetRightMargin(0.05)
myC.SetLeftMargin(0.125)
myC.SetTopMargin(0.07)

gr1 = rt.TGraph(len(x1), np.array(x1), np.array(y1))
gr1.SetMarkerStyle(21)
gr1.SetMarkerColor(rt.kBlack)
gr1.Draw("ap")

gr1.SetTitle("")
gr1.GetXaxis().SetTitle("X (inches)")
gr1.GetXaxis().SetLimits(0.0, 3.0)
gr1.GetYaxis().SetTitle("Y (inches)")
gr1.GetYaxis().SetRangeUser(0.0, 6.0)

gr2 = rt.TGraph(len(x2), np.array(x2), np.array(y2))
gr2.SetMarkerStyle(21)
gr2.SetMarkerColor(rt.kRed)
gr2.Draw("psame")

gr3 = rt.TGraph(len(x3), np.array(x3), np.array(y3))
gr3.SetMarkerStyle(21)
gr3.SetMarkerColor(rt.kBlue)
gr3.Draw("psame")

gr4 = rt.TGraph(len(x4), np.array(x4), np.array(y4))
gr4.SetMarkerStyle(21)
gr4.SetMarkerColor(rt.kGreen)
gr4.Draw("psame")

texts = []
for idx in range(len(x2)):
    text = rt.TLatex(x2[idx]-0.03, y2[idx]+0.07, "(%.3f"%x2[idx]+", %.3f"%y2[idx]+")")
    text.SetTextFont(43)
    text.SetTextSize(15)
    texts.append(text)

for idx in range(len(x3)):
    text = rt.TLatex(x3[idx]-0.03, y3[idx]+0.07, "(%.3f"%x3[idx]+", %.3f"%y3[idx]+")")
    text.SetTextFont(43)
    text.SetTextSize(15)
    texts.append(text)

for idx in range(len(x4)):
    text = rt.TLatex(x4[idx]-0.33, y4[idx]+0.07, "(%.3f"%x4[idx]+", %.3f"%y4[idx]+")")
    text.SetTextFont(43)
    text.SetTextSize(15)
    texts.append(text)

for text in texts:
    text.Draw()

text = rt.TLatex(x1[0]-0.03, y1[0]-0.13, "(%.3f"%x1[0]+", %.3f"%y1[0]+")")
text.SetTextFont(43)
text.SetTextSize(15)
text.Draw()

leg = rt.TLegend(0.11, 0.93, 0.94, 0.98)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.03)
leg.SetNColumns(4)

leg.AddEntry(gr2, "0.12 x 0.30", "p")
leg.AddEntry(gr1, "0.52 x 0.47", "p")
leg.AddEntry(gr3, "0.24 x 0.30", "p")
leg.AddEntry(gr4, "0.32 x 0.94", "p")
leg.Draw()

myC.SaveAs("cooling_fin.pdf")
myC.SaveAs("cooling_fin.png")

